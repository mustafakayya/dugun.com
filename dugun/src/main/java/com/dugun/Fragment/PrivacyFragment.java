package com.dugun.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dugun.Activity.SettingsActivity;
import com.dugun.R;

/**
 * Created by ugurbasarir on 2/20/17.
 */

public class PrivacyFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_privacy, container, false);
        ((SettingsActivity) getActivity()).changeToolBarTitle("Gizlilik İlkemiz");

        TextView txt_privacy = (TextView) view.findViewById(R.id.txt_privacy);
        txt_privacy.setMovementMethod(new ScrollingMovementMethod());
        return view;
    }
}
