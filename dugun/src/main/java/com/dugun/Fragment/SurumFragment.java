package com.dugun.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dugun.Activity.SettingsActivity;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.SuccessModel;
import com.dugun.R;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dugun.Others.Utils.isNetworkAvailable;

/**
 * Created by ugurbasarir on 2/20/17.
 */

public class SurumFragment extends BaseFragment {

   private EditText title, content;
   private Button send;

   @Nullable
   @Override
   public View onCreateView (LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      View view = inflater.inflate (R.layout.fragment_feedback, container, false);
      ((SettingsActivity) mActivity).changeToolBarTitle ("Geri Bildirim");

      send = (Button) view.findViewById (R.id.feedback_send);
      title = (EditText) view.findViewById (R.id.title);
      content = (EditText) view.findViewById (R.id.content);

      send.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            if (!title.getText ().toString ().isEmpty () && !content.getText ().toString ().isEmpty ())
               sendFeedback ();
            else
               AppAlertDialog.showMessage (mActivity,
                                           getString (R.string.required_field));
         }
      });

      return view;
   }

   private void sendFeedback () {
      if (!isNetworkAvailable (mActivity)) {
         AppAlertDialog.showMessage (mActivity, getString (R.string.connection_failed));
         return;
      }
      showDialog ();
      HashMap<String, String> data = new HashMap<> ();
      data.put ("type", "feedback");
      data.put ("name", title.getText ().toString ());
      data.put ("email", user.getAccessToken ().getUsername ());
      data.put ("dataValue", content.getText ().toString ());
      ApiInterface apiInterface = ApiClient.getApiClient ().create (ApiInterface.class);
      apiInterface.addCoupleFeedback (user.getAccessToken ().getToken (),
                                      data).enqueue (
              new Callback<SuccessModel> () {
                 @Override
                 public void onResponse (Call<SuccessModel> call, Response<SuccessModel> response) {
                    dismissDialog ();
                    Toast.makeText (mActivity, "Gönderildi", Toast.LENGTH_SHORT).show ();
                 }

                 @Override public void onFailure (Call<SuccessModel> call, Throwable t) {
                    dismissDialog ();
                    AppAlertDialog.showMessage (mActivity,
                                                getString (R.string.failure_message));
                 }

              }
      );
   }
}
