package com.dugun.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.dugun.R;

import java.util.ArrayList;

public class CitySelectorFragment extends DialogFragment implements AdapterView.OnItemClickListener {

   CitySelectorDialogListener mListener;
   ArrayList<String>          arrayList;

   @Nullable
   @Override
   public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      getDialog ().getWindow ().requestFeature (Window.FEATURE_NO_TITLE);
      View view = inflater.inflate (R.layout.dialog_room_list_fragment,
                                    container, false);
      ListView list_room = (ListView) view.findViewById (R.id.list_room);
      ArrayAdapter<String> dataAdapter = new ArrayAdapter<String> (getActivity (),
                                                                   android.R.layout.simple_list_item_1,
                                                                   arrayList);
      list_room.setAdapter (dataAdapter);
      list_room.setOnItemClickListener (this);
      return view;
   }

   @Override
   public void onItemClick (AdapterView<?> adapterView, View view, int i, long l) {
      mListener.onCitySelected (i);
      dismiss ();
   }

   public interface CitySelectorDialogListener {

      public void onCitySelected (int i);
   }

   public static CitySelectorFragment newInstance (CitySelectorDialogListener listener, ArrayList<String> arrayList) {
      CitySelectorFragment fragment = new CitySelectorFragment ();
      fragment.mListener = listener;
      fragment.arrayList = arrayList;
      return fragment;
   }

}