package com.dugun.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.dugun.Activity.SettingsActivity;
import com.dugun.Api.GeneralVariables;
import com.dugun.R;

/**
 * Created by ugurbasarir on 2/20/17.
 */

public class WebviewFragment extends BaseFragment {

   private       WebView webView;
   public static int     type;
   private       String  title, url;

   @Nullable
   @Override
   public View onCreateView (LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      View view = inflater.inflate (R.layout.fragment_webview, container, false);

      switch (type) {
         case GeneralVariables.PRIVACY:
            title = getResources ().getString (R.string.title_privacy);
            url = SettingsActivity.settingsUrls.getPrivacy_page_url ();
            break;
         case GeneralVariables.ABOUT_US:
            title = getResources ().getString (R.string.title_about_us);
            url = SettingsActivity.settingsUrls.getAbout_us_page_url ();
            break;
         case GeneralVariables.TERMS:
            title = getResources ().getString (R.string.title_terms);
            url = SettingsActivity.settingsUrls.getTerms_page_url ();
            break;
         case GeneralVariables.FAQ:
            title = getResources ().getString (R.string.title_faq);
            url = SettingsActivity.settingsUrls.getFaq_page_url ();
            break;
         case GeneralVariables.SURUM:
            title = getResources ().getString (R.string.title_this_version);
            url = SettingsActivity.settingsUrls.getVersion_page_url ();
            break;
      }

      ((SettingsActivity) getActivity ()).changeToolBarTitle (title);

      webView = (WebView) view.findViewById (R.id.webview);
      webView.loadUrl (url);

      return view;
   }
}
