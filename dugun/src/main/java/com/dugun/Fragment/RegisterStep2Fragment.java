package com.dugun.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.dugun.Adapter.OrganizationAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.CategoryModel;
import com.dugun.Others.RegisterStatus;
import com.dugun.R;
import com.dugun.Wrapper.CategoryWrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dugun.Others.Utils.isNetworkAvailable;


public class RegisterStep2Fragment extends RegisterBaseFragment implements AdapterView.OnItemClickListener {

    List<CategoryModel> categoryList = new ArrayList<>();
    OrganizationAdapter adapter;
    Button btn_signup;
    ListView list_organization;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.register_step2, container, false);
        } else {
            return view;
        }
        btn_signup = (Button) view.findViewById(R.id.btn_next);
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Integer> selectedItem = new ArrayList<Integer>();
                for (int i = 0; i < categoryList.size(); i++) {
                    if (categoryList.get(i).isDefault()) {
                        selectedItem.add(categoryList.get(i).getId());
                    }
                }
                int[] item = new int[selectedItem.size()];
                for (int i = 0; i < selectedItem.size(); i++) {
                    item[i] = selectedItem.get(i);
                }

                nextStep(item);
            }
        });
        list_organization = (ListView) view.findViewById(R.id.list_organization_type);
        getOrganizationList();
        return view;
    }

    private void getOrganizationList() {
        if (!isNetworkAvailable(getActivity())) {
            AppAlertDialog.showMessage(getActivity(), getString(R.string.connection_failed));
            return;
        }
        showDialog();
        categoryList = new ArrayList<>();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.categoryListWithoutToken().enqueue(
                new Callback<CategoryWrapper>() {
                    @Override
                    public void onResponse(Call<CategoryWrapper> call, Response<CategoryWrapper> response) {
                        dismissDialog();
                        if (response != null & response.body() != null && response.body().getCategory() != null) {
                            categoryList = response.body().getCategory();
                            adapter = new OrganizationAdapter(getContext(), categoryList);
                            list_organization.setAdapter(adapter);
                            list_organization.setOnItemClickListener(RegisterStep2Fragment.this);
                            updateUIWithValidation();
                        }
                    }

                    @Override
                    public void onFailure(Call<CategoryWrapper> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(mActivity, getString(R.string.failure_message));
                    }
                }
        );
    }


    @Override
    public void onResume() {
        super.onResume();
        updateUIWithValidation();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        categoryList.get(i).setDefault(!categoryList.get(i).isDefault());
        adapter.notifyDataSetChanged();
        updateUIWithValidation();
    }

    private void updateUIWithValidation() {
        boolean hasCheckItem = false;
        for (int i = 0; i < categoryList.size(); i++) {
            if (categoryList.get(i).isDefault()) {
                hasCheckItem = true;
                break;
            }
        }
        if (hasCheckItem) {
            activityCommunicator.passDataToActivity(12, RegisterStatus.FINISHED, null);
        } else {
            activityCommunicator.passDataToActivity(12, RegisterStatus.FOCUSED, null);
        }
        btn_signup.setEnabled(hasCheckItem);
    }

    public void nextStep(int[] item) {
        HashMap<Object, Object> hashMap = new HashMap<>();
        hashMap.put("coupleTodoCategoryIds", item);
        activityCommunicator.passDataToActivity(3, null, hashMap);
    }
}
