package com.dugun.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.dugun.Activity.FirstActivity;
import com.dugun.Activity.SettingsActivity;
import com.dugun.Api.GeneralVariables;
import com.dugun.R;

import static com.dugun.Others.Utils.deleteToken;

/**
 * Created by ugurbasarir on 2/20/17.
 */

public class SettingsFragment extends BaseFragment implements View.OnClickListener {

   private LinearLayout aboutUs, privacy, faq, feedback, terms, contactUs, bu_surum_hakkinda;
   private Button exit;
   private Switch notification, map;

   @Nullable
   @Override
   public View onCreateView (LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      View view = inflater.inflate (R.layout.settings_layout, container, false);
      ((SettingsActivity) getActivity ()).changeToolBarTitle (
              getResources ().getString (R.string.title_settings));

      aboutUs = (LinearLayout) view.findViewById (R.id.settings_about_us);
      aboutUs.setOnClickListener (this);
      privacy = (LinearLayout) view.findViewById (R.id.settings_privacy);
      privacy.setOnClickListener (this);
      faq = (LinearLayout) view.findViewById (R.id.settings_faq);
      faq.setOnClickListener (this);
      feedback = (LinearLayout) view.findViewById (R.id.settings_feedback);
      feedback.setOnClickListener (this);
      terms = (LinearLayout) view.findViewById (R.id.terms);
      terms.setOnClickListener (this);
      contactUs = (LinearLayout) view.findViewById (R.id.contact_us);
      contactUs.setOnClickListener (this);
      bu_surum_hakkinda = (LinearLayout) view.findViewById (R.id.bu_surum_hakkinda);
      bu_surum_hakkinda.setOnClickListener (this);
      exit = (Button) view.findViewById (R.id.exit);
      exit.setOnClickListener (this);
      notification = (Switch) view.findViewById (R.id.switch_notification);
      notification.setChecked (true);
      map = (Switch) view.findViewById (R.id.switch_map);
      map.setChecked (false);

      return view;
   }

   @Override
   public void onClick (View view) {
      switch (view.getId ()) {
         case R.id.settings_about_us:
            WebviewFragment.type = GeneralVariables.ABOUT_US;
            replaceFragment (new WebviewFragment (), R.id.fragment_container, true, null);
            break;
         case R.id.settings_privacy:
            WebviewFragment.type = GeneralVariables.PRIVACY;
            replaceFragment (new WebviewFragment (), R.id.fragment_container, true, null);
            break;
         case R.id.terms:
            WebviewFragment.type = GeneralVariables.TERMS;
            replaceFragment (new WebviewFragment (), R.id.fragment_container, true, null);
            break;
         case R.id.settings_faq:
            WebviewFragment.type = GeneralVariables.FAQ;
            replaceFragment (new WebviewFragment (), R.id.fragment_container, true, null);
            break;
         case R.id.settings_feedback:
            replaceFragment (new FeedbackFragment (), R.id.fragment_container, true, null);
            break;
         case R.id.bu_surum_hakkinda:
            WebviewFragment.type = GeneralVariables.SURUM;
            replaceFragment (new WebviewFragment (), R.id.fragment_container, true, null);
            break;
         case R.id.contact_us:
            replaceFragment (new ContactUsFragment (), R.id.fragment_container, true, null);
            break;
         case R.id.exit:
            deleteToken (getActivity ());
            getActivity ().startActivity (new Intent (getActivity (), FirstActivity.class));
            break;
      }
   }
}
