package com.dugun.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.CityModel;
import com.dugun.Others.RegisterStatus;
import com.dugun.R;
import com.dugun.Wrapper.CityWrapper;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dugun.Others.Utils.getDateForServer;
import static com.dugun.Others.Utils.isNetworkAvailable;


public class RegisterStep1Fragment extends RegisterBaseFragment implements
        DatePickerDialog.OnDateSetListener,
        CitySelectorFragment.CitySelectorDialogListener,
        View.OnClickListener {

    ImageView img_gelin, img_damat;
    TextView txt_gelin, txt_damat, txt_city;
    Button btn_date, btn_next;
    LinearLayout layout_select_city;
    CheckBox checkBox;
    List<CityModel> cityModelList;
    CityModel selectedCityModel;
    String weddingDate = null, weddingDateMonth = null, weddingDateYear = null;
    final String weddingTime = "19:00";
    String coupleType;
    View mView;
    boolean isWeddingDateKnown = true;


    private ImageView bh1, bh2, bh3, bh4, gh1, gh2, gh3, gh4;
    private ArrayList<ImageView> bhs = new ArrayList<>();
    private ArrayList<ImageView> ghs = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.register_step1, container, false);
        } else {
            return mView;
        }
        btn_next = (Button) mView.findViewById(R.id.btn_next);
        btn_next.setOnClickListener(this);


        img_gelin = (ImageView) mView.findViewById(R.id.img_gelin);
        img_damat = (ImageView) mView.findViewById(R.id.img_damat);

        txt_gelin = (TextView) mView.findViewById(R.id.txt_gelin);
        txt_damat = (TextView) mView.findViewById(R.id.txt_damat);
        txt_city = (TextView) mView.findViewById(R.id.txt_city);

        btn_date = (Button) mView.findViewById(R.id.btn_date);


        bh1 = (ImageView) mView.findViewById(R.id.bh1);
        bh2 = (ImageView) mView.findViewById(R.id.bh2);
        bh3 = (ImageView) mView.findViewById(R.id.bh3);
        bh4 = (ImageView) mView.findViewById(R.id.bh4);
        bhs.add(bh1);
        bhs.add(bh2);
        bhs.add(bh3);
        bhs.add(bh4);
        gh1 = (ImageView) mView.findViewById(R.id.gh1);
        gh2 = (ImageView) mView.findViewById(R.id.gh2);
        gh3 = (ImageView) mView.findViewById(R.id.gh3);
        gh4 = (ImageView) mView.findViewById(R.id.gh4);
        ghs.add(gh1);
        ghs.add(gh2);
        ghs.add(gh3);
        ghs.add(gh4);


        layout_select_city = (LinearLayout) mView.findViewById(R.id.layout_select_city);

        checkBox = (CheckBox) mView.findViewById(R.id.checkbox);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isWeddingDateKnown = !b;
                if (isWeddingDateKnown) {
                    btn_date.setText("GÜN  /  AY  /  YIL");
                } else {
                    btn_date.setText("AY  /  YIL");
                }
                weddingDate = null;
                weddingDateMonth = null;
                weddingDateYear = null;
                enabledButton();
            }
        });

        img_gelin.setOnClickListener(this);
        img_damat.setOnClickListener(this);
        btn_date.setOnClickListener(this);
        layout_select_city.setOnClickListener(this);
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        enabledButton();
    }

    private void userSelected(int userType) {
        if (userType == 1) {
            img_gelin.setSelected(true);
            img_damat.setSelected(false);
            txt_gelin.setTextColor(getResources().getColor(R.color.purple_color));
            txt_damat.setTextColor(getResources().getColor(R.color.dugun_text_black));
            coupleType = "bride";
            animate(true);
        } else {
            img_gelin.setSelected(false);
            img_damat.setSelected(true);
            txt_damat.setTextColor(getResources().getColor(R.color.purple_color));
            txt_gelin.setTextColor(getResources().getColor(R.color.dugun_text_black));
            coupleType = "groom";
            animate(false);
        }
        enabledButton();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_gelin:
                userSelected(1);
                break;
            case R.id.img_damat:
                userSelected(2);
                break;
            case R.id.btn_date:
                if (isWeddingDateKnown)
                    showDatePickerDialog();
                else
                    showMonthYearPicker();
                break;
            case R.id.layout_select_city:
                getCities();
                break;
            case R.id.btn_next:
                nextStep();
                break;
        }
    }

    private void showMonthYearPicker() {

        Calendar cal = Calendar.getInstance();
        new MonthPickerDialog.Builder(getActivity(),
                (selectedMonth, selectedYear) -> {
                    weddingDateMonth = (selectedMonth+1) +"";
                    weddingDateYear = selectedYear+"";
                    btn_date.setText(weddingDateMonth+"  /  "+weddingDateYear);
                }
                , cal.get(Calendar.YEAR), cal.get(Calendar.MONTH))
                .setMinYear(cal.get(Calendar.YEAR))
                .setMaxYear(cal.get(Calendar.YEAR)+10)
                .build().show();
    }

    public void nextStep() {
        HashMap<Object, Object> hashMap = new HashMap<>();
        hashMap.put("coupleTypeId", coupleType);
        hashMap.put("cityId", selectedCityModel.getId());
        hashMap.put("isWeddingDateKnown", isWeddingDateKnown);
        if (isWeddingDateKnown)
            hashMap.put("weddingDate", getDateForServer(weddingDate) + " " + weddingTime + ":00");
        else {
            hashMap.put("weddingDateMonth", weddingDateMonth);
            hashMap.put("weddingDateYear", weddingDateYear);
        }
        activityCommunicator.passDataToActivity(2, null, hashMap);
    }

    private void getCities() {
        if (!isNetworkAvailable(getActivity())) {
            AppAlertDialog.showMessage(getActivity(), getString(R.string.connection_failed));
            return;
        }
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getCities().enqueue(new Callback<CityWrapper>() {
            @Override
            public void onResponse(Call<CityWrapper> call, Response<CityWrapper> response) {
                dismissDialog();
                cityModelList = response.body().getCities();
                ArrayList<String> list = new ArrayList<String>();
                for (int i = 0; i < response.body().getCities().size(); i++) {
                    list.add(response.body().getCities().get(i).getName());
                }
                DialogFragment fragment = CitySelectorFragment.newInstance(RegisterStep1Fragment.this,
                        list);
                fragment.show(getActivity().getSupportFragmentManager(), "filter_selector");
            }

            @Override
            public void onFailure(Call<CityWrapper> call, Throwable t) {
                dismissDialog();
                AppAlertDialog.showMessage(mActivity, getString(R.string.failure_message));
            }
        });
    }


    private void showDatePickerDialog() {
        Calendar cal = Calendar.getInstance();
        DatePickerDialog dpd;


        if (isWeddingDateKnown)
            dpd = DatePickerDialog.newInstance(
                    this,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)
            );
        else
            dpd = DatePickerDialog.newInstance(
                    this,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)
            );
        dpd.setMinDate(cal);

        dpd.setAccentColor(getResources().getColor(R.color.colorAccent));
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String day = "" + dayOfMonth;
        if (dayOfMonth < 10)
            day = "0" + dayOfMonth;
        String month = "";
        if (++monthOfYear < 10)
            month = "0" + monthOfYear;
        else {
            month = "" + monthOfYear;
        }
        weddingDate = day + "/" + month + "/" + year;
        btn_date.setText(weddingDate);
        checkBox.setChecked(false);
        enabledButton();
    }

    @Override
    public void onCitySelected(int i) {
        selectedCityModel = cityModelList.get(i);
        txt_city.setText(cityModelList.get(i).getName());
        enabledButton();
    }


    private void enabledButton() {
        if (coupleType != null && selectedCityModel != null && checkDateEntered()) {
            activityCommunicator.passDataToActivity(11, RegisterStatus.FINISHED, null);
            btn_next.setEnabled(true);
        } else {
            activityCommunicator.passDataToActivity(11, RegisterStatus.FOCUSED, null);
            btn_next.setEnabled(false);
        }
    }

    boolean checkDateEntered() {
        if (isWeddingDateKnown) {
            return weddingDate != null;
        } else {
            return weddingDateMonth != null && weddingDateYear != null;
        }
    }

    private void animate(final boolean isBride) {

        final int animateTime = 200;

        for (int i = 0; i < bhs.size(); i++) {
            bhs.get(i).setVisibility(View.INVISIBLE);
            ghs.get(i).setVisibility(View.INVISIBLE);
        }

        final Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < bhs.size(); i++) {
                        final int finalI = i;
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isBride) {
                                    bhs.get(finalI).setVisibility(View.VISIBLE);
                                    if (finalI == bhs.size() - 1) {
                                        bhs.get(finalI).setAlpha((float) 1);
                                    } else {
                                        bhs.get(finalI).setAlpha((float) 0.5);
                                    }
                                } else {
                                    ghs.get(finalI).setVisibility(View.VISIBLE);
                                    if (finalI == ghs.size() - 1) {
                                        ghs.get(finalI).setAlpha((float) 1);
                                    } else {
                                        ghs.get(finalI).setAlpha((float) 0.5);
                                    }
                                }

                            }
                        });
                        sleep(animateTime);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        thread.start();

    }

}
