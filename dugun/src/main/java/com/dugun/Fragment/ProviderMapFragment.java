package com.dugun.Fragment;

import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dugun.Activity.ProviderDetailActivity;
import com.dugun.Model.ProviderModel2;
import com.dugun.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import static com.dugun.Activity.ProvidersActivity.providerList;

/**
 * Created by ugurbasarir on 2/20/17.
 */

public class ProviderMapFragment extends BaseFragment implements LocationListener, GoogleMap.OnInfoWindowClickListener {

    private View view;
    GoogleMap googleMap;


    private LinearLayout infoLay;
    private TextView name, videoCount, imgCount, reviewCount, description, detail;
    private ImageView close;

    private ProviderModel2 selectedProvider;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_provider_map, container, false);
        } catch (InflateException e) {
            e.printStackTrace();
        }

        infoLay = (LinearLayout) view.findViewById(R.id.info_lay);
        infoLay.setVisibility(View.INVISIBLE);
        name = (TextView) view.findViewById(R.id.name);
        videoCount = (TextView) view.findViewById(R.id.video_count);
        imgCount = (TextView) view.findViewById(R.id.img_count);
        reviewCount = (TextView) view.findViewById(R.id.comment_count);
        description = (TextView) view.findViewById(R.id.desc);
        detail = (TextView) view.findViewById(R.id.detail);
        close = (ImageView) view.findViewById(R.id.close);


        ((SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.mapView)).getMapAsync(googleMap1 -> {
            googleMap = googleMap1;
            setMapActions();
        });

        detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ProviderDetailActivity.class);
                i.putExtra("provider", selectedProvider);
                startActivity(i);
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoLay.setVisibility(View.INVISIBLE);
            }
        });


        return view;
    }


    void setMapActions(){


        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                for (int i = 0; i < providerList.size(); i++) {
                    if (marker.getTitle().equals(providerList.get(i).getName())) {
                        selectedProvider = providerList.get(i);
                    }
                }

                if (selectedProvider != null) {
                    name.setText(selectedProvider.getName());
                    videoCount.setText(String.valueOf(selectedProvider.getVideoCount()));
                    imgCount.setText(String.valueOf(selectedProvider.getImageCount()));
                    reviewCount.setText(String.valueOf(selectedProvider.getReviewCount()));
                    description.setText(selectedProvider.getDescription());
                }
                infoLay.setVisibility(View.VISIBLE);
            }
        });

        for (int i = 0; i < providerList.size(); i++) {
            if (providerList.get(i).getMapLng() != null) {
                if (i == 0) {
                    cameraZoom(Double.valueOf(providerList.get(i).getMapLat()),
                            Double.valueOf(providerList.get(i).getMapLng()));
                }
                putProvidersMarkers(providerList.get(i).getName(),
                        Double.valueOf(providerList.get(i).getMapLat()),
                        Double.valueOf(providerList.get(i).getMapLng()));
            }
        }

    }


    private void cameraZoom(double lat, double lng) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(lat, lng))
                .zoom(17)
                .bearing(0)
                .tilt(40)
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void putProvidersMarkers(String name, double lat, double lng) {
        googleMap.addMarker(new MarkerOptions()
                .title(name)
                .position(new LatLng(lat, lng))
                .icon(BitmapDescriptorFactory.fromResource(
                        R.drawable.map_icon)));
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
    }
}
