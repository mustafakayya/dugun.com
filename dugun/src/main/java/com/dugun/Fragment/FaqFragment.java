package com.dugun.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.dugun.Activity.SettingsActivity;
import com.dugun.Adapter.FaqAdapter;
import com.dugun.Model.FaqModel;
import com.dugun.R;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 2/20/17.
 */

public class FaqFragment extends BaseFragment {

   private GridView gridView;

   @Nullable
   @Override
   public View onCreateView (LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      View view = inflater.inflate (R.layout.fragment_faq, container, false);
      ((SettingsActivity) getActivity ()).changeToolBarTitle ("Sıkça Sorulan Sorular");

      gridView = (GridView) view.findViewById (R.id.list_faq);

      ArrayList<FaqModel> faqs = new ArrayList<> ();
      faqs.add (new FaqModel (1, "Vampires The Romantic Ideology",
                              "Yinelenen bir sayfa içeriğinin okuyucunun dikkatini dağıttığı bilinen bir gerçektir. Lorem Ipsum kullanmanın amacı, sürekli 'buraya metin gelecek, buraya metin gelecek' yazmaya kıyasla daha dengeli bir harf dağılımı sağlayarak okunurluğu artırmasıdır. "));
      faqs.add (new FaqModel (2, "Vampires The Romantic Ideology",
                              "Yinelenen bir sayfa içeriğinin okuyucunun dikkatini dağıttığı bilinen bir gerçektir. Lorem Ipsum kullanmanın amacı, sürekli 'buraya metin gelecek, buraya metin gelecek' yazmaya kıyasla daha dengeli bir harf dağılımı sağlayarak okunurluğu artırmasıdır. "));

      gridView.setAdapter (new FaqAdapter (getActivity (), faqs));

      return view;
   }
}
