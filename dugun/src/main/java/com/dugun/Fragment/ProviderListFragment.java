package com.dugun.Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.dugun.Activity.ProviderDetailActivity;
import com.dugun.Adapter.ProviderListAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Api.GeneralVariables;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Interface.LikeCallback;
import com.dugun.Model.SuccessModel;
import com.dugun.R;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dugun.Activity.ProvidersActivity.providerList;
import static com.dugun.Others.Utils.isNetworkAvailable;

/**
 * Created by ugurbasarir on 2/20/17.
 */

public class ProviderListFragment extends BaseFragment implements LikeCallback {

    private GridView gridView;
    private ProviderListAdapter adapter;
    private String TAG = "ProviderListFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_provider_list, container, false);

        gridView = (GridView) view.findViewById(R.id.gridView);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(mActivity, ProviderDetailActivity.class);
                i.putExtra("provider", providerList.get(position));
                startActivity(i);
            }
        });

        adapter = new ProviderListAdapter(mActivity, providerList, ProviderListFragment.this);
        gridView.setAdapter(adapter);

        return view;
    }

    @Override
    public void setLike(final int position) {

        if (providerList.get(position).isLiked()) {
            AlertDialog.Builder alertbox = new AlertDialog.Builder(getActivity());
            alertbox.setMessage("Beğenmekten vazgeçmek istediğinize emin misiniz?");
            alertbox.setTitle("Uyarı");
            alertbox.setPositiveButton(
                    "Evet",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            setLike(true, position);
                        }
                    });
            alertbox.setNeutralButton("Iptal",(dialogInterface, i) -> dialogInterface.dismiss());
            alertbox.show();
        } else {
            setLike(false, position);
        }

    }

    private void setLike(boolean isLiked, final int position) {
        if (!isNetworkAvailable(mActivity)) {
            AppAlertDialog.showMessage(mActivity, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        int itemId = providerList.get(position).getProvider().getId();
        HashMap<String, Object> data = new HashMap<>();
        data.put("itemType", "provider");
        data.put("itemId", itemId);
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        if (isLiked) {
            try {
                apiInterface.deleteCoupleLikes(user.getAccessToken().getToken(),
                        user.getUser().getId(), data).enqueue(
                        new Callback<Object>() {
                            @Override
                            public void onResponse(Call<Object> call, Response<Object> response) {
                                dismissDialog();
                                providerList.get(position).setLiked(false);
                                adapter.notifyDataSetChanged();
                                GeneralVariables.removeFromLikes(itemId,false);
                            }

                            @Override
                            public void onFailure(Call<Object> call, Throwable t) {
                                dismissDialog();
                                Log.d(TAG, "onFailure: " + t.getMessage());
                                AppAlertDialog.showMessage(mActivity,
                                        getString(R.string.failure_message));
                            }

                        }
                );
            } catch (Exception f) {
                dismissDialog();
                providerList.get(position).setLiked(false);
                adapter.notifyDataSetChanged();
            }
        } else {
            apiInterface.addCoupleLikes(user.getAccessToken().getToken(),
                    user.getUser().getId(), data).enqueue(
                    new Callback<SuccessModel>() {
                        @Override
                        public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                            dismissDialog();
                            if (response != null & response.body() != null) {
                                providerList.get(position).setLiked(true);
                                adapter.notifyDataSetChanged();
                                GeneralVariables.addToLikes(itemId,false);
                            }
                        }

                        @Override
                        public void onFailure(Call<SuccessModel> call, Throwable t) {
                            dismissDialog();
                            Log.d(TAG, "onFailure: " + t.getMessage());
                            AppAlertDialog.showMessage(mActivity,
                                    getString(R.string.failure_message));
                        }

                    }
            );
        }

    }
}
