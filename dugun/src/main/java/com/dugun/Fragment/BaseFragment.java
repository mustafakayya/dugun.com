package com.dugun.Fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.dugun.Activity.BaseActivity;
import com.dugun.Wrapper.UserWrapper;

import static com.dugun.Others.Utils.getUser;


public class BaseFragment extends Fragment {

   public BaseActivity mActivity;
   public Resources    resources;
   Dialog pDialog;
   UserWrapper user;

   @Override
   public void onResume () {
      super.onResume ();
   }


   @Override
   public void onCreate (Bundle savedInstanceState) {
      super.onCreate (savedInstanceState);
      mActivity = (BaseActivity) getActivity ();
      resources = getResources ();
      user = getUser(mActivity);
   }

   @Override
   public void onAttach (Context context) {
      super.onAttach (context);
   }

   public void replaceFragment (Fragment fragment, int layout, boolean addToBackStack, Bundle bundle) {
      FragmentTransaction fTransaction = getActivity ().getSupportFragmentManager ().beginTransaction ();

      if (addToBackStack)
         fTransaction.addToBackStack (null);
      else
         removeAllBackStack ();

      if (bundle != null) {
         fragment.setArguments (bundle);
      }
      fTransaction.replace (layout, fragment);
      fTransaction.commitAllowingStateLoss ();
   }

   public void removeAllBackStack () {
      FragmentManager fm    = getActivity ().getSupportFragmentManager ();
      int             count = fm.getBackStackEntryCount ();
      for (int i = 0; i < count; i++) {
         fm.popBackStack ();
      }
   }


   public boolean popFragment () {
      boolean isPop = false;
      if (getChildFragmentManager ().getBackStackEntryCount () > 0) {
         isPop = true;
         getChildFragmentManager ().popBackStack ();
      }
      return isPop;
   }

   public void showDialog () {
      if (pDialog == null) {
         pDialog = ProgressDialog.show (mActivity, null, "Lütfen bekleyiniz...");
         pDialog.setCancelable (false);
         pDialog.setCanceledOnTouchOutside (false);
         if (!pDialog.isShowing ())
            pDialog.show ();
      }
   }

   public void dismissDialog () {
      if (pDialog != null) {
         if (pDialog.isShowing ()) {
            pDialog.dismiss ();
            pDialog = null;
         }
      }
   }

}
