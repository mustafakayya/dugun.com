package com.dugun.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dugun.Activity.SettingsActivity;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.R;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Others.Utils.isNetworkAvailable;

/**
 * Created by ugurbasarir on 2/20/17.
 */

public class ContactUsFragment extends BaseFragment {

    private EditText content;
    private Button send;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        ((SettingsActivity) getActivity()).changeToolBarTitle("Bize Ulaşın");

        send = (Button) view.findViewById(R.id.send);
        content = (EditText) view.findViewById(R.id.content);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!content.getText().toString().isEmpty())
                    sendContactForm();
                else
                    AppAlertDialog.showMessage(getActivity(),
                            getString(R.string.required_field));
            }
        });

        return view;
    }

    private void sendContactForm() {
        if (!isNetworkAvailable(getActivity())) {
            AppAlertDialog.showMessage(getActivity(), getString(R.string.connection_failed));
            return;
        }
        showDialog();
        HashMap<String, String> data = new HashMap<>();
        data.put("type", "feedback");
        data.put("email", user.getAccessToken().getUsername());
        data.put("dataValue", content.getText().toString());
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.sendContactForm(user.getAccessToken().getToken(),
                data).enqueue(
                new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        dismissDialog();
                        Toast.makeText(getActivity(), "Gönderildi",
                                Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(getActivity(),
                                getString(R.string.failure_message));
                    }

                }
        );
    }
}
