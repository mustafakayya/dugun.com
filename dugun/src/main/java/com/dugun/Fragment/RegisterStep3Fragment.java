package com.dugun.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dugun.Activity.PasswordActivity;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Others.FragmentActivityCommunicator;
import com.dugun.Others.RegisterStatus;
import com.dugun.Others.TextWatcherAdapter;
import com.dugun.Others.Utils;
import com.dugun.R;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.HashMap;


public class RegisterStep3Fragment extends RegisterBaseFragment {

    EditText edt_name, edt_mail, edt_pass, edt_phone;
    ImageView img_name, img_mail, img_pass, img_phone;
    private final TextWatcher watcher = validationTextWatcher();
    Button btn_completed, btn_mail_msg, btn_pass_msg, btn_phone_msg, show_pass;

    CheckBox checkbox_contract;
    TextView txt_contract;
    private boolean passIsHidden = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register_step3, container, false);
        edt_name = (EditText) view.findViewById(R.id.edt_name);
        edt_mail = (EditText) view.findViewById(R.id.edt_mail);
        edt_pass = (EditText) view.findViewById(R.id.edt_pass);
        edt_phone = (EditText) view.findViewById(R.id.edt_phone);

        img_name = (ImageView) view.findViewById(R.id.img_name);
        img_mail = (ImageView) view.findViewById(R.id.img_mail);
        img_pass = (ImageView) view.findViewById(R.id.img_pass);
        img_phone = (ImageView) view.findViewById(R.id.img_phone);

        edt_name.addTextChangedListener(watcher);
        edt_mail.addTextChangedListener(watcher);
        edt_phone.addTextChangedListener(watcher);
        edt_pass.addTextChangedListener(watcher);

        btn_completed = (Button) view.findViewById(R.id.btn_completed);
        btn_mail_msg = (Button) view.findViewById(R.id.btn_mail_msg);
        btn_pass_msg = (Button) view.findViewById(R.id.btn_pass_msg);
        btn_phone_msg = (Button) view.findViewById(R.id.btn_phone_msg);
        show_pass = (Button) view.findViewById(R.id.pass_show);

        btn_completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_pass.getText().length() < 6) {
                    AppAlertDialog.showMessage(getActivity(),
                            getString(R.string.password_length_error));
                } else {
                    String phoneNumber = checkPhoneNumber();
                    if(phoneNumber==null)
                        AppAlertDialog.showMessage(getActivity(),
                                "Gecerli telefon numarasi giriniz");
                    else
                        nextStep(phoneNumber);
                }
            }
        });

        show_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passIsHidden) {
                    edt_pass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    show_pass.setText(getString(R.string.hide_password));
                    edt_pass.setTransformationMethod(null);
                    passIsHidden = false;
                } else {
                    edt_pass.setInputType(
                            InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    show_pass.setText(getString(R.string.show_password));
                    edt_pass.setTransformationMethod(new Utils.AsteriskPasswordTransformationMethod());
                    passIsHidden = true;
                }
            }
        });

        checkbox_contract = (CheckBox) view.findViewById(R.id.checkbox_contract);
        txt_contract = (TextView) view.findViewById(R.id.txt_user_contract);
        checkbox_contract.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    txt_contract.setTextColor(
                            getActivity().getResources().getColor(R.color.purple_color));
                } else {
                    txt_contract.setTextColor(getActivity().getResources().getColor(
                            R.color.register_line_disabled_color));
                }
                updateUIWithValidation();
            }
        });

        updateUIWithValidation();
        edt_pass.setTransformationMethod(new Utils.AsteriskPasswordTransformationMethod());

        return view;
    }


    String checkPhoneNumber(){
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber validPhoneNumber = phoneUtil.parse(edt_phone.getText().toString(),"TR");
            if(phoneUtil.isValidNumber(validPhoneNumber)){
                String phonenumber = phoneUtil.format(validPhoneNumber, PhoneNumberUtil.PhoneNumberFormat.NATIONAL);
                return "009"+(phonenumber.replaceAll(" ",""));
            }else
                return null;
        }catch (NumberParseException ex){
            return null;
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        updateUIWithValidation();
    }


    private TextWatcher validationTextWatcher() {
        return new TextWatcherAdapter() {
            public void afterTextChanged(final Editable gitDirEditText) {
                updateUIWithValidation();
            }
        };
    }

    private void updateUIWithValidation() {
        if (populated(edt_phone)) {
            btn_phone_msg.setVisibility(View.GONE);
            img_phone.setImageResource(R.drawable.ic_telefonsecili);
        } else {
            btn_phone_msg.setVisibility(View.VISIBLE);
            img_phone.setImageResource(R.drawable.ic_telefon);
        }
        if (Utils.validateEmail(edt_mail.getText().toString())) {
            img_mail.setImageResource(R.drawable.ic_eposta2);
        } else {
            img_mail.setImageResource(R.drawable.ic_eposta);
        }
        if (populated(edt_mail)) {
            btn_mail_msg.setVisibility(View.GONE);
        } else {
            btn_mail_msg.setVisibility(View.VISIBLE);
        }

        if (populated(edt_pass)) {
            img_pass.setImageResource(R.drawable.ic_passwordcopy);
            btn_pass_msg.setVisibility(View.GONE);
            show_pass.setVisibility(View.VISIBLE);
        } else {
            btn_pass_msg.setVisibility(View.VISIBLE);
            img_pass.setImageResource(R.drawable.ic_password);
            show_pass.setVisibility(View.GONE);
        }

        if (populated(edt_name)) {
            img_name.setImageResource(R.drawable.ic_adsoyaderkeksecili);
        } else {
            img_name.setImageResource(R.drawable.ic_adsoyaderkek);
        }


        final boolean populated = populated(edt_phone) && populated(edt_name) && Utils.validateEmail(
                edt_mail.getText().toString()) && populated(
                edt_pass) && checkbox_contract.isChecked();
        btn_completed.setEnabled(populated);
        if (populated) {
            activityCommunicator.passDataToActivity(13, RegisterStatus.FINISHED, null);
        } else {
            activityCommunicator.passDataToActivity(13, RegisterStatus.FOCUSED, null);
        }
    }

    private boolean populated(final EditText editText) {
        return editText.length() > 0;
    }


    public void nextStep(String phonenumber) {
        HashMap<Object, Object> hashMap = new HashMap<>();
        hashMap.put("name", edt_name.getText().toString());
        hashMap.put("phone", phonenumber);
        hashMap.put("email", edt_mail.getText().toString());
        hashMap.put("password", edt_pass.getText().toString());
        activityCommunicator.passDataToActivity(21, null, hashMap);
    }
}
