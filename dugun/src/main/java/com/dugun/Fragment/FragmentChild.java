package com.dugun.Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;

import com.dugun.Activity.ModelsActivity;
import com.dugun.Activity.ProvidersActivity;
import com.dugun.Adapter.LikeMediaAdapter;
import com.dugun.Adapter.LikeProviderAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Api.GeneralVariables;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.LikeMediaModel;
import com.dugun.Model.LikeProviderModel;
import com.dugun.Model.ProviderModel;
import com.dugun.Model.SuccessModel;
import com.dugun.R;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dugun.Activity.ProvidersActivity.providerList;
import static com.dugun.Others.Utils.isNetworkAvailable;

/**
 * Created by ugurbasarir on 9/1/2017.
 */
public class FragmentChild extends BaseFragment {

    private GridView gridView;
    private ArrayList<LikeProviderModel> providerList;
    private ArrayList<LikeMediaModel> mediaList;
    private LikeMediaAdapter mediaAdapter;
    private LikeProviderAdapter providerAdapter;
    private Button btnNavigate;
    private int catId;
    private String catName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        providerList = bundle.getParcelableArrayList("providerList");
        mediaList = bundle.getParcelableArrayList("mediaList");
        catId = bundle.getInt("catId");
        catName = bundle.getString("catName");


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_child, container, false);

        gridView = (GridView) view.findViewById(R.id.gridView);
        btnNavigate = (Button) view.findViewById(R.id.btnNavigate);

        if (providerList != null) {
            if (providerList.size() > 0) {
                providerAdapter = new LikeProviderAdapter(getActivity(), providerList,position -> {
                    LikeProviderModel providerModel = providerList.get(position);
                    if(providerModel.isLiked()){
                        AlertDialog.Builder alertbox = new AlertDialog.Builder(getActivity());
                        alertbox.setMessage("Beğenmekten vazgeçmek istediğinize emin misiniz?");
                        alertbox.setTitle("Uyarı");
                        alertbox.setPositiveButton(
                                "Evet",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        setLike(true,providerModel.getProvider().getId(), position);
                                    }
                                });
                        alertbox.setNeutralButton("Iptal",(dialogInterface, i) -> dialogInterface.dismiss());
                        alertbox.show();
                    }else{
                        setLike(false,providerModel.getProvider().getId(), position);
                    }
                });
                gridView.setAdapter(providerAdapter);
                btnNavigate.setVisibility(View.GONE);
            } else {
                btnNavigate.setText("Firmalar");
                btnNavigate.setVisibility(View.VISIBLE);
                btnNavigate.setOnClickListener(view1 -> {
                    startActivity(
                            new Intent(getActivity(), ProvidersActivity.class));
                });
            }
        }
        if (mediaList != null) {
            if (mediaList.size() > 0) {
                mediaAdapter = new LikeMediaAdapter(getActivity(), mediaList);
                gridView.setAdapter(mediaAdapter);
                btnNavigate.setVisibility(View.GONE);
            } else {
                btnNavigate.setText("Galeriler");
                btnNavigate.setVisibility(View.VISIBLE);
                btnNavigate.setOnClickListener(view1 -> {
                    Intent i = new Intent(getActivity(), ModelsActivity.class);
                    i.putExtra("providerCategoryId", catId);
                    i.putExtra("categoryName", catName);
                    startActivity(i);
                });
            }
        }

        return view;
    }



    private void setLike(boolean isLiked, int itemId,int position) {
        if (!isNetworkAvailable(getActivity())) {
            AppAlertDialog.showMessage(getActivity(), getString(R.string.connection_failed));
            return;
        }
        showDialog();

        HashMap<String, Object> data = new HashMap<>();
        data.put("itemType", "provider");
        data.put("itemId", itemId);
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        if (isLiked) {

                apiInterface.deleteCoupleLikes(user.getAccessToken().getToken(),
                        user.getUser().getId(), data).enqueue(
                        new Callback<Object>() {
                            @Override
                            public void onResponse(Call<Object> call, Response<Object> response) {
                                dismissDialog();
                                providerList.get(position).setLiked(false);
                                providerAdapter.notifyDataSetChanged();
                                GeneralVariables.removeFromLikes(itemId,false);
                            }

                            @Override
                            public void onFailure(Call<Object> call, Throwable t) {
                                dismissDialog();
                                AppAlertDialog.showMessage(mActivity,
                                        getString(R.string.failure_message));
                            }

                        }
                );

        } else {
            apiInterface.addCoupleLikes(user.getAccessToken().getToken(),
                    user.getUser().getId(), data).enqueue(
                    new Callback<SuccessModel>() {
                        @Override
                        public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                            dismissDialog();
                            if (response != null & response.body() != null) {
                                providerList.get(position).setLiked(true);
                                providerAdapter.notifyDataSetChanged();
                                GeneralVariables.addToLikes(itemId,false);
                            }
                        }

                        @Override
                        public void onFailure(Call<SuccessModel> call, Throwable t) {
                            dismissDialog();
                            AppAlertDialog.showMessage(mActivity,
                                    getString(R.string.failure_message));
                        }

                    }
            );
        }

    }

}
