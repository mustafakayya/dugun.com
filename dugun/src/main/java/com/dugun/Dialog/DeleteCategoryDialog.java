package com.dugun.Dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.dugun.Interface.DeleteCategoryCallback;
import com.dugun.Model.CategoryModel;
import com.dugun.R;


public class DeleteCategoryDialog extends DialogFragment {

   private Button notSureBtn, dealBtn;
   private       TextView               name;
   public static DeleteCategoryCallback callback;
   private       CategoryModel          categoryModel;

   public DeleteCategoryDialog (CategoryModel categoryModel) {
      this.categoryModel = categoryModel;
   }

   public static DeleteCategoryDialog newInstance (CategoryModel categoryModel) {
      DeleteCategoryDialog fragment = new DeleteCategoryDialog (categoryModel);
      return fragment;
   }

   @Nullable
   @Override
   public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      View view = inflater.inflate (R.layout.dialog_delete_category_layout,
                                    container, false);
      FrameLayout layout_close = (FrameLayout) view.findViewById (R.id.layout_close);
      layout_close.setOnClickListener (new View.OnClickListener () {
         @Override
         public void onClick (View view) {
            dismiss ();
         }
      });

      notSureBtn = (Button) view.findViewById (R.id.not_sure_btn);
      dealBtn = (Button) view.findViewById (R.id.deal_btn);
      name = (TextView) view.findViewById (R.id.name);

      name.setText (categoryModel.getName ());

      notSureBtn.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            callback.deleteCategoryCallback (false);
            dismiss ();
         }
      });
      dealBtn.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            callback.deleteCategoryCallback (true);
            dismiss ();
         }
      });


      return view;
   }

   @Override
   public void onStart () {
      super.onStart ();

      Dialog dialog = getDialog ();
      if (dialog != null) {
         dialog.getWindow ().setBackgroundDrawable (new ColorDrawable (Color.TRANSPARENT));
      }
   }


   @NonNull
   @Override
   public Dialog onCreateDialog (Bundle savedInstanceState) {
      Dialog dialog = super.onCreateDialog (savedInstanceState);
      dialog.getWindow ().requestFeature (Window.FEATURE_NO_TITLE);
      return dialog;

   }

}
