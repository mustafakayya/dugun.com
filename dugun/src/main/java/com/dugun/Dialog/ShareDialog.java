package com.dugun.Dialog;


import android.app.Dialog;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dugun.Others.USER;
import com.dugun.Others.Utils;
import com.dugun.R;


public class ShareDialog extends DialogFragment implements View.OnClickListener {

    private LinearLayout shareLay;
    private ImageView shareImg;
    private TextView day, hour, min, lblCoupleName;
    public String dayStr, hourStr, minStr, shareUrl;

    public static ShareDialog newInstance() {
        ShareDialog fragment = new ShareDialog();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_share_layout, container, false);
        FrameLayout layout_close = (FrameLayout) view.findViewById(R.id.layout_close);
        layout_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        view.findViewById(R.id.facebook).setOnClickListener(this);
        view.findViewById(R.id.twitter).setOnClickListener(this);
        view.findViewById(R.id.share).setOnClickListener(this);
        shareLay = (LinearLayout) view.findViewById(R.id.share_lay);
        shareImg = (ImageView) view.findViewById(R.id.share_img);
        lblCoupleName = (TextView) view.findViewById(R.id.lblCoupleName_dShare);
        lblCoupleName.setText(USER.getProfile(getContext()).getUser().getCoupleName());

        // Bu resim servisten gelecek
        shareImg.setImageResource(R.drawable.ic_gelinlik);
        day = (TextView) view.findViewById(R.id.dayCount);
        day.setText(dayStr);
        hour = (TextView) view.findViewById(R.id.hourCount);
        hour.setText(hourStr);
        min = (TextView) view.findViewById(R.id.minCount);
        min.setText(minStr);
        Glide.with(getActivity()).load(shareUrl).into(shareImg);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;

    }

    void shareDetail() {
        String pdfName = "en_mutlu_gunumuz.pdf";
        Utils.getBitmapFromView(shareLay, pdfName);
        Intent shareIntent = Utils.createPdfShareIntent("En Mutlu Günümüz", pdfName);
        startActivity(Intent.createChooser(shareIntent, ""));
    }


    @Override
    public void onClick(View view) {
        shareDetail();
    }
}
