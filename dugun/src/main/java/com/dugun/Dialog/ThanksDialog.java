package com.dugun.Dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.dugun.R;


public class ThanksDialog extends DialogFragment {

   private LinearLayout goMyOffers;
   private Button       searchMore;

   public static ThanksDialog newInstance () {
      ThanksDialog fragment = new ThanksDialog ();
      return fragment;
   }

   @Nullable
   @Override
   public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      View view = inflater.inflate (R.layout.dialog_thanks,
                                    container, false);
      FrameLayout layout_close = (FrameLayout) view.findViewById (R.id.layout_close);
      layout_close.setOnClickListener (new View.OnClickListener () {
         @Override
         public void onClick (View view) {
            dismiss ();
         }
      });

      goMyOffers = (LinearLayout) view.findViewById (R.id.go_my_offers);
      searchMore = (Button) view.findViewById (R.id.search_more);

      searchMore.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {

         }
      });


      return view;
   }

   @Override
   public void onStart () {
      super.onStart ();

      Dialog dialog = getDialog ();
      if (dialog != null) {
         dialog.getWindow ().setBackgroundDrawable (new ColorDrawable (Color.TRANSPARENT));
      }
   }


   @NonNull
   @Override
   public Dialog onCreateDialog (Bundle savedInstanceState) {
      Dialog dialog = super.onCreateDialog (savedInstanceState);
      dialog.getWindow ().requestFeature (Window.FEATURE_NO_TITLE);
      return dialog;

   }


}
