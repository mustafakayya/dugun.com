package com.dugun.Dialog;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.dugun.R;

import static com.dugun.Api.GeneralVariables.CAMERA;
import static com.dugun.Api.GeneralVariables.GALLERY;


/**
 * Created by ugurbasarir on 6/8/16.
 */
public class CameraGalleryDialog extends DialogFragment {

   private LinearLayout camera, gallery;
   private Button dismiss;

   @Override
   public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

      View view = inflater.inflate (R.layout.dialog_camera_gallery, container, false);

      camera = (LinearLayout) view.findViewById (R.id.CameraGalleryDialogCameraLayout);
      gallery = (LinearLayout) view.findViewById (R.id.CameraGalleryDialogGalleryLayout);
      dismiss = (Button) view.findViewById (R.id.CameraGalleryDialogDismissLayout);

      camera.setOnClickListener (new View.OnClickListener () {
         @Override
         public void onClick (View v) {
            Intent cameraIntent = new Intent (android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            getActivity ().startActivityForResult (cameraIntent, CAMERA);
            dismiss ();
         }
      });
      gallery.setOnClickListener (new View.OnClickListener () {
         @Override
         public void onClick (View v) {
            Intent intent = new Intent (Intent.ACTION_PICK,
                                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType ("image/*");
            getActivity ().startActivityForResult (Intent.createChooser (intent, ""), GALLERY);
            dismiss ();
         }
      });
      dismiss.setOnClickListener (new View.OnClickListener () {
         @Override
         public void onClick (View v) {
            dismiss ();
         }
      });

      return view;
   }

}
