package com.dugun.Dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.dugun.Adapter.FormOptionsAdapter;
import com.dugun.Interface.PickerSelectCallback;
import com.dugun.Model.FormOptionsModel;
import com.dugun.R;

import java.util.ArrayList;


public class PickerDialog extends DialogFragment {

   private Context                                              context;
   private PickerSelectCallback                                 callback;
   private ArrayList<FormOptionsModel> options;
   private Button                                               ok, cancel;
   private GridView gridView;

   public PickerDialog (Context context, PickerSelectCallback callback, ArrayList<FormOptionsModel> options) {
      this.context = context;
      this.callback = callback;
      this.options = options;
   }

   @Nullable
   @Override
   public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      View view = inflater.inflate (R.layout.dialog_picker, container, false);

      gridView = (GridView) view.findViewById (R.id.gridView);
      gridView.setAdapter (new FormOptionsAdapter (context, options));
      gridView.setOnItemClickListener (new AdapterView.OnItemClickListener () {
         @Override
         public void onItemClick (AdapterView<?> parent, View view, int position, long id) {
            callback.setSelect (position);
            dismiss ();
         }
      });
      ok = (Button) view.findViewById (R.id.ok);
      ok.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {

         }
      });
      cancel = (Button) view.findViewById (R.id.cancel);
      cancel.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            dismiss ();
         }
      });

      return view;
   }


   @Override
   public void onStart () {
      super.onStart ();

      Dialog dialog = getDialog ();
      if (dialog != null) {
         dialog.getWindow ().setBackgroundDrawable (new ColorDrawable (Color.TRANSPARENT));
      }
   }


   @NonNull
   @Override
   public Dialog onCreateDialog (Bundle savedInstanceState) {
      Dialog dialog = super.onCreateDialog (savedInstanceState);
      dialog.getWindow ().requestFeature (Window.FEATURE_NO_TITLE);
      return dialog;

   }

}
