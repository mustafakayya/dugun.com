package com.dugun.Api;

import com.dugun.Api.couplelikemodel.GetCoupleLike;
import com.dugun.Api.coupleprovidermodel.GetCoupleProvider;
import com.dugun.Api.coupleprovidermodel.GetCoupleProviders;
import com.dugun.Model.BadgesModel;
import com.dugun.Model.ConceptModel;
import com.dugun.Model.CoupleIdModel;
import com.dugun.Model.IdModel;
import com.dugun.Model.LikeScopesModel;
import com.dugun.Model.NewLeadInteraction;
import com.dugun.Model.PhoneModel;
import com.dugun.Model.ProviderModel;
import com.dugun.Model.ResultIdModel;
import com.dugun.Model.SettingsModel;
import com.dugun.Model.StatsModel;
import com.dugun.Model.SuccessModel;
import com.dugun.Wrapper.CategoryWrapper;
import com.dugun.Wrapper.CityWrapper;
import com.dugun.Wrapper.FilterWrapper;
import com.dugun.Wrapper.FormDataWrapper;
import com.dugun.Wrapper.LeadsInteractionsWrapper;
import com.dugun.Wrapper.LeadsWrapper;
import com.dugun.Wrapper.LikeStatsWrapper;
import com.dugun.Wrapper.LoginAccessModelWrapper;
import com.dugun.Wrapper.ModelGalleryModelWrapper;
import com.dugun.Wrapper.ModelsWrapper;
import com.dugun.Wrapper.ProviderGalleryWrapper;
import com.dugun.Wrapper.ProviderInfoRequestFormsWrapper;
import com.dugun.Wrapper.ProviderWrapper;
import com.dugun.Wrapper.SharePhotoWrapper;
import com.dugun.Wrapper.TestimonialWrapper;
import com.dugun.Wrapper.TodoWrapper;
import com.dugun.Wrapper.UserPhotosWrapper;
import com.dugun.Wrapper.UserWrapper;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ApiInterface {

    //Authorization
    @POST("access-tokens")
    Call<LoginAccessModelWrapper> login(
            @Body HashMap<Object, Object> body);

    // Edit Couple
    @PUT("me")
    @Headers({"Content-Type : application/json"})
    Call<SuccessModel> editProfile(
            @Header("X-ACCESS-TOKEN") String token,
            @Body HashMap<Object, Object> body);

    @Multipart
    @POST("me/photos")
    Call<UserPhotosWrapper> uploadPhoto(
            @Header("X-ACCESS-TOKEN") String token,
            @Part("cover") RequestBody cover,
            @Part MultipartBody.Part file);


    @GET("couples/{coupleId}/photos/share")
    Call<SharePhotoWrapper> getSharePhoto(@Header("X-ACCESS-TOKEN") String token,
                                          @Path("coupleId") Integer coupleId);

    //Couple Like
    @GET("couples/*/likes")
    Call<GetCoupleLike> coupleLikes(
            @Header("X-ACCESS-TOKEN") String token);

    //Couple To-do
    @GET("couple-todo/{coupleId}/tasks")
    Call<TodoWrapper> getAllTodos(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("coupleId") Integer coupleId);

    //Couple To-do by id
    @GET("couple-todo/{coupleId}/tasks")
    Call<TodoWrapper> getAllTodosById(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("coupleId") Integer coupleId,
            @QueryMap Map<String, Integer> options);

    // Delete To-do
    @DELETE("couple-todo/{coupleId}/tasks/{taskId}")
    Call<SuccessModel> deleteTodo(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("coupleId") Integer coupleId,
            @Path("taskId") Integer taskId);

    //Stats
    @GET("me/stats")
    Call<StatsModel> stats(
            @Header("X-ACCESS-TOKEN") String token);

    //Like Stats
    @GET("me/likes?scopes[]=onlyIds")
    Call<LikeScopesModel> likeStats(
            @Header("X-ACCESS-TOKEN") String token);

    // Provider Form Data
    @GET("providers/{providerId}/data")
    Call<FormDataWrapper> getProviderFormData(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("providerId") Integer providerId);

    //Get Provider Categories
    @GET("provider-categories")
    Call<ProviderWrapper> getProviderCategories(
            @Header("X-ACCESS-TOKEN") String token);

    //Get All Providers by Filter
    @GET("providers/search")
    Call<ProviderWrapper> getAllProvidersById(
            @Header("X-ACCESS-TOKEN") String token,
            @QueryMap Map<String, Integer> options);

    // Provider Form Data
    @GET("providers/{providerId}?scopes[]=providerDetailDiscounts")
    Call<ProviderModel> getProviderDiscount(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("providerId") Integer providerId);


    // Add Couple Feedback
    @POST("/couples-feedback")
    @Headers({"Content-Type : application/json"})
    Call<SuccessModel> addCoupleFeedback(
            @Header("X-ACCESS-TOKEN") String token,
            @Body HashMap<String, String> body);

    // Contact Us
    @POST("/contact-forms")
    @Headers({"Content-Type : application/json"})
    Call<Object> sendContactForm(
            @Header("X-ACCESS-TOKEN") String token,
            @Body HashMap<String, String> body);

    // Get Model Categories
    @GET("galleries")
    @Headers({"Content-Type : application/json"})
    Call<ModelsWrapper> getModelGalleries(
            @Header("X-ACCESS-TOKEN") String token,
            @QueryMap Map<String, String> options);

    // New Lead Info Requests
    @GET("providers/{providerId}/info-request-forms")
    Call<ProviderInfoRequestFormsWrapper> getProviderInfoRequestForms(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("providerId") Integer providerId);

    // Get Model Categories
    @GET("galleries/{galleryId}/medias")
    @Headers({"Content-Type : application/json"})
    Call<ModelGalleryModelWrapper> getModelGallery(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("galleryId") Integer galleryId,
            @QueryMap Map<String, String> options);

    // Get Filter Data
    @GET("/providers/search/filters")
    @Headers({"Content-Type : application/json"})
    Call<FilterWrapper> getFilterData(
            @Header("X-ACCESS-TOKEN") String token,
            @QueryMap Map<String, Object> options);

    // Get Provider Detail Gallery Images
    @GET("/providers/{providerId}/medias")
    @Headers({"Content-Type : application/json"})
    Call<ProviderGalleryWrapper> getGalleryImages(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("providerId") Integer providerId);

    // Get Provider Detail Gallery Images
    @GET("/providers/{providerId}/phone")
    @Headers({"Content-Type : application/json"})
    Call<PhoneModel> getPhone(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("providerId") Integer providerId);


    @GET("/provider-testimonial")
    @Headers({"Content-Type : application/json"})
    Call<TestimonialWrapper> getTestimonials(@Header("X-ACCESS-TOKEN") String token
            , @QueryMap Map<String, Object> options);

    @GET("providers/search")
    Call<ProviderWrapper> getProvidersByName(@Header("X-ACCESS-TOKEN") String token,@Query("name")String name);


    // Get Like Stats
    @GET("me/likes/stats")
    @Headers({"Content-Type : application/json"})
    Call<LikeStatsWrapper> getLikeStats(
            @Header("X-ACCESS-TOKEN") String token);

    // Get Like Data
    @GET("me/likes")
    @Headers({"Content-Type : application/json"})
    Call<JsonObject> getLikeData(
            @Header("X-ACCESS-TOKEN") String token,
            @QueryMap Map<String, Object> categoryId);

    @POST("couples/{coupleId}/likes")
    @Headers({"Content-Type : application/json"})
    Call<SuccessModel> addCoupleLikes(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("coupleId") Integer coupleId,
            @Body HashMap<String, Object> body);

    @HTTP(method = "DELETE", path = "couples/{coupleId}/likes", hasBody = true)
    Call<Object> deleteCoupleLikes(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("coupleId") Integer coupleId,
            @Body HashMap<String, Object> body);

    @POST("info-requests")
    @Headers({"Content-Type : application/json"})
    Call<Object> addNewLead(
            @Header("X-ACCESS-TOKEN") String token,
            @Body HashMap<Object, Object> body);

    @DELETE("couples/*/likes")
    @Headers({"Content-Type : application/json"})
    Call<Object> deleteCoupleLikes(
            @Header("X-ACCESS-TOKEN") String token,
            @Body HashMap<Object, Object> body);

    //Couple Provider
    @GET("couple-providers")
    Call<GetCoupleProviders> getAllCoupleProviders(
            @Header("X-ACCESS-TOKEN") String token);

    @GET("couple-providers/*")
    Call<GetCoupleProvider> getCoupleProvider(
            @Header("X-ACCESS-TOKEN") String token);

    @POST("couple-providers")
    @Headers({"Content-Type : application/json"})
    Call<IdModel> addCoupleProvider(
            @Header("X-ACCESS-TOKEN") String token,
            @Body HashMap<Object, Object> body);

    @PUT("couple/*")
    @Headers({"Content-Type : application/json"})
    Call<Object> editCoupleProvider(
            @Header("X-ACCESS-TOKEN") String token,
            @Body HashMap<Object, Object> body);

    @DELETE("couple/*")
    @Headers({"Content-Type : application/json"})
    Call<Object> deleteCoupleProvider(
            @Header("X-ACCESS-TOKEN") String token);

    //Couple-TO-DO
    @GET("couple-todo/categories")
    Call<CategoryWrapper> categoryList(
            @Header("X-ACCESS-TOKEN") String token);

    //Couple-TO-DO
    @GET("couple-todo/categories")
    Call<CategoryWrapper> categoryListWithoutToken();

    //Couple To do Add Category
    @POST("couple-todo/{coupleId}")
    @Headers({"Content-Type : application/json"})
    Call<SuccessModel> addCategory(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("coupleId") Integer coupleId,
            @Body HashMap<Object, Object> body);

    // Lead Add Comment
    @POST("leads/{leadId}/interactions")
    @Headers({"Content-Type : application/json"})
    Call<Object> addComment(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("leadId") Integer leadId,
            @Body HashMap<Object, Object> body);


    @GET("/leads/{leadId}")
    Call<LeadsInteractionsWrapper> getLeadInteractions(@Header("X-ACCESS-TOKEN") String token, @Path("leadId") int leadId );

    @POST("/leads/{leadId}/interactions")
    Call<SuccessModel> newLeadInteraction(@Header("X-ACCESS-TOKEN") String token,@Path("leadId") int leadId , @Body NewLeadInteraction message);


    //Couple To do Delete Category
    @HTTP(method = "DELETE", path = "couple-todo/{coupleId}", hasBody = true)
    Call<SuccessModel> deleteCategory(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("coupleId") Integer coupleId,
            @Body HashMap<Object, Object> body);

    //Get Couple Tasks
    @GET("couple-todo/*/tasks")
    Call<Object> getTasks(
            @Header("X-ACCESS-TOKEN") String token);

    // Add Couple Tasks
    @POST("couple-todo/{coupleId}/tasks")
    Call<ResultIdModel> addTask(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("coupleId") Integer coupleId,
            @Body HashMap<Object, Object> body);

    // Update Couple Tasks
    @PUT("couple-todo/{coupleId}/tasks/{taskId}")
    Call<SuccessModel> updateTask(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("coupleId") Integer coupleId,
            @Path("taskId") Integer taskId,
            @Body HashMap<Object, Object> body);

    //Edit Couple Tasks
    @PUT("couple-todo/*/tasks/*")
    Call<Object> editTask(
            @Header("X-ACCESS-TOKEN") String token);

    // Get Concepts
    @GET("provider-forms/{providerId}")
    Call<ConceptModel> getConcepts(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("providerId") int providerId);

    // Get Concepts
    @PUT("couple/{coupleId}")
    Call<SuccessModel> updateConcepts(
            @Header("X-ACCESS-TOKEN") String token,
            @Path("coupleId") int coupleId,@Body  HashMap<String, Object[]> body);

    //Couple Feedbacks
    @GET("couples-feedback")
    @Headers({"Content-Type : application/json"})
    Call<Object> getAllFeedBacks(
            @Header("X-ACCESS-TOKEN") String token);

    @GET("couples-feedback/*")
    @Headers({"Content-Type : application/json"})
    Call<Object> getFeedBack(
            @Header("X-ACCESS-TOKEN") String token);

    @POST("couples-feedback")
    @Headers({"Content-Type : application/json"})
    Call<Object> addFeedBack(
            @Header("X-ACCESS-TOKEN") String token,
            @Body HashMap<Object, Object> body);

    @PUT("couples-feedback/*")
    @Headers({"Content-Type : application/json"})
    Call<Object> editFeedBack(
            @Header("X-ACCESS-TOKEN") String token,
            @Body HashMap<Object, Object> body);

    @DELETE("couples-feedback/*")
    @Headers({"Context-Type : application/json"})
    Call<Object> deleteFeedBack(
            @Header("X-ACCESS-TOKEN") String token);

    //Galleries
    @GET("galleries")
    Call<Object> getAllGalleries(
            @Header("X-ACCESS-TOKEN") String token);

    @GET("galleries/*")
    Call<Object> getGallery(
            @Header("X-ACCESS-TOKEN") String token);

    @GET("galleries/*/medias")
    Call<Object> getGalleryMedias(
            @Header("X-ACCESS-TOKEN") String token);

    @GET("galleries/*/medias/*")
    Call<Object> getMediaDetail(
            @Header("X-ACCESS-TOKEN") String token);

    @GET("galleries/*/related-medias")
    Call<Object> getGalleryRelatedMedias(
            @Header("X-ACCESS-TOKEN") String token);

    //Location
    @GET("cities")
    Call<CityWrapper> getCities();

    @GET("districts")
    Call<Object> getDistricts();

    @GET("districts/*")
    Call<Object> getDistrict();

    //Register
    @POST("couple")
    Call<CoupleIdModel> register(
            @Body HashMap<Object, Object> body);

    //Settings
    @GET("settings")
    Call<SettingsModel> settings();

    //User
    @GET("me")
    Call<UserWrapper> getProfile(
            @Header("X-ACCESS-TOKEN") String token);

    @POST("me/photos")
    Call<Object> getPhotos(
            @Header("X-ACCESS-TOKEN") String token,
            @Body HashMap<Object, Object> body);

    @GET("me/leads")
    Call<LeadsWrapper> getLeads(
            @Header("X-ACCESS-TOKEN") String token);


    @GET("me/leads")
    Call<LeadsWrapper> getLeadsByCatId(
            @Header("X-ACCESS-TOKEN") String token, @Query("categoryId") int catId);

    @PUT("me/password")
    Call<SuccessModel> setPassword(
            @Header("X-ACCESS-TOKEN") String token,
            @Body HashMap<Object, Object> body);

    @GET("me/badges?scopes[]=notEarnedBadges")
    Call<BadgesModel> getBadges(
            @Header("X-ACCESS-TOKEN") String token);


    @PUT("me/badges/new/{coupleBadgeId}")
    Call<SuccessModel> notifyBadge(
            @Header("X-ACCESS-TOKEN") String token,@Path("coupleBadgeId")int badgeId);

}
