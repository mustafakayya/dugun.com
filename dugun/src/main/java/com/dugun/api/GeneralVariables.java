package com.dugun.Api;

import android.os.Environment;

import com.dugun.Model.CategoryModel;
import com.dugun.Model.LikeScopesModel;

import java.util.List;

/**
 * Created by ugurbasarir on 22/08/17.
 */

public class GeneralVariables {

//   public static String CONSUMER_KEY = "f3007680b89d45bb886deff7e3ec4eb0";


    public static LikeScopesModel LIKES;
    public static List<CategoryModel> categoryModels;

    public static String SHARED_ACCESS_TOKEN = "com.dugun.accessToken";
    public static String SHARED_PSW = "com.dugun.password";
    public static String FIRST_RUNNING = "com.dugun.isFirstRunning";

    public static String APP_DIR = Environment.getDataDirectory().getAbsolutePath() + "/dugun.com/";
    public static String COVER_DIR = "profile/cover.jpg";

    public static final int PRIVACY = 0, ABOUT_US = 1, TERMS = 2, FAQ = 3, SURUM = 4, FULL_DATE = 10, PLAN_DATE = 11, TIME = 12, MONTH_NAME = 13, SIMPLE_DATE = 14,MONTH_YEAR=15;
    public static final int CAMERA = 100, GALLERY = 101;


    public static void clear(){
        LIKES = null;
        categoryModels = null;
    }

    public static boolean isLiked(int id , boolean isMedia){
        if(LIKES==null){
            LIKES = new LikeScopesModel();
        }
        if(isMedia){
            return LIKES.getData().getMedia().contains(new Integer(id));
        }else{
            return LIKES.getData().getProvider().contains(new Integer(id));
        }
    }

    public static void addToLikes(int id , boolean isMedia){
        if(LIKES==null){
            LIKES = new LikeScopesModel();
        }


        if(isMedia){
            LIKES.getData().getMedia().add(id);
        }else{
            LIKES.getData().getProvider().add(id);
        }
    }

    public static void removeFromLikes(int id , boolean isMedia){
        if(LIKES==null){
            LIKES = new LikeScopesModel();
        }

        if(isMedia){
            LIKES.getData().getMedia().remove(new Integer(id));
        }else{
            LIKES.getData().getProvider().remove(new Integer(id));
        }
    }
}
