package com.dugun.Api.coupleprovidermodel;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by burak on 04/07/2017.
 */

public class GetCoupleProvider {

    @SerializedName("data")
    public InnerData data;

    public class InnerData {
        @SerializedName("id")
        public int id;
        @SerializedName("coupleId")
        public int coupleId;
        @SerializedName("notes")
        public String notes;
        @SerializedName("organizationDate")
        public Date organizationDate;
        @SerializedName("organizationTypeId")
        public int organizationTypeId;
        @SerializedName("leadId")
        public int leadId;
        @SerializedName("name")
        public String name;
        @SerializedName("categoryId")
        public int categoryId;
        @SerializedName("providerId")
        public int providerId;

        @SerializedName("provider")
        public InnerProvider provider;
        @SerializedName("couple")
        public InnerCouple couple;

        public class InnerProvider {
            @SerializedName("id")
            public int id;
            @SerializedName("categoryId")
            public int categoryId;
            @SerializedName("name")
            public String name;
            @SerializedName("statusId")
            public int statusId;
            @SerializedName("cityId")
            public int cityId;
            @SerializedName("districtId")
            public int districtId;
        }

        public class InnerCouple {
            @SerializedName("id")
            public int id;
            @SerializedName("name")
            public String name;
            @SerializedName("email")
            public String email;
            @SerializedName("weddingDate")
            public Date weddingDate;
        }
    }
}
