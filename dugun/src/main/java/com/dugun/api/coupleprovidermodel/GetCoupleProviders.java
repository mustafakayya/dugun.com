package com.dugun.Api.coupleprovidermodel;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Created by burak on 04/07/2017.
 */

public class GetCoupleProviders {

    @SerializedName("data")
    public List<CoupleProviderData> data;

    public class CoupleProviderData {

        @SerializedName("coupleProvider")
        public CoupleProvider coupleProvider;
        @SerializedName("leadId")
        public int leadId;

        public class CoupleProvider {
            @SerializedName("id")
            public int id;
            @SerializedName("categoryId")
            public int categoryId;
            @SerializedName("name")
            public String name;
            @SerializedName("notes")
            public String notes;
            @SerializedName("organizationTypeId")
            public int organizationTypeId;
            @SerializedName("organization")
            public Date organization;
            @SerializedName("agreedAt")
            public Date agreedAt;

            @SerializedName("couple")
            public InnerCouple couple;
            @SerializedName("testimonials")
            public List<InnerTestimonials> testimonials;
            @SerializedName("provider")
            public InnerProvider provider;

            public class InnerCouple {
                @SerializedName("id")
                public int id;
                @SerializedName("uId")
                public String uId;
                @SerializedName("name")
                public String name;
                @SerializedName("phone")
                public String phone;
                @SerializedName("email")
                public String email;
                @SerializedName("weddingDate")
                public Date weddingDate;
            }

            public class InnerTestimonials {
                @SerializedName("id")
                public int id;
                @SerializedName("testimonial")
                public String testimonial;
            }

            public class InnerProvider {
                @SerializedName("id")
                public int id;
                @SerializedName("slug")
                public String slug;
                @SerializedName("categoryId")
                public int categoryId;
                @SerializedName("name")
                public String name;
                @SerializedName("statusId")
                public int statusId;
                @SerializedName("cityId")
                public int cityId;
                @SerializedName("districtId")
                public int districtId;
                @SerializedName("city")
                public InnerCity city;

                public class InnerCity {
                    @SerializedName("id")
                    public int id;
                    @SerializedName("name")
                    public String name;
                    @SerializedName("slug")
                    public String slug;
                }
            }

        }
    }

}
