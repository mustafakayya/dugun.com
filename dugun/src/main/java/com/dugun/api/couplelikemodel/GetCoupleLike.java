package com.dugun.Api.couplelikemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by burak on 04/07/2017.
 */

public class GetCoupleLike {

   @SerializedName ("data")
   public GetCoupleLikeData data;

   public class GetCoupleLikeData {

      @SerializedName ("provider")
      public List<Provider> provider;

      public class Provider {

         @SerializedName ("id")
         public int           id;
         @SerializedName ("itemId")
         public int           itemId;
         @SerializedName ("provider")
         public InnerProvider provider;

         public class InnerProvider {

            @SerializedName ("id")
            public int    id;
            @SerializedName ("slug")
            public String slug;
            @SerializedName ("categoryId")
            public int    categoryId;
            @SerializedName ("name")
            public String name;
            @SerializedName ("providerImage")
            public String providerImage;
            @SerializedName ("city")
            public City   city;

            public class City {

               @SerializedName ("id")
               public int    id;
               @SerializedName ("name")
               public String name;
               @SerializedName ("slug")
               public String slug;
            }
         }

      }

   }

}
