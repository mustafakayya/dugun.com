package com.dugun.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dugun.Activity.MainActivity;
import com.dugun.Activity.ModelsActivity;
import com.dugun.Activity.ProvidersActivity;
import com.dugun.Model.CategoryModel;
import com.dugun.R;

import java.util.ArrayList;


public class CompanyDetailExpandableListAdapter extends BaseExpandableListAdapter {

   private Context                  context;
   private ArrayList<CategoryModel> categoryList;


   public CompanyDetailExpandableListAdapter (Context context, ArrayList<CategoryModel> categoryList) {
      this.context = context;
      this.categoryList = categoryList;
   }


   @Override
   public int getGroupCount () {
      return categoryList.size ();
   }

   @Override
   public int getChildrenCount (int i) {
      return 1;
   }

   @Override
   public Object getGroup (int i) {
      return null;
   }

   @Override
   public Object getChild (int i, int i1) {
      return null;
   }

   @Override
   public long getGroupId (int i) {
      return i;
   }

   @Override
   public long getChildId (int i, int i1) {
      return i1;
   }

   @Override
   public boolean hasStableIds () {
      return false;
   }

   @Override
   public View getGroupView (int i, boolean b, View convertView, ViewGroup viewGroup) {
      if (convertView == null) {
         LayoutInflater layoutInflater = (LayoutInflater) context.
                 getSystemService (Context.LAYOUT_INFLATER_SERVICE);
         convertView = layoutInflater.inflate (R.layout.category_header_item, null);
      }

      ImageView img_category_icon = (ImageView) convertView.findViewById (R.id.img_category_icon);
      ImageView img_plus          = (ImageView) convertView.findViewById (R.id.img_plus);
      TextView  txt_category_name = (TextView) convertView.findViewById (R.id.txt_category_name);
      txt_category_name.setText (categoryList.get (i).getName ());

      if (b) {
         img_category_icon.setImageResource (R.drawable.ic_uncheckedcategory);
         img_plus.setImageResource (R.drawable.minus);
         txt_category_name.setTextColor (context.getResources ().getColor (R.color.green_color));
      } else {
//         img_category_icon.setImageResource (categoryList.get (i).getImg ());
         img_plus.setImageResource (R.drawable.plus);
         txt_category_name.setTextColor (
                 context.getResources ().getColor (R.color.dugun_text_black));
      }

      return convertView;
   }

   @Override
   public View getChildView (int i, int i1, boolean b, View convertView, ViewGroup viewGroup) {
      if (convertView == null) {
         LayoutInflater layoutInflater = (LayoutInflater) context.
                 getSystemService (Context.LAYOUT_INFLATER_SERVICE);
         convertView = layoutInflater.inflate (R.layout.category_child_item, null);

         LinearLayout models = (LinearLayout) convertView.findViewById (R.id.child_item_models);
         models.setOnClickListener (new View.OnClickListener () {
            @Override public void onClick (View v) {
               switch (MainActivity.openItem) {
                  case 2:
                     context.startActivity (new Intent (context, ModelsActivity.class));
                     break;
               }

            }
         });
         LinearLayout companies = (LinearLayout) convertView.findViewById (
                 R.id.child_item_providers);
         companies.setOnClickListener (new View.OnClickListener () {
            @Override public void onClick (View v) {
               switch (MainActivity.openItem) {
                  case 2:
                     context.startActivity (
                             new Intent (context, ProvidersActivity.class));
                     break;
               }

            }
         });
         LinearLayout favorites = (LinearLayout) convertView.findViewById (
                 R.id.child_item_favorites);
         favorites.setOnClickListener (new View.OnClickListener () {
            @Override public void onClick (View v) {
//               switch (MainActivity.openItem) {
//                  case 2:
//                     context.startActivity (
//                             new Intent (context, LikeTabbedActivity.class));
//                     break;
//               }

            }
         });

      }
      return convertView;
   }

   @Override
   public boolean isChildSelectable (int i, int i1) {
      return false;
   }
}
