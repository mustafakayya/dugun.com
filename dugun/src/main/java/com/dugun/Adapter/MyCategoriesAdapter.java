package com.dugun.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dugun.Model.CategoryModel;
import com.dugun.R;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class MyCategoriesAdapter extends BaseAdapter {

   private Context                  context;
   private ArrayList<CategoryModel> categoryList;

   public MyCategoriesAdapter (Context context, ArrayList<CategoryModel> categoryList) {
      this.context = context;
      this.categoryList = categoryList;
   }

   @Override
   public int getCount () {
      return categoryList.size ();
   }

   @Override
   public CategoryModel getItem (int i) {
      return categoryList.get (i);
   }

   @Override
   public long getItemId (int i) {
      return i;
   }

   @Override
   public View getView (int position, View convertView, ViewGroup viewGroup) {
      if (convertView == null) {
         convertView = LayoutInflater.from (context).
                 inflate (R.layout.list_item_my_categories, viewGroup, false);
      }

      final LinearLayout child = (LinearLayout) convertView.findViewById (R.id.child_layout);

      ImageView plus = (ImageView) convertView.findViewById (R.id.plus);

      plus.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            if (child.getVisibility () == View.GONE)
               child.setVisibility (View.VISIBLE);
            else
               child.setVisibility (View.GONE);
         }
      });


      return convertView;
   }

}
