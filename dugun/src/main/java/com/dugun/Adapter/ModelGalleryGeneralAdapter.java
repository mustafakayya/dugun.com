package com.dugun.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dugun.Model.MediaInfoTagModel;
import com.dugun.R;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ModelGalleryGeneralAdapter extends BaseAdapter {

   private Context                      context;
   private ArrayList<MediaInfoTagModel> generalList;

   public ModelGalleryGeneralAdapter (Context context, ArrayList<MediaInfoTagModel> generalList) {
      this.context = context;
      this.generalList = generalList;
   }

   @Override
   public int getCount () {
      return generalList.size ();
   }

   @Override
   public MediaInfoTagModel getItem (int i) {
      return generalList.get (i);
   }

   @Override
   public long getItemId (int i) {
      return i;
   }

   @Override
   public View getView (int position, View convertView, ViewGroup viewGroup) {
      if (convertView == null) {
         convertView = LayoutInflater.from (this.context).
                 inflate (R.layout.item_detail_general, viewGroup, false);
      }

      TextView lbl = (TextView) convertView.findViewById (R.id.lbl);
      TextView txt = (TextView) convertView.findViewById (R.id.txt);

      txt.setText (generalList.get (position).getGalleryCategoryInfoTagOption ().getValue ());
      lbl.setText (generalList.get (position).getGalleryCategoryInfoTagOption ().getGalleryCategoryInfoTag ().getName ());

      return convertView;
   }

}
