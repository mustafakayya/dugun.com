package com.dugun.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dugun.Model.LikeStatsModel;
import com.dugun.R;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class LikeStatsAdapter extends BaseAdapter {

   private Context                   context;
   private ArrayList<LikeStatsModel> likeStatList;

   public LikeStatsAdapter (Context context, ArrayList<LikeStatsModel> likeStatList) {
      this.context = context;
      this.likeStatList = likeStatList;
   }

   @Override
   public int getCount () {
      return likeStatList.size ();
   }

   @Override
   public LikeStatsModel getItem (int i) {
      return likeStatList.get (i);
   }

   @Override
   public long getItemId (int i) {
      return i;
   }

   @Override
   public View getView (int position, View convertView, ViewGroup viewGroup) {
      if (convertView == null) {
         convertView = LayoutInflater.from (this.context).
                 inflate (R.layout.list_item_like, viewGroup, false);
      }

      ImageView img  = (ImageView) convertView.findViewById (R.id.img);
      TextView  name = (TextView) convertView.findViewById (R.id.name);

      name.setText (likeStatList.get (position).getName () +
                            " (" + likeStatList.get (position).getLikeCount () + ")");

      String iName = "cat_" + likeStatList.get (position).getId ();
      int    resId = context.getResources ().getIdentifier (iName, "drawable", context.getPackageName ());
      if (resId != 0)
         img.setImageResource (resId);


      return convertView;
   }

}
