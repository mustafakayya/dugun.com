package com.dugun.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dugun.Model.MyBudgetModel;
import com.dugun.R;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class MyBudgetsAdapter extends BaseAdapter {

   private Context                  context;
   private ArrayList<MyBudgetModel> myBudgetsList;

   public MyBudgetsAdapter (Context context, ArrayList<MyBudgetModel> myBudgetsList) {
      this.context = context;
      this.myBudgetsList = myBudgetsList;
   }

   @Override
   public int getCount () {
      return myBudgetsList.size ();
   }

   @Override
   public MyBudgetModel getItem (int i) {
      return myBudgetsList.get (i);
   }

   @Override
   public long getItemId (int i) {
      return i;
   }

   @Override
   public View getView (int position, View convertView, ViewGroup viewGroup) {
      if (convertView == null) {
         convertView = LayoutInflater.from (this.context).
                 inflate (R.layout.list_item_my_budget, viewGroup, false);
      }

      TextView     name      = (TextView) convertView.findViewById (R.id.my_budget_name);
      TextView     allocated = (TextView) convertView.findViewById (R.id.my_budget_allocated);
      TextView     spent     = (TextView) convertView.findViewById (R.id.my_budget_spent);
      LinearLayout lay1      = (LinearLayout) convertView.findViewById (R.id.my_budget_lay1);
      LinearLayout lay2      = (LinearLayout) convertView.findViewById (R.id.my_budget_lay2);
      TextView     comma1    = (TextView) convertView.findViewById (R.id.comma1);
      TextView     comma2    = (TextView) convertView.findViewById (R.id.comma2);
      TextView     zero1     = (TextView) convertView.findViewById (R.id.zero1);
      TextView     zero2     = (TextView) convertView.findViewById (R.id.zero2);


      name.setText (myBudgetsList.get (position).getName ());
      allocated.setText (String.valueOf (myBudgetsList.get (position).getAllocated ()));
      spent.setText (String.valueOf (myBudgetsList.get (position).getSpent ()));

      if (position != (myBudgetsList.size () - 1)) {

         name.setTextColor (context.getResources ().getColor (R.color.green_color));
         spent.setTextColor (context.getResources ().getColor (R.color.black));
         allocated.setTextColor (context.getResources ().getColor (R.color.black));
         comma1.setTextColor (context.getResources ().getColor (R.color.black));
         comma2.setTextColor (context.getResources ().getColor (R.color.black));
         zero1.setTextColor (context.getResources ().getColor (R.color.black));
         zero2.setTextColor (context.getResources ().getColor (R.color.black));


         name.setBackgroundColor (context.getResources ().getColor (R.color.white));
         lay1.setBackgroundColor (context.getResources ().getColor (R.color.white));
         lay2.setBackgroundColor (context.getResources ().getColor (R.color.white));

      } else { // Total Item

         name.setTextColor (context.getResources ().getColor (R.color.white));
         spent.setTextColor (context.getResources ().getColor (R.color.white));
         allocated.setTextColor (context.getResources ().getColor (R.color.white));
         comma1.setTextColor (context.getResources ().getColor (R.color.white));
         comma2.setTextColor (context.getResources ().getColor (R.color.white));
         zero1.setTextColor (context.getResources ().getColor (R.color.white));
         zero2.setTextColor (context.getResources ().getColor (R.color.white));


         name.setBackgroundColor (context.getResources ().getColor (R.color.green_color));
         lay1.setBackgroundColor (context.getResources ().getColor (R.color.green_color));
         lay2.setBackgroundColor (context.getResources ().getColor (R.color.green_color));

      }

      return convertView;
   }

}
