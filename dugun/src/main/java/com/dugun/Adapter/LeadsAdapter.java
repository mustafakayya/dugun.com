package com.dugun.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dugun.Activity.NotesActivity;
import com.dugun.Activity.TodoFinishActivity;
import com.dugun.Model.LeadsModel;
import com.dugun.R;

import java.util.ArrayList;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class LeadsAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<LeadsModel> infoRequestList;
    private LeadsAdapterInterface adapterInterface;

    public LeadsAdapter(Context context, ArrayList<LeadsModel> infoRequestList,LeadsAdapterInterface adapterInterface) {
        this.context = context;
        this.infoRequestList = infoRequestList;
        this.adapterInterface = adapterInterface;
    }

    @Override
    public int getCount() {
        return infoRequestList.size();
    }

    @Override
    public LeadsModel getItem(int i) {
        return infoRequestList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).
                    inflate(R.layout.list_item_leads, viewGroup, false);
        }

        TextView name = (TextView) convertView.findViewById(R.id.name);
        LinearLayout dealLay = (LinearLayout) convertView.findViewById(R.id.dealLay);
        Button deal = (Button) convertView.findViewById(R.id.deal);
        LinearLayout detail = (LinearLayout) convertView.findViewById(R.id.my_bids_go);

        name.setText(infoRequestList.get(position).getProvider().getName());

        deal.setOnClickListener(view -> adapterInterface.onDealClicked(infoRequestList.get(position)));
        detail.setOnClickListener(view ->   adapterInterface.onDetailClicked(infoRequestList.get(position)));

        return convertView;
    }

    public interface LeadsAdapterInterface{
        void onDealClicked(LeadsModel leadsModel);
        void onDetailClicked(LeadsModel leadsModel);
    }

}
