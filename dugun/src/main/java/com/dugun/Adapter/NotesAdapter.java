package com.dugun.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dugun.Model.LeadInteractionModel;
import com.dugun.Others.Utils;
import com.dugun.R;

import java.util.ArrayList;
import java.util.List;

import static com.dugun.Api.GeneralVariables.MONTH_NAME;

/**
 * Created by mustafakaya on 12/7/17.
 */

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.BaseNoteHolder> {


    final int TitleType= 0 ;
    final int NoteType= 1 ;

    List<LeadInteractionModel> interactionModels;
    String title;

    public NotesAdapter(List<LeadInteractionModel> interactionModels,String title) {
        setInteractionModels(interactionModels,false);
        this.title = title;
    }

    public void setInteractionModels(List<LeadInteractionModel> interactionModels,boolean notify) {
        if(interactionModels == null)
            interactionModels = new ArrayList<>();
        this.interactionModels = interactionModels;
        if(notify)
            notifyDataSetChanged();


    }

    void addInteraction(LeadInteractionModel interactionModel){
        interactionModels.add(interactionModel);
        notifyItemInserted(interactionModels.size());
    }

    @Override
    public BaseNoteHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if(viewType == TitleType){
            View rowView = inflater.inflate(R.layout.listrow_notes_title,parent,false);
            return new TitleHolder(rowView);
        }else{
            View rowView = inflater.inflate(R.layout.listrow_note,parent,false);
            return new NoteHolder(rowView);
        }


    }

    @Override
    public void onBindViewHolder(BaseNoteHolder holder, int position) {
        if(holder instanceof NoteHolder){
            LeadInteractionModel model =  interactionModels.get(position-1);
            ((NoteHolder)holder).lblNote.setText(model.getComment());
            ((NoteHolder)holder).lblDate.setText("Kaydetme Tarihi : " +Utils.getDateForUser(model.getCreatedAt(),MONTH_NAME));
        }else{
            ((TitleHolder)holder).lblTitle.setText(title);
        }

    }

    @Override
    public int getItemViewType(int position) {

        return position == 0 ? TitleType : NoteType;
    }

    @Override
    public int getItemCount() {
        return interactionModels.size()+1;
    }

    class BaseNoteHolder extends RecyclerView.ViewHolder{
        public BaseNoteHolder(View itemView) {
            super(itemView);
        }
    }


    class NoteHolder extends BaseNoteHolder{

        TextView lblNote,lblDate;

        public NoteHolder(View itemView) {
            super(itemView);
            lblDate = itemView.findViewById(R.id.lblDate_noteListRow);
            lblNote = itemView.findViewById(R.id.lblNote_noteListRow);
        }
    }

    class TitleHolder extends BaseNoteHolder {

        TextView lblTitle;

        public TitleHolder(View itemView) {
            super(itemView);
            lblTitle = itemView.findViewById(R.id.lblTitle_notesTitleListRow);
        }
    }
}
