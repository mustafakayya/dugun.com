package com.dugun.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dugun.Model.ModelsModel;
import com.dugun.R;


import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ModelsAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ModelsModel> modelList;

    public ModelsAdapter(Context context, ArrayList<ModelsModel> modelList) {
        this.context = context;
        this.modelList = modelList;
    }

    @Override
    public int getCount() {
        return modelList.size();
    }

    @Override
    public ModelsModel getItem(int i) {
        return modelList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).
                    inflate(R.layout.list_item_model, viewGroup, false);
        }

        ImageView img = (ImageView) convertView.findViewById(R.id.img);
        TextView count = (TextView) convertView.findViewById(R.id.count);
        TextView name = (TextView) convertView.findViewById(R.id.name);

        Glide.with(context).load(
                modelList.get(position).getMainImage().getImageUrl().getThumbnail()).into(img);
        name.setText(modelList.get(position).getName());
        count.setText(String.valueOf(modelList.get(position).getMediaCount()));

        return convertView;
    }

}
