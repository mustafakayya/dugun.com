package com.dugun.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dugun.Model.FormOptionsModel;
import com.dugun.R;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class FormOptionsAdapter extends BaseAdapter {

   private Context                     context;
   private ArrayList<FormOptionsModel> optList;

   public FormOptionsAdapter (Context context, ArrayList<FormOptionsModel> optList) {
      this.context = context;
      this.optList = optList;
   }

   @Override
   public int getCount () {
      return optList.size ();
   }

   @Override
   public FormOptionsModel getItem (int i) {
      return optList.get (i);
   }

   @Override
   public long getItemId (int i) {
      return i;
   }

   @Override
   public View getView (final int position, View convertView, ViewGroup viewGroup) {
      if (convertView == null) {
         convertView = LayoutInflater.from (context).
                 inflate (R.layout.list_item_form_options, viewGroup, false);
      }

      TextView txt = (TextView) convertView.findViewById (R.id.optionText);
      txt.setText (optList.get (position).getOptionText ());


      return convertView;
   }

}
