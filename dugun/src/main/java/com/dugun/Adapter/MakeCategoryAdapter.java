package com.dugun.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dugun.Model.CategoryModel;
import com.dugun.R;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class MakeCategoryAdapter extends BaseAdapter {

   private Context                  context;
   private ArrayList<CategoryModel> categoryList;

   public MakeCategoryAdapter (Context context, ArrayList<CategoryModel> categoryList) {
      this.context = context;
      this.categoryList = categoryList;
   }

   @Override
   public int getCount () {
      return categoryList.size ();
   }

   @Override
   public CategoryModel getItem (int i) {
      return categoryList.get (i);
   }

   @Override
   public long getItemId (int i) {
      return i;
   }

   @Override
   public View getView (int position, View convertView, ViewGroup viewGroup) {
      if (convertView == null) {
         convertView = LayoutInflater.from (this.context).
                 inflate (R.layout.list_item_todo_category, viewGroup, false);
      }

      ImageView img  = (ImageView) convertView.findViewById (R.id.img);
      TextView  name = (TextView) convertView.findViewById (R.id.name);

//      Picasso.with (context).load (categoryList.get (position).getIcon ()).into (img);
      String iName = "cat_" + categoryList.get (position).getId ();
      int    resId = context.getResources ().getIdentifier (iName, "drawable", context.getPackageName ());
      if (resId != 0)
         img.setImageResource (resId);
      name.setText (categoryList.get (position).getName () + " (" + categoryList.get (position).getCount () + ")");

      return convertView;
   }

}
