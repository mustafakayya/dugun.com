package com.dugun.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.dugun.Interface.OpenPickerDialogCallback;
import com.dugun.Model.FilterModel;
import com.dugun.R;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class FilterAdapter extends BaseAdapter {

   private Context                  context;
   private ArrayList<FilterModel>   filterList;
   private OpenPickerDialogCallback callback;

   public FilterAdapter (Context context, ArrayList<FilterModel> filterList, OpenPickerDialogCallback callback) {
      this.context = context;
      this.filterList = filterList;
      this.callback = callback;
   }

   @Override
   public int getCount () {
      return filterList.size ();
   }

   @Override
   public FilterModel getItem (int i) {
      return filterList.get (i);
   }

   @Override
   public long getItemId (int i) {
      return i;
   }

   @Override
   public View getView (final int position, View convertView, ViewGroup viewGroup) {
      if (convertView == null) {
         convertView = LayoutInflater.from (this.context).
                 inflate (R.layout.item_filter, viewGroup, false);
      }

      ImageView    btn      = (ImageView) convertView.findViewById (R.id.btn);
      TextView     title    = (TextView) convertView.findViewById (R.id.title);
      TextView     item     = (TextView) convertView.findViewById (R.id.item);
      TextView     defTitle = (TextView) convertView.findViewById (R.id.default_title);
      LinearLayout active   = (LinearLayout) convertView.findViewById (R.id.active);
      Switch switchBtn = (Switch) convertView.findViewById (R.id.switchBtn);
      switchBtn.setVisibility (View.GONE);

//      if(position == filterList.size () -1){
//         switchBtn.setVisibility (View.VISIBLE);
//         btn.setVisibility (View.GONE);
//
//         Log.d (TAG, "getView: ");
//      }

      defTitle.setText (filterList.get (position).getFieldName ());
      title.setText (filterList.get (position).getFieldName ());

      if (filterList.get (position).getSelectedFormOption () == null) {
         active.setVisibility (View.INVISIBLE);
         btn.setImageResource (R.drawable.ic_rightarrow);
      } else {
         active.setVisibility (View.VISIBLE);
         item.setText (filterList.get (position).getSelectedFormOption ().getOptionText ());
         btn.setImageResource (R.drawable.ic_filter_close);
      }

      btn.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
               callback.openPicker (position);
         }
      });

      return convertView;
   }

}
