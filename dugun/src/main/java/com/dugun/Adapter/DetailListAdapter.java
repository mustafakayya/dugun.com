package com.dugun.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dugun.Model.DetailModel;
import com.dugun.R;

import java.util.ArrayList;



public class DetailListAdapter extends BaseAdapter {

    Activity               mActivity;
    ArrayList<DetailModel> detailList;

    public DetailListAdapter (Activity mActivity, ArrayList<DetailModel> detailList) {
        this.mActivity = mActivity;
        this.detailList = detailList;
    }



    @Override
    public int getCount() {
        return detailList.size();
    }

    @Override
    public DetailModel getItem(int i) {
        return detailList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        DetailModel item = getItem(i);
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mActivity);
            convertView = inflater.inflate(R.layout.detail_item_layout, viewGroup, false);
            viewHolder.img_icon = (ImageView) convertView.findViewById(R.id.img_icon);
            viewHolder.txt_detail_title = (TextView) convertView.findViewById(R.id.txt_detail_title);
            viewHolder.txt_detail_content = (TextView) convertView.findViewById(R.id.txt_detail_content);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if(i == 0){
            viewHolder.txt_detail_content.setTextColor(mActivity.getResources().getColor(R.color.green_color));
        }else{
            viewHolder.txt_detail_content.setTextColor(mActivity.getResources().getColor(R.color.purple_color));
        }
        viewHolder.txt_detail_title.setText(item.getText());
        viewHolder.txt_detail_content.setText(item.getDetail());
        viewHolder.img_icon.setImageDrawable(mActivity.getResources().getDrawable(item.getResID()));

        return convertView;
    }

    private static class ViewHolder {
        ImageView img_icon;
        TextView txt_detail_title, txt_detail_content;
    }

}
