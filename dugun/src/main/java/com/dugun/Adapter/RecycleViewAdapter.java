package com.dugun.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dugun.R;

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.ViewHolder> {

    public Activity activity;

    public RecycleViewAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_rozet, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

    }


    @Override
    public int getItemCount() {
        return 8;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_badge;
        public ImageView img_badge;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txt_badge = (TextView) itemLayoutView.findViewById(R.id.txt_badge);
            img_badge = (ImageView) itemLayoutView.findViewById(R.id.img_badge);
        }

    }
}