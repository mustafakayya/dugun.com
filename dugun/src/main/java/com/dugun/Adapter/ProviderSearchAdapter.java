package com.dugun.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dugun.Model.CategoryModel;
import com.dugun.R;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ProviderSearchAdapter extends BaseAdapter {

   private Context                  context;
   private ArrayList<CategoryModel> providerList;

   public ProviderSearchAdapter (Context context, ArrayList<CategoryModel> providerList) {
      this.context = context;
      this.providerList = providerList;
   }

   @Override
   public int getCount () {
      return providerList.size ();
   }

   @Override
   public CategoryModel getItem (int i) {
      return providerList.get (i);
   }

   @Override
   public long getItemId (int i) {
      return i;
   }

   @Override
   public View getView (int position, View convertView, ViewGroup viewGroup) {
      if (convertView == null) {
         convertView = LayoutInflater.from (this.context).
                 inflate (R.layout.list_item_provider_search, viewGroup, false);
      }

      ImageView img  = (ImageView) convertView.findViewById (R.id.company_img);
      TextView  text = (TextView) convertView.findViewById (R.id.company_name);

      text.setText (providerList.get (position).getName ());

      String iName = "cat_" + providerList.get (position).getProviderCategory ().getId ();
      int resId = context.getResources ().getIdentifier (iName, "drawable", context.getPackageName ());
      if (resId != 0)
         img.setImageResource (resId);


      return convertView;
   }

}
