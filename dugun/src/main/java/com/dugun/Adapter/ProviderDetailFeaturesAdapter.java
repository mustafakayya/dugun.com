package com.dugun.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dugun.Model.FormDataModel;
import com.dugun.R;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ProviderDetailFeaturesAdapter extends BaseAdapter {

   private Context   context;
   private ArrayList<FormDataModel> formDataList;

   public ProviderDetailFeaturesAdapter (Context context, ArrayList<FormDataModel> formDataList) {
      this.context = context;
      this.formDataList = formDataList;
   }

   @Override
   public int getCount () {
      return formDataList.size ();
   }

   @Override
   public FormDataModel getItem (int i) {
      return formDataList.get (i);
   }

   @Override
   public long getItemId (int i) {
      return i;
   }

   @Override
   public View getView (int position, View convertView, ViewGroup viewGroup) {
      if (convertView == null) {
         convertView = LayoutInflater.from (this.context).
                 inflate (R.layout.list_item_provider_detail_feature, viewGroup, false);
      }

      TextView title = (TextView) convertView.findViewById (R.id.title);
      title.setText (formDataList.get (position).getGroupName ());

      return convertView;
   }

}
