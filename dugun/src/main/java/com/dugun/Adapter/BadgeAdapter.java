package com.dugun.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dugun.Model.BadgeModel;
import com.dugun.R;

import java.util.List;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class BadgeAdapter extends BaseAdapter {

   private Context          context;
   private List<BadgeModel> badgeList;

   public BadgeAdapter (Context context, List<BadgeModel> badgeList) {
      this.context = context;
      this.badgeList = badgeList;
   }

   @Override
   public int getCount () {
      return badgeList.size ();
   }

   @Override
   public BadgeModel getItem (int i) {
      return badgeList.get (i);
   }

   @Override
   public long getItemId (int i) {
      return i;
   }

   @Override
   public View getView (int position, View convertView, ViewGroup viewGroup) {
      if (convertView == null) {
         convertView = LayoutInflater.from (context).
                 inflate (R.layout.list_item_rozet, viewGroup, false);
      }

      LinearLayout layout = (LinearLayout) convertView.findViewById (R.id.layout);
      TextView  name = (TextView) convertView.findViewById (R.id.txt_badge);
      ImageView img  = (ImageView) convertView.findViewById (R.id.img_badge);

      name.setText (badgeList.get (position).getName ());

      String iName = "badge_" + badgeList.get (position).getId ();
      int    resId = context.getResources ().getIdentifier (iName, "drawable",
                                                            context.getPackageName ());
      if (resId != 0)
         img.setImageResource (resId);

      if (badgeList.get (position).getEarned ())
         layout.setAlpha (1f);
      else {
         layout.setAlpha (0.4f);
      }

      return convertView;
   }

}
