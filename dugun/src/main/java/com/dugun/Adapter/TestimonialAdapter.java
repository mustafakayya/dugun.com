package com.dugun.Adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dugun.Model.TestimonialModel;
import com.dugun.Others.Utils;
import com.dugun.R;

import java.util.ArrayList;

import static com.dugun.Api.GeneralVariables.MONTH_NAME;


/**
 * Created by mustafakaya on 12/2/17.
 */

public class TestimonialAdapter extends RecyclerView.Adapter<TestimonialAdapter.TestimonialHolder> {

    ArrayList<TestimonialModel> testimonialModels;

    public TestimonialAdapter(ArrayList<TestimonialModel> testimonialModels) {
        if(testimonialModels == null )
            testimonialModels = new ArrayList<>();
        this.testimonialModels = testimonialModels;
    }

    @Override
    public TestimonialHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View rowView = inflater.inflate(R.layout.listrow_testimonial,parent,false);
        TestimonialHolder holder = new TestimonialHolder(rowView);
        return holder;
    }

    @Override
    public void onBindViewHolder(TestimonialHolder holder, int position) {
        holder.setViews(testimonialModels.get(position));
    }

    @Override
    public int getItemCount() {
        return testimonialModels.size();
    }

    static class TestimonialHolder extends RecyclerView.ViewHolder{

        TextView lblAvatar,lblCoupleName,lblDate,lblReview;

        public TestimonialHolder(View itemLayoutView) {
            super(itemLayoutView);
            lblAvatar = itemLayoutView.findViewById(R.id.lblAvatar_testimonialList);
            lblCoupleName = itemLayoutView.findViewById(R.id.lblCoupleName_testimonialList);
            lblDate = itemLayoutView.findViewById(R.id.lblDate_testimonialList);
            lblReview = itemLayoutView.findViewById(R.id.lblReview_testimonialList);
        }

        void setViews(TestimonialModel testimonialModel){
            lblAvatar.setText(testimonialModel.getAvatarName());
            lblCoupleName.setText(testimonialModel.getCoupleName());
            lblReview.setText(testimonialModel.getTestimonial());
            if(!TextUtils.isEmpty(testimonialModel.getOrganizationDate()))
                lblDate.setText("Dugun Tarihi : " + Utils.getDateForUser(testimonialModel.getOrganizationDate(),MONTH_NAME));
            else
                lblDate.setText("Yorum Tarihi : " +Utils.getDateForUser(testimonialModel.getCreatedAt(),MONTH_NAME));
        }

    }
}
