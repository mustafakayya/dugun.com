package com.dugun.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dugun.Activity.LeadsListActivity;
import com.dugun.Activity.LikeTabbedActivity;
import com.dugun.Activity.ModelsActivity;
import com.dugun.Activity.ProvidersActivity;
import com.dugun.Activity.TodoListActivity;
import com.dugun.Model.CategoryModel;
import com.dugun.R;

import java.util.List;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class MainCategoryAdapter extends BaseAdapter {

   Activity            activity;
   List<CategoryModel> categoryList;

   public MainCategoryAdapter (Activity activity, List<CategoryModel> categoryList) {
      this.activity = activity;
      this.categoryList = categoryList;
   }

   @Override
   public int getCount () {
      return categoryList.size ();
   }

   @Override
   public CategoryModel getItem (int i) {
      return categoryList.get (i);
   }

   @Override
   public long getItemId (int i) {
      return i;
   }

   @Override
   public View getView (final int position, View convertView, ViewGroup viewGroup) {
      if (convertView == null) {
         convertView = LayoutInflater.from (this.activity).
                 inflate (R.layout.list_item_main_category, viewGroup, false);
      }

      LinearLayout models    = (LinearLayout) convertView.findViewById (R.id.child_item_models);
      LinearLayout providers = (LinearLayout) convertView.findViewById (R.id.child_item_providers);
      LinearLayout favorites = (LinearLayout) convertView.findViewById (R.id.child_item_favorites);
      LinearLayout leads     = (LinearLayout) convertView.findViewById (R.id.child_item_my_leads);
      LinearLayout plans     = (LinearLayout) convertView.findViewById (R.id.child_item_my_plans);


      if (categoryList.get (position).isHasGallery ())
         models.setVisibility (View.VISIBLE);
      else
         models.setVisibility (View.GONE);

      models.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            Intent i = new Intent (activity, ModelsActivity.class);
            i.putExtra ("providerCategoryId",
                        categoryList.get (position).getProviderCategory ().id);
            i.putExtra ("categoryName", categoryList.get (position).getName ());
            activity.startActivity (i);
         }
      });

      providers.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            Intent i = new Intent (activity, ProvidersActivity.class);
            i.putExtra ("category", categoryList.get (position));
            activity.startActivity (i);
         }
      });

      favorites.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            Intent i = new Intent (activity, LikeTabbedActivity.class);
            i.putExtra ("category", categoryList.get (position));
            i.putExtra ("from", 1);
            activity.startActivity (i);
         }
      });

      leads.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            Intent i = new Intent (activity, LeadsListActivity.class);
            i.putExtra ("category", categoryList.get (position));
            activity.startActivity (i);
         }
      });

      plans.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            Intent i = new Intent (activity, TodoListActivity.class);
            i.putExtra ("category", categoryList.get (position));
            activity.startActivity (i);
         }
      });


      ImageView      plus  = (ImageView) convertView.findViewById (R.id.img_plus);
      FrameLayout container  = (FrameLayout) convertView.findViewById (R.id.header_container);
      final CardView child = (CardView) convertView.findViewById (R.id.child);


      container.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            if (categoryList.get (position).isOpen ()) {
               for (int i = 0; i < categoryList.size (); i++) {
                  categoryList.get (i).setOpen (false);
               }
            } else {
               for (int i = 0; i < categoryList.size (); i++) {
                  categoryList.get (i).setOpen (false);
               }
               categoryList.get (position).setOpen (true);
            }
            notifyDataSetChanged ();
         }
      });

      if (categoryList.get (position).isOpen ())
         child.setVisibility (View.VISIBLE);
      else
         child.setVisibility (View.GONE);


      ImageView img_category_icon = (ImageView) convertView.findViewById (R.id.img_category_icon);
      ImageView img_plus          = (ImageView) convertView.findViewById (R.id.img_plus);
      TextView  txt_category_name = (TextView) convertView.findViewById (R.id.txt_category_name);

      txt_category_name.setText (categoryList.get (position).getName ());
      if (child.getVisibility () == View.VISIBLE) {
         img_category_icon.setImageResource (R.drawable.ic_uncheckedcategory);
         int padding = (int) activity.getResources ().getDimension (R.dimen.check_icon_padding);
         img_category_icon.setPadding (padding, padding, padding, padding);
         img_plus.setImageResource (R.drawable.minus);
         txt_category_name.setTextColor (activity.getResources ().getColor (R.color.green_color));
      } else {
         String iName    = "cat_" + categoryList.get (position).getProviderCategory ().getId ();
         int    resId    = activity.getResources ().getIdentifier (iName, "drawable", activity.getPackageName ());
         if (resId != 0)
            img_category_icon.setImageResource (resId);
//         Picasso.with (activity).load (URL_TYPE + categoryList.get (position).getIcon ()).into (img_category_icon);
         img_category_icon.setPadding (0, 0, 0, 0);
         img_plus.setImageResource (R.drawable.plus);
         txt_category_name.setTextColor (
                 activity.getResources ().getColor (R.color.dugun_text_black));
      }


      return convertView;
   }

}
