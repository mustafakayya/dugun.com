package com.dugun.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dugun.Model.CategoryModel;
import com.dugun.R;

import java.util.List;


public class OrganizationAdapter extends BaseAdapter {

   private Context             context;
   private List<CategoryModel> categoryList;

   public OrganizationAdapter (Context context, List<CategoryModel> categoryList) {
      this.context = context;
      this.categoryList = categoryList;
   }

   @Override
   public int getCount () {
      return categoryList.size ();
   }

   @Override
   public CategoryModel getItem (int i) {
      return categoryList.get (i);
   }

   @Override
   public long getItemId (int i) {
      return i;
   }

   @Override
   public View getView (int position, View convertView, ViewGroup viewGroup) {
      CategoryModel category = getItem (position);
      ViewHolder    viewHolder; // view lookup cache stored in tag
      if (convertView == null) {
         viewHolder = new ViewHolder ();
         LayoutInflater inflater = LayoutInflater.from (context);
         convertView = inflater.inflate (R.layout.layout_item_organization, viewGroup, false);
         viewHolder.name = (TextView) convertView.findViewById (R.id.txt_organization_name);
         viewHolder.img_check = (ImageView) convertView.findViewById (R.id.img_check);
         convertView.setTag (viewHolder);
      } else {
         viewHolder = (ViewHolder) convertView.getTag ();
      }

      viewHolder.name.setText (category.getName ());

      if (category.isDefault ()) {
         convertView.setBackgroundColor (
                 context.getResources ().getColor (R.color.light_purple_color));
         viewHolder.name.setTextColor (context.getResources ().getColor (R.color.purple_color));
         viewHolder.img_check.setImageResource (R.drawable.ic_checkedmini);

      } else {
         convertView.setBackgroundColor (context.getResources ().getColor (R.color.transparent));
         viewHolder.img_check.setImageResource (R.drawable.ic_unchecked);
         viewHolder.name.setTextColor (context.getResources ().getColor (R.color.dugun_text_black));
      }

      return convertView;
   }

   private static class ViewHolder {

      TextView  name;
      ImageView img_check;
   }

}
