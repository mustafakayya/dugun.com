package com.dugun.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dugun.Activity.ProviderDetailActivity;
import com.dugun.Interface.SetCompletedCallback;
import com.dugun.Model.TodoModel;
import com.dugun.R;


import java.util.ArrayList;

import static com.dugun.Activity.TodoListActivity.isCompleted;
import static com.dugun.Api.GeneralVariables.PLAN_DATE;
import static com.dugun.Api.GeneralVariables.SIMPLE_DATE;
import static com.dugun.Api.GeneralVariables.TIME;
import static com.dugun.Others.Utils.getDateForUser;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class TodoListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<TodoModel> todoList;
    private SetCompletedCallback callback;
    private String TAG = "TodoListAdapter";

    public TodoListAdapter(Context context, ArrayList<TodoModel> todoList, SetCompletedCallback callback) {
        this.context = context;
        this.todoList = todoList;
        this.callback = callback;
    }

    @Override
    public int getCount() {
        return todoList.size();
    }

    @Override
    public TodoModel getItem(int i) {
        return todoList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.list_item_todos, viewGroup, false);

        }

        LinearLayout completedLay = (LinearLayout) convertView.findViewById(R.id.completed_lay);
        LinearLayout uncompletedLay = (LinearLayout) convertView.findViewById(R.id.uncompleted_lay);

        if (isCompleted) {
            uncompletedLay.setVisibility(View.GONE);
            completedLay.setVisibility(View.VISIBLE);

            ImageView img = (ImageView) convertView.findViewById(R.id.check2);
            TextView title = (TextView) convertView.findViewById(R.id.title2);
            TextView date = (TextView) convertView.findViewById(R.id.date2);

            title.setText(todoList.get(position).getTitle());
            date.setText(getDateForUser(todoList.get(position).getCompletedAt(), SIMPLE_DATE));

            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.setCompletedCallback(position);
                }
            });

        } else {

            uncompletedLay.setVisibility(View.VISIBLE);
            completedLay.setVisibility(View.GONE);

            ImageView img = (ImageView) convertView.findViewById(R.id.check1);
            TextView title = (TextView) convertView.findViewById(R.id.title1);
            TextView content = (TextView) convertView.findViewById(R.id.content1);
            TextView date = (TextView) convertView.findViewById(R.id.date1);
            TextView time = (TextView) convertView.findViewById(R.id.time1);
            LinearLayout notiLay = (LinearLayout) convertView.findViewById(R.id.noti_layout1);
            LinearLayout goBtn = (LinearLayout) convertView.findViewById(R.id.go_btn);
            ImageView goBtnImg = (ImageView) convertView.findViewById(R.id.go_btn_img);

            if (todoList.get(position).getNotifyAt() != null) {
                notiLay.setVisibility(View.VISIBLE);
                date.setText(getDateForUser(todoList.get(position).getNotifyAt(), PLAN_DATE));
                time.setText(getDateForUser(todoList.get(position).getNotifyAt(), TIME));
            } else {
                notiLay.setVisibility(View.GONE);
            }

            if (todoList.get(position).getActionType() == null || todoList.get(position).getActionType().equals("")) {
                goBtn.setVisibility(View.GONE);
            } else {
                goBtn.setVisibility(View.VISIBLE);
                Glide.with(context).load(R.drawable.camera_icon).into(goBtnImg);
                goBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(TAG, "onClick: goBtn clicked.");
                        Intent i;
                        switch (todoList.get(position).getActionType()) {
                            case "provider":
                                i = new Intent(context, ProviderDetailActivity.class);
                                i.putExtra("providerId", todoList.get(position).getActionItemId());
                                context.startActivity(new Intent(context, ProviderDetailActivity.class));
                                break;
                            case "gallery":
                                i = new Intent(context, ProviderDetailActivity.class);
                                i.putExtra("providerId", todoList.get(position).getActionItemId());
                                context.startActivity(new Intent(context, ProviderDetailActivity.class));
                                break;
                            case "webView":
                                context.startActivity(new Intent(Intent.ACTION_VIEW).setData(
                                        Uri.parse(todoList.get(position).getWebViewUrl())));
                                break;
                        }
                    }
                });
            }

            if (todoList.get(position).getCompletedAt() != null) {
                img.setImageResource(R.drawable.ic_checkedmekan);
            } else {
                img.setImageResource(R.drawable.ic_uncheckedcategory);
            }

            title.setText(todoList.get(position).getTitle());
            content.setText(todoList.get(position).getDescription());

            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.setCompletedCallback(position);
                }
            });
        }


        return convertView;
    }


}
