package com.dugun.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dugun.Model.ServiceContentModel;
import com.dugun.R;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ServiceContentAdapter extends BaseAdapter {

   private Context                        context;
   private ArrayList<ServiceContentModel> serviceContentList;

   public ServiceContentAdapter (Context context, ArrayList<ServiceContentModel> serviceContentList) {
      this.context = context;
      this.serviceContentList = serviceContentList;
   }

   @Override
   public int getCount () {
      return serviceContentList.size ();
   }

   @Override
   public ServiceContentModel getItem (int i) {
      return serviceContentList.get (i);
   }

   @Override
   public long getItemId (int i) {
      return i;
   }

   @Override
   public View getView (int position, View convertView, ViewGroup viewGroup) {
      if (convertView == null) {
         convertView = LayoutInflater.from (this.context).
                 inflate (R.layout.list_item_service_content, viewGroup, false);
      }

      ImageView status = (ImageView) convertView.findViewById (R.id.status);
      TextView  name   = (TextView) convertView.findViewById (R.id.name);

      name.setText (serviceContentList.get (position).getName ());
      if (serviceContentList.get (position).getStatus ())
         status.setImageResource (R.drawable.ic_page1);
      else
         status.setImageResource (R.drawable.ic_popupclose);

      return convertView;
   }

}
