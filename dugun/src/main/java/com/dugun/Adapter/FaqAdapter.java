package com.dugun.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dugun.Model.FaqModel;
import com.dugun.R;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class FaqAdapter extends BaseAdapter {

   private Context             context;
   private ArrayList<FaqModel> faqList;

   public FaqAdapter (Context context, ArrayList<FaqModel> faqList) {
      this.context = context;
      this.faqList = faqList;
   }

   @Override
   public int getCount () {
      return faqList.size ();
   }

   @Override
   public FaqModel getItem (int i) {
      return faqList.get (i);
   }

   @Override
   public long getItemId (int i) {
      return i;
   }

   @Override
   public View getView (final int position, View convertView, ViewGroup viewGroup) {
      if (convertView == null) {
         convertView = LayoutInflater.from (context).
                 inflate (R.layout.list_item_faq, viewGroup, false);
      }

      final TextView question = (TextView) convertView.findViewById (R.id.faq_question);
      final TextView answer = (TextView) convertView.findViewById (R.id.faq_answer);
      final ImageView show_img = (ImageView) convertView.findViewById (R.id.faq_show_img);
      final LinearLayout show = (LinearLayout) convertView.findViewById (R.id.faq_show);

      question.setText (faqList.get (position).getQuestion ());
      answer.setText (faqList.get (position).getAnswer ());


      show.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            if (answer.getVisibility () == View.GONE) {
               show_img.setImageResource (R.drawable.minus);
               answer.setVisibility (View.VISIBLE);
            } else {
               show_img.setImageResource (R.drawable.plus);
               answer.setVisibility (View.GONE);
            }
         }
      });


      return convertView;
   }

}
