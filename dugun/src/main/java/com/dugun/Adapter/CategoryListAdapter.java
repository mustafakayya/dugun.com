package com.dugun.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dugun.Model.CategoryModel;
import com.dugun.R;

import java.util.ArrayList;


/**
 * Created by ugurbasarir on 30.06.2017.
 */
public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.CategoryHolder> {

   private Context                  context;
   private ArrayList<CategoryModel> categories;
   private OnAddClickListener       addClickListener;
   private OnItemClickListener      itemClickListener;

   public CategoryListAdapter (Context context, ArrayList<CategoryModel> categories, OnAddClickListener addClickListener, OnItemClickListener itemClickListener) {
      this.context = context;
      if (categories == null)
         categories = new ArrayList<> ();
      this.categories = categories;
      this.addClickListener = addClickListener;
      this.itemClickListener = itemClickListener;
   }

   @Override
   public CategoryHolder onCreateViewHolder (ViewGroup parent, int viewType) {
      View v = LayoutInflater.from (parent.getContext ())
              .inflate (R.layout.list_item_category, parent, false);
      CategoryHolder ch = new CategoryHolder (v);
      return ch;
   }

   @Override
   public void onBindViewHolder (CategoryHolder holder, final int position) {

      final CategoryModel currentCategories = categories.get (position);

      if (categories.get (position).isSelected ()) {
         holder.mainLayout.setPadding (0, 0, 0, 30);
         holder.addBtnText.setText ("ÇIKART");
         holder.addBtnText.setTextColor (Color.parseColor ("#B9B9B9"));
      } else {
         holder.mainLayout.setPadding (0, 0, 0, 0);
         holder.addBtnText.setText ("EKLE");
         holder.addBtnText.setTextColor (Color.parseColor ("#E96398"));
      }

      holder.setCategory (categories.get (position));
      holder.addBtn.setOnClickListener (new View.OnClickListener () {
         @Override
         public void onClick (View v) {
            addClickListener.addClickListener (currentCategories);
         }
      });
      holder.v.setOnClickListener (new View.OnClickListener () {
         @Override
         public void onClick (View v) {
            itemClickListener.OnItemClick (v, currentCategories);
         }
      });

      String iName = "cat_" + categories.get (position).getProviderCategory ().getId ();
      int resId = context.getResources ().getIdentifier (iName, "drawable",
                                                         context.getPackageName ());
      if (resId != 0)
         holder.img.setImageResource (resId);
   }

   @Override
   public long getItemId (int position) {
      return position;
   }

   @Override
   public int getItemCount () {
      return categories.size ();
   }


   public class CategoryHolder extends RecyclerView.ViewHolder {

      private CategoryModel category;
      private LinearLayout  addBtn, mainLayout;
      private TextView name, addBtnText;
      private ImageView img;
      private View      v;

      public CategoryHolder (View v) {
         super (v);
         this.v = v;
         name = (TextView) v.findViewById (R.id.category_title);
         img = (ImageView) v.findViewById (R.id.category_img);
         addBtn = (LinearLayout) v.findViewById (R.id.category_add_btn);
         addBtnText = (TextView) v.findViewById (R.id.category_add_btn_txt);
         mainLayout = (LinearLayout) v.findViewById (R.id.main_layout_category);
      }

      public CategoryModel getCategory () {
         return category;
      }

      public void setCategory (CategoryModel category) {
         this.category = category;
         name.setText (category.getName ());

//         Picasso.with (context).load (URL_TYPE + category.getIcon ()).into (img);
      }
   }


   public interface OnAddClickListener {

      void addClickListener (CategoryModel category);
   }

   public interface OnItemClickListener {

      void OnItemClick (View v, CategoryModel category);
   }
}
