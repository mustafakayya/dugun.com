package com.dugun.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dugun.Model.ConceptModel;
import com.dugun.Model.FormOptionsModel;
import com.dugun.R;


/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ConceptAdapter extends BaseAdapter {

    private Context context;
    private ConceptModel concept;

    public ConceptAdapter(Context context, ConceptModel concept) {
        this.context = context;
        this.concept = concept;
    }

    @Override
    public int getCount() {
        return concept.getFormOptions().size();
    }

    @Override
    public FormOptionsModel getItem(int i) {
        return concept.getFormOptions().get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).
                    inflate(R.layout.list_item_concept, viewGroup, false);
        }

        ImageView img = (ImageView) convertView.findViewById(R.id.concept_item_img);
        ImageView status = (ImageView) convertView.findViewById(R.id.concept_item_status);
        TextView name = (TextView) convertView.findViewById(R.id.concept_item_name);

        name.setText(concept.getFormOptions().get(position).getOptionText());

        String path = concept.getFormOptions().get(position).getImageUrl();
        if (!concept.getFormOptions().get(position).getImageUrl().startsWith("http:"))
            path = "https:" + path;
        Glide.with(context).load(path).into(img);

        if (concept.getFormOptions().get(position).isSelected())
            status.setImageResource(R.drawable.ic_checkedmekan);
        else
            status.setImageResource(R.drawable.ic_uncheckedcategory);

        return convertView;
    }

}
