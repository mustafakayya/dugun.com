package com.dugun.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dugun.Interface.LikeCallback;
import com.dugun.Model.ProviderModel2;
import com.dugun.R;


import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ProviderListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ProviderModel2> providerList;
    private LikeCallback callback;

    public ProviderListAdapter(Context context, ArrayList<ProviderModel2> providerList, LikeCallback callback) {
        this.context = context;
        if(providerList==null)
            providerList = new ArrayList<>();
        this.providerList = providerList;
        this.callback = callback;
    }

    @Override
    public int getCount() {
        return providerList.size();
    }

    @Override
    public ProviderModel2 getItem(int i) {
        return providerList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).
                    inflate(R.layout.list_item_provider, viewGroup, false);
        }

        ImageView img = (ImageView) convertView.findViewById(R.id.company_img);
        TextView name = (TextView) convertView.findViewById(R.id.company_name);
        ImageView isLiked = (ImageView) convertView.findViewById(R.id.isLiked);
        LinearLayout videoLay = (LinearLayout) convertView.findViewById(R.id.video_lay);
        TextView videoCount = (TextView) convertView.findViewById(R.id.video_count);
        LinearLayout imgLay = (LinearLayout) convertView.findViewById(R.id.img_lay);
        LinearLayout commentLay = (LinearLayout) convertView.findViewById(R.id.comment_lay);
        TextView imgCount = (TextView) convertView.findViewById(R.id.img_count);
        TextView commentCount = (TextView) convertView.findViewById(R.id.comment_count);
        ImageView ivDiscount = convertView.findViewById(R.id.ivDiscount);

        ProviderModel2 provider = providerList.get(position);

        Glide.with(context).load(provider.getImageUrl()).into(img);
        name.setText(providerList.get(position).getName());

        if (provider.isLiked())
            isLiked.setImageResource(R.drawable.ic_favorieklebig2);
        else {
            isLiked.setImageResource(R.drawable.ic_favorieklebig1);
        }

        ivDiscount.setVisibility(provider.getDiscountCount() > 0 ? View.VISIBLE : View.GONE);

        videoCount.setText(String.valueOf(provider.getVideoCount()));
        imgCount.setText(String.valueOf(provider.getImageCount()));
        commentCount.setText(String.valueOf(provider.getReviewCount()));

        videoLay.setVisibility(provider.getVideoCount() > 0 ? View.VISIBLE : View.INVISIBLE);
        imgLay.setVisibility(provider.getImageCount() > 0 ? View.VISIBLE : View.INVISIBLE);
        commentLay.setVisibility(provider.getReviewCount() > 0 ? View.VISIBLE : View.INVISIBLE);



        isLiked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.setLike(position);
            }
        });

        return convertView;
    }

}
