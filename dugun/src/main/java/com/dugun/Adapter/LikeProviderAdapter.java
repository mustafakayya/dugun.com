package com.dugun.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dugun.Activity.ProviderDetailActivity;
import com.dugun.Interface.LikeCallback;
import com.dugun.Model.LikeProviderModel;
import com.dugun.R;


import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class LikeProviderAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<LikeProviderModel> providerList;
    private LikeCallback callback;

    public LikeProviderAdapter(Context context, ArrayList<LikeProviderModel> providerList,LikeCallback callback) {
        this.context = context;
        this.providerList = providerList;
        this.callback = callback;
    }

    @Override
    public int getCount() {
        return providerList.size();
    }

    @Override
    public LikeProviderModel getItem(int i) {
        return providerList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).
                    inflate(R.layout.list_item_liked_provider, viewGroup, false);
        }

        LinearLayout layout = (LinearLayout) convertView.findViewById(R.id.layout);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        ImageView img = (ImageView) convertView.findViewById(R.id.img);
        ImageView liked = (ImageView) convertView.findViewById(R.id.isLiked);

        liked.setImageResource(providerList.get(position).isLiked() ? R.drawable.ic_favorieklebig2 : R.drawable.ic_favorieklebig1);

        liked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.setLike(position);
            }
        });


        name.setText(providerList.get(position).getProvider().getName());
        Glide.with(context).load(
                providerList.get(position).getProvider().getProviderImage()).into(img);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProviderDetailActivity.class);
                intent.putExtra("providerId", providerList.get(position).getProvider().getId());
                intent.putExtra("categoryId", providerList.get(position).getProvider().getCategory().getId());
                intent.putExtra("cityId", providerList.get(position).getProvider().getCity().getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        return convertView;
    }

}
