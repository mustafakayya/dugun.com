package com.dugun.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dugun.Model.FormDataInnerData;
import com.dugun.Model.FormDataModel;
import com.dugun.R;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ProviderDetailGeneralAdapter extends BaseAdapter {

   private Context       context;
   private FormDataModel formData;

   public ProviderDetailGeneralAdapter (Context context, FormDataModel formData) {
      this.context = context;
      this.formData = formData;
   }

   @Override
   public int getCount () {
      return formData.getData ().size ();
   }

   @Override
   public FormDataInnerData getItem (int i) {
      return formData.getData ().get (i);
   }

   @Override
   public long getItemId (int i) {
      return i;
   }

   @Override
   public View getView (int position, View convertView, ViewGroup viewGroup) {
      if (convertView == null) {
         convertView = LayoutInflater.from (this.context).
                 inflate (R.layout.item_provider_detail_general, viewGroup, false);
      }

      TextView lbl = (TextView) convertView.findViewById (R.id.lbl);
      TextView txt = (TextView) convertView.findViewById (R.id.txt);

      lbl.setText (formData.getData ().get (position).getLabel ());
      txt.setText (formData.getData ().get (position).getValue ().get (0).getText ());

      return convertView;
   }

}
