package com.dugun.Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dugun.Interface.OpenPickerDialogCallback;
import com.dugun.Model.ProviderInfoRequestFormsModel;
import com.dugun.Others.Utils;
import com.dugun.R;

import java.util.ArrayList;

import static com.dugun.Api.GeneralVariables.MONTH_NAME;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class LeadsNewInfoRequestFormsAdapter extends BaseAdapter  {

    private Context context;
    private ArrayList<ProviderInfoRequestFormsModel> formDataList;
    private OpenPickerDialogCallback callback;

    public LeadsNewInfoRequestFormsAdapter(Context context, ArrayList<ProviderInfoRequestFormsModel> formDataList, OpenPickerDialogCallback callback) {
        this.context = context;
        this.formDataList = formDataList;
        this.callback = callback;
    }

    @Override
    public int getCount() {
        return formDataList.size();
    }

    @Override
    public ProviderInfoRequestFormsModel getItem(int i) {
        return formDataList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).
                    inflate(R.layout.list_item_leads_new_info_request_forms, viewGroup, false);
        }

        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView value = (TextView) convertView.findViewById(R.id.value);

        LinearLayout choose = (LinearLayout) convertView.findViewById(R.id.choose);

        title.setText(formDataList.get(position).getFieldLabel());

        if (formDataList.get(position).getSelectedFormOption() != null)
            value.setText(formDataList.get(position).getSelectedFormOption().getOptionText());
        else if (formDataList.get(position).getFieldDataType().equalsIgnoreCase("date"))
            value.setText(formDataList.get(position).getSelectedDate() !=null ? Utils.getDateForUser(formDataList.get(position).getSelectedDate(), MONTH_NAME) : "");
        else if(  formDataList.get(position).getInfoRequestFormOptions()!=null && formDataList.get(position).getInfoRequestFormOptions().size() > 0)
            value.setText(
                    formDataList.get(position).getInfoRequestFormOptions().get(0).getOptionText());
        else if(formDataList.get(position).getTextValue()!=null){
            value.setText(
                    formDataList.get(position).getTextValue());
        }else{
            value.setText("");
        }

        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.openPicker(position);
            }
        });

        return convertView;
    }

}


