package com.dugun.Adapter;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dugun.Model.FormDataInnerData;
import com.dugun.R;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ProviderDetailFeatureAdapter extends BaseAdapter {

   private Context                      context;
   private ArrayList<FormDataInnerData> dataList;
   private int type;

   public ProviderDetailFeatureAdapter (Context context, ArrayList<FormDataInnerData> dataList, int type) {
      this.context = context;
      this.dataList = dataList;
      this.type = type;
   }

   @Override
   public int getCount () {
      return dataList.size ();
   }

   @Override
   public FormDataInnerData getItem (int i) {
      return dataList.get (i);
   }

   @Override
   public long getItemId (int i) {
      return i;
   }

   @Override
   public View getView (int position, View convertView, ViewGroup viewGroup) {
      if (convertView == null) {
         if(type == 1 || type == 3){
            convertView = LayoutInflater.from (context).
                    inflate (R.layout.list_item_provider_detail_feature_type_1, viewGroup, false);
         }else if(type == 2){
            convertView = LayoutInflater.from (context).
                    inflate (R.layout.list_item_provider_detail_feature_type_2, viewGroup, false);
         }

      }

      if(type == 1){
         TextView title = (TextView) convertView.findViewById (R.id.title);
         TextView content = (TextView) convertView.findViewById (R.id.content);

         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            title.setText (Html.fromHtml(dataList.get (position).getLabel (), Html.FROM_HTML_MODE_COMPACT));
            content.setText (Html.fromHtml(dataList.get (position).getValue ().get (0).getText (), Html.FROM_HTML_MODE_COMPACT));
         }else{
            title.setText (Html.fromHtml(dataList.get (position).getLabel ()));
            content.setText (Html.fromHtml(dataList.get (position).getValue ().get (0).getText ()));
         }
      }else if (type == 2){
         ImageView status = (ImageView) convertView.findViewById (R.id.status);
         TextView  content = (TextView) convertView.findViewById (R.id.content);

         if(dataList.get (position).getValue ().get (0).getText ().equals ("Evet")){
            status.setImageResource (R.drawable.ic_checkedmini);
         }else{
            status.setImageResource (R.drawable.ic_unchecked);
         }

         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            content.setText (Html.fromHtml(dataList.get (position).getLabel (), Html.FROM_HTML_MODE_COMPACT));
         }else{
            content.setText (Html.fromHtml(dataList.get (position).getLabel ()));
         }

      }else if (type == 3){
         TextView title = (TextView) convertView.findViewById (R.id.title);
         TextView content = (TextView) convertView.findViewById (R.id.content);

            title.setText(Html.fromHtml(dataList.get (position).getLabel ()));
            String str = "";
            for (int i = 0; i < dataList.get (position).getValue ().size (); i++) {
               str = str + dataList.get (position).getValue ().get (i).getText () + ", ";
            }
            content.setText (str.substring (0, str.length () - 2));

      }





      return convertView;
   }

}
