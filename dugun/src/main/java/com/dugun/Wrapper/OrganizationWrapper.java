package com.dugun.Wrapper;

import com.dugun.Model.Organization;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrganizationWrapper {

    @SerializedName("data")
    @Expose
    private List<Organization> organization = null;

    public List<Organization> getOrganization() {
        return organization;
    }

    public void setOrganization(List<Organization> organization) {
        this.organization = organization;
    }

}