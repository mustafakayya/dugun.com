package com.dugun.Wrapper;

import com.dugun.Model.FilterModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FilterWrapper {

   @SerializedName ("data")
   @Expose
   private ArrayList<FilterModel> filters = null;

   public ArrayList<FilterModel> getFilters () {
      return filters;
   }

   public void setFilters (ArrayList<FilterModel> filters) {
      this.filters = filters;
   }
}