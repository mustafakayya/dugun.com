package com.dugun.Wrapper;

import com.dugun.Model.FormDataModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FormDataWrapper {

   @SerializedName ("data")
   private ArrayList<FormDataModel> formDataList = null;

   public ArrayList<FormDataModel> getFormDataList () {
      return formDataList;
   }

   public void setFormDataList (ArrayList<FormDataModel> formDataList) {
      this.formDataList = formDataList;
   }
}