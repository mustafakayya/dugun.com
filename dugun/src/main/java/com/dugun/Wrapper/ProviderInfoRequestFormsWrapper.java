package com.dugun.Wrapper;

import com.dugun.Model.ProviderInfoRequestFormsModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProviderInfoRequestFormsWrapper {

   @SerializedName ("data")
   private ArrayList<ProviderInfoRequestFormsModel> infoRequestFormList = null;

   public ArrayList<ProviderInfoRequestFormsModel> getInfoRequestFormList () {
      return infoRequestFormList;
   }

   public void setInfoRequestFormList (ArrayList<ProviderInfoRequestFormsModel> infoRequestFormList) {
      this.infoRequestFormList = infoRequestFormList;
   }
}