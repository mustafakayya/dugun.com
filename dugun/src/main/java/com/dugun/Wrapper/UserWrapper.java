package com.dugun.Wrapper;

import com.dugun.Model.AccessTokenModel;
import com.dugun.Model.UserModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserWrapper {

   @SerializedName ("apiVersion")
   @Expose
   private String apiVersion;

   @SerializedName ("accessToken")
   @Expose
   private AccessTokenModel accessToken;

   @SerializedName ("user")
   @Expose
   private UserModel user;

   public String getApiVersion () {
      return apiVersion;
   }

   public void setApiVersion (String apiVersion) {
      this.apiVersion = apiVersion;
   }

   public AccessTokenModel getAccessToken () {
      return accessToken;
   }

   public void setAccessToken (AccessTokenModel accessToken) {
      this.accessToken = accessToken;
   }

   public UserModel getUser () {
      return user;
   }

   public void setUser (UserModel user) {
      this.user = user;
   }
}