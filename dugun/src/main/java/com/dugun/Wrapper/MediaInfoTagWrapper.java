package com.dugun.Wrapper;

import com.dugun.Model.MediaInfoTagModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MediaInfoTagWrapper {

   @SerializedName ("mediaInfoTags")
   @Expose
   private List<MediaInfoTagModel> mediaInfoTags = null;

   public List<MediaInfoTagModel> getMediaInfoTags () {
      return mediaInfoTags;
   }

   public void setMediaInfoTags (List<MediaInfoTagModel> mediaInfoTags) {
      this.mediaInfoTags = mediaInfoTags;
   }
}