package com.dugun.Wrapper;

import com.dugun.Model.ModelGalleryModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ModelGalleryModelWrapper {

   @SerializedName ("data")
   @Expose
   private ArrayList<ModelGalleryModel> models = null;

   public ArrayList<ModelGalleryModel> getModels () {
      return models;
   }

   public void setModels (ArrayList<ModelGalleryModel> models) {
      this.models = models;
   }
}