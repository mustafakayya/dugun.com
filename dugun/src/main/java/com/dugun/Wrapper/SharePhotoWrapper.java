package com.dugun.Wrapper;

import com.dugun.Model.SharePhotoModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mustafakaya on 1/27/18.
 */

public class SharePhotoWrapper {

    @SerializedName("data")
    SharePhotoModel data;

    @SerializedName("message")
    String message;

    public SharePhotoModel getData() {
        return data;
    }

    public void setData(SharePhotoModel data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
