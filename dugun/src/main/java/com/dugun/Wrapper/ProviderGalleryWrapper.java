package com.dugun.Wrapper;

import com.dugun.Model.ProviderGalleryModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProviderGalleryWrapper {

   @SerializedName ("meta")
   private Meta meta;

   public class Meta {

      @SerializedName ("total")
      private int total;

      @SerializedName ("perPage")
      private int perPage;

      @SerializedName ("currentPage")
      private int currentPage;

      @SerializedName ("lastPage")
      private int lastPage;

      public int getTotal () {
         return total;
      }

      public void setTotal (int total) {
         this.total = total;
      }

      public int getPerPage () {
         return perPage;
      }

      public void setPerPage (int perPage) {
         this.perPage = perPage;
      }

      public int getCurrentPage () {
         return currentPage;
      }

      public void setCurrentPage (int currentPage) {
         this.currentPage = currentPage;
      }

      public int getLastPage () {
         return lastPage;
      }

      public void setLastPage (int lastPage) {
         this.lastPage = lastPage;
      }
   }

   @SerializedName ("data")
   private ArrayList<ProviderGalleryModel> gallery = null;

   public Meta getMeta () {
      return meta;
   }

   public void setMeta (Meta meta) {
      this.meta = meta;
   }

   public ArrayList<ProviderGalleryModel> getGallery () {
      return gallery;
   }

   public void setGallery (ArrayList<ProviderGalleryModel> gallery) {
      this.gallery = gallery;
   }
}