package com.dugun.Wrapper;

import com.dugun.Model.UserPhotos;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mustafakaya on 11/30/17.
 */

public class UserPhotosWrapper {

    @SerializedName("data")
    @Expose
    InnerWrapper data;

    public UserPhotosWrapper() {
    }

    public InnerWrapper getData() {
        return data;
    }

    public void setData(InnerWrapper data) {
        this.data = data;
    }


   public class InnerWrapper{
        @SerializedName("photos")
        @Expose
        public UserPhotos photos;
    }
}
