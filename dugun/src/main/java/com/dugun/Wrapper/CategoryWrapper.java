package com.dugun.Wrapper;

import com.dugun.Model.CategoryModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CategoryWrapper {

   @SerializedName ("data")
   @Expose
   private ArrayList<CategoryModel> category = null;

   public ArrayList<CategoryModel> getCategory () {
      return category;
   }

   public void setCategory (ArrayList<CategoryModel> category) {
      this.category = category;
   }

}