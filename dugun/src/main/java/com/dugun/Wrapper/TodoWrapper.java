package com.dugun.Wrapper;

import com.dugun.Model.TodoModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TodoWrapper {

   @SerializedName ("data")
   @Expose
   private ArrayList<TodoModel> todo = null;

   public ArrayList<TodoModel> getTodo () {
      return todo;
   }

   public void setTodo (ArrayList<TodoModel> todo) {
      this.todo = todo;
   }
}