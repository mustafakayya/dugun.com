package com.dugun.Wrapper;

import com.dugun.Model.AccessTokenModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginWrapper {

    @SerializedName("accessToken")
    @Expose
    private AccessTokenModel accessToken;
    @SerializedName("status_code")
    @Expose
    private Integer          statusCode;
    @SerializedName("status_text")
    @Expose
    private String           statusText;
    @SerializedName("code")
    @Expose
    private Integer          code;
    @SerializedName("message")
    @Expose
    private String           message;
    @SerializedName("errors")
    @Expose
    private List<Object> errors = null;

    public AccessTokenModel getAccessToken () {
        return accessToken;
    }

    public void setAccessToken (AccessTokenModel accessToken) {
        this.accessToken = accessToken;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Object> getErrors() {
        return errors;
    }

    public void setErrors(List<Object> errors) {
        this.errors = errors;
    }

}