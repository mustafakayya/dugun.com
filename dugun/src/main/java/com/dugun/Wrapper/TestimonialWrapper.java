package com.dugun.Wrapper;

import com.dugun.Model.TestimonialModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mustafakaya on 12/2/17.
 */

public class TestimonialWrapper {

    @SerializedName("data")
    ArrayList<TestimonialModel> data;

    public TestimonialWrapper() {
    }

    public ArrayList<TestimonialModel> getData() {
        return data;
    }

    public void setData(ArrayList<TestimonialModel> data) {
        this.data = data;
    }
}
