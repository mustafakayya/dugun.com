package com.dugun.Wrapper;

import com.dugun.Model.LeadInteractionModel;
import com.dugun.Model.LeadMessageModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mustafakaya on 11/30/17.
 */

public class LeadsInteractionsWrapper {

    @SerializedName("data")
    @Expose
    LeadData data;

    public LeadsInteractionsWrapper() {
    }

    public LeadData getData() {
        return data;
    }

    public void setData(LeadData data) {
        this.data = data;
    }

    public class LeadData {

        @SerializedName("interactions")
        @Expose
        List<LeadInteractionModel> interactions;

        public LeadData() {
        }

        public List<LeadInteractionModel> getInteractions() {
            return interactions;
        }

        public void setInteractions(List<LeadInteractionModel> interactions) {
            this.interactions = interactions;
        }
    }
}
