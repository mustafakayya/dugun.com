package com.dugun.Wrapper;

import com.dugun.Model.LikeStatsModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LikeStatsWrapper {

   @SerializedName ("data")
   @Expose
   private Category stats = null;

   public class Category {

      @SerializedName ("categories")
      @Expose
      private ArrayList<LikeStatsModel> categories = null;

      public ArrayList<LikeStatsModel> getCategories () {
         return categories;
      }

      public void setCategories (ArrayList<LikeStatsModel> categories) {
         this.categories = categories;
      }
   }

   public Category getStats () {
      return stats;
   }

   public void setStats (Category stats) {
      this.stats = stats;
   }
}