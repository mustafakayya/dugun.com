package com.dugun.Wrapper;

import com.dugun.Model.ProviderModel2;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProviderWrapper {

   @SerializedName ("data")
   @Expose
   private ArrayList<ProviderModel2> providers = null;

   public ArrayList<ProviderModel2> getProviders () {
      return providers;
   }

   public void setProviders (ArrayList<ProviderModel2> providers) {
      this.providers = providers;
   }
}