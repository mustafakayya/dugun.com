package com.dugun.Wrapper;

import com.dugun.Model.LoginAccessModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginAccessModelWrapper {

    @SerializedName("accessToken")
    @Expose
    private LoginAccessModel accessToken;

    public LoginAccessModel getAccessToken () {
        return accessToken;
    }

    public void setAccessToken (LoginAccessModel accessToken) {
        this.accessToken = accessToken;
    }
}