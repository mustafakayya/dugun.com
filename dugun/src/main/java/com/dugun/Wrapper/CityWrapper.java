package com.dugun.Wrapper;

import com.dugun.Model.CityModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CityWrapper {

   @SerializedName ("data")
   @Expose
   private List<CityModel> cities = null;

   public List<CityModel> getCities () {
      return cities;
   }

   public void setCities (List<CityModel> cities) {
      this.cities = cities;
   }

}