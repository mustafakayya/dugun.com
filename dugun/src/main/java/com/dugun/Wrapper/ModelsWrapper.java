package com.dugun.Wrapper;

import com.dugun.Model.MetaModel;
import com.dugun.Model.ModelsModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ModelsWrapper {

   @SerializedName ("meta")
   @Expose
   private MetaModel meta = null;

   @SerializedName ("data")
   @Expose
   private ArrayList<ModelsModel> models = null;

   public MetaModel getMeta () {
      return meta;
   }

   public void setMeta (MetaModel meta) {
      this.meta = meta;
   }

   public ArrayList<ModelsModel> getModels () {
      return models;
   }

   public void setModels (ArrayList<ModelsModel> models) {
      this.models = models;
   }
}