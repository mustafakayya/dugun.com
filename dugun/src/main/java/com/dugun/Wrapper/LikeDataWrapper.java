package com.dugun.Wrapper;

import com.dugun.Model.LikeMediaModel;
import com.dugun.Model.LikeProviderModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LikeDataWrapper {

   @SerializedName ("data")
   private Data data;

   public class Data {

      @SerializedName ("media")
      private ArrayList<LikeMediaModel> mediaList = null;

      @SerializedName ("provider")
      private ArrayList<LikeProviderModel> providerList = null;

      public ArrayList<LikeMediaModel> getMediaList () {
         return mediaList;
      }

      public void setMediaList (ArrayList<LikeMediaModel> mediaList) {
         this.mediaList = mediaList;
      }

      public ArrayList<LikeProviderModel> getProviderList () {
         return providerList;
      }

      public void setProviderList (ArrayList<LikeProviderModel> providerList) {
         this.providerList = providerList;
      }
   }

   public Data getData () {
      return data;
   }

   public void setData (Data data) {
      this.data = data;
   }
}