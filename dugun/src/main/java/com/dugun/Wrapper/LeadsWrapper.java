package com.dugun.Wrapper;

import com.dugun.Model.LeadsModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LeadsWrapper {

   @SerializedName ("data")
   private ArrayList<LeadsModel> infoRequests = null;

   public ArrayList<LeadsModel> getInfoRequests () {
      return infoRequests;
   }

   public void setInfoRequests (ArrayList<LeadsModel> infoRequests) {
      this.infoRequests = infoRequests;
   }
}