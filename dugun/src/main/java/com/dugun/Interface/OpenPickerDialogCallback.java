package com.dugun.Interface;

/**
 * Created by ugurbasarir on 28/02/16.
 */
public interface OpenPickerDialogCallback {

   void openPicker (int position);
}
