package com.dugun.Interface;

import android.graphics.Bitmap;

/**
 * Created by ugurbasarir on 28/02/16.
 */
public interface CameraGalleryReturnListener {
    void callBackCameraGallery(String selectedImagePath, Bitmap selectedImage);
}
