package com.dugun.Others;

import android.content.Context;

import com.dugun.Model.CityModel;
import com.dugun.Wrapper.UserPhotosWrapper;
import com.dugun.Wrapper.UserWrapper;

import static com.dugun.Others.Utils.getUser;
import static com.dugun.Others.Utils.saveUser;


/**
 * Created by ugur on 13.11.2017.
 */

public class USER {

    static UserWrapper u;

    public static UserWrapper getProfile(Context c) {
        if (u == null)
            u = getUser(c);
        return u;
    }

    public static void setProfile(Context c, UserWrapper profileUser) {
        u = profileUser;
        saveUser(c, u);
    }

    public static void clear(Context c) {
        Utils.clearUser(c);
    }

    public static void updatePhoto(Context c , UserPhotosWrapper wrapper){
        u = getProfile(c);
        u.getUser().setPhotos(wrapper.getData().photos);
        saveUser(c,u);
    }


    public static void updateCity(Context c , CityModel cityModel){
        u = getProfile(c);
        u.getUser().setCity(cityModel);
        saveUser(c,u);
    }

}
