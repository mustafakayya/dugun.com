package com.dugun.Others;

import com.dugun.R;



public enum RegisterStatus {

    UNFINISHED(0, R.drawable.ic_unfinished,R.color.register_line_disabled_color),
    FOCUSED(1,R.drawable.ic_focused,R.color.purple_color),
    FINISHED(2,R.drawable.ic_finished,R.color.purple_color);

    private int itemID;
    private int resID;
    private int colorID;

    private RegisterStatus(int itemID,int resID,int colorID){
        this.itemID = itemID;
        this.resID = resID;
        this.colorID = colorID;
    }


    public int getItemID() {
        return itemID;
    }

    public int getResID() {
        return resID;
    }

    public int getColorID() {
        return colorID;
    }
}
