package com.dugun.Others;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;

import com.dugun.Model.AccessTokenModel;
import com.dugun.Wrapper.UserWrapper;
import com.google.gson.Gson;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.dugun.Api.GeneralVariables.APP_DIR;
import static com.dugun.Api.GeneralVariables.COVER_DIR;
import static com.dugun.Api.GeneralVariables.FIRST_RUNNING;
import static com.dugun.Api.GeneralVariables.FULL_DATE;
import static com.dugun.Api.GeneralVariables.MONTH_NAME;
import static com.dugun.Api.GeneralVariables.MONTH_YEAR;
import static com.dugun.Api.GeneralVariables.PLAN_DATE;
import static com.dugun.Api.GeneralVariables.SHARED_ACCESS_TOKEN;
import static com.dugun.Api.GeneralVariables.SHARED_PSW;
import static com.dugun.Api.GeneralVariables.SIMPLE_DATE;
import static com.dugun.Api.GeneralVariables.TIME;


public class Utils {

    public static Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    public static String beautifyPhoneNumber(String number){
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        try{
           Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(number,"TR");
           return phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
        }catch (NumberParseException e){
            return number;
        }

    }


    public static class UtilityLocation {

        public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        public static boolean checkPermission(final Context context) {
            int currentAPIVersion = Build.VERSION.SDK_INT;
            if (currentAPIVersion >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(context,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                        alertBuilder.setCancelable(true);
                        alertBuilder.setTitle("Permission necessary");
                        alertBuilder.setMessage("Camera permission is necessary");
                        alertBuilder.setPositiveButton(android.R.string.yes,
                                new DialogInterface.OnClickListener() {
                                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                    public void onClick(DialogInterface dialog, int which) {
                                        ActivityCompat.requestPermissions(
                                                (Activity) context,
                                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                                    }
                                });
                        AlertDialog alert = alertBuilder.create();
                        alert.show();

                    } else {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }


    }

    public static class AsteriskPasswordTransformationMethod extends PasswordTransformationMethod {

        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {

            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            public char charAt(int index) {
                return '*'; // This is the important part
            }

            public int length() {
                return mSource.length(); // Return default
            }

            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }

    public static void savePassword(Context context, String password) {
        Log.d("SavedPassword", password);
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SHARED_PSW, password);
        editor.commit();
    }

    public static String getPassword(Context context) {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        String password = preferences.getString(SHARED_PSW, null);
        if (TextUtils.isEmpty(password))
            return null;
        else
            return password;
    }

    public static void saveToken(Context context, String accessToken) {

        // SaveToAppController Class
        AccessTokenModel accessTokenObj = new AccessTokenModel();
        accessTokenObj.setToken(accessToken);
        if (USER.getProfile(context) == null) {
            UserWrapper user = new UserWrapper();
            user.setAccessToken(accessTokenObj);
            USER.setProfile(context, user);
        } else {
            USER.getProfile(context).setAccessToken(accessTokenObj);
        }

        // SaveToShared
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SHARED_ACCESS_TOKEN, accessToken);
        editor.commit();

    }

    public static boolean getToken(Context context) {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        String accessToken = preferences.getString(SHARED_ACCESS_TOKEN, "");

        if (accessToken.equalsIgnoreCase(""))
            return false;
        else {
            AccessTokenModel accessTokenObj = new AccessTokenModel();
            accessTokenObj.setToken(accessToken);
            if (USER.getProfile(context) == null) {
                UserWrapper user = new UserWrapper();
                user.setAccessToken(accessTokenObj);
                USER.setProfile(context, user);
            } else {
                USER.getProfile(context).setAccessToken(accessTokenObj);
            }

            return true;
        }
    }

    public static void deleteToken(Context context) {

        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SHARED_ACCESS_TOKEN, null);
        editor.commit();

    }

    public static void saveUser(Context context, UserWrapper userWrapper) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("UserWrapper", new Gson().toJson(userWrapper));
        editor.commit();
    }

    public static void clearUser(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("UserWrapper", null);
        editor.commit();
    }

    public static UserWrapper getUser(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String s = preferences.getString("UserWrapper", null);
        if (s != null)
            return new Gson().fromJson(s, UserWrapper.class);
        else
            return null;
    }

//   public static String getUser (Context context) {
//      SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences (context);
//      String password = preferences.getString (SHARED_PSW, null);
//      if (password == "")
//         return null;
//      else
//         return password;
//   }

    public static void setIsFirstRunning(Context context, boolean isFirst) {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(FIRST_RUNNING, isFirst);
        editor.commit();
    }

    public static boolean getIsFirstRunning(Context context) {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(FIRST_RUNNING, true);
    }

    public static String getDateForServer(String date) {
        String[] dateParts = date.split("/");

        return dateParts[2] + "-" + dateParts[1] + "-" + dateParts[0];
    }

    public static String getDateForUser(String serverDate, int type) {
        SimpleDateFormat sF, uF;
        Date result;
        switch (type) {
            case FULL_DATE:
                sF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                try {
                    result = sF.parse(serverDate);
                    uF = new SimpleDateFormat("dd/MMM/yyyy HH:mm");
                    return uF.format(result);
                } catch (ParseException e) {
                    e.printStackTrace();
                    return null;
                }
            case PLAN_DATE:
                sF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                try {
                    result = sF.parse(serverDate);
                    uF = new SimpleDateFormat("dd MMM");
                    return uF.format(result);
                } catch (ParseException e) {
                    e.printStackTrace();
                    return null;
                }
            case TIME:
                sF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                try {
                    result = sF.parse(serverDate);
                    uF = new SimpleDateFormat("HH:mm");
                    return uF.format(result);
                } catch (ParseException e) {
                    e.printStackTrace();
                    return null;
                }
            case MONTH_NAME:
                sF = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    result = sF.parse(serverDate);
                    uF = new SimpleDateFormat("dd/MM/yyyy");
                    return uF.format(result);
                } catch (ParseException e) {
                    sF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                    try {
                        result = sF.parse(serverDate);
                        uF = new SimpleDateFormat("dd/MM/yyyy");
                        return uF.format(result);
                    } catch (ParseException ex) {
                        ex.printStackTrace();
                        return null;
                    }
                }
            case MONTH_YEAR:
                sF = new SimpleDateFormat("MM/yyyy");
                try {
                    result = sF.parse(serverDate);
                    uF = new SimpleDateFormat("MM/yyyy");
                    return uF.format(result);
                } catch (ParseException e) {
                    sF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                    try {
                        result = sF.parse(serverDate);
                        uF = new SimpleDateFormat("MM/yyyy");
                        return uF.format(result);
                    } catch (ParseException ex) {
                        ex.printStackTrace();
                        return null;
                    }
                }
            case SIMPLE_DATE:
                sF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                try {
                    result = sF.parse(serverDate);
                    uF = new SimpleDateFormat("dd.MM.yyyy");
                    return uF.format(result);
                } catch (ParseException e) {
                    e.printStackTrace();
                    return null;
                }
            default:
                return null;
        }
    }

    public static Integer[] getWeddingCount(String weddingDate) {

        SimpleDateFormat sF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

        try {
            Date wD = sF.parse(weddingDate);
            Date now = new Date();
            long sec = TimeUnit.MILLISECONDS.toSeconds(wD.getTime() - now.getTime());

            int dayX = (int) (sec / 86400);
            int hourX = (int) ((sec / 3600) % 24);
            int minX = (int) ((sec / 60) % 60);

            return new Integer[]{dayX, hourX, minX};

        } catch (ParseException e) {
            e.printStackTrace();
            return new Integer[]{0, 0, 0};
        }

    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static Bitmap getBitmapFromView(View view, String fileName) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);

        convertToPdf(returnedBitmap, fileName);

        return returnedBitmap;
    }

    public static void convertToPdf(Bitmap bitmap, String fileName) {

        try {
            File outputFile = new File(
                    Environment.getExternalStorageDirectory() + File.separator + fileName);
            if (!outputFile.exists()) outputFile.createNewFile();

            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(outputFile));
            document.open();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            Image image = Image.getInstance(stream.toByteArray());
//         float scaler = ((document.getPageSize ().getWidth () - document.leftMargin ()
//                 - document.rightMargin () - 0) / image.getWidth ()) * 100;
//         image.scalePercent (scaler);
            image.scaleToFit(PageSize.A4.getWidth(), PageSize.A4.getHeight());
            image.setAlignment(Image.TOP);

            document.add(image);
            document.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static Intent createPdfShareIntent(String subject, String fileName) {

        File pdfFile = new File(
                Environment.getExternalStorageDirectory() + File.separator + fileName);
        Intent intent = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:"));
        intent.setType("application/pdf");
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(pdfFile));
        intent.putExtra(Intent.EXTRA_TEXT, "");

        return intent;
    }

    public static String getMapUrl(String lat, String lng) {
        return "<!DOCTYPE html><html><head><style> #map {  height: 250px; width: 100%; } </style></head><body> <div id=\"map\"></div><script> function initMap() { var uluru = {lat: " + lat + ", lng: " + lng + " }; var map = new google.maps.Map(document.getElementById('map'), { zoom: 10, center: uluru }); var marker = new google.maps.Marker({ position: uluru, map: map }); } </script> <script async defer src=\"https://maps.googleapis.com/maps/api/js?key=" + "AIzaSyA3eXkoAXXslnbQH6nSoQq8iRRqVbRXjhQ" + "&callback=initMap\"> </script> </body> </html>";
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void saveCoverImageToDir(Bitmap bitmap) {
        OutputStream output;
        File dir = new File(APP_DIR);
        dir.mkdirs();
        File file = new File(dir, "profile/cover.jpg");
        try {
            output = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
            output.flush();
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean getCoverImageFromDir() {
        File file = new File(APP_DIR, COVER_DIR);
        return file.exists();
    }
}
