package com.dugun.Others;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


/**
 * Created by mustafakaya on 11/29/17.
 */

public class NetworkInterceptor implements Interceptor {

    public static String CONSUMER_KEY = "f21d01a56d9a4330d5aae8f4e4982bc7";

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();
        Request newRequest;

        newRequest = request.newBuilder()
                .addHeader("X-CONSUMER-KEY", CONSUMER_KEY)
                .build();
        return chain.proceed(newRequest);

    }
}
