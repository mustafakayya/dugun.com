package com.dugun.Others;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class PasswordTextWatcher implements TextWatcher {

    private EditText editText;
    TextChangedListener listener;

    public PasswordTextWatcher(EditText e,TextChangedListener listener) {
        editText = e;
        this.listener = listener;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        listener.textChanged(editText,charSequence.toString());

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    public interface TextChangedListener{
        void textChanged(EditText e,String s);
    }


}