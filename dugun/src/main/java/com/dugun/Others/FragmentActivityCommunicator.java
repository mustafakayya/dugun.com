package com.dugun.Others;

import java.util.HashMap;

public interface FragmentActivityCommunicator {
    void passDataToActivity(int stepId, RegisterStatus registerStatus, HashMap<Object,Object> hashMap);
}