package com.dugun.Others;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;

import com.dugun.Interface.CameraGalleryReturnListener;

import java.io.FileNotFoundException;

import static com.dugun.Api.GeneralVariables.GALLERY;


/**
 * Created by ugurbasarir on 01/12/16.
 */
public class MyOnActivityResultClass {

    public static String selectedImagePath;

    public static void result(Activity activity, CameraGalleryReturnListener cameraGalleryReturnListener, int requestCode, int resultCode, Intent data) {
        Log.d("MyOnActivityResult", "RequestCode" + String.valueOf(requestCode));
        Log.d("MyOnActivityResult", "ResultCode" + String.valueOf(resultCode));

        if (resultCode == Activity.RESULT_OK && null != data) {

            if (requestCode == GALLERY) {

                Uri      imageUri       = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = activity.getContentResolver().query(imageUri,
                                                                    filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                selectedImagePath = cursor.getString(columnIndex);
                cursor.close();
                Log.d("MyOnActivityResult", "SelectedImagePath" + selectedImagePath);

                Bitmap bitmap;
                try {
                    bitmap = BitmapFactory.decodeStream(activity.getContentResolver().openInputStream(imageUri));

                    cameraGalleryReturnListener.callBackCameraGallery(selectedImagePath, bitmap);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            } else {

                try {
                    Bundle extras = data.getExtras();
                    selectedImagePath = data.getData().getPath();
                    Bitmap bitmap = (Bitmap) extras.get("data");

                    String[] projection = {MediaStore.Images.Media.DATA};
                    Cursor cursor = activity.managedQuery(
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            projection, null, null, null);
                    int column_index_data = cursor
                            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToLast();

                    selectedImagePath = cursor.getString(column_index_data);

                    cameraGalleryReturnListener.callBackCameraGallery(selectedImagePath, bitmap);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
