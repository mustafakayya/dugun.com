package com.dugun.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Fragment.RegisterStep1Fragment;
import com.dugun.Fragment.RegisterStep2Fragment;
import com.dugun.Fragment.RegisterStep3Fragment;
import com.dugun.Model.CoupleIdModel;
import com.dugun.Others.FragmentActivityCommunicator;
import com.dugun.Others.RegisterStatus;
import com.dugun.R;
import com.dugun.Wrapper.LoginAccessModelWrapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dugun.Others.Utils.isNetworkAvailable;
import static com.dugun.Others.Utils.savePassword;
import static com.dugun.Others.Utils.saveToken;

public class RegisterActivity extends BaseActivity implements FragmentActivityCommunicator {

    private ImageView img_step1, img_step2, img_step3;
    private View view_line1, view_line2;

    private RegisterStatus step1, step2, step3;
    private boolean isFinished = false;
    private HashMap<Object, Object> hashMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        LinearLayout toolbar_register = (LinearLayout) findViewById(R.id.toolbar_register);
        img_step1 = (ImageView) toolbar_register.findViewById(R.id.img_step1);
        img_step2 = (ImageView) toolbar_register.findViewById(R.id.img_step2);
        img_step3 = (ImageView) toolbar_register.findViewById(R.id.img_step3);
        view_line1 = (View) toolbar_register.findViewById(R.id.view_line1);
        view_line2 = (View) toolbar_register.findViewById(R.id.view_line2);
        ImageView img_back = (ImageView) toolbar_register.findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        step1 = RegisterStatus.UNFINISHED;
        step2 = RegisterStatus.UNFINISHED;
        step3 = RegisterStatus.UNFINISHED;
        stepStatus();

        passDataToActivity(1, null, null);
    }


    @Override
    public void passDataToActivity(int stepId, RegisterStatus registerStatus, HashMap<Object, Object> hashMap) {
        switch (stepId) {
            case 1:
                changeFragment(new RegisterStep1Fragment());
                break;
            case 2:
                changeFragment(new RegisterStep2Fragment());
                break;
            case 3:
                changeFragment(new RegisterStep3Fragment());
                break;
            case 11:
                img_step1.setImageResource(registerStatus.getResID());
                img_step2.setImageResource(RegisterStatus.UNFINISHED.getResID());
                img_step3.setImageResource(RegisterStatus.UNFINISHED.getResID());
                view_line1.setBackgroundColor(
                        getResources().getColor(RegisterStatus.UNFINISHED.getColorID()));
                view_line2.setBackgroundColor(
                        getResources().getColor(RegisterStatus.UNFINISHED.getColorID()));
                break;
            case 12:
                img_step2.setImageResource(registerStatus.getResID());
                img_step3.setImageResource(RegisterStatus.UNFINISHED.getResID());
                view_line1.setBackgroundColor(getResources().getColor(registerStatus.getColorID()));
                view_line2.setBackgroundColor(
                        getResources().getColor(RegisterStatus.UNFINISHED.getColorID()));
                break;
            case 13:
                img_step3.setImageResource(registerStatus.getResID());
                view_line2.setBackgroundColor(getResources().getColor(registerStatus.getColorID()));
                break;
            case 21:
                isFinished = true;
                break;
        }
        setRegisterValue(hashMap);
    }


    @Override
    public void onBackPressed() {
        int fragments = getSupportFragmentManager().getBackStackEntryCount();
        if (fragments == 1) {
            startActivity(new Intent(RegisterActivity.this, FirstActivity.class));
            finish();
            return;
        }

        super.onBackPressed();
    }

    public void stepStatus() {
        img_step1.setImageResource(step1.getResID());
        img_step2.setImageResource(step2.getResID());
        img_step3.setImageResource(step3.getResID());
    }

    public void setRegisterValue(HashMap<Object, Object> stephashMap) {
        if (stephashMap == null)
            return;
        for (Map.Entry e : stephashMap.entrySet()) {
            hashMap.put(e.getKey(), e.getValue());
        }
        if (isFinished) {
            if (!isNetworkAvailable(getApplicationContext())) {
                AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
                return;
            }
            showDialog();
            final ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
            apiInterface.register(hashMap).enqueue(new Callback<CoupleIdModel>() {
                @Override
                public void onResponse(Call<CoupleIdModel> call, final Response<CoupleIdModel> response) {
                    if (response.isSuccessful()) {
                        HashMap<Object, Object> hashMap2 = new HashMap<>();
                        hashMap2.put("username", String.valueOf(hashMap.get("email")));
                        hashMap2.put("password", String.valueOf(hashMap.get("password")));
                        hashMap2.put("userType", "couple");
                        apiInterface.login(hashMap2).enqueue(
                                new Callback<LoginAccessModelWrapper>() {
                                    @Override
                                    public void onResponse(Call<LoginAccessModelWrapper> call, Response<LoginAccessModelWrapper> response) {
                                        dismissDialog();
                                        if (response.isSuccessful()) {
                                            saveToken(getApplicationContext(),
                                                    response.body().getAccessToken().getId());
                                            savePassword(getApplicationContext(),
                                                    String.valueOf(hashMap.get("password")));
                                            startActivity(
                                                    new Intent(RegisterActivity.this, MainActivity.class));
                                            finish();
                                        } else {
                                            try {
                                                showErrorMessage(response.errorBody().string());
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<LoginAccessModelWrapper> call, Throwable t) {
                                        dismissDialog();
                                        AppAlertDialog.showMessage(RegisterActivity.this,
                                                getString(R.string.failure_message));
                                    }
                                });

                    } else {
//                  try {
                        dismissDialog();
                        showErrorMessage("Geçerli bir telefon numarası değil.");
//                  } catch (IOException e) {
//                     e.printStackTrace ();
//                  }
                    }

                }

                @Override
                public void onFailure(Call<CoupleIdModel> call, Throwable t) {
                    dismissDialog();
                    AppAlertDialog.showMessage(RegisterActivity.this,
                            getString(R.string.failure_message));
                }
            });

        }
    }

}
