package com.dugun.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Api.GeneralVariables;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Others.PasswordTextWatcher;
import com.dugun.Others.USER;
import com.dugun.Others.Utils;
import com.dugun.R;
import com.dugun.Wrapper.LoginAccessModelWrapper;

import java.io.IOException;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Others.Utils.isNetworkAvailable;
import static com.dugun.Others.Utils.savePassword;
import static com.dugun.Others.Utils.saveToken;

public class LoginActivity extends BaseActivity implements PasswordTextWatcher.TextChangedListener {

    private EditText edt_mail, edt_pass;
    private ImageView img_mail, img_pass;
    private LinearLayout back;
    private Button loginBtn, forgot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        edt_mail = (EditText) findViewById(R.id.edt_mail);
        edt_pass = (EditText) findViewById(R.id.edt_pass);
        img_mail = (ImageView) findViewById(R.id.img_mail);
        img_pass = (ImageView) findViewById(R.id.img_pass);
        edt_mail.addTextChangedListener(new PasswordTextWatcher(edt_mail, this));
        edt_pass.addTextChangedListener(new PasswordTextWatcher(edt_pass, this));

        loginBtn = (Button) findViewById(R.id.btn_login);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_mail.getText().toString().length() > 0 && edt_pass.getText().toString().length() > 0) {
                    login();
                }
            }
        });
        back = (LinearLayout) findViewById(R.id.toolbar_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        forgot = (Button) findViewById(R.id.forgot);
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppAlertDialog.showMessage(LoginActivity.this, "Geliştirme Aşamasında...");
            }
        });

    }

    private void login() {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        if(edt_pass.getText().length()<6){
            AppAlertDialog.showMessage(this,
                    getString(R.string.password_length_error));
            return;
        }

        showDialog();
        HashMap<Object, Object> hashMap = new HashMap<>();
        hashMap.put("username", edt_mail.getText().toString());
        hashMap.put("password", edt_pass.getText().toString());
        hashMap.put("userType", "couple");
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.login(hashMap).enqueue(new Callback<LoginAccessModelWrapper>() {
            @Override
            public void onResponse(Call<LoginAccessModelWrapper> call, Response<LoginAccessModelWrapper> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    GeneralVariables.clear();
                    USER.clear(getApplicationContext());
                    saveToken(getApplicationContext(), response.body().getAccessToken().getId());
                    savePassword(getApplicationContext(), edt_pass.getText().toString());
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();
                } else {
                    try {
                        showErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginAccessModelWrapper> call, Throwable t) {
                dismissDialog();
                AppAlertDialog.showMessage(LoginActivity.this, getString(R.string.failure_message));
            }
        });
    }


    @Override
    public void textChanged(EditText e, String s) {
        if (e == edt_mail) {
            if (s.length() > 0) {
                if (Utils.validateEmail(s)) {
                    img_mail.setImageResource(R.drawable.ic_eposta2);
                    e.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_logincheck, 0);
                } else {
                    e.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    img_mail.setImageResource(R.drawable.ic_eposta);
                }
            } else {
                img_mail.setImageResource(R.drawable.ic_eposta);
                e.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
        }
        if (e == edt_pass) {
            if (s.length() > 0) {
                img_pass.setImageResource(R.drawable.ic_passwordcopy);
            } else {
                img_pass.setImageResource(R.drawable.ic_password);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(LoginActivity.this, FirstActivity.class));
        finish();
    }
}
