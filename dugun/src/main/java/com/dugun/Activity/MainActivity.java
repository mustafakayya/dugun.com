package com.dugun.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.dugun.Adapter.MainCategoryAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Api.GeneralVariables;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.BadgesModel;
import com.dugun.Wrapper.SharePhotoWrapper;
import com.dugun.badges.BadgeDialog;
import com.dugun.Dialog.ShareDialog;
import com.dugun.Model.CategoryModel;
import com.dugun.Model.LikeScopesModel;
import com.dugun.Model.StatsModel;
import com.dugun.Model.SuccessModel;
import com.dugun.Others.USER;
import com.dugun.R;
import com.dugun.Wrapper.CategoryWrapper;
import com.dugun.Wrapper.UserWrapper;
import com.dugun.badges.BadgeTask;
import com.dugun.badges.BadgeType;


import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Others.Utils.dpToPx;
import static com.dugun.Others.Utils.getIsFirstRunning;
import static com.dugun.Others.Utils.getWeddingCount;
import static com.dugun.Others.Utils.isNetworkAvailable;
import static com.dugun.Others.Utils.setIsFirstRunning;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    public static int openItem;
    private GridView list;
    private ImageView share, website, cover;
    private LinearLayout myBadges, myFavorites, myTodos, sample, counterLay;
    private Button searchCompany, selectConcept, addCategory/*, calculatePrice*/;
    private TextView plansCompleted, plansAll, badgesCompleted, badgesAll, likeCount,
            coupleName, dayCount, hourCount, minCount, counterFinishText, counterTitle;
    public static ArrayList<CategoryModel> categories = new ArrayList<>();
    private Timer timer;
    private StatsModel.InnerData stats;
    private boolean isConnected = true;
    private String TAG = "MainActivity";

    private static final String[] INITIAL_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        plansAll = (TextView) findViewById(R.id.plans_all);
        plansCompleted = (TextView) findViewById(R.id.plans_completed);
        badgesAll = (TextView) findViewById(R.id.badges_all);
        badgesCompleted = (TextView) findViewById(R.id.badges_completed);
        likeCount = (TextView) findViewById(R.id.like_count);
        coupleName = (TextView) findViewById(R.id.couple_name);
        dayCount = (TextView) findViewById(R.id.dayCount);
        hourCount = (TextView) findViewById(R.id.hourCount);
        minCount = (TextView) findViewById(R.id.minCount);
        website = (ImageView) findViewById(R.id.website);
        website.setOnClickListener(this);
        share = (ImageView) findViewById(R.id.share);
        share.setOnClickListener(this);
        myBadges = (LinearLayout) findViewById(R.id.my_badges);
        myBadges.setOnClickListener(this);
        myFavorites = (LinearLayout) findViewById(R.id.my_favorites);
        myFavorites.setOnClickListener(this);
        myTodos = (LinearLayout) findViewById(R.id.my_todos);
        myTodos.setOnClickListener(this);
        searchCompany = (Button) findViewById(R.id.search_company);
        searchCompany.setOnClickListener(this);
        selectConcept = (Button) findViewById(R.id.select_concept);
        selectConcept.setOnClickListener(this);
        addCategory = (Button) findViewById(R.id.add_category);
        addCategory.setOnClickListener(this);
        list = (GridView) findViewById(R.id.list);
        sample = (LinearLayout) findViewById(R.id.sample);

        cover = (ImageView) findViewById(R.id.cover);
//      calculatePrice = (Button) findViewById (R.id.calculate_price);
        counterLay = (LinearLayout) findViewById(R.id.counter_lay);
        counterFinishText = (TextView) findViewById(R.id.counter_finish_text);
        counterTitle = (TextView) findViewById(R.id.counter_title);

        getUser();
        showBadge();
        list.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return event.getAction() == MotionEvent.ACTION_MOVE;
            }

        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(INITIAL_PERMS, 3);
        }
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }


    }

    private void showBadge() {
        new BadgeTask(getApplicationContext(), BadgeType.SignUp,getSupportFragmentManager()).execute();
    }

    private void showShareDialog() {
        if (!isNetworkAvailable(this)) {
            AppAlertDialog.showMessage( this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getSharePhoto(user.getAccessToken().getToken(),user.getUser().getId()).enqueue(new Callback<SharePhotoWrapper>() {
            @Override
            public void onResponse(Call<SharePhotoWrapper> call, Response<SharePhotoWrapper> response) {
                dismissDialog();
                if(response!=null && response.isSuccessful()) {
                    ShareDialog fragment = ShareDialog.newInstance();
                    fragment.dayStr = dayCount.getText().toString();
                    fragment.hourStr = hourCount.getText().toString();
                    fragment.minStr = minCount.getText().toString();
                    fragment.shareUrl = response.body().getData().getUrl();
                    fragment.show(getSupportFragmentManager(), "shareDialog");
                }else if (response!=null){
                    AppAlertDialog.showMessage(MainActivity.this,"Gecerli profil resminiz bulunamadi");
                }
            }

            @Override
            public void onFailure(Call<SharePhotoWrapper> call, Throwable t) {
                dismissDialog();
                AppAlertDialog.showMessage(MainActivity.this,"Gecerli profil resminiz bulunamadi");
            }
        });



    }

    private void setToolBar() {
        LinearLayout toolbar = (LinearLayout) findViewById(R.id.toolbar);
        ImageView img_detail = (ImageView) toolbar.findViewById(R.id.img_detail);
        img_detail.setOnClickListener(this);
    }


    private void setCategoryList() {

        for (int i = 0; i < categories.size(); i++) {
            categories.get(i).setOpen(false);
        }
        categories.get(0).setOpen(true);
        MainCategoryAdapter adapter = new MainCategoryAdapter(this, categories);
        list.setAdapter(adapter);
        setListViewHeight(list);

        dismissDialog();

    }

    private void setListViewHeight(GridView listView) {

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = dpToPx(126) + (dpToPx(65) * (categories.size() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();

    }


    @Override
    public void onClick(View view) {
        if (!isConnected) return;
        switch (view.getId()) {
            case R.id.website:
                startActivity(new Intent(Intent.ACTION_VIEW).setData(
                        Uri.parse(getResources().getString(R.string.website_url))));
                break;
            case R.id.img_detail:
                Intent i = new Intent(this, DetailActivity.class);
                i.putExtra("todo_completed", String.valueOf(stats.todoCounts.completed));
                i.putExtra("todo_all", String.valueOf(stats.todoCounts.all));
                i.putExtra("badges_completed", String.valueOf(stats.badgeCounts.completed));
                i.putExtra("badges_all", String.valueOf(stats.badgeCounts.all));
                i.putExtra("info_req_cost", String.valueOf(stats.infoRequests.cost));
                i.putExtra("info_req_count", String.valueOf(stats.infoRequests.counts));
                i.putExtra("like_count", String.valueOf(stats.likeCounts));
                startActivity(i);
//            finish ();
                break;
            case R.id.share:
                showShareDialog();
                break;
            case R.id.my_badges:
                startActivity(new Intent(this, MyBadgesActivity.class));
                break;
            case R.id.my_favorites:
                startActivity(new Intent(this, LikeCategoryActivity.class));
                break;
            case R.id.my_todos:
                startActivity(new Intent(this, TodoCategoriesActivity.class));
                break;
            case R.id.search_company:
                startActivity(new Intent(this, SearchCategoryActivity.class));
                break;
            case R.id.select_concept:
                startActivity(new Intent(this, ConceptActivity.class));
                break;
//         case R.id.calculate_price:
//            Toast.makeText (getApplicationContext (), "Servis hazır değil",
//                            Toast.LENGTH_SHORT).show ();
//            break;
            case R.id.add_category:
                startActivity(new Intent(this, MyCategoriesActivity.class));
                finish();
                break;
        }
    }

    private void getUser() {
        if (!isNetworkAvailable(context)) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            isConnected = false;
            return;
        }
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getProfile(user.getAccessToken().getToken()).enqueue(
                new Callback<UserWrapper>() {
                    @Override
                    public void onResponse(Call<UserWrapper> call, Response<UserWrapper> response) {
                        if (response.isSuccessful()) {
                            USER.setProfile(context, response.body());
                            user = USER.getProfile(context);
                            coupleName.setText(user.getUser().getCoupleName());
                            timer = new Timer();
                            timer.schedule(new WriteDateParts(), 0, 1000 * 60);
//                       if (getCoverImageFromDir ()) {
//                          Picasso.with (getApplicationContext ())
//                                  .load (new File (APP_DIR + COVER_DIR))
//                                  .into (cover);
//                       } else {
                           loadCoverPhoto();
//                       }
                            getCategoryList();
                        }

                    }

                    @Override
                    public void onFailure(Call<UserWrapper> call, Throwable t) {
                        dismissDialog();
                        Log.d(TAG, "onFailure: getProfile() " + t.getMessage());
                        AppAlertDialog.showMessage(MainActivity.this,
                                getString(R.string.failure_message));
                    }
                });
    }



    void loadCoverPhoto(){

        if (user.getUser().getUserPhotoUrl() != null) {
            Glide.with(this)
                    .load(user.getUser().getUserPhotoUrl())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            cover.setImageResource(R.drawable.ic_default_cover);
                            return true;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(cover);
        }else{
            cover.setImageResource(R.drawable.ic_default_cover);
        }

    }

    private void getCategoryList() {
        if (!isNetworkAvailable(context)) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            isConnected = false;
            return;
        }
        categories = new ArrayList<>();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.categoryList(user.getAccessToken().getToken()).enqueue(
                new Callback<CategoryWrapper>() {
                    @Override
                    public void onResponse(Call<CategoryWrapper> call, Response<CategoryWrapper> response) {
                        if (response != null & response.body() != null && response.body().getCategory() != null) {
                            getLikeStats();
                            ArrayList<CategoryModel> tempCategories = response.body().getCategory();
                            categories.clear();
                            for (int i = 0; i < tempCategories.size(); i++) {
                                if (tempCategories.get(i).isSelected())
                                    categories.add(tempCategories.get(i));
                            }

                            GeneralVariables.categoryModels = categories;
                            if (categories.size() != 0) {
                                setCategoryList();
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<CategoryWrapper> call, Throwable t) {
                        dismissDialog();
                        Log.d(TAG, "onFailure: categoryList() " + t.getMessage());
                        AppAlertDialog.showMessage(MainActivity.this,
                                getString(R.string.failure_message));
                    }
                });
    }

    private void getStats() {
        if (!isNetworkAvailable(context)) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            isConnected = false;
            return;
        }
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.stats(user.getAccessToken().getToken()).enqueue(
                new Callback<StatsModel>() {
                    @Override
                    public void onResponse(Call<StatsModel> call, Response<StatsModel> response) {

                        if (response != null & response.body() != null) {
                            stats = response.body().data;
                            plansCompleted.setText(String.valueOf(stats.todoCounts.completed));
                            plansAll.setText("/" + String.valueOf(stats.todoCounts.all));
                            badgesCompleted.setText(String.valueOf(stats.badgeCounts.completed));
                            badgesAll.setText("/" + String.valueOf(stats.badgeCounts.all));
                            likeCount.setText(String.valueOf(stats.likeCounts));
                        }
                    }

                    @Override
                    public void onFailure(Call<StatsModel> call, Throwable t) {
                        dismissDialog();
                        Log.d(TAG, "onFailure: stats() " + t.getMessage());
                        AppAlertDialog.showMessage(MainActivity.this,
                                getString(R.string.failure_message));
                    }

                }
        );
    }

    private void getLikeStats() {
        if (!isNetworkAvailable(context)) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            isConnected = false;
            return;
        }
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.likeStats(user.getAccessToken().getToken()).enqueue(
                new Callback<LikeScopesModel>() {
                    @Override
                    public void onResponse(Call<LikeScopesModel> call, Response<LikeScopesModel> response) {
                        dismissDialog();
                        if (response != null & response.body() != null) {
                            GeneralVariables.LIKES = response.body();
                        }
                    }

                    @Override
                    public void onFailure(Call<LikeScopesModel> call, Throwable t) {
                        dismissDialog();
                        Log.d(TAG, "onFailure: stats() " + t.getMessage());
                        GeneralVariables.LIKES = new LikeScopesModel();
//                    AppAlertDialog.showMessage (MainActivity.this,
//                                                getString (R.string.failure_message));
                    }

                }
        );
        dismissDialog();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    class WriteDateParts extends TimerTask {

        @Override
        public void run() {
            if (user.getUser().getWeddingDate() != null) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Integer[] parts = getWeddingCount(user.getUser().getWeddingDate());
                        if (parts[2] < 0 || parts[0] < 0 || parts[1] < 0) {
                            counterFinishText.setVisibility(View.VISIBLE);
                            counterLay.setVisibility(View.INVISIBLE);
                            counterTitle.setVisibility(View.INVISIBLE);
                        } else {
                            counterFinishText.setVisibility(View.INVISIBLE);
                            counterLay.setVisibility(View.VISIBLE);
                            counterTitle.setVisibility(View.VISIBLE);
                            dayCount.setText(String.valueOf(parts[0]));
                            hourCount.setText(String.valueOf(parts[1]));
                            minCount.setText(String.valueOf(parts[2]));
                        }
                    }
                });
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setToolBar();
        user = USER.getProfile(getApplicationContext());
        if (user != null && user.getUser() != null) {
            loadCoverPhoto();
            coupleName.setText(user.getUser().getCoupleName());
        }
        getStats();
    }
}
