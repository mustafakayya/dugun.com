package com.dugun.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.dugun.Adapter.FilterAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Dialog.PickerDialog;
import com.dugun.Interface.OpenPickerDialogCallback;
import com.dugun.Interface.PickerSelectCallback;
import com.dugun.Model.FormOptionsModel;
import com.dugun.R;
import com.dugun.Wrapper.FilterWrapper;
import com.dugun.Wrapper.UserWrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dugun.Activity.ProvidersActivity.filterList;
import static com.dugun.Others.Utils.getUser;
import static com.dugun.Others.Utils.isNetworkAvailable;

/**
 * Created by ugurbasarir on 18/08/17.
 */

public class FilterActivity extends Activity implements OpenPickerDialogCallback, PickerSelectCallback {

    private GridView gridView;
    private ImageView close;
    private Button apply;
    private int categoryId;
    private FilterAdapter adapter;
    private Dialog dialog;
    private int selectedFilterItemPos;
    private UserWrapper user;
    private boolean isFirstReq = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        user = getUser(getApplicationContext());

        categoryId = getIntent().getIntExtra("categoryId", 0);
        gridView = (GridView) findViewById(R.id.gridView);
        apply = (Button) findViewById(R.id.apply);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        });
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("cityId", filterList);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });

        getFilterData(user.getUser().getCity().getId());

    }


    private void getFilterData(int cityId) {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        Map<String, Object> data = new HashMap<>();
        data.put("cityId", cityId);
        data.put("categoryId", categoryId);
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getFilterData(user.getAccessToken().getToken(),
                data).enqueue(
                new Callback<FilterWrapper>() {
                    @Override
                    public void onResponse(Call<FilterWrapper> call, Response<FilterWrapper> response) {
                        dismissDialog();
                        if (response != null & response.body() != null) {

                            if (isFirstReq) {
                                filterList = response.body().getFilters();
                                if(filterList!=null && filterList.size()>0)
                                    filterList.remove(0);

                                Optional<Integer> cityIndex = Stream.range(0 , filterList.size()-1).filter(value -> filterList.get(value
                                ).getFilterName().equalsIgnoreCase("cityId")).findFirst();
                                if(cityIndex.isPresent()) {
                                    int index = cityIndex.get();
                                    for (int i = 0; i < filterList.get(index).getFormOptions().size(); i++) {
                                        if (filterList.get(index).getFormOptions().get(i).getOptionValue()
                                                == user.getUser().getCity().getId()) {
                                            filterList.get(index).setSelectedFormOption(
                                                    filterList.get(index).getFormOptions().get(i));
                                        }
                                    }
                                }
                                setGeneralInfo();
                            } else {

                                ArrayList<FormOptionsModel> districts = response.body().getFilters().get(
                                        2).getFormOptions();
                                filterList.get(2).setFormOptions(districts);
                                setGeneralInfo();
                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<FilterWrapper> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(FilterActivity.this,
                                getString(R.string.failure_message));
                    }

                }
        );
    }

    private void setGeneralInfo() {
        adapter = new FilterAdapter(getApplicationContext(), filterList, this);
        gridView.setAdapter(adapter);
    }


    @Override
    public void openPicker(int position) {

        this.selectedFilterItemPos = position;
        if (filterList.get(position).getFieldName().equals("İlçe")) {
            if (filterList.get(position - 1).getSelectedFormOption() == null) {
                Toast.makeText(getApplicationContext(), "Bir şehir seçiniz",
                        Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if (filterList.get(position).getSelectedFormOption() == null) {
            PickerDialog fragment = new PickerDialog(
                    getApplicationContext(), this, filterList.get(position).getFormOptions());
            fragment.show(getFragmentManager(), "");
        } else {
            filterList.get(position).setSelectedFormOption(null);
            if (position == 1) // 1 is city options
                filterList.get(2).setSelectedFormOption(null);
            adapter.notifyDataSetChanged();
            setButtonLabel();
        }


    }

    @Override
    public void setSelect(int position) {

        filterList.get(selectedFilterItemPos).setSelectedFormOption(
                filterList.get(selectedFilterItemPos).getFormOptions().get(position));
        adapter.notifyDataSetChanged();
        setButtonLabel();

        if (selectedFilterItemPos == 1) {
            isFirstReq = false;
            getFilterData(
                    filterList.get(selectedFilterItemPos).getSelectedFormOption().getOptionValue());
        }

    }

    private void setButtonLabel() {
        int appliedFilterCount = 0;
        for (int i = 0; i < filterList.size(); i++) {
            if (filterList.get(i).getSelectedFormOption() != null)
                appliedFilterCount++;
        }
        apply.setText("UYGULA (" + appliedFilterCount + ")");
    }

    public void showDialog() {
        dialog = ProgressDialog.show(FilterActivity.this, null, "Lütfen bekleyiniz...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        if (!dialog.isShowing())
            dialog.show();

    }

    public void dismissDialog() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }
}
