package com.dugun.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.dugun.Adapter.LikeStatsAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Api.GeneralVariables;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.CategoryModel;
import com.dugun.Model.LikeStatsModel;
import com.dugun.R;
import com.dugun.Wrapper.LikeStatsWrapper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dugun.Others.Utils.isNetworkAvailable;

public class LikeCategoryActivity extends BaseActivity {

    private GridView gridView;
    private LikeStatsAdapter adapter;
    private ArrayList<LikeStatsModel> likeStatList;
    private String TAG = "LikeCategoryActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_like_category);
        setToolbarGeneral(getResources().getString(R.string.title_my_favorites));


        gridView = (GridView) findViewById(R.id.list_my_favorites);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CategoryModel category = new CategoryModel();
                category.setId(likeStatList.get(position).getId());
                category.setName(likeStatList.get(position).getName());

                Intent i = new Intent(LikeCategoryActivity.this, LikeTabbedActivity.class);
                i.putExtra("category", category);
                i.putExtra("from", 2);
                startActivity(i);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getLikeStats();
    }

    private void getLikeStats() {
        if (!isNetworkAvailable(context)) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getLikeStats(user.getAccessToken().getToken()).enqueue(
                new Callback<LikeStatsWrapper>() {
                    @Override
                    public void onResponse(Call<LikeStatsWrapper> call, Response<LikeStatsWrapper> response) {
                        dismissDialog();
                        if (response != null & response.body() != null) {

                            likeStatList = response.body().getStats().getCategories();

                            int likeCount = 0;
                            for (int i = 0; i < likeStatList.size(); i++) {
                                likeCount = likeCount + Integer.parseInt(
                                        likeStatList.get(i).getLikeCount());
                            }
                            setToolbarGeneral(getResources().getString(
                                    R.string.title_my_favorites) + " (" + likeCount + ")");

                            adapter = new LikeStatsAdapter(context, likeStatList);
                            gridView.setAdapter(adapter);
                        }
                    }

                    @Override
                    public void onFailure(Call<LikeStatsWrapper> call, Throwable t) {
                        dismissDialog();
                        Log.d(TAG, "getLikeStats () -> " + t.getMessage());
                        AppAlertDialog.showMessage(LikeCategoryActivity.this,
                                getString(R.string.failure_message));
                    }
                }
        );
    }

}
