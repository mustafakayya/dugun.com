package com.dugun.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

import com.dugun.R;

public class SplashActivity extends Activity{

   @Override
   protected void onCreate (Bundle savedInstanceState) {
      super.onCreate (savedInstanceState);
      setContentView (R.layout.activity_splash);


      new CountDownTimer (1000, 500) {

         public void onTick (long millisUntilFinished) {

         }

         public void onFinish () {
            startActivity (new Intent (SplashActivity.this, FirstActivity.class));
            finish ();
         }

      }.start ();

   }

}
