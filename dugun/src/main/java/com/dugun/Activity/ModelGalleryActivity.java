package com.dugun.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dugun.Adapter.ModelGalleryGeneralAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Api.GeneralVariables;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.MediaInfoTagModel;
import com.dugun.Model.ModelGalleryModel;
import com.dugun.Model.ProviderModel;
import com.dugun.Model.SuccessModel;
import com.dugun.Others.Utils;
import com.dugun.R;
import com.dugun.Wrapper.ModelGalleryModelWrapper;
import com.gtomato.android.ui.transformer.LinearViewTransformer;
import com.gtomato.android.ui.widget.CarouselView;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



import static com.dugun.Others.Utils.dpToPx;
import static com.dugun.Others.Utils.isNetworkAvailable;

public class ModelGalleryActivity extends BaseActivity {

    private CarouselView carousel;
    private ImageView mail, close,left,right;
    private TextView providerName, providerTel;
    private LinearLayout contacts, call;
    private ArrayList<ModelGalleryModel> modelList;
    private int galleryId, scrollPosition = 0,providerId;
    String categoryName;
    MyDataAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model_gallery);

        galleryId = getIntent().getIntExtra("galleryId", 0);

        providerId = getIntent().getIntExtra("providerId",-1);
        if(getIntent().hasExtra("categoryName"))
            categoryName = getIntent().getStringExtra("categoryName");
        else
            categoryName = "Firmalar";
        carousel = (CarouselView) findViewById(R.id.carousel);
        contacts = (LinearLayout) findViewById(R.id.contacts);
        contacts.setVisibility(View.GONE);
        call = (LinearLayout) findViewById(R.id.call_provider);
        mail = (ImageView) findViewById(R.id.mail_provider);
        close = (ImageView) findViewById(R.id.close);
        providerName = (TextView) findViewById(R.id.provider_name);
        providerTel = (TextView) findViewById(R.id.provider_tel);
        left = findViewById(R.id.ivLeft);
        right = findViewById(R.id.ivRight);

        carousel.setOnScrollListener(new CarouselView.OnScrollListener() {
            @Override
            public void onScrollEnd(CarouselView carouselView) {
                super.onScrollEnd(carouselView);

                scrollPosition = carouselView.getCurrentPosition();
                setProviderContacts();

            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dialIntent = new Intent(Intent.ACTION_DIAL);
                dialIntent.setData(Uri.parse("tel:" + modelList.get(
                        scrollPosition).getGallery().getProvider().getPhone()));
                startActivity(dialIntent);
            }
        });
        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ModelGalleryActivity.this, LeadsNewActivity.class);
                i.putExtra("providerId",
                        modelList.get(scrollPosition).getGallery().getProvider().getId());
                i.putExtra("providerName",
                        modelList.get(scrollPosition).getGallery().getProvider().getName());
                startActivity(i);
            }
        });

        left.setOnClickListener(view -> {
            int position = carousel.getCurrentPosition();
            if(position>0)
                carousel.smoothScrollToPosition(position-1);
        });
        right.setOnClickListener(view -> {
            int position = carousel.getCurrentPosition();
            int count = carousel.getAdapter().getItemCount();
            if(position<count-1){
                carousel.smoothScrollToPosition(position+1);
            }
        });


        if(providerId!=-1)
            getDiscountInfo();
        else {
            showDialog();
            adapter = new MyDataAdapter(false);
            getModelGallery();
        }



    }



    public void getDiscountInfo() {

        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getProviderDiscount(user.getAccessToken().getToken(), providerId)
                .enqueue(new Callback<ProviderModel>() {
                    @Override
                    public void onResponse(Call<ProviderModel> call, Response<ProviderModel> response) {
                        if(response.isSuccessful() ) {
                            adapter = new MyDataAdapter(response.body().isHasActiveDiscount());
                            getModelGallery();
                        }else {
                            adapter = new MyDataAdapter(false);
                            getModelGallery();
                        }

                    }

                    @Override
                    public void onFailure(Call<ProviderModel> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(ModelGalleryActivity.this,
                                getString(R.string.failure_message));
                    }
                });

    }

    private void getModelGallery() {

        Map<String, String> data = new HashMap<>();
        data.put("scopes[]", "infoTags");
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getModelGallery(user.getAccessToken().getToken(),
                galleryId, data).enqueue(
                new Callback<ModelGalleryModelWrapper>() {
                    @Override
                    public void onResponse(Call<ModelGalleryModelWrapper> call, Response<ModelGalleryModelWrapper> response) {
                        dismissDialog();

                        modelList = response.body().getModels();
                        carousel.setTransformer(new LinearViewTransformer());
                        carousel.setAdapter(adapter);
                        setProviderContacts();
                    }

                    @Override
                    public void onFailure(Call<ModelGalleryModelWrapper> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(ModelGalleryActivity.this,
                                getString(R.string.failure_message));
                    }
                }
        );
    }


    private void setLike(final int scrollPos,boolean isLiked) {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();



        HashMap<String, Object> data = new HashMap<>();
        data.put("itemType", "media");
        data.put("itemId", modelList.get(scrollPos).getId());
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        if (!isLiked) {
            apiInterface.addCoupleLikes(user.getAccessToken().getToken(),
                    user.getUser().getId(), data).enqueue(
                    new Callback<SuccessModel>() {
                        @Override
                        public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                            dismissDialog();
                            if (response != null & response.body() != null) {
                                GeneralVariables.addToLikes(modelList.get(scrollPos).getId(),true);

                                adapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onFailure(Call<SuccessModel> call, Throwable t) {
                            dismissDialog();

                            AppAlertDialog.showMessage(ModelGalleryActivity.this,
                                    getString(R.string.failure_message));
                        }

                    }
            );
        } else {
            apiInterface.deleteCoupleLikes(user.getAccessToken().getToken(),
                    user.getUser().getId(), data).enqueue(
                    new Callback<Object>() {
                        @Override
                        public void onResponse(Call<Object> call, Response<Object> response) {
                            dismissDialog();
                            GeneralVariables.removeFromLikes(modelList.get(scrollPos).getId(),true);
                            adapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onFailure(Call<Object> call, Throwable t) {
                            dismissDialog();
                            AppAlertDialog.showMessage(ModelGalleryActivity.this,
                                    getString(R.string.failure_message));
                        }

                    }
            );
        }

    }



    public class MyDataAdapter extends CarouselView.Adapter<ViewHolder> {

        boolean hasDiscount;

        public MyDataAdapter(boolean hasDiscount) {
            this.hasDiscount = hasDiscount;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view;
            view = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.list_item_model_gallery, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
//         holder.getView().bind(position + 1);
            Glide.with(holder.img.getContext()).load(
                    modelList.get(position).getImageUrl().getPrev()).into(holder.img);
            holder.name.setText(modelList.get(position).getTitle());
            if(TextUtils.isEmpty(modelList.get(position).getExpertComment())){
                holder.note_label.setVisibility(View.GONE);
            }else{
                holder.note_label.setVisibility(View.VISIBLE);
            }
            holder.note.setText(modelList.get(position).getExpertComment());
            holder.writer.setText(modelList.get(position).getExpert() == null ? "" : modelList.get(
                    position).getExpert().getName());

            ArrayList<MediaInfoTagModel> tags = modelList.get(position).getMediaInfoTags();
            holder.grd.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    ((tags.size() + 1) / 2) * dpToPx(45)));
            holder.grd.setAdapter(new ModelGalleryGeneralAdapter(getApplicationContext(), tags));
            ModelGalleryModel.Gallery gallery = modelList.get(position).getGallery();
            holder.lblProduct.setText(gallery == null || gallery.getProvider()==null? "Firmalar" : gallery.getProvider().getName());
            holder.lblProduct.setOnClickListener(view -> {
                if(gallery==null || gallery.getProvider()==null){
                    if(providerId==-1)
                        startActivity(new Intent(ModelGalleryActivity.this, SearchCategoryActivity.class));
                    else{
                        Intent i = new Intent (ModelGalleryActivity.this, ProvidersActivity.class);
                        i.putExtra ("categoryId", providerId);
                        i.putExtra("categoryName",categoryName);
                        startActivity (i);
                    }
                }else{
                    Intent i = new Intent(ModelGalleryActivity.this, ProviderDetailActivity.class);
                    i.putExtra("providerId", gallery.getProviderId());
                    i.putExtra("categoryId", gallery.getProvider().getCategory().getId());
                    i.putExtra("cityId", gallery.getProvider().getCity().getId());
                    startActivity(i);
                }

            });

            if (GeneralVariables.isLiked(modelList.get(position).getId(),true))
                holder.ivLike.setImageResource(R.drawable.ic_favorieklebig2);
            else
                holder.ivLike.setImageResource(R.drawable.ic_favorieklebig1);

            holder.ivDiscount.setVisibility(hasDiscount ? View.VISIBLE : View.GONE);

            holder.ivLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setLike(scrollPosition,GeneralVariables.isLiked(modelList.get(position).getId(),true));
                }
            });

        }

        @Override
        public int getItemCount() {
            if(modelList == null)
                return 0;
            return modelList.size();
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView img,ivLike,ivDiscount;
        public TextView name, note, writer,note_label,lblProduct;
        public GridView grd;

        private ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            img = (ImageView) itemLayoutView.findViewById(R.id.image);
            name = (TextView) itemLayoutView.findViewById(R.id.name);
            note = (TextView) itemLayoutView.findViewById(R.id.note);
            ivLike = (ImageView) itemLayoutView.findViewById(R.id.ivLike);
            note_label = (TextView) itemLayoutView.findViewById(R.id.note_label);
            writer = (TextView) itemLayoutView.findViewById(R.id.writer);
            grd = (GridView) itemLayoutView.findViewById(R.id.grd);
            lblProduct = (TextView) itemLayoutView.findViewById(R.id.lblProduct);
            ivDiscount = itemLayoutView.findViewById(R.id.ivDiscount);
        }

    }

    private void setProviderContacts() {
        if (modelList.get(scrollPosition).getGallery() == null || modelList.get(scrollPosition).getGallery().getProvider()==null) {
            contacts.setVisibility(View.GONE);
        } else {
            contacts.setVisibility(View.VISIBLE);
            providerName.setText(
                    modelList.get(scrollPosition).getGallery().getProvider().getName());
            providerTel.setText(Utils.beautifyPhoneNumber(
                    modelList.get(scrollPosition).getGallery().getProvider().getPhone()));
        }
    }

}
