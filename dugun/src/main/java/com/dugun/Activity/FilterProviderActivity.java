package com.dugun.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.dugun.Adapter.ProviderListAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Api.GeneralVariables;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Interface.LikeCallback;
import com.dugun.Model.ProviderModel2;
import com.dugun.Model.SuccessModel;
import com.dugun.R;
import com.dugun.Wrapper.ProviderWrapper;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Others.Utils.isNetworkAvailable;


/**
 * Created by mustafakaya on 12/25/17.
 */

public class FilterProviderActivity extends BaseActivity implements LikeCallback {

    public static final String FILTER_KEY = "provider.filter";
    private GridView gridView;
    private ProviderListAdapter adapter;
    ArrayList<ProviderModel2> providerList;
    private String TAG = "FilterProviderActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filterprovider);

        String filterKey = getIntent().getStringExtra(FILTER_KEY);
        setToolbarGeneral(filterKey);
        gridView = findViewById(R.id.gridView);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(FilterProviderActivity.this, ProviderDetailActivity.class);
                i.putExtra("provider", providerList.get(position));
                startActivity(i);
            }
        });
        fetchProviders(filterKey);
    }

    private void fetchProviders(String filterKey) {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(FilterProviderActivity.this,
                    getString(R.string.connection_failed));
            return;
        }
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getProvidersByName(user.getAccessToken().getToken(), filterKey).enqueue(new Callback<ProviderWrapper>() {
            @Override
            public void onResponse(Call<ProviderWrapper> call, Response<ProviderWrapper> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    providerList = response.body().getProviders();
                    adapter = new ProviderListAdapter(FilterProviderActivity.this, providerList, FilterProviderActivity.this);
                    gridView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ProviderWrapper> call, Throwable t) {
                dismissDialog();
                AppAlertDialog.showMessage(FilterProviderActivity.this,
                        getString(R.string.failure_message));
            }
        });
    }

    @Override
    public void setLike(int position) {
        if (providerList.get(position).isLiked()) {
            AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
            alertbox.setMessage("Beğenmekten vazgeçmek istediğinize emin misiniz?");
            alertbox.setTitle("Uyarı");
            alertbox.setPositiveButton(
                    "Evet",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            setLike(true, position);
                        }
                    });
            alertbox.setNeutralButton("Iptal", (dialogInterface, i) -> dialogInterface.dismiss());
            alertbox.show();
        } else {
            setLike(false, position);
        }
    }


    private void setLike(boolean isLiked, final int position) {
        if (!isNetworkAvailable(FilterProviderActivity.this)) {
            AppAlertDialog.showMessage(FilterProviderActivity.this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        int itemId = providerList.get(position).getProvider().getId();
        HashMap<String, Object> data = new HashMap<>();
        data.put("itemType", "provider");
        data.put("itemId", itemId);
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        if (isLiked) {
            try {
                apiInterface.deleteCoupleLikes(user.getAccessToken().getToken(),
                        user.getUser().getId(), data).enqueue(
                        new Callback<Object>() {
                            @Override
                            public void onResponse(Call<Object> call, Response<Object> response) {
                                dismissDialog();
                                providerList.get(position).setLiked(false);
                                adapter.notifyDataSetChanged();
                                GeneralVariables.removeFromLikes(itemId, false);
                            }

                            @Override
                            public void onFailure(Call<Object> call, Throwable t) {
                                dismissDialog();
                                Log.d(TAG, "onFailure: " + t.getMessage());
                                AppAlertDialog.showMessage(FilterProviderActivity.this,
                                        getString(R.string.failure_message));
                            }

                        }
                );
            } catch (Exception f) {
                dismissDialog();
                providerList.get(position).setLiked(false);
                adapter.notifyDataSetChanged();
            }
        } else {
            apiInterface.addCoupleLikes(user.getAccessToken().getToken(),
                    user.getUser().getId(), data).enqueue(
                    new Callback<SuccessModel>() {
                        @Override
                        public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                            dismissDialog();
                            if (response != null & response.body() != null) {
                                providerList.get(position).setLiked(true);
                                adapter.notifyDataSetChanged();
                                GeneralVariables.addToLikes(itemId, false);
                            }
                        }

                        @Override
                        public void onFailure(Call<SuccessModel> call, Throwable t) {
                            dismissDialog();
                            Log.d(TAG, "onFailure: " + t.getMessage());
                            AppAlertDialog.showMessage(FilterProviderActivity.this,
                                    getString(R.string.failure_message));
                        }

                    }
            );
        }

    }
}
