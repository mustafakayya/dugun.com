package com.dugun.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dugun.R;

public class BodyTypeActivity extends BaseActivity implements View.OnClickListener {

   private LinearLayout close;
   private Button       showAdvices;
   private ImageView    elma, armut, tersUcgen, dikdortgen, kumSaati,
           cElma, cArmut, cTersUcgen, cDikdortgen, cKumSaati;
   private int selectedItem;

   @Override
   protected void onCreate (Bundle savedInstanceState) {
      super.onCreate (savedInstanceState);
      setContentView (R.layout.activity_body_type);

      close = (LinearLayout) findViewById (R.id.close);
      close.setOnClickListener (this);
      showAdvices = (Button) findViewById (R.id.show_advices);
      showAdvices.setOnClickListener (this);
      elma = (ImageView) findViewById (R.id.elma);
      elma.setOnClickListener (this);
      armut = (ImageView) findViewById (R.id.armut);
      armut.setOnClickListener (this);
      tersUcgen = (ImageView) findViewById (R.id.ters_ucgen);
      tersUcgen.setOnClickListener (this);
      dikdortgen = (ImageView) findViewById (R.id.dikdortgen);
      dikdortgen.setOnClickListener (this);
      kumSaati = (ImageView) findViewById (R.id.kum_saati);
      kumSaati.setOnClickListener (this);
      cElma = (ImageView) findViewById (R.id.check_elma);
      cArmut = (ImageView) findViewById (R.id.check_armut);
      cTersUcgen = (ImageView) findViewById (R.id.check_ters_ucgen);
      cDikdortgen = (ImageView) findViewById (R.id.check_dikdortgen);
      cKumSaati = (ImageView) findViewById (R.id.check_kum_saati);

   }


   @Override public void onClick (View v) {
      switch (v.getId ()) {
         case R.id.close:
            finish ();
            break;
         case R.id.show_advices:
            startActivity (
                    new Intent (this, BodyTypeDetailActivity.class).putExtra ("id", selectedItem));
            break;
         case R.id.elma:
            unSelectAllItem ();
            elma.setImageResource (R.drawable.ic_bodyelmaselected);
            cElma.setVisibility (View.VISIBLE);
            selectedItem = 1;
            break;
         case R.id.armut:
            unSelectAllItem ();
            armut.setImageResource (R.drawable.ic_bodyarmutselected);
            cArmut.setVisibility (View.VISIBLE);
            selectedItem = 2;
            break;
         case R.id.ters_ucgen:
            unSelectAllItem ();
            tersUcgen.setImageResource (R.drawable.ic_bodytesrucgenselected);
            cTersUcgen.setVisibility (View.VISIBLE);
            selectedItem = 3;
            break;
         case R.id.dikdortgen:
            unSelectAllItem ();
            dikdortgen.setImageResource (R.drawable.ic_bodydiktortgenselected);
            cDikdortgen.setVisibility (View.VISIBLE);
            selectedItem = 4;
            break;
         case R.id.kum_saati:
            unSelectAllItem ();
            kumSaati.setImageResource (R.drawable.ic_bodykumsaatiselected);
            cKumSaati.setVisibility (View.VISIBLE);
            selectedItem = 5;
            break;
      }
   }

   private void unSelectAllItem () {
      selectedItem = 0;
      elma.setImageResource (R.drawable.ic_bodyelma);
      cElma.setVisibility (View.INVISIBLE);
      armut.setImageResource (R.drawable.ic_bodyarmut);
      cArmut.setVisibility (View.INVISIBLE);
      tersUcgen.setImageResource (R.drawable.ic_bodytestucgen);
      cTersUcgen.setVisibility (View.INVISIBLE);
      dikdortgen.setImageResource (R.drawable.ic_bodydikdortgen);
      cDikdortgen.setVisibility (View.INVISIBLE);
      kumSaati.setImageResource (R.drawable.ic_bodykumsaati);
      cKumSaati.setVisibility (View.INVISIBLE);
   }

}
