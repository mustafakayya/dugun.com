package com.dugun.Activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dugun.Adapter.NotesAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.NewLeadInteraction;
import com.dugun.Model.SuccessModel;
import com.dugun.R;
import com.dugun.Wrapper.LeadsInteractionsWrapper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dugun.Others.Utils.isNetworkAvailable;

public class NotesActivity extends BaseActivity {


    private EditText message;
    private Button send;
    private int leadId;

    private RecyclerView recyclerView;
    NotesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);
        setToolbarGeneral("Not Al");

        leadId = getIntent().getIntExtra("leadId", 0);


        adapter = new NotesAdapter(null, getIntent().getStringExtra("providerName"));
        message = (EditText) findViewById(R.id.message);
        send = (Button) findViewById(R.id.send);
        recyclerView = findViewById(R.id.recyclerView_aNotes);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(true);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        getNotes();

    }

    private void sendMessage() {
        if (!isNetworkAvailable(context)) {
            AppAlertDialog.showMessage(NotesActivity.this,
                    getString(R.string.connection_failed));
            return;
        }
        closeKeyboard();
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.newLeadInteraction(user.getAccessToken().getToken(), leadId,
                new NewLeadInteraction(message.getText().toString())).enqueue(
                new Callback<SuccessModel>() {
                    @Override
                    public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                        dismissDialog();
                        if(response!=null && response.isSuccessful()) {
                            Toast.makeText(context, "Notunuz eklendi ",
                                    Toast.LENGTH_SHORT).show();
                            message.setText("");
                            getNotes();
                        }
                    }

                    @Override
                    public void onFailure(Call<SuccessModel> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(NotesActivity.this,
                                getString(R.string.failure_message));
                    }

                }
        );
    }
    
    void getNotes(){
        if (!isNetworkAvailable(context)) {
            AppAlertDialog.showMessage(NotesActivity.this,
                    getString(R.string.connection_failed));
            return;
        }
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getLeadInteractions(user.getAccessToken().getToken(),leadId).enqueue(new Callback<LeadsInteractionsWrapper>() {
            @Override
            public void onResponse(Call<LeadsInteractionsWrapper> call, Response<LeadsInteractionsWrapper> response) {
                dismissDialog();
                if(response!=null && response.isSuccessful())
                    adapter.setInteractionModels(response.body().getData().getInteractions(),true);
            }

            @Override
            public void onFailure(Call<LeadsInteractionsWrapper> call, Throwable t) {
                dismissDialog();
                AppAlertDialog.showMessage(NotesActivity.this,
                        getString(R.string.failure_message));
            }
        });
    }


}
