package com.dugun.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Api.GeneralVariables;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Fragment.ProviderListFragment;
import com.dugun.Fragment.ProviderMapFragment;
import com.dugun.Model.CategoryModel;
import com.dugun.Model.FilterModel;
import com.dugun.Model.ProviderModel2;
import com.dugun.R;
import com.dugun.Wrapper.ProviderWrapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Api.GeneralVariables.LIKES;
import static com.dugun.Others.Utils.isNetworkAvailable;
import static com.dugun.R.id.list_btn;
import static com.dugun.R.id.map_btn;

public class ProvidersActivity extends BaseActivity {

    private LinearLayout selectCity, search, filter;
    private RelativeLayout mapBtn, listBtn;

    public static ArrayList<FilterModel> filterList;
    public static ArrayList<ProviderModel2> providerList;
    public static int categoryId;
    private String TAG = "ProvidersActivity";
    public static int whichFragmentVisible = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_providers);

        if(getIntent().hasExtra("category")) {
            CategoryModel category = getIntent().getParcelableExtra("category");
            categoryId = category.getProviderCategory().getId();
            setToolbarGeneral(category.getName());
        }else if(getIntent().hasExtra("categoryId")) {
            categoryId = getIntent().getIntExtra("categoryId", 0);
            setToolbarGeneral(getIntent().getStringExtra("categoryName"));
        }


        mapBtn = (RelativeLayout) findViewById(map_btn);
        listBtn = (RelativeLayout) findViewById(list_btn);
        filter = (LinearLayout) findViewById(R.id.filter);
        filterList = null;

        getProviders();


        mapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(1);
            }
        });
        listBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(0);
            }
        });
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(ProvidersActivity.this, FilterActivity.class);
                i.putExtra("categoryId", categoryId);
                startActivityForResult(i, 1);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    getProviders();
                    break;
                case Activity.RESULT_CANCELED:
                    break;
            }
        }

    }


    private void loadFragment(int id) {
        whichFragmentVisible = id;

        switch (id) {
            case 0:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container,
                                new ProviderListFragment()).commitAllowingStateLoss();

                listBtn.setVisibility(View.GONE);
                mapBtn.setVisibility(View.VISIBLE);
//            selectCity.setVisibility (View.VISIBLE);
                break;
            case 1:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container,
                                new ProviderMapFragment()).commitAllowingStateLoss();
                listBtn.setVisibility(View.VISIBLE);
                mapBtn.setVisibility(View.GONE);
//            selectCity.setVisibility (View.GONE);
                break;
        }
    }

    private void getProviders() {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(ProvidersActivity.this,
                    getString(R.string.connection_failed));
            return;
        }
        showDialog();
        Map<String, Integer> data = new HashMap<>();
        data.put("categoryId",categoryId);
        data.put("limit", 100);
        if (filterList != null) {
            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getSelectedFormOption() != null) {
                    String filterName = filterList.get(i).getFilterName();
                    String searchType = filterList.get(i).getSearchType();
                    if(searchType!=null && searchType.equals("array"))
                        filterName+="[]";
                    data.put(filterName,
                            filterList.get(i).getSelectedFormOption().getOptionValue());
                }
            }
        }else{
            data.put("cityId", user.getUser().getCity().getId());
        }
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getAllProvidersById(user.getAccessToken().getToken(),
                data).enqueue(
                new Callback<ProviderWrapper>() {
                    @Override
                    public void onResponse(Call<ProviderWrapper> call, Response<ProviderWrapper> response) {
                        dismissDialog();
                        if (response != null & response.body() != null) {

                            providerList = response.body().getProviders();
                            for (int i = 0; i < providerList.size(); i++) {
                                if (providerList.get(i).getProvider() != null)
                                    if (GeneralVariables.isLiked(providerList.get(i).getProvider().getId(),false)) {
                                        providerList.get(i).setLiked(true);
                                    }
                            }
                            loadFragment(0);
                        }

                    }

                    @Override
                    public void onFailure(Call<ProviderWrapper> call, Throwable t) {
                        dismissDialog();
                        Log.d(TAG, "onFailure: getAllProvidersById()" + t.getMessage());
                        AppAlertDialog.showMessage(ProvidersActivity.this,
                                getString(R.string.failure_message));
                    }

                }
        );
    }

}
