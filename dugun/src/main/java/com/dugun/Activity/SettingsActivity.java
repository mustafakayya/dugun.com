package com.dugun.Activity;

import android.os.Bundle;

import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Fragment.SettingsFragment;
import com.dugun.Model.SettingsModel;
import com.dugun.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Others.Utils.isNetworkAvailable;

public class SettingsActivity extends BaseActivity {

    public static SettingsModel.SettingsData settingsUrls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setToolbarGeneral(getResources().getString(R.string.title_settings));

        getSettings();

    }

    public void changeToolBarTitle(String title) {
        setToolbarGeneral(title);
    }

    private void getSettings() {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.settings().enqueue(
                new Callback<SettingsModel>() {
                    @Override
                    public void onResponse(Call<SettingsModel> call, Response<SettingsModel> response) {
                        dismissDialog();
                        if (response != null & response.body() != null) {
                            settingsUrls = response.body().data;
                            getSupportFragmentManager().beginTransaction().replace(
                                    R.id.fragment_container, new SettingsFragment()).commit();

                        }
                    }

                    @Override
                    public void onFailure(Call<SettingsModel> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(SettingsActivity.this,
                                getString(R.string.failure_message));
                    }

                }
        );
    }

}
