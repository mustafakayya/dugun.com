package com.dugun.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dugun.Adapter.TodoListAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Interface.SetCompletedCallback;
import com.dugun.Model.CategoryModel;
import com.dugun.Model.SuccessModel;
import com.dugun.Model.TodoModel;
import com.dugun.R;
import com.dugun.Wrapper.TodoWrapper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Dialog.AppAlertDialog.showMessage;
import static com.dugun.Others.Utils.isNetworkAvailable;

public class TodoListActivity extends BaseActivity implements SetCompletedCallback {

    private GridView gridView;
    private TodoListAdapter adapter;
    private Button add, where;
    private TextView title;
    private LinearLayout back;
    private ArrayList<TodoModel> todoList;
    private CategoryModel category;
    private String TAG = "TodoListActivity";
    public static boolean isDefault;
    public static boolean isCompleted;
    private boolean isNewList = false;

    private View bar_completed, bar_general;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_list);


        title = (TextView) findViewById(R.id.title);
        back = (LinearLayout) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        gridView = (GridView) findViewById(R.id.gridView);
        add = (Button) findViewById(R.id.add);
        where = (Button) findViewById(R.id.where);

        bar_completed = findViewById(R.id.bar_completed);
        bar_general = findViewById(R.id.bar_general);


        todoList = getIntent().getParcelableArrayListExtra("todoList");
        if (todoList == null)
            category = getIntent().getParcelableExtra("category");


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TodoListActivity.this, TodoNewActivity.class);
                i.putExtra("categoryId", todoList.get(0).getCategory().getId());
                startActivity(i);
            }
        });
        where.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < MainActivity.categories.size(); i++) {
                    if (MainActivity.categories.get(i).getId() == todoList.get(0).getCategory().getId()) {
                        todoList.get(0).getCategory().setProviderCategory(MainActivity.categories.get(i).getProviderCategory());
                    }
                }
                Intent i = new Intent(TodoListActivity.this, TodoFinishActivity.class);
                i.putExtra("category", todoList.get(0).getCategory());
                startActivity(i);
            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i = new Intent(TodoListActivity.this, TodoNewActivity.class);
                i.putExtra("todo", todoList.get(position));
                startActivity(i);


                // TODO değişecekkkk
                if (position < 3)
                    isDefault = true;
                else
                    isDefault = false;
//            startActivity (i);

            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        getTodos(todoList == null ? category.getId() : todoList.get(0).getCategory().getId());
    }

    private void getTodos(int categoryId) {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        Map<String, Integer> data = new HashMap<>();
        data.put("categoryIds[]", categoryId);
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getAllTodosById(user.getAccessToken().getToken(),
                user.getUser().getId(), data).enqueue(
                new Callback<TodoWrapper>() {
                    @Override
                    public void onResponse(Call<TodoWrapper> call, Response<TodoWrapper> response) {
                        dismissDialog();
                        isNewList = true;
                        makeList(response.body().getTodo());
                    }

                    @Override
                    public void onFailure(Call<TodoWrapper> call, Throwable t) {
                        dismissDialog();
                        Log.d(TAG, "getTodos () -> " + t.getMessage());
                        showMessage(TodoListActivity.this, getString(R.string.failure_message));
                    }

                }
        );
    }

    private void makeList(ArrayList<TodoModel> list) {

        todoList = list;
        isCompleted = true;

        for (int i = 0; i < todoList.size(); i++) {
            if (todoList.get(i).getCompletedAt() == null)
                isCompleted = false;
        }

        if (isCompleted) {
            bar_general.setVisibility(View.GONE);
            bar_completed.setVisibility(View.VISIBLE);
            title.setText(todoList.get(0).getCategory().getName() + " " + getString(
                    R.string.title_my_todos));
        } else {
            bar_general.setVisibility(View.VISIBLE);
            bar_completed.setVisibility(View.GONE);
            setToolbarGeneral(
                    todoList.get(0).getCategory().getName() + " " + getString(
                            R.string.title_my_todos));
        }
        if (todoList.size() > 0)
            if (adapter != null) {
                if (isNewList) {
                    adapter = new TodoListAdapter(getApplicationContext(), todoList,
                            TodoListActivity.this);
                    gridView.setAdapter(adapter);
                } else {
                    adapter.notifyDataSetChanged();
                }
            } else {
                adapter = new TodoListAdapter(getApplicationContext(), todoList,
                        TodoListActivity.this);
                gridView.setAdapter(adapter);
            }

    }


    private void updateTodo(final int id) {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        HashMap<Object, Object> hashMap = new HashMap<>();
        hashMap.put("coupleId", user.getUser().getId());
        hashMap.put("taskId", todoList.get(id).getId());
        hashMap.put("completed", todoList.get(id).getCompletedAt() == null ? 1 : 0);
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.updateTask(user.getAccessToken().getToken(),
                user.getUser().getId(), todoList.get(id).getId(),
                hashMap).enqueue(
                new Callback<SuccessModel>() {

                    @Override
                    public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                        dismissDialog();
                        if (response.isSuccessful()) {

                            if (todoList.get(id).getCompletedAt() == null) {
                                SimpleDateFormat sF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                                String date = sF.format(new java.util.Date());
                                todoList.get(id).setCompletedAt(date);
                            } else {
                                todoList.get(id).setCompletedAt(null);
                            }
                            makeList(todoList);


                        } else {
                            showMessage(TodoListActivity.this, String.valueOf(response.message()));
                        }
                    }

                    @Override
                    public void onFailure(Call<SuccessModel> call, Throwable t) {
                        dismissDialog();
                        Log.d(TAG, "updateTodo () -> " + t.getMessage());
                        showMessage(TodoListActivity.this, getString(R.string.failure_message));
                    }
                }
        );
    }

    @Override
    public void setCompletedCallback(int id) {
        updateTodo(id);
    }


}
