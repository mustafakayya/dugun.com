package com.dugun.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Api.GeneralVariables;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.ProviderGalleryModel;
import com.dugun.Model.ProviderModel2;
import com.dugun.Model.SuccessModel;
import com.dugun.Others.Utils;
import com.dugun.R;
import com.dugun.Wrapper.ProviderGalleryWrapper;
import com.dugun.Wrapper.UserWrapper;
import com.gtomato.android.ui.transformer.LinearViewTransformer;
import com.gtomato.android.ui.widget.CarouselView;


import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Api.GeneralVariables.LIKES;
import static com.dugun.Others.Utils.getUser;
import static com.dugun.Others.Utils.isNetworkAvailable;

/**
 * Created by ugurbasarir on 18/08/17.
 */

public class ModelDetailGalleryActivity extends Activity {

    private ArrayList<ProviderGalleryModel> imageList;
    private CarouselView carousel;
    private ImageView mail, close, left, right;
    private LinearLayout call;
    private TextView providerName, providerTel;
    private int scrollPosition = 0;
    private ProviderModel2 provider;
    private String telNumber;
    private Dialog dialog;
    private String TAG = "ModelDetailGalleryActivity";
    private UserWrapper user;
    private MyDataAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model_detail_gallery);

        user = getUser(getApplicationContext());

        provider = getIntent().getParcelableExtra("provider");
        telNumber = getIntent().getStringExtra("telephone");

        providerName = (TextView) findViewById(R.id.provider_name);
        providerName.setText(provider.getName());
        providerTel = (TextView) findViewById(R.id.provider_tel);
        providerTel.setText(Utils.beautifyPhoneNumber(telNumber));

        left = findViewById(R.id.ivLeft);
        right = findViewById(R.id.ivRight);

        call = (LinearLayout) findViewById(R.id.call_provider);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dialIntent = new Intent(Intent.ACTION_DIAL);
                dialIntent.setData(Uri.parse("tel:" + telNumber));
                startActivity(dialIntent);
            }
        });
        mail = (ImageView) findViewById(R.id.mail_provider);
        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ModelDetailGalleryActivity.this, LeadsNewActivity.class);
                i.putExtra("providerId", provider.getProvider().getId());
                i.putExtra("providerName", provider.getName());
                startActivity(i);
            }
        });
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        carousel = (CarouselView) findViewById(R.id.carousel);
        carousel.setOnScrollListener(new CarouselView.OnScrollListener() {
            @Override
            public void onScrollEnd(CarouselView carouselView) {
                super.onScrollEnd(carouselView);
                scrollPosition = carouselView.getCurrentPosition();
            }
        });

        left.setOnClickListener(view -> {
            int position = carousel.getCurrentPosition();
            if (position > 0)
                carousel.smoothScrollToPosition(position - 1);
        });
        right.setOnClickListener(view -> {
            int position = carousel.getCurrentPosition();
            int count = carousel.getAdapter().getItemCount();
            if (position < count - 1) {
                carousel.smoothScrollToPosition(position + 1);
            }
        });

        getGalleryImages();

    }


    private void getGalleryImages() {

        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getGalleryImages(user.getAccessToken().getToken(),
                provider.getProvider().getId()).enqueue(
                new Callback<ProviderGalleryWrapper>() {
                    @Override
                    public void onResponse(Call<ProviderGalleryWrapper> call, Response<ProviderGalleryWrapper> response) {
                        if (response != null & response.body() != null) {
                            dismissDialog();

                            imageList = response.body().getGallery();
                            carousel.setTransformer(new LinearViewTransformer());
                            adapter = new MyDataAdapter();
                            carousel.setAdapter(adapter);
                        }
                    }

                    @Override
                    public void onFailure(Call<ProviderGalleryWrapper> call, Throwable t) {
                        dismissDialog();
                        Log.d(TAG, "onFailure: " + t.getMessage());
                        AppAlertDialog.showMessage(ModelDetailGalleryActivity.this,
                                getString(R.string.failure_message));
                    }

                }
        );
    }

    private void setLike(final int scrollPos, boolean isLiked) {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();


        HashMap<String, Object> data = new HashMap<>();
        data.put("itemType", "media");
        data.put("itemId", imageList.get(scrollPos).getId());
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        if (!isLiked) {
            apiInterface.addCoupleLikes(user.getAccessToken().getToken(),
                    user.getUser().getId(), data).enqueue(
                    new Callback<SuccessModel>() {
                        @Override
                        public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                            dismissDialog();
                            if (response != null & response.body() != null) {
                                GeneralVariables.addToLikes(imageList.get(scrollPos).getId(), true);

                                adapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onFailure(Call<SuccessModel> call, Throwable t) {
                            dismissDialog();
                            Log.d(TAG, "onFailure: " + t.getMessage());
                            AppAlertDialog.showMessage(ModelDetailGalleryActivity.this,
                                    getString(R.string.failure_message));
                        }

                    }
            );
        } else {
            apiInterface.deleteCoupleLikes(user.getAccessToken().getToken(),
                    user.getUser().getId(), data).enqueue(
                    new Callback<Object>() {
                        @Override
                        public void onResponse(Call<Object> call, Response<Object> response) {
                            dismissDialog();
                            GeneralVariables.removeFromLikes(imageList.get(scrollPos).getId(), true);
                            adapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onFailure(Call<Object> call, Throwable t) {
                            dismissDialog();
                            Log.d(TAG, "onFailure: " + t.getMessage());
                            AppAlertDialog.showMessage(ModelDetailGalleryActivity.this,
                                    getString(R.string.failure_message));
                        }

                    }
            );
        }

    }


    private class MyDataAdapter extends CarouselView.Adapter<ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view;
            view = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.list_item_provider_gallery, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            Glide.with(holder.img.getContext())
                    .load(imageList.get(position).getImageUrl().getPrev())
                    .into(holder.img);
            holder.currentNum.setText(String.valueOf(position + 1));
            holder.totalNum.setText(" / " + String.valueOf(imageList.size()));

            if (GeneralVariables.isLiked(imageList.get(position).getId(), true))
                holder.liked.setImageResource(R.drawable.ic_favorieklebig2);
            else
                holder.liked.setImageResource(R.drawable.ic_favorieklebig1);


            holder.like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setLike(scrollPosition, GeneralVariables.isLiked(imageList.get(position).getId(), true));
                }
            });
            holder.share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = "dugun.com/redirect/gallery/" +
                            imageList.get(position).getGalleryId() +
                            "?media_id=" + imageList.get(position).getId();
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, url);
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);
                }
            });

        }

        @Override
        public int getItemCount() {
            return imageList.size();
        }

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img, liked;
        TextView currentNum, totalNum;
        LinearLayout like, share;

        private ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            img = (ImageView) itemLayoutView.findViewById(R.id.image);
            liked = (ImageView) itemLayoutView.findViewById(R.id.liked);
            currentNum = (TextView) itemLayoutView.findViewById(R.id.current_number);
            totalNum = (TextView) itemLayoutView.findViewById(R.id.total_number);
            like = (LinearLayout) itemLayoutView.findViewById(R.id.like);
            share = (LinearLayout) itemLayoutView.findViewById(R.id.share);
        }

    }

    public void showDialog() {
        dialog = ProgressDialog.show(ModelDetailGalleryActivity.this, null, "Lütfen bekleyiniz...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        if (!dialog.isShowing())
            dialog.show();

    }

    public void dismissDialog() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

}
