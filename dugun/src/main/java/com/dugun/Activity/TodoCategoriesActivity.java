package com.dugun.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.dugun.Adapter.MakeCategoryAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.CategoryModel;
import com.dugun.Model.TodoModel;
import com.dugun.R;
import com.dugun.Wrapper.TodoWrapper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Others.Utils.isNetworkAvailable;

public class TodoCategoriesActivity extends BaseActivity {

    private GridView gridView;
    private MakeCategoryAdapter adapter;
    private ArrayList<TodoModel> todoList;
    private ArrayList<CategoryModel> categories;
    private String TAG = "TodoCategoriesActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_categories);
        setToolbarGeneral(getResources().getString(R.string.title_my_todos));

        gridView = (GridView) findViewById(R.id.gridView);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ArrayList<TodoModel> todos = new ArrayList<>();
                for (int i = 0; i < todoList.size(); i++) {
                    if (todoList.get(i).getCategory().getId() == categories.get(position).getId()) {
                        todos.add(todoList.get(i));
                    }
                }

                Intent i = new Intent(TodoCategoriesActivity.this, TodoListActivity.class);
                i.putParcelableArrayListExtra("todoList", todos);
                startActivity(i);

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getTodos();
    }

    private void getTodos() {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getAllTodos(user.getAccessToken().getToken(),
                user.getUser().getId()).enqueue(
                new Callback<TodoWrapper>() {
                    @Override
                    public void onResponse(Call<TodoWrapper> call, Response<TodoWrapper> response) {
                        dismissDialog();
                        if (response.body().getTodo() != null) {
                            todoList = response.body().getTodo();
                            setToolbarGeneral(getResources().getString(R.string.title_my_todos) +
                                    " (" + String.valueOf(todoList.size()) + ")");
                            groupPlans(todoList);
                        }
                    }

                    @Override
                    public void onFailure(Call<TodoWrapper> call, Throwable t) {
                        dismissDialog();
                        Log.d(TAG, "getTodos () -> " + t.getMessage());
                        AppAlertDialog.showMessage(TodoCategoriesActivity.this,
                                getString(R.string.failure_message));
                    }
                }
        );
    }

    private void groupPlans(ArrayList<TodoModel> todoList) {

        ArrayList ids = new ArrayList();
        categories = new ArrayList<>();

        for (int i = 0; i < todoList.size(); i++) {
            int currentID = todoList.get(i).getCategory().getId();

            if (!ids.contains(currentID)) {

                ids.add(currentID);

                CategoryModel cat = todoList.get(i).getCategory();
                Log.d(TAG, cat.getIcon());
                for (int j = 0; j < todoList.size(); j++) {
                    if (todoList.get(j).getCategory().getId() == currentID)
                        cat.setCount(cat.getCount() + 1);
                }
                categories.add(cat);
            }

        }

        adapter = new MakeCategoryAdapter(getApplicationContext(), categories);
        gridView.setAdapter(adapter);

    }

}
