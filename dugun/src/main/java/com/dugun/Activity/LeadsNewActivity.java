package com.dugun.Activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.dugun.Adapter.LeadsNewInfoRequestFormsAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Dialog.PickerDialog;
import com.dugun.Interface.OpenPickerDialogCallback;
import com.dugun.Interface.PickerSelectCallback;
import com.dugun.Model.ProviderInfoRequestFormsModel;
import com.dugun.Others.Utils;
import com.dugun.R;
import com.dugun.Wrapper.ProviderInfoRequestFormsWrapper;
import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Api.GeneralVariables.MONTH_NAME;
import static com.dugun.Api.GeneralVariables.SIMPLE_DATE;
import static com.dugun.Others.Utils.dpToPx;
import static com.dugun.Others.Utils.getDateForServer;
import static com.dugun.Others.Utils.getDateForUser;
import static com.dugun.Others.Utils.isNetworkAvailable;

public class LeadsNewActivity extends BaseActivity implements View.OnClickListener, OpenPickerDialogCallback,
        PickerSelectCallback, DatePickerDialog.OnDateSetListener {

    private EditText name, tel, mail, date, msg;
    private Button edit, save;
    private GridView grdFeatures;
    private LinearLayout dateLay;
    private int providerId;
    private int selectedInfoReqPos;
    private boolean status;

    private ArrayList<ProviderInfoRequestFormsModel> infoList;
    private LeadsNewInfoRequestFormsAdapter adapter;
    private HashMap<Object, Object> reqBody = new HashMap<>();
    private String TAG = "LeadsNewActivity";
    private String weddingDate, weddingTime = "19:00";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leads_new);
        setToolbarGeneral(getIntent().getStringExtra("providerName"));

        providerId = getIntent().getIntExtra("providerId", 0);
        name = (EditText) findViewById(R.id.name);
        tel = (EditText) findViewById(R.id.tel);
        mail = (EditText) findViewById(R.id.mail);
        date = (EditText) findViewById(R.id.date);
        msg = (EditText) findViewById(R.id.msg);
        grdFeatures = (GridView) findViewById(R.id.grid_features);

        edit = (Button) findViewById(R.id.edit);
        edit.setOnClickListener(this);
        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(this);
        dateLay = (LinearLayout) findViewById(R.id.date_layout);
        dateLay.setOnClickListener(this);

        getProviderFormData();

    }

    @Override
    protected void onResume() {
        super.onResume();
        name.setText(user.getUser().getName());
        tel.setText(user.getUser().getPhone() != null ? Utils.beautifyPhoneNumber(user.getUser().getPhone()) : "");
        mail.setText(user.getAccessToken().getUsername());
        date.setText(getDateForUser(user.getUser().getWeddingDate(), SIMPLE_DATE));
    }

    private void getProviderFormData() {
        if (isNetworkAvailable(context)) {
            showDialog();
            ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
            apiInterface.getProviderInfoRequestForms(user.getAccessToken().getToken(),
                    providerId).enqueue(
                    new Callback<ProviderInfoRequestFormsWrapper>() {
                        @Override
                        public void onResponse(Call<ProviderInfoRequestFormsWrapper> call, Response<ProviderInfoRequestFormsWrapper> response) {
                            dismissDialog();
                            if (response != null & response.body() != null) {

                                ArrayList<ProviderInfoRequestFormsModel> list = response.body().getInfoRequestFormList();

                                infoList = new ArrayList<>();
                                for (int i = 0; i < list.size(); i++) {

                                    if (list.get(i).getFieldDataType().equals("name")) {
                                        reqBody.put(list.get(i).getId(), user.getUser().getCoupleName());
                                    } else if (list.get(i).getFieldDataType().equals("email")) {
                                        reqBody.put(list.get(i).getId(), String.valueOf(user.getAccessToken().getUsername()));
                                    } else if (list.get(i).getFieldDataType().equals("phone")) {
                                        reqBody.put(list.get(i).getId(), user.getUser().getPhone());
                                    } else if (list.get(i).getFieldDataType().equals("weddate")) {
                                        if (weddingDate != null) {
                                            reqBody.put(list.get(i).getId(), getDateForServer(weddingDate) + " " + weddingTime);
                                        } else {
                                            reqBody.put(list.get(i).getId(), user.getUser().getWeddingDate());
                                        }
                                    } else if (list.get(i).getFieldDataType().equals("message")) {
                                        reqBody.put(list.get(i).getId(), String.valueOf(msg.getText().toString()));
                                    } else {
                                        if (list.get(i).getInfoRequestFormOptions().size() > 0) {
                                            reqBody.put(list.get(i).getId(), list.get(i).getInfoRequestFormOptions().get(0).getId());
                                        }
                                        infoList.add(list.get(i));
                                    }

                                }
                                Log.d(TAG, new Gson().toJson(reqBody));

                                setFeatures(infoList);
                            }
                        }

                        @Override
                        public void onFailure(Call<ProviderInfoRequestFormsWrapper> call, Throwable t) {
                            Log.d(TAG, "onFailure :" + t.getMessage());
                            dismissDialog();
                            AppAlertDialog.showMessage(LeadsNewActivity.this,
                                    getString(R.string.failure_message));
                        }

                    }
            );
        } else
            showToast(getString(R.string.connection_failed));

    }

    private void addNewLead() {
        if (!isNetworkAvailable(context)) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }

        HashMap<Object, Object> providerBody = new HashMap<>();
        providerBody.put("id", providerId);

        HashMap<Object, Object> mainReqBody = new HashMap<>();
        mainReqBody.put("data", reqBody);
        mainReqBody.put("provider", providerBody);
        mainReqBody.put("coupleId", user.getUser().getId());
        mainReqBody.put("device", "mobile");
        if (!msg.getText().toString().isEmpty())
            mainReqBody.put("message", msg.getText().toString());
        mainReqBody.put("phone", tel.getText().toString());
        mainReqBody.put("name", name.getText().toString());
        mainReqBody.put("email", mail.getText().toString());
        if (weddingDate != null)
            mainReqBody.put("weddingDate", getDateForServer(weddingDate) + " " + weddingTime);
        else
            mainReqBody.put("weddingDate", user.getUser().getWeddingDate());


        JSONObject obj = new JSONObject(mainReqBody);
        Log.d(TAG, obj.toString());

        showDialog();
        Log.d(TAG, mainReqBody.toString());
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.addNewLead(user.getAccessToken().getToken(),
                mainReqBody).enqueue(
                new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        dismissDialog();
                        if (response != null & response.body() != null) {
                            showToast("Teklifiniz gönderildi");
                            finish();
                        } else
                            AppAlertDialog.showMessage(LeadsNewActivity.this,
                                    getString(R.string.parameter_error));

                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        Log.d(TAG, "onFailure :" + t.getMessage());
                        dismissDialog();
                        AppAlertDialog.showMessage(LeadsNewActivity.this,
                                getString(R.string.failure_message));
                    }

                }
        );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit:
                String text;
                if (edit.getText().equals("Kaydet")) {
                    status = false;
                    text = "Düzenle";
                } else {
                    status = true;
                    text = "Kaydet";
                }
                edit.setText(text);
                name.setEnabled(status);
                tel.setEnabled(status);
                mail.setEnabled(status);
                break;
            case R.id.save:
                if (status)
                    showToast("Lütfen bilgilerinizi kaydedin.");
                else
                    addNewLead();
                break;
            case R.id.date_layout:
                showDatePickerDialog();
                break;
        }
    }

    private void setFeatures(ArrayList<ProviderInfoRequestFormsModel> features) {
        grdFeatures.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                (features.size() * dpToPx(60))));
        adapter = new LeadsNewInfoRequestFormsAdapter(context, features, this);
        grdFeatures.setAdapter(adapter);
    }


    @Override
    public void openPicker(int position) {
        this.selectedInfoReqPos = position;
        ProviderInfoRequestFormsModel model = infoList.get(position);
        if (model.getFieldDataType().equalsIgnoreCase("date")) {
            Calendar cal = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                            String day = "" + dayOfMonth;
                            if (dayOfMonth < 10)
                                day = "0" + dayOfMonth;
                            String month = "";
                            if (++monthOfYear < 10)
                                month = "0" + monthOfYear;
                            else {
                                month = "" + monthOfYear;
                            }
                            String selectedDate = day + "/" + month + "/" + year + " " + weddingTime;
                            infoList.get(position).setSelectedDate(selectedDate);
                            reqBody.remove(infoList.get(selectedInfoReqPos).getId());
                            reqBody.put(infoList.get(selectedInfoReqPos).getId(),
                                    getDateForServer(day + "/" + month + "/" + year) + " " + weddingTime);

                            adapter.notifyDataSetChanged();
                        }
                    },
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)
            );
            dpd.setMinDate(cal);
            dpd.setAccentColor(getResources().getColor(R.color.colorAccent));
            dpd.show(getFragmentManager(), "Datepickerdialog");
        } else if ((model.getFieldDataType().equalsIgnoreCase("budget") ||
                model.getFieldDataType().equalsIgnoreCase("venue")) &&
                model.getFieldType().equalsIgnoreCase("text")) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            final EditText edittext = new EditText(this);
            if(model.getFieldDataType().equalsIgnoreCase("budget"))
                edittext.setInputType(InputType.TYPE_CLASS_NUMBER);
            alert.setMessage(model.getFieldLabel());
            alert.setView(edittext);
            alert.setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    infoList.get(selectedInfoReqPos).setTextValue(edittext.getText().toString());
                    adapter.notifyDataSetChanged();
                    reqBody.remove(infoList.get(selectedInfoReqPos).getId());
                    reqBody.put(infoList.get(selectedInfoReqPos).getId(),
                            infoList.get(selectedInfoReqPos).getTextValue());
                    dialog.dismiss();
                }
            });
            alert.setNegativeButton("Iptal", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // what ever you want to do with No option.
                    dialog.dismiss();
                }
            });

            alert.show();
        } else {
            PickerDialog fragment = new PickerDialog(
                    context, this, model.getInfoRequestFormOptions());
            fragment.show(getFragmentManager(), "");
        }
    }

    @Override
    public void setSelect(int position) {
        infoList.get(selectedInfoReqPos).setSelectedFormOption(
                infoList.get(selectedInfoReqPos).getInfoRequestFormOptions().get(position));
        adapter.notifyDataSetChanged();

        reqBody.remove(infoList.get(selectedInfoReqPos).getId());
        reqBody.put(infoList.get(selectedInfoReqPos).getId(),
                infoList.get(selectedInfoReqPos).getInfoRequestFormOptions().get(
                        position).getId());

    }

    private void showDatePickerDialog() {
        Calendar cal = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(cal);
        dpd.setAccentColor(getResources().getColor(R.color.colorAccent));
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String day = "" + dayOfMonth;
        if (dayOfMonth < 10)
            day = "0" + dayOfMonth;
        String month = "";
        if (++monthOfYear < 10)
            month = "0" + monthOfYear;
        else {
            month = "" + monthOfYear;
        }
        weddingDate = day + "/" + month + "/" + year;
        date.setText(getDateForUser(weddingDate + " " + weddingTime, MONTH_NAME));

    }


}
