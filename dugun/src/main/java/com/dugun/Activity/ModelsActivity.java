package com.dugun.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.annimon.stream.Stream;
import com.dugun.Adapter.ModelsAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.ModelsModel;
import com.dugun.R;
import com.dugun.Wrapper.ModelsWrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Others.Utils.isNetworkAvailable;

public class ModelsActivity extends BaseActivity {

    private Button getCompanies;
    private LinearLayout chooseBodyType;
    private GridView gridView;
    private ArrayList<ModelsModel> modelList;
    private int providerCategoryId;
    private String categoryName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_models);
        categoryName = getIntent().getStringExtra("categoryName");
        setToolbarGeneral(categoryName + " Modelleri");

        providerCategoryId = getIntent().getIntExtra("providerCategoryId", 0);

        getCompanies = (Button) findViewById(R.id.get_companies);
        chooseBodyType = (LinearLayout) findViewById(R.id.choose_body_type);
        gridView = (GridView) findViewById(R.id.gridView);


        chooseBodyType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ModelsActivity.this, BodyTypeActivity.class));
            }
        });
        getCompanies.setVisibility(View.GONE);
        getCompanies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(
                        new Intent(ModelsActivity.this, ProvidersActivity.class));
            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(ModelsActivity.this, ModelGalleryActivity.class);
                i.putExtra("galleryId", modelList.get(position).getId());
                i.putExtra("providerId",providerCategoryId);
                i.putExtra("categoryName",categoryName);
                startActivity(i);
            }
        });

        getModelGalleries();

    }

    private void getModelGalleries() {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        Map<String, String> data = new HashMap<>();
        data.put("categoryIds[]", String.valueOf(providerCategoryId));
        data.put("status", "1");
        data.put("scopes[]", "listingWebsite");
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getModelGalleries(user.getAccessToken().getToken(),
                data).enqueue(
                new Callback<ModelsWrapper>() {
                    @Override
                    public void onResponse(Call<ModelsWrapper> call, Response<ModelsWrapper> response) {
                        dismissDialog();
                        if (response.isSuccessful() && response.body() != null) {
                            modelList = (ArrayList<ModelsModel>) Stream.of(response.body().getModels()).filter(value -> value.getMainImage() != null).toList();
                            if (modelList.size() > 0) {
                                gridView.setAdapter(
                                        new ModelsAdapter(getApplicationContext(), modelList));
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ModelsWrapper> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(ModelsActivity.this,
                                getString(R.string.failure_message));
                    }
                }
        );
    }


}
