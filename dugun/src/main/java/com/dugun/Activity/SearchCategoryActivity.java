package com.dugun.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.GridView;
import android.widget.ImageView;

import com.dugun.Adapter.ProviderSearchAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.CategoryModel;
import com.dugun.R;
import com.dugun.Wrapper.CategoryWrapper;
import com.dugun.Wrapper.ProviderWrapper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Others.Utils.isNetworkAvailable;

public class SearchCategoryActivity extends BaseActivity {

    private GridView gridView;
    private AutoCompleteTextView filter;
    private ProviderSearchAdapter adapter;
    private ArrayList<CategoryModel> categoryList;
    private ImageView clean;
    private String filterArr[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_provider);

        gridView = (GridView) findViewById(R.id.gridView);
        clean = (ImageView) findViewById(R.id.clean);
        clean.setVisibility(View.GONE);
        filter = (AutoCompleteTextView) findViewById(R.id.filter);
        filter.setThreshold(1);

        filter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                ArrayList<CategoryModel> filteredCategoryList = new ArrayList<>();

                for (int i = 0; i < categoryList.size(); i++) {
                    if (categoryList.get(i).getName().contains(s))
                        filteredCategoryList.add(categoryList.get(i));
                }

                adapter = new ProviderSearchAdapter(getApplicationContext(), filteredCategoryList);
                gridView.setAdapter(adapter);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (filter.getText().toString().length() > 0) {
                    clean.setVisibility(View.VISIBLE);
                } else {
                    clean.setVisibility(View.GONE);
                }
            }

        });

        filter.setOnKeyListener((view, actionId, keyEvent) -> {

            if ((actionId == EditorInfo.IME_NULL || actionId == 66)
                    && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                closeKeyboard();
                if(adapter.getCount()==0){
                    Intent i = new Intent(SearchCategoryActivity.this, FilterProviderActivity.class);
                    i.putExtra(FilterProviderActivity.FILTER_KEY,filter.getText().toString());
                    startActivity(i);
                }
            }
            return false;
        });



        clean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter.setText("");
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(SearchCategoryActivity.this, ProvidersActivity.class);
                i.putExtra("category", categoryList.get(position));
                startActivity(i);
            }
        });

        findViewById(R.id.btnClose_aSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getProviders();


    }


    private void getProviders() {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.categoryList(user.getAccessToken().getToken()).enqueue(
                new Callback<CategoryWrapper>() {
                    @Override
                    public void onResponse(Call<CategoryWrapper> call, Response<CategoryWrapper> response) {
                        dismissDialog();

                        categoryList = new ArrayList<>();
                        categoryList = response.body().getCategory();
                        adapter = new ProviderSearchAdapter(getApplicationContext(), categoryList);
                        gridView.setAdapter(adapter);

                        filterArr = new String[categoryList.size()];
                        for (int i = 0; i < categoryList.size(); i++) {
                            filterArr[i] = categoryList.get(i).getName();
                        }
                        filter.setAdapter(
                                new ArrayAdapter<>(SearchCategoryActivity.this,
                                        android.R.layout.select_dialog_item, filterArr));
                    }

                    @Override
                    public void onFailure(Call<CategoryWrapper> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(SearchCategoryActivity.this,
                                getString(R.string.failure_message));
                    }
                }
        );
    }

}
