package com.dugun.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Others.USER;
import com.dugun.R;
import com.dugun.Wrapper.ErrorWrapper;
import com.dugun.Wrapper.UserWrapper;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends AppCompatActivity {

   public AppCompatActivity appCompatActivity;
   public static List<AppCompatActivity> appCompatActivityList = new ArrayList<> ();

   Dialog dialog;

   Context context;
   UserWrapper user;


   @Override
   protected void onCreate (Bundle savedInstanceState) {
      super.onCreate (savedInstanceState);
      appCompatActivity = this;
      appCompatActivityList.add (this);
      context = getApplicationContext ();
      user = USER.getProfile (context);
   }

   @Override
   protected void onDestroy () {
      super.onDestroy ();
      appCompatActivityList.remove (appCompatActivity);
   }

   public void removeBackStackActivity () {
      for (int i = 0; i < appCompatActivityList.size (); i++) {
         appCompatActivityList.get (i).finish ();
      }
   }

   public void changeFragment (Fragment f) {
      getSupportFragmentManager ().beginTransaction ().replace (R.id.fragment_container,
                                                                f).addToBackStack (null).commit ();
   }

   public void setToolbarGeneral (String title) {
      LinearLayout toolbar       = (LinearLayout) findViewById (R.id.toolbar_general);
      TextView     toolbar_title = (TextView) toolbar.findViewById (R.id.toolbar_title);
      toolbar_title.setText (title);
      LinearLayout back_lay = (LinearLayout) toolbar.findViewById (R.id.toolbar_back);
      back_lay.setOnClickListener (new View.OnClickListener () {
         @Override
         public void onClick (View view) {
            onBackPressed ();
         }
      });
   }


   @Override
   protected void attachBaseContext (Context newBase) {
      super.attachBaseContext (CalligraphyContextWrapper.wrap (newBase));
   }

//   @Override
//   public boolean dispatchTouchEvent (MotionEvent event) {
//      View    view = getCurrentFocus ();
//      boolean ret  = super.dispatchTouchEvent (event);
//
//      if (view instanceof EditText) {
//         View w           = getCurrentFocus ();
//         int  scrcoords[] = new int[2];
//         w.getLocationOnScreen (scrcoords);
//         float x = event.getRawX () + w.getLeft () - scrcoords[0];
//         float y = event.getRawY () + w.getTop () - scrcoords[1];
//
//         if (event.getAction () == MotionEvent.ACTION_UP
//                 && (x < w.getLeft () || x >= w.getRight () || y < w.getTop () || y > w.getBottom ())) {
//            closeKeyboard ();
//         }
//      }
//      return ret;
//   }

   public void closeKeyboard () {
      InputMethodManager imm = (InputMethodManager) getSystemService (Context.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow (getWindow ().getCurrentFocus ().getWindowToken (), 0);
   }


   public void showDialog () {
      if (dialog == null) {
         dialog = ProgressDialog.show (appCompatActivity, null, "Lütfen bekleyiniz...");
         dialog.setCancelable (false);
         dialog.setCanceledOnTouchOutside (false);
         if (!dialog.isShowing ())
            dialog.show ();
      }
   }

   public void dismissDialog () {
      if (dialog != null) {
         if (dialog.isShowing ()) {
            dialog.dismiss ();
            dialog = null;
         }
      }
   }

   public void showToast (String msg) {
      Toast.makeText (getApplicationContext (), msg, Toast.LENGTH_SHORT).show ();
   }

   public void showErrorMessage (String errorBody) {
      Gson                      gson    = new Gson ();
      TypeAdapter<ErrorWrapper> adapter = gson.getAdapter (ErrorWrapper.class);
      try {
         ErrorWrapper registerResponse = adapter.fromJson (errorBody);
         AppAlertDialog.showMessage (this, registerResponse.getMessage ());
      } catch (IOException e) {
         e.printStackTrace ();
         AppAlertDialog.showMessage (this, "Bir hata oluştu");
      }
   }

   /**
    * Memory problems would be automatic running time method.
    */
   @Override
   public void onLowMemory () {
      super.onLowMemory ();
      System.gc ();
      Runtime.getRuntime ().gc ();
   }

}
