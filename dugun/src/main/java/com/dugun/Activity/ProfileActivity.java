package com.dugun.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.SuccessModel;
import com.dugun.Others.USER;
import com.dugun.Others.Utils;
import com.dugun.R;
import com.dugun.Wrapper.UserWrapper;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dugun.Api.GeneralVariables.MONTH_NAME;
import static com.dugun.Api.GeneralVariables.MONTH_YEAR;
import static com.dugun.Others.Utils.deleteToken;
import static com.dugun.Others.Utils.getDateForServer;
import static com.dugun.Others.Utils.getDateForUser;
import static com.dugun.Others.Utils.isNetworkAvailable;

public class ProfileActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener,
        View.OnClickListener {

    private EditText edt_partner_name, edt_mail, edt_name, edt_phone, edt_date, edt_partner_mail;
    private ImageView img_date;
    private Button btn_update;
    private CheckBox checkbox;
    private String weddingDate;
    private final String weddingTime = " 19:00";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        setToolbarGeneral(getResources().getString(R.string.title_my_profile));


        edt_name = (EditText) findViewById(R.id.edt_name);
        edt_mail = (EditText) findViewById(R.id.edt_mail);
        edt_partner_name = (EditText) findViewById(R.id.edt_partner_name);
        edt_partner_mail = (EditText) findViewById(R.id.edt_partner_mail);
        edt_phone = (EditText) findViewById(R.id.edt_phone);
        edt_date = (EditText) findViewById(R.id.edt_date);
        img_date = (ImageView) findViewById(R.id.img_date);
        btn_update = (Button) findViewById(R.id.btn_update);
        checkbox = (CheckBox) findViewById(R.id.checkbox);
        img_date.setOnClickListener(this);
        btn_update.setOnClickListener(this);
        getProfile(true);


    }

    private void getProfile(final boolean showDialog) {
        if (isNetworkAvailable(getApplicationContext())) {
            if (showDialog)
                showDialog();
            ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
            apiInterface.getProfile(user.getAccessToken().getToken()).enqueue(
                    new Callback<UserWrapper>() {
                        @Override
                        public void onResponse(Call<UserWrapper> call, Response<UserWrapper> response) {
                            dismissDialog();
                            if (response != null) {
                                USER.setProfile(context, response.body());
                                ProfileActivity.super.user = USER.getProfile(getApplicationContext());
                                if (showDialog) {
                                    edt_name.setText(user.getUser().getName());
                                    edt_mail.setText(user.getAccessToken().getUsername());
                                    edt_partner_name.setText(user.getUser().getPartnerName());
                                    edt_phone.setText(Utils.beautifyPhoneNumber(user.getUser().getPhone()));
                                    edt_partner_mail.setText(user.getUser().getPartnerEmail());
                                    if (user.getUser().getWeddingDateKnown() && user.getUser().getWeddingDate() != null) {
                                        weddingDate = getDateForUser(user.getUser().getWeddingDate() + weddingTime, MONTH_NAME);
                                        edt_date.setText(weddingDate);
                                    } else if (user.getUser().getWeddingDate() != null) {
                                        weddingDate = getDateForUser(user.getUser().getWeddingDate() + weddingTime, MONTH_YEAR);
                                        edt_date.setText(weddingDate);
                                    }
                                    checkbox.setChecked(user.getUser().getWeddingDateKnown());
                                    setCheckboxListener(false);

                                } else {
                                    Toast.makeText(getApplicationContext(),
                                            getString(R.string.changes_successful),
                                            Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<UserWrapper> call, Throwable t) {
                            dismissDialog();
                            AppAlertDialog.showMessage(ProfileActivity.this,
                                    getString(R.string.failure_message));
                        }
                    });
        } else
            showToast(getString(R.string.connection_failed));

    }


    private void showDatePickerDialog() {
        Calendar cal = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(cal);
        dpd.setOnCancelListener(dialogInterface -> checkbox.setChecked(false));
        dpd.setAccentColor(getResources().getColor(R.color.colorAccent));
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_date:
                if(checkbox.isChecked())
                    showDatePickerDialog();
                else
                    showMonthYearPicker();
                break;
            case R.id.btn_update:
                SetProfile();
                break;
        }
    }

    private void showMonthYearPicker() {
        Calendar cal = Calendar.getInstance();
        new MonthPickerDialog.Builder(this,
                (selectedMonth, selectedYear) -> {
                    weddingDate= (selectedMonth+1) +"/"+selectedYear;
                    edt_date.setText(weddingDate);
                }
                , cal.get(Calendar.YEAR), cal.get(Calendar.MONTH))
                .setMinYear(cal.get(Calendar.YEAR))
                .setMaxYear(cal.get(Calendar.YEAR)+10)
                .build().show();
    }

    private void SetProfile() {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        String emailString = edt_mail.getText().toString();
        if (!Utils.validateEmail(emailString)) {
            AppAlertDialog.showMessage(this, "Lutfen gecerli email adresi giriniz");
            return;
        }
        if (checkPhoneNumber() == null) {
            AppAlertDialog.showMessage(this, "Lutfen gecerli telefon numarasi giriniz");
            return;
        }
        if(TextUtils.isEmpty(edt_date.getText())){
            AppAlertDialog.showMessage(this, "Lutfen tahmini yada kesin dugun tarihi seciniz");
            return;
        }
        if (!emailString.equals(user.getAccessToken().getUsername())) {
            new AlertDialog.Builder(this)
                    .setTitle("Uyarı")
                    .setMessage("E-mail değişikliği sonucu hesabınıza tekrar giriş yapmanız gerekmektedir")
                    .setPositiveButton("Tamam", (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                        sendUpdateRequest(true);
                    }).setNegativeButton("Iptal", (dialogInterface, i) -> {
                dialogInterface.dismiss();
            }).create().show();
        } else {
            sendUpdateRequest(false);
        }
    }


    void sendUpdateRequest(boolean logout) {
        showDialog();
        HashMap<Object, Object> hashMap = new HashMap<>();
        hashMap.put("coupleId", user.getUser().getId());
        hashMap.put("email", edt_mail.getText().toString());
        hashMap.put("name", String.valueOf(edt_name.getText()));
        hashMap.put("partnerName", String.valueOf(edt_partner_name.getText()));
        hashMap.put("partnerEmail", String.valueOf(edt_partner_mail.getText()));

        hashMap.put("phone", checkPhoneNumber());
        hashMap.put("isWeddingDateKnown", checkbox.isChecked());
        if (!edt_date.getText().toString().isEmpty() && checkbox.isChecked()) {
            if (weddingDate != null)
                hashMap.put("weddingDate", getDateForServer(weddingDate) + weddingTime);
        } else if (weddingDate != null) {
            String[] weddingDateInfo = weddingDate.split("/");
            hashMap.put("weddingDateMonth", weddingDateInfo[0]);
            hashMap.put("weddingDateYear", weddingDateInfo[1]);
        }


        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.editProfile(user.getAccessToken().getToken(),
                hashMap).enqueue(
                new Callback<SuccessModel>() {
                    @Override
                    public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                        if (response != null && response.body() != null) {
                            if (response.body().success) {
                                if (logout) {
                                    dismissDialog();
                                    logout();
                                } else {
                                    getProfile(false);
                                }
                            } else {
                                dismissDialog();
                                AppAlertDialog.showMessage(ProfileActivity.this,
                                        getString(R.string.failure_message));
                            }
                        } else {
                            dismissDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<SuccessModel> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(ProfileActivity.this,
                                getString(R.string.failure_message));
                    }
                }
        );
    }

    private void logout() {
        deleteToken(this);
        startActivity(new Intent(this, FirstActivity.class));
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String day = "" + dayOfMonth;
        if (dayOfMonth < 10)
            day = "0" + dayOfMonth;
        String month = "";
        if (++monthOfYear < 10)
            month = "0" + monthOfYear;
        else {
            month = "" + monthOfYear;
        }
        weddingDate = day + "/" + month + "/" + year;
        edt_date.setText(getDateForUser(weddingDate + weddingTime, MONTH_NAME));
        setCheckboxListener(true);
        checkbox.setChecked(true);
        setCheckboxListener(false);
    }

    void setCheckboxListener(boolean clear) {
        checkbox.setOnCheckedChangeListener(clear ? null : (compoundButton, b) -> {
            if (b) {
                showDatePickerDialog();
            } else {
                edt_date.setText("");
            }
        });
    }

    String checkPhoneNumber() {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber validPhoneNumber = phoneUtil.parse(edt_phone.getText().toString().replace(" ", ""), "TR");
            if (phoneUtil.isValidNumber(validPhoneNumber)) {
                String phonenumber = phoneUtil.format(validPhoneNumber, PhoneNumberUtil.PhoneNumberFormat.NATIONAL);
                return "009" + (phonenumber.replaceAll(" ", ""));
            } else
                return null;
        } catch (NumberParseException ex) {
            return null;
        }

    }


}
