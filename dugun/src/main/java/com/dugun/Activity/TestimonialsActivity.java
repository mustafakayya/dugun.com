package com.dugun.Activity;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.dugun.Adapter.TestimonialAdapter;
import com.dugun.Model.TestimonialModel;
import com.dugun.R;

import java.util.ArrayList;

/**
 * Created by mustafakaya on 12/2/17.
 */

public class TestimonialsActivity extends BaseActivity {

    RecyclerView recyclerView;

    public static final String TESTIMONIAL_KEY = "testimonial.key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testimonials);
        setToolbarGeneral("YORUMLAR");
        recyclerView = findViewById(R.id.recyclerView_aTestimonials);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.addItemDecoration(new DividerItemDecoration(context,DividerItemDecoration.VERTICAL));
        ArrayList<TestimonialModel> testimonialModels = getIntent().getParcelableArrayListExtra(TESTIMONIAL_KEY);
        recyclerView.setAdapter(new TestimonialAdapter(testimonialModels));
    }
}
