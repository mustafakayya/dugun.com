package com.dugun.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.annimon.stream.IntStream;
import com.annimon.stream.Stream;
import com.dugun.Adapter.ConceptAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.ConceptModel;
import com.dugun.Model.SuccessModel;
import com.dugun.R;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Others.Utils.isNetworkAvailable;

public class ConceptActivity extends BaseActivity {

    private TextView selectedConcepts, selectedConceptCount;
    private Button save;
    private GridView gridView;
    private ConceptAdapter adapter;
    private ConceptModel concept;
    private String TAG = "ConceptActivity";
    int selectedConceptSize;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_concept);
        setToolbarGeneral(getResources().getString(R.string.title_concept));

        selectedConcepts = (TextView) findViewById(R.id.selected_concepts);
        selectedConceptCount = (TextView) findViewById(R.id.selected_concept_count);
        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(view -> addConcepts());
        gridView = (GridView) findViewById(R.id.list_concepts);

        getConcepts();

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                boolean isNewSelect = !concept.getFormOptions().get(position).isSelected();

                if(isNewSelect){
                    if(selectedConceptSize < 3 ){
                        concept.getFormOptions().get(position).setSelected(true);
                        adapter.notifyDataSetChanged();
                        writeSelectedConcepts();
                    }else{
                        Toast.makeText(appCompatActivity, "En fazla 3 konsept seçebilirsiniz", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    concept.getFormOptions().get(position).setSelected(false);
                    adapter.notifyDataSetChanged();
                    writeSelectedConcepts();
                }


            }
        });

    }

    private void addConcepts() {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();

        Object[] types = Stream.of(concept.getFormOptions()).filter(value -> value.isSelected()).flatMap(formOptionsModel -> Stream.of(String.valueOf(formOptionsModel.getOptionValue()))).toArray();
        HashMap<String, Object[]> hashMap = new HashMap<>();
        hashMap.put("weddingPlaceType", types);
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.updateConcepts(user.getAccessToken().getToken(), user.getUser().getId(),hashMap).enqueue(
                new Callback<SuccessModel>() {
                    @Override
                    public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                        dismissDialog();
                        if (response.isSuccessful()) {
                            int[] weddingTypes = Stream.of(concept.getFormOptions()).filter(value -> value.isSelected()).mapToInt(formOptionsModel -> formOptionsModel.getOptionValue()).toArray();
                            user.getUser().setWeddingPlaceType(weddingTypes);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<SuccessModel> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(ConceptActivity.this,
                                getString(R.string.failure_message));
                    }
                }
        );
    }


    private void getConcepts() {
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getConcepts(user.getAccessToken().getToken(), 139).enqueue(
                new Callback<ConceptModel>() {
                    @Override
                    public void onResponse(Call<ConceptModel> call, Response<ConceptModel> response) {
                        dismissDialog();
                        if (response!=null && response.isSuccessful()) {
                            concept = response.body();

                            for (int i = 0; i < concept.getFormOptions().size(); i++) {
                                for (int j = 0; j < user.getUser().getWeddingPlaceType().length; j++) {
                                    if (user.getUser().getWeddingPlaceType()[j] == concept.getFormOptions().get(i).getOptionValue())
                                        concept.getFormOptions().get(i).setSelected(true);
                                }
                            }

                            adapter = new ConceptAdapter(getApplicationContext(), concept);
                            gridView.setAdapter(adapter);
                            writeSelectedConcepts();
                        }else{
                            AppAlertDialog.showMessage(ConceptActivity.this,
                                    getString(R.string.failure_message));
                        }
                    }

                    @Override
                    public void onFailure(Call<ConceptModel> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(ConceptActivity.this,
                                getString(R.string.failure_message));
                    }
                }
        );
    }

    private void writeSelectedConcepts() {
        String a = "";
        selectedConceptSize = 0;
        for (int i = 0; i < concept.getFormOptions().size(); i++) {
            if (concept.getFormOptions().get(i).isSelected()) {
                if (a.equals("")) {
                    a = concept.getFormOptions().get(i).getOptionText();
                } else {
                    a = a + ", " + concept.getFormOptions().get(i).getOptionText();
                }
                selectedConceptSize ++;
            }
        }
        selectedConcepts.setText(a);
        selectedConceptCount.setText(selectedConceptSize+"");
    }


}
