package com.dugun.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.SuccessModel;
import com.dugun.Others.Utils;
import com.dugun.R;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Others.Utils.isNetworkAvailable;

public class PasswordActivity extends BaseActivity {

    private EditText oldPass, newPass, newPassAgain;
    private Button btn_update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        setToolbarGeneral(getResources().getString(R.string.title_password));


        oldPass = (EditText) findViewById(R.id.old_pass);
        newPass = (EditText) findViewById(R.id.new_pass);
        newPassAgain = (EditText) findViewById(R.id.new_pass_again);
        btn_update = (Button) findViewById(R.id.btn_update);

//      Log.d ("OldPass", Utils.getPassword (getApplicationContext ()));

        oldPass.setTransformationMethod(new Utils.AsteriskPasswordTransformationMethod());
        newPass.setTransformationMethod(new Utils.AsteriskPasswordTransformationMethod());
        newPassAgain.setTransformationMethod(new Utils.AsteriskPasswordTransformationMethod());

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!oldPass.getText().toString().isEmpty() &&
                        !newPass.getText().toString().isEmpty() &&
                        !newPassAgain.getText().toString().isEmpty()) {
                    if (oldPass.getText().toString().equals(
                            Utils.getPassword(getApplicationContext()))) {

                        if(newPass.getText().length() > 5 && newPassAgain.getText().length()>5) {

                            if (newPass.getText().toString().equals(newPassAgain.getText().toString())) {
                                updatePassword();
                            } else {
                                AppAlertDialog.showMessage(PasswordActivity.this,
                                        getString(R.string.unmatched_new_pass));
                            }
                        }else{
                            AppAlertDialog.showMessage(PasswordActivity.this,
                                    getString(R.string.password_length_error));
                        }
                    } else {
                        AppAlertDialog.showMessage(PasswordActivity.this,
                                getString(R.string.incorrect_old_pass));
                    }
                } else {
                    AppAlertDialog.showMessage(PasswordActivity.this,
                            getString(R.string.required_field));
                }
            }
        });

    }

    private void updatePassword() {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }




        showDialog();
        HashMap<Object, Object> hashMap = new HashMap<>();
        hashMap.put("oldPassword", oldPass.getText().toString());
        hashMap.put("newPassword", newPass.getText().toString());
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.setPassword(user.getAccessToken().getToken(), hashMap).enqueue(
                new Callback<SuccessModel>() {
                    @Override
                    public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                        dismissDialog();
                        if (response.body().success) {
                            Utils.savePassword(getApplicationContext(),
                                    newPass.getText().toString());
                            showToast("Şifreniz değiştirildi");
                            finish();
                        } else {
                            AppAlertDialog.showMessage(PasswordActivity.this,
                                    getString(R.string.failure_message));
                        }
                    }

                    @Override
                    public void onFailure(Call<SuccessModel> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(PasswordActivity.this,
                                getString(R.string.failure_message));
                    }
                }
        );

    }

}
