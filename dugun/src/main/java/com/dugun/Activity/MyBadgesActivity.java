package com.dugun.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.dugun.Adapter.BadgeAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.BadgeModel;
import com.dugun.Model.BadgesModel;
import com.dugun.R;
import com.dugun.badges.BadgeDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Others.Utils.isNetworkAvailable;

public class MyBadgesActivity extends BaseActivity {

    private List<BadgeModel> badgeList;
    private BadgeAdapter adapter;
    private GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_badges);

        setToolbarGeneral(
                getResources().getString(R.string.title_my_badges) );

        gridView = (GridView) findViewById(R.id.list_badge);

        getBadges();
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                BadgeModel badgeModel = badgeList.get(i);
                BadgeDialog fragment = BadgeDialog.newInstance(badgeModel);
                fragment.show(getSupportFragmentManager(), "badgeFragment");
            }
        });

    }

    private void getBadges() {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getBadges(user.getAccessToken().getToken()).enqueue(
                new Callback<BadgesModel>() {
                    @Override
                    public void onResponse(Call<BadgesModel> call, Response<BadgesModel> response) {
                        dismissDialog();
                        if (response.isSuccessful()) {

                            badgeList = new ArrayList<>();

                            for (int i = 0; i < response.body().getData().size(); i++) {
                                BadgeModel badge = response.body().getData().get(i).getBadge();
                                badge.setEarned(true);
                                badgeList.add(badge);
                            }
                            for (int i = 0; i < response.body().getNotEarnedBadges().size(); i++) {
                                BadgeModel badge = response.body().getNotEarnedBadges().get(i);
                                badge.setEarned(false);
                                badgeList.add(badge);
                            }

                            adapter = new BadgeAdapter(getApplicationContext(), badgeList);
                            gridView.setAdapter(adapter);

                        } else {
                            AppAlertDialog.showMessage(MyBadgesActivity.this,
                                    getString(R.string.failure_message));
                        }
                    }

                    @Override
                    public void onFailure(Call<BadgesModel> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(MyBadgesActivity.this,
                                getString(R.string.failure_message));
                    }
                }
        );

    }
}
