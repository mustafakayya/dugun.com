package com.dugun.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.GridView;

import com.dugun.Adapter.LeadsAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.CategoryModel;
import com.dugun.Model.LeadsModel;
import com.dugun.Model.ProviderCategory;
import com.dugun.R;
import com.dugun.Wrapper.LeadsWrapper;
import com.dugun.badges.BadgeTask;
import com.dugun.badges.BadgeType;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dugun.Others.Utils.isNetworkAvailable;

public class LeadsListActivity extends BaseActivity {

    private GridView gridView;
    private LeadsAdapter adapter;
    private ArrayList<LeadsModel> leadsList;
    private CategoryModel category;

    final int LEAD_REQUEST_CODE = 33;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leads_list);
        setToolbarGeneral("Tekliflerim");

        gridView = (GridView) findViewById(R.id.gridView);

        leadsList = getIntent().getParcelableArrayListExtra("leads");
        if (leadsList != null) {
            if (leadsList.size() > 0)
                category = leadsList.get(0).getProvider().getCategory();
            setDataOnView();
        } else {
            category = getIntent().getParcelableExtra("category");
            getLeads();
        }

    }


    private void getLeads() {
        if (!isNetworkAvailable(this)) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        int categoryId = category.getProviderCategory() !=null ? category.getProviderCategory().getId() : category.getId();
        apiInterface.getLeadsByCatId(user.getAccessToken().getToken(),categoryId).enqueue(
                new Callback<LeadsWrapper>() {
                    @Override
                    public void onResponse(Call<LeadsWrapper> call, Response<LeadsWrapper> response) {
                        dismissDialog();

                        if(response != null && response.isSuccessful())
                            leadsList = response.body().getInfoRequests();
                        else
                            leadsList = new ArrayList<>();
                        setDataOnView();
                    }

                    @Override
                    public void onFailure(Call<LeadsWrapper> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(LeadsListActivity.this,
                                getString(R.string.failure_message));
                    }
                }
        );
    }

    private void setDataOnView() {
        setToolbarGeneral(category.getName() + " Tekliflerim (" + leadsList.size() + ")");
        adapter = new LeadsAdapter(this, leadsList, new LeadsAdapter.LeadsAdapterInterface() {
            @Override
            public void onDealClicked(LeadsModel leadsModel) {
                ProviderCategory categoryModelInfo = new ProviderCategory();
                categoryModelInfo.setId(category.getId());
                category.setProviderCategory(categoryModelInfo);
                Intent i = new Intent(context, TodoFinishActivity.class)
                        .putExtra("provider", leadsModel.getProvider())
                        .putExtra("category",category);
                startActivityForResult(i, LEAD_REQUEST_CODE);
            }

            @Override
            public void onDetailClicked(LeadsModel leadsModel) {
                Intent i = new Intent(context, NotesActivity.class);
                i.putExtra("leadId", leadsModel.getId());
                i.putExtra("providerName", leadsModel.getProvider().getName());
                startActivity(i);
            }
        });
        gridView.setAdapter(adapter);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LEAD_REQUEST_CODE) {
            getLeads();
            checkProviderBadge();
        }
    }


    private void checkProviderBadge() {
        new BadgeTask(getApplicationContext(), BadgeType.Provider,getSupportFragmentManager()).execute();
    }
}
