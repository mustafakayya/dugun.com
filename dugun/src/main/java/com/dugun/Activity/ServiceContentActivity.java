package com.dugun.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.dugun.Adapter.ServiceContentAdapter;
import com.dugun.Model.ServiceContentModel;
import com.dugun.R;

import java.util.ArrayList;

public class ServiceContentActivity extends BaseActivity {

   private GridView gridView;


   @Override
   protected void onCreate (Bundle savedInstanceState) {
      super.onCreate (savedInstanceState);
      setContentView (R.layout.activity_service_content);
      setToolbarGeneral ("Hizmet İçeriği");

      gridView = (GridView) findViewById (R.id.list_contents);


      final ArrayList<ServiceContentModel> serviceContentList = new ArrayList<> ();
      serviceContentList.add (
              new ServiceContentModel (1, "Yemek Servisi", false));
      serviceContentList.add (
              new ServiceContentModel (1, "Aynı Anda Birden Fazla Düğün", false));
      serviceContentList.add (
              new ServiceContentModel (1, "Catering Firması Getirme İmkanı", false));
      serviceContentList.add (
              new ServiceContentModel (1, "Engelli Girişi", false));

      final ServiceContentAdapter adapter = new ServiceContentAdapter (getApplicationContext (),
                                                                       serviceContentList);
      gridView.setAdapter (adapter);

      gridView.setOnItemClickListener (new AdapterView.OnItemClickListener () {
         @Override
         public void onItemClick (AdapterView<?> parent, View view, int position, long id) {
            if (serviceContentList.get (position).getStatus ()) {
               serviceContentList.get (position).setStatus (false);
            } else {
               serviceContentList.get (position).setStatus (true);
            }
            adapter.notifyDataSetChanged ();
         }
      });


   }

}
