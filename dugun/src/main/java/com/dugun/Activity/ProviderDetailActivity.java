package com.dugun.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dugun.Adapter.ProviderDetailFeaturesAdapter;
import com.dugun.Adapter.ProviderDetailGeneralAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Api.GeneralVariables;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.FormDataModel;
import com.dugun.Model.PhoneModel;
import com.dugun.Model.ProviderModel2;
import com.dugun.Model.SuccessModel;
import com.dugun.Model.TestimonialModel;
import com.dugun.Others.CustomScrollView;
import com.dugun.Others.Utils;
import com.dugun.R;
import com.dugun.Wrapper.FormDataWrapper;
import com.dugun.Wrapper.ProviderWrapper;
import com.dugun.Wrapper.TestimonialWrapper;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Api.GeneralVariables.MONTH_NAME;
import static com.dugun.Others.Utils.dpToPx;
import static com.dugun.Others.Utils.getMapUrl;
import static com.dugun.Others.Utils.isNetworkAvailable;

public class ProviderDetailActivity extends BaseActivity implements View.OnClickListener {

    private TextView providerName, providerTel, imgCount, videoCount, reviewCount;
    private ImageView img, mailProvider, likeImg;
    private LinearLayout like, share, callProvider,commentContainer;
    private GridView grdGenerals, grdFeatures;
    private WebView webView;
    private CustomScrollView scroll;
    private ProviderModel2 provider;
    private Button gallery,btnShowTestimonials;
    ArrayList<FormDataModel> features;
    private String TAG = "ProviderDetailActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_detail);
        setToolbarGeneral("Firma Detay");

        providerName = (TextView) findViewById(R.id.provider_name);
        providerTel = (TextView) findViewById(R.id.provider_tel);
        btnShowTestimonials = (Button)findViewById(R.id.btnShowTestimonials);
        img = (ImageView) findViewById(R.id.img);
        likeImg = (ImageView) findViewById(R.id.like_img);
        like = (LinearLayout) findViewById(R.id.like);
        like.setOnClickListener(this);
        share = (LinearLayout) findViewById(R.id.share);
        share.setOnClickListener(this);
        callProvider = (LinearLayout) findViewById(R.id.call_provider);
        callProvider.setOnClickListener(this);
        mailProvider = (ImageView) findViewById(R.id.mail_provider);
        mailProvider.setOnClickListener(this);
        webView = (WebView) findViewById(R.id.webView);
        scroll = (CustomScrollView) findViewById(R.id.scroll);
        gallery = (Button) findViewById(R.id.gallery);
        gallery.setOnClickListener(this);
        imgCount = (TextView) findViewById(R.id.img_count);
        videoCount = (TextView) findViewById(R.id.video_count);
        reviewCount = (TextView) findViewById(R.id.comment_count);
        grdGenerals = (GridView) findViewById(R.id.grid_generals);
        grdFeatures = (GridView) findViewById(R.id.grid_features);
        commentContainer = (LinearLayout)findViewById(R.id.commentContainer);


        grdFeatures.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(ProviderDetailActivity.this,
                        ProviderDetailFeatureActivity.class);
                if (features.get(position).getGroupName().equals("Kapasite")) {
                    i.putExtra("type", 1);
                    i.putExtra("data", features.get(position));
                } else if (features.get(position).getGroupName().equals("İletişim")) {
                    i.putExtra("type", 1);
                    i.putExtra("data", features.get(position));
                } else if (features.get(position).getGroupName().equals("Genel")) {
                    i.putExtra("type", 1);
                    i.putExtra("data", features.get(position));
                } else if (features.get(position).getGroupName().equals("Özellik")) {
                    i.putExtra("type", 2);
                    i.putExtra("data", features.get(position));
                } else if (features.get(position).getGroupName().equals("Sorular")) {
                    i.putExtra("type", 3);
                    i.putExtra("data", features.get(position));
                }
                startActivity(i);
            }
        });


        provider = getIntent().getParcelableExtra("provider");
        if (provider == null) {
            int providerId = getIntent().getIntExtra("providerId", 0);
            int categoryId = getIntent().getIntExtra("categoryId", 0);
            int cityId = getIntent().getIntExtra("cityId", 0);
            getProviderDetail(providerId, categoryId, cityId);
        } else {
            setDataToViews();
        }

    }

    private void setDataToViews() {
        setToolbarGeneral(provider.getName());
        getProviderFormData();
        imgCount.setText(String.valueOf(provider.getImageCount()));
        videoCount.setText(String.valueOf(provider.getVideoCount()));
        reviewCount.setText(String.valueOf(provider.getReviewCount()));
        providerName.setText(provider.getName());
        provider.setLiked(GeneralVariables.isLiked(provider.getProvider().getId(),false));
        if (provider.isLiked())
            likeImg.setImageResource(R.drawable.ic_favorieklebig2);
        else
            likeImg.setImageResource(R.drawable.ic_favorieklebig1);

        if(provider.getReviewCount()>0)
            fetchReviews();


        getPhone();
        Glide.with(this).load(provider.getImageUrl()).into(img);
        if (provider.getMapLng() != null)
            setWebview(provider.getMapLat(),
                    provider.getMapLng());
    }

    private void fetchReviews() {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        HashMap<String,Object> queryMap = new HashMap<>();
        queryMap.put("page",1);
        queryMap.put("limit",100);
        queryMap.put("providerIds[]",provider.getProvider().getId());
        queryMap.put("approvalStatus",1);
        apiInterface.getTestimonials(user.getAccessToken().getToken(),queryMap).enqueue(new Callback<TestimonialWrapper>() {
            @Override
            public void onResponse(Call<TestimonialWrapper> call, Response<TestimonialWrapper> response) {
                    if(response!=null && response.isSuccessful()){
                        commentContainer.setVisibility(View.VISIBLE);
                        TestimonialWrapper testimonialWrapper = response.body();
                        showTestimonial(testimonialWrapper.getData().get(0));
                        btnShowTestimonials.setVisibility(testimonialWrapper.getData().size() > 1 ? View.VISIBLE : View.GONE );
                        btnShowTestimonials.setOnClickListener(view -> {
                                Intent intent = new Intent(ProviderDetailActivity.this,TestimonialsActivity.class);
                                intent.putExtra(TestimonialsActivity.TESTIMONIAL_KEY,testimonialWrapper.getData());
                                startActivity(intent);
                        });
                    }else{
                        commentContainer.setVisibility(View.GONE);
                    }
            }

            @Override
            public void onFailure(Call<TestimonialWrapper> call, Throwable t) {
                commentContainer.setVisibility(View.GONE);
            }
        });

    }


    void showTestimonial(TestimonialModel testimonialModel){
        TextView lblAvatar = findViewById(R.id.lblAvatar_testimonialList);
        TextView lblCoupleName = findViewById(R.id.lblCoupleName_testimonialList);
        TextView lblDate = findViewById(R.id.lblDate_testimonialList);
        TextView lblReview = findViewById(R.id.lblReview_testimonialList);

        lblAvatar.setText(testimonialModel.getAvatarName());
        lblCoupleName.setText(testimonialModel.getCoupleName());
        lblReview.setText(testimonialModel.getTestimonial());
        if(!TextUtils.isEmpty(testimonialModel.getOrganizationDate()))
            lblDate.setText("Dugun Tarihi : " + Utils.getDateForUser(testimonialModel.getOrganizationDate(),MONTH_NAME));
        else
            lblDate.setText("Yorum Tarihi : " +Utils.getDateForUser(testimonialModel.getCreatedAt(),MONTH_NAME));
    }

    private void getProviderDetail(int providerId, int categoryId, int cityId) {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        Map<String, Integer> data = new HashMap<>();
        data.put("providerIds[]", providerId);
        data.put("categoryId", categoryId);
        data.put("cityId", cityId);
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getAllProvidersById(user.getAccessToken().getToken(),
                data).enqueue(
                new Callback<ProviderWrapper>() {
                    @Override
                    public void onResponse(Call<ProviderWrapper> call, Response<ProviderWrapper> response) {
                        dismissDialog();
                        if (response != null & response.body() != null) {
                            if (response.body().getProviders().size() > 0) {
                                provider = response.body().getProviders().get(0);
                                setDataToViews();
                            } else {
                                showToast("Veri alınamadı!");
                                onBackPressed();
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<ProviderWrapper> call, Throwable t) {
                        dismissDialog();
                        Log.d(TAG, "onFailure: getAllProvidersById() " + t.getMessage());
                        AppAlertDialog.showMessage(ProviderDetailActivity.this,
                                getString(R.string.failure_message));
                    }

                }
        );
    }


    private void getProviderFormData() {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getProviderFormData(user.getAccessToken().getToken(),
                provider.getProvider().getId()).enqueue(
                new Callback<FormDataWrapper>() {
                    @Override
                    public void onResponse(Call<FormDataWrapper> call, Response<FormDataWrapper> response) {
                        dismissDialog();
                        if (response != null & response.body() != null) {
                            if (response.body().getFormDataList().size() > 0) {
                                FormDataModel generals = new FormDataModel();
                                features = new ArrayList<>();
                                for (int i = 0; i < response.body().getFormDataList().size(); i++) {
                                    if (response.body().getFormDataList().get(i).getGroupId() == -1) {
                                        generals = response.body().getFormDataList().get(i);
                                    } else {
                                        features.add(response.body().getFormDataList().get(i));
                                    }
                                }
                                setGeneralInfo(generals);
                                setFeatures();

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<FormDataWrapper> call, Throwable t) {
                        dismissDialog();
                        Log.d(TAG, "onFailure: getProviderFormData() " + t.getMessage());
                        AppAlertDialog.showMessage(ProviderDetailActivity.this,
                                getString(R.string.failure_message));
                    }

                }
        );
    }

    private void setGeneralInfo(FormDataModel generals) {
        grdGenerals.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                ((generals.getData().size() + 1) / 2) * dpToPx(60)));
        grdGenerals.setAdapter(
                new ProviderDetailGeneralAdapter(getApplicationContext(), generals));
    }

    private void setFeatures() {
        grdFeatures.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                (features.size()) * dpToPx(55)));
        grdFeatures.setAdapter(
                new ProviderDetailFeaturesAdapter(getApplicationContext(), features));
    }

    private void setWebview(String lat, String lng) {
        if (lat != null && lng != null) {
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setSupportZoom(true);
            webView.setWebViewClient(new WebViewClient());
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadData(getMapUrl(lat, lng), "text/html; charset=utf-8", "UTF-8");
            webView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    final int action = event.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            scroll.setEnableScrolling(false);
                            break;
                        case MotionEvent.ACTION_UP:
                            scroll.setEnableScrolling(true);
                            break;
                    }
                    return false;
                }
            });
        } else {
            webView.setVisibility(View.GONE);
        }
    }

    private void share(int providerId) {
        String url = "https://dugun.com/redirect/provider/" + providerId;
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.like):
                if (provider.isLiked()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ProviderDetailActivity.this);
                    builder.setTitle("Emin misiniz?");
                    builder.setPositiveButton("Evet", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            setLike(true);
                            dialog.dismiss();
                        }
                    });
                    builder.setNegativeButton("Hayır", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    setLike(false);
                }
                break;
            case (R.id.share):
                share(provider.getId());
                break;
            case (R.id.call_provider):
                try {
                    Intent dialIntent = new Intent(Intent.ACTION_DIAL);
                    dialIntent.setData(Uri.parse("tel:" + providerTel.getText().toString()));
                    startActivity(dialIntent);
                } catch (Exception ignored) {
                }
                break;
            case (R.id.mail_provider):
                Intent i = new Intent(ProviderDetailActivity.this, LeadsNewActivity.class);
                i.putExtra("providerId", provider.getProvider().getId());
                i.putExtra("providerName", provider.getName());
                startActivity(i);
                break;
            case (R.id.gallery):
                Intent is = new Intent(ProviderDetailActivity.this, ModelDetailGalleryActivity.class);
                is.putExtra("provider", provider);
                is.putExtra("telephone", providerTel.getText().toString());
                startActivity(is);
                break;
        }
    }

    private void setLike(boolean isLiked) {
        if (!isNetworkAvailable(ProviderDetailActivity.this)) {
            AppAlertDialog.showMessage(ProviderDetailActivity.this,
                    getString(R.string.connection_failed));
            return;
        }
        showDialog();
        HashMap<String, Object> data = new HashMap<>();
        data.put("itemType", "provider");
        data.put("itemId", provider.getProvider().getId());
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        if (isLiked) {
            try {
                apiInterface.deleteCoupleLikes(user.getAccessToken().getToken(),
                        user.getUser().getId(), data).enqueue(
                        new Callback<Object>() {
                            @Override
                            public void onResponse(Call<Object> call, Response<Object> response) {
                                dismissDialog();
                                GeneralVariables.removeFromLikes(provider.getProvider().id,false);
                                provider.setLiked(false);
                                likeImg.setImageResource(R.drawable.ic_favorieklebig1);
                            }

                            @Override
                            public void onFailure(Call<Object> call, Throwable t) {
                                dismissDialog();
                                Log.d(TAG, "onFailure: " + t.getMessage());
                                AppAlertDialog.showMessage(ProviderDetailActivity.this,
                                        getString(R.string.failure_message));
                            }

                        }
                );
            } catch (Exception f) {
                dismissDialog();
                provider.setLiked(false);
                likeImg.setImageResource(R.drawable.ic_favorieklebig1);
            }
        } else {
            apiInterface.addCoupleLikes(user.getAccessToken().getToken(),
                    user.getUser().getId(), data).enqueue(
                    new Callback<SuccessModel>() {
                        @Override
                        public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                            dismissDialog();
                            if (response != null & response.body() != null) {
                                provider.setLiked(true);
                                GeneralVariables.removeFromLikes(provider.getProvider().id,true);
                                likeImg.setImageResource(R.drawable.ic_favorieklebig2);
                            } else {
                                try {
                                    JSONObject jObjError = new JSONObject(
                                            response.errorBody().string());
                                    AppAlertDialog.showMessage(ProviderDetailActivity.this,
                                            jObjError.getString("message"));
                                } catch (Exception e) {
                                    AppAlertDialog.showMessage(ProviderDetailActivity.this,
                                            e.getMessage());
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<SuccessModel> call, Throwable t) {
                            dismissDialog();
                            Log.d(TAG, "onFailure: " + t.getMessage());
                            AppAlertDialog.showMessage(ProviderDetailActivity.this,
                                    getString(R.string.failure_message));
                        }

                    }
            );
        }

    }

    private void getPhone() {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getPhone(user.getAccessToken().getToken(),
                provider.getProvider().getId()).enqueue(
                new Callback<PhoneModel>() {
                    @Override
                    public void onResponse(Call<PhoneModel> call, Response<PhoneModel> response) {
                        dismissDialog();
                        if (response != null & response.body() != null) {
                            providerTel.setText(Utils.beautifyPhoneNumber(response.body().getData()));
                        } else {
                            providerTel.setText("");
                        }
                    }

                    @Override
                    public void onFailure(Call<PhoneModel> call, Throwable t) {
                        dismissDialog();
                        Log.d(TAG, "onFailure: " + t.getMessage());
                        AppAlertDialog.showMessage(ProviderDetailActivity.this,
                                getString(R.string.failure_message));
                    }

                }
        );
    }

}
