package com.dugun.Activity;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Fragment.CitySelectorFragment;
import com.dugun.Model.CityModel;
import com.dugun.Model.SuccessModel;
import com.dugun.Others.USER;
import com.dugun.R;
import com.dugun.Wrapper.CityWrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dugun.Others.Utils.isNetworkAvailable;
import static com.dugun.R.id.layout_select_city;

public class ChangeCityActivity extends BaseActivity implements CitySelectorFragment.CitySelectorDialogListener, View.OnClickListener {

    private List<CityModel> cityModelList = new ArrayList<>();
    private TextView txt_city;
    private CityModel selectedCityModel;
    private Button update;
    private LinearLayout select;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_city);
        setToolbarGeneral(getResources().getString(R.string.title_change_city));

        selectedCityModel = user.getUser().getCity();
        txt_city = (TextView) findViewById(R.id.txt_city);
        txt_city.setText(selectedCityModel.getName());
        select = (LinearLayout) findViewById(R.id.layout_select_city);
        select.setOnClickListener(this);
        update = (Button) findViewById(R.id.btn_update);
        update.setOnClickListener(this);

    }


    private void getCities() {
        if (isNetworkAvailable(getApplicationContext())) {
            showDialog();
            ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
            apiInterface.getCities().enqueue(new Callback<CityWrapper>() {
                @Override
                public void onResponse(Call<CityWrapper> call, Response<CityWrapper> response) {
                    dismissDialog();
                    cityModelList = response.body().getCities();
                    openCityList();
                }

                @Override
                public void onFailure(Call<CityWrapper> call, Throwable t) {
                    dismissDialog();
                    AppAlertDialog.showMessage(ChangeCityActivity.this,
                            getString(R.string.failure_message));
                }
            });
        } else
            showToast(getString(R.string.connection_failed));
    }

    private void updateCity() {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        HashMap<Object, Object> hashMap = new HashMap<>();
        hashMap.put("coupleId", user.getUser().getId());
        hashMap.put("cityId", selectedCityModel.getId());
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.editProfile(user.getAccessToken().getToken(),
                hashMap).enqueue(
                new Callback<SuccessModel>() {
                    @Override
                    public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                        dismissDialog();
                        if (response != null && response.isSuccessful() && response.body().success) {
                            USER.updateCity(ChangeCityActivity.this, selectedCityModel);
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.changes_successful),
                                    Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            AppAlertDialog.showMessage(ChangeCityActivity.this,
                                    getString(R.string.failure_message));
                        }
                    }


                    @Override
                    public void onFailure(Call<SuccessModel> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(ChangeCityActivity.this,
                                getString(R.string.failure_message));
                    }
                }
        );
    }

    private void openCityList() {
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < cityModelList.size(); i++) {
            list.add(cityModelList.get(i).getName());
        }
        DialogFragment fragment = CitySelectorFragment.newInstance(ChangeCityActivity.this, list);
        fragment.show(getSupportFragmentManager(), "filter_selector");
    }

    @Override
    public void onCitySelected(int i) {
        selectedCityModel = cityModelList.get(i);
        txt_city.setText(cityModelList.get(i).getName());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case layout_select_city:
                if (cityModelList != null && cityModelList.size() > 0) {
                    openCityList();
                } else {
                    getCities();
                }
                break;
            case R.id.btn_update:
                updateCity();
                break;
        }
    }
}
