package com.dugun.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;

import com.dugun.Others.MyVideoView;
import com.dugun.R;

import static com.dugun.Others.Utils.getToken;
import static com.dugun.R.id.btn_next;

public class FirstActivity extends BaseActivity implements View.OnClickListener {

   @Override
   protected void onCreate (Bundle savedInstanceState) {
      super.onCreate (savedInstanceState);
      setContentView (R.layout.activity_first);
//        prepareVideoView();
      startVideo ();
      Button btn_login = (Button) findViewById (R.id.btn_login);
      btn_login.setOnClickListener (this);
      Button btn_signup = (Button) findViewById (R.id.btn_next);
      btn_signup.setOnClickListener (this);

      if (getToken (context)) {
         startActivity (new Intent (FirstActivity.this, MainActivity.class));
         finish ();
      }

   }

   private void startVideo () {
      MyVideoView     videoView       = (MyVideoView) findViewById (R.id.videoView);
      MediaController mediaController = new MediaController (this);
      mediaController.setAnchorView (videoView);
      Uri uri = Uri.parse ("android.resource://" + getPackageName () + "/" + R.raw.sample);
      videoView.setMediaController (mediaController);
      videoView.setMediaController (null);
      videoView.setVideoURI (uri);
      videoView.requestFocus ();
      videoView.start ();
   }

   @Override
   protected void onResume () {
      super.onResume ();
      startVideo ();
   }

   @Override
   public void onClick (View view) {
      switch (view.getId ()) {
         case R.id.btn_login:
            startActivity (new Intent (FirstActivity.this, LoginActivity.class));
            finish ();
            break;
         case btn_next:
            startActivity (new Intent (FirstActivity.this, RegisterActivity.class));
            finish ();
            break;
      }
   }


//    private void prepareVideoView() {
//        mPreview = (SurfaceView) findViewById(R.id.surfaceView);
//        getWindow().setFormat(PixelFormat.UNKNOWN);
//        mPreview = (SurfaceView) findViewById(R.id.surfaceView);
//        holder = mPreview.getHolder();
//        holder.addCallback(this);
//        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
//        mMediaPlayer = new MediaPlayer();
//    }
//
//    @Override
//    public void surfaceCreated(SurfaceHolder surfaceHolder) {
//        mMediaPlayer.setDisplay(holder);
//        play();
//    }
//
//    @Override
//    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
//
//    }
//
//    @Override
//    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
//
//    }
//
//
//    public void play() {
//        try {
//            AssetFileDescriptor afd;
//            afd = getAssets().openFd("sample.mp4");
//            String path = "android.resource://" + getPackageName() + "/" + R.raw.sample;
//            mMediaPlayer.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(), afd.getLength());
//            mMediaPlayer.prepare();
//            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//            int videoWidth = mMediaPlayer.getVideoWidth();
//            int videoHeight = mMediaPlayer.getVideoHeight();
//            int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
//            android.view.ViewGroup.LayoutParams lp = mPreview.getLayoutParams();
//            lp.width = screenWidth;
//            lp.height = (int) (((float) videoHeight / (float) videoWidth) * (float) screenWidth);
////            mPreview.setLayoutParams(lp);
//            mMediaPlayer.setOnPreparedListener(FirstActivity.this);
//            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//
//        } catch (IllegalArgumentException e) {
//            e.printStackTrace();
//        } catch (IllegalStateException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    protected void onPause() {
//        super.onPause();
//        mMediaPlayer.release();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        releaseMediaPlayer();
//    }
//
//    private void releaseMediaPlayer() {
//        if (mMediaPlayer != null) {
//            mMediaPlayer.release();
//            mMediaPlayer = null;
//        }
//    }
//
//    @Override
//    public void onPrepared(MediaPlayer mediaPlayer) {
//        mMediaPlayer.start();
//    }

}
