package com.dugun.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.ResultIdModel;
import com.dugun.Model.SuccessModel;
import com.dugun.Model.TodoModel;
import com.dugun.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Api.GeneralVariables.FULL_DATE;
import static com.dugun.Api.GeneralVariables.MONTH_NAME;
import static com.dugun.Dialog.AppAlertDialog.showMessage;
import static com.dugun.Others.Utils.getDateForServer;
import static com.dugun.Others.Utils.getDateForUser;
import static com.dugun.Others.Utils.isNetworkAvailable;

public class TodoNewActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener {

   private TextView date;
   private EditText title, content;
   private LinearLayout calendar;
   private Button       save, delete;
   private TodoModel todo;
   private String weddingDate, weddingTime = "00:00";
   private int categoryId;
   private String TAG = "TodoNewActivity";


   @Override
   protected void onCreate (Bundle savedInstanceState) {
      super.onCreate (savedInstanceState);
      setContentView (R.layout.activity_todo_new);

      title = (EditText) findViewById (R.id.new_plan_title);
      content = (EditText) findViewById (R.id.new_plan_content);
      date = (TextView) findViewById (R.id.new_plan_date);
      calendar = (LinearLayout) findViewById (R.id.new_plan_calendar);
      save = (Button) findViewById (R.id.new_plan_save);
      delete = (Button) findViewById (R.id.plan_delete);


      todo = getIntent ().getParcelableExtra ("todo");
      if (todo == null) {
         setToolbarGeneral (getString (R.string.title_add_new_plan));
         delete.setVisibility (View.GONE);
         categoryId = getIntent ().getIntExtra ("categoryId", 0);
         Log.d (TAG, "Current Category Id = " + categoryId);
         title.setEnabled (true);
         content.setEnabled (true);
      } else {
         setToolbarGeneral (todo.getTitle ());
         delete.setVisibility (View.VISIBLE);
         title.setText (todo.getTitle ());
         content.setText (todo.getDescription ());
         if (todo.getNotifyAt () != null)
            date.setText (getDateForUser (todo.getNotifyAt (), FULL_DATE));

         if(todo.getActionType () == null){
            title.setEnabled (true);
            content.setEnabled (true);
         }else{
            title.setEnabled (false);
            content.setEnabled (false);
         }

      }

      calendar.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            showDatePickerDialog ();
         }
      });
      save.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            if (todo == null)
               addNewTodo ();
            else
               updateTodo ();
         }
      });
      delete.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            deleteTodo (todo.getId ());
         }
      });

   }

   private void addNewTodo () {
      if (!isNetworkAvailable (context)) {
         AppAlertDialog.showMessage (this, getString (R.string.connection_failed));
         return;
      }
      showDialog ();
      HashMap<Object, Object> hashMap = new HashMap<> ();
      hashMap.put ("title", title.getText ().toString ());
      hashMap.put ("description", content.getText ().toString ());
      HashMap<Object, Object> categoryMap = new HashMap<> ();
      categoryMap.put ("id", categoryId);
      hashMap.put ("category", categoryMap);
      if (weddingDate != null) {
         String dateForServer = getDateForServer (weddingDate);
         hashMap.put ("notifyAt", dateForServer + " " + weddingTime);
      }

      ApiInterface apiInterface = ApiClient.getApiClient ().create (ApiInterface.class);
      apiInterface.addTask (user.getAccessToken ().getToken (),
                            user.getUser ().getId (), hashMap).enqueue (
              new Callback<ResultIdModel> () {

                 @Override
                 public void onResponse (Call<ResultIdModel> call, Response<ResultIdModel> response) {
                    dismissDialog ();
                    if (response.isSuccessful ()) {
                       showToast ("Kaydedildi");
                       finish ();
                    } else {
                       AppAlertDialog.showMessage (TodoNewActivity.this,
                                                   String.valueOf (response.message ()));
                    }
                 }

                 @Override public void onFailure (Call<ResultIdModel> call, Throwable t) {
                    dismissDialog ();
                    Log.d (TAG, "addNewTodo () -> " + t.getMessage ());
                    AppAlertDialog.showMessage (TodoNewActivity.this,
                                                getString (R.string.failure_message));
                 }
              }
      );
   }

   private void updateTodo () {
      if (!isNetworkAvailable (context)) {
         AppAlertDialog.showMessage (this, getString (R.string.connection_failed));
         return;
      }
      showDialog ();
      HashMap<Object, Object> hashMap = new HashMap<> ();
      hashMap.put ("coupleId", user.getUser ().getId ());
      hashMap.put ("taskId", todo.getId ());
      hashMap.put ("title", title.getText ().toString ());
      hashMap.put ("description", content.getText ().toString ());
      if (weddingDate != null) {
         String dateForServer = getDateForServer (weddingDate);
         hashMap.put ("notifyAt", dateForServer + " " + weddingTime);
      }

      ApiInterface apiInterface = ApiClient.getApiClient ().create (ApiInterface.class);
      apiInterface.updateTask ( user.getAccessToken ().getToken (),
                               user.getUser ().getId (), todo.getId (), hashMap).enqueue (
              new Callback<SuccessModel> () {

                 @Override
                 public void onResponse (Call<SuccessModel> call, Response<SuccessModel> response) {
                    dismissDialog ();
                    if (response.isSuccessful ()) {
                       setToolbarGeneral (title.getText ().toString ());
                       showToast ("Güncellendi");

                       finish ();
                    } else {
                       AppAlertDialog.showMessage (TodoNewActivity.this,
                                                   String.valueOf (response.message ()));
                    }
                 }

                 @Override public void onFailure (Call<SuccessModel> call, Throwable t) {
                    dismissDialog ();
                    Log.d (TAG, "updateTodo () -> " + t.getMessage ());
                    AppAlertDialog.showMessage (TodoNewActivity.this,
                                                getString (R.string.failure_message));
                 }
              }
      );
   }

   private void deleteTodo (final int taskId) {
      if (!isNetworkAvailable (context)) {
         AppAlertDialog.showMessage (this, getString (R.string.connection_failed));
         return;
      }
      showDialog ();
      ApiInterface apiInterface = ApiClient.getApiClient ().create (ApiInterface.class);
      apiInterface.deleteTodo (user.getAccessToken ().getToken (),
                               user.getUser ().getId (), taskId).enqueue (
              new Callback<SuccessModel> () {
                 @Override
                 public void onResponse (Call<SuccessModel> call, Response<SuccessModel> response) {
                    dismissDialog ();
                    showToast ("Silindi");

                    finish ();
                 }

                 @Override public void onFailure (Call<SuccessModel> call, Throwable t) {
                    dismissDialog ();
                    Log.d (TAG, "getTodos () -> " + t.getMessage ());
                    showMessage (TodoNewActivity.this, getString (R.string.failure_message));
                 }

              }
      );
   }

   private void showDatePickerDialog () {
      Calendar cal = Calendar.getInstance ();
      DatePickerDialog dpd = DatePickerDialog.newInstance (
              this,
              cal.get (Calendar.YEAR),
              cal.get (Calendar.MONTH),
              cal.get (Calendar.DAY_OF_MONTH)
      );
      dpd.setMinDate (cal);
      dpd.setAccentColor (getResources ().getColor (R.color.colorAccent));
      dpd.show (getFragmentManager (), "Datepickerdialog");
   }

   private void showTimePickerDialog () {
      Calendar now = Calendar.getInstance ();
      TimePickerDialog dpd = TimePickerDialog.newInstance (
              this,
              now.get (Calendar.HOUR),
              now.get (Calendar.MINUTE),
              true
      );
      dpd.show (getFragmentManager (), "Timepickerdialog");
   }

   @Override
   public void onDateSet (DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

      String day   = (dayOfMonth < 10 ? "0" : "") + dayOfMonth;
      String month = (++monthOfYear < 10 ? "0" : "") + monthOfYear;

      weddingDate = day + "/" + month + "/" + year;
      date.setText (getDateForUser (weddingDate + " " + weddingTime, MONTH_NAME));
      showTimePickerDialog ();
   }

   @Override
   public void onTimeSet (RadialPickerLayout view, int hourOfDay, int minute, int second) {
      String hour = String.valueOf (hourOfDay);
      String min  = String.valueOf (minute);

      hour = (hour.length () == 1 ? "0" : "") + hour;
      min = (min.length () == 1 ? "0" : "") + min;

      weddingTime = hour + ":" + min;
      date.setText (getDateForUser (weddingDate + " " + weddingTime, MONTH_NAME));
   }

}
