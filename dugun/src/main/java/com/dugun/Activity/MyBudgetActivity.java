package com.dugun.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.dugun.Adapter.MyBudgetsAdapter;
import com.dugun.Model.MyBudgetModel;
import com.dugun.R;

import java.util.ArrayList;

public class MyBudgetActivity extends BaseActivity {

   private GridView         gridView;
   private MyBudgetsAdapter adapter;
   private Button           budgetButton;

   @Override
   protected void onCreate (Bundle savedInstanceState) {
      super.onCreate (savedInstanceState);
      setContentView (R.layout.activity_my_budget);
      setToolbarGeneral (getResources ().getString (R.string.title_my_budgets));

      gridView = (GridView) findViewById (R.id.list_budget);
      budgetButton = (Button) findViewById (R.id.budget_button);

      final ArrayList<MyBudgetModel> myBudgetsList = new ArrayList<> ();
      myBudgetsList.add (new MyBudgetModel (1, "Gelinlik", 14000, 7000));
      myBudgetsList.add (new MyBudgetModel (2, "Gelin Ayakkabısı", 1000, 0));

      int alloc = 0, spent = 0;
      for (int i = 0; i < myBudgetsList.size (); i++) {
         alloc = alloc + myBudgetsList.get (i).getAllocated ();
         spent = spent + myBudgetsList.get (i).getSpent ();
      }
      myBudgetsList.add (new MyBudgetModel (3, "Toplam", alloc, spent));


      adapter = new MyBudgetsAdapter (getApplicationContext (), myBudgetsList);
      gridView.setAdapter (adapter);


      gridView.setOnItemClickListener (new AdapterView.OnItemClickListener () {
         @Override
         public void onItemClick (AdapterView<?> parent, View view, int position, long id) {


         }
      });

      budgetButton.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            startActivity (new Intent (MyBudgetActivity.this, MyCategoriesActivity.class));
         }
      });

   }


}
