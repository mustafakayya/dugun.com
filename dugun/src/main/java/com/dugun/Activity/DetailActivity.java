package com.dugun.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.dugun.Adapter.ConceptAdapter;
import com.dugun.Adapter.DetailListAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Dialog.CameraGalleryDialog;
import com.dugun.Interface.CameraGalleryReturnListener;
import com.dugun.Model.ConceptModel;
import com.dugun.Model.DetailModel;
import com.dugun.Model.SuccessModel;
import com.dugun.Others.MyOnActivityResultClass;
import com.dugun.Others.USER;
import com.dugun.R;
import com.dugun.Wrapper.UserPhotosWrapper;
import com.dugun.badges.BadgeTask;
import com.dugun.badges.BadgeType;


import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Others.Utils.isNetworkAvailable;
import static com.dugun.R.id.cover_photo;

public class DetailActivity extends BaseActivity implements
        AdapterView.OnItemClickListener,
        View.OnClickListener, CameraGalleryReturnListener {

    private ImageView changePhoto, coverPhoto, settings, notitfications;
    private DetailListAdapter adapter;
    private ListView list_detail;
    private String todoCompleted, todoAll, badgesCompleted, badgesAll, infoReqCost, infoReqCount, likeCount;
    private String TAG = "DetailActivity";
    private ConceptModel concept;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        setToolbarGeneral(getString(R.string.title_details));


        settings = (ImageView) findViewById(R.id.img_settings);
        settings.setOnClickListener(this);
        notitfications = (ImageView) findViewById(R.id.img_settings);
        notitfications.setOnClickListener(this);
        changePhoto = (ImageView) findViewById(R.id.change_photo);
        changePhoto.setOnClickListener(this);
        coverPhoto = (ImageView) findViewById(cover_photo);
        list_detail = (ListView) findViewById(R.id.list_detail);
        list_detail.setOnItemClickListener(this);
        loadCoverPhoto();
//      if(getCoverImageFromDir()){
//         Picasso.with (getApplicationContext ())
//                 .load (new File (APP_DIR + COVER_DIR))
//                 .into (coverPhoto);
//      }else{
//         Picasso.with (getApplicationContext ())
//                 .load(R.drawable.sampleimage)
////                 .load (USER.getUser ().getCoverPhotoUrl ())
//                 .into (coverPhoto);
//      }
        todoCompleted = getIntent().getStringExtra("todo_completed");
        todoAll = getIntent().getStringExtra("todo_all");
        badgesCompleted = getIntent().getStringExtra("badges_completed");
        badgesAll = getIntent().getStringExtra("badges_all");
        infoReqCost = getIntent().getStringExtra("info_req_cost");
        infoReqCount = getIntent().getStringExtra("info_req_count");
        likeCount = getIntent().getStringExtra("like_count");


    }


    void loadCoverPhoto(){

        if (user.getUser().getUserPhotoUrl() != null) {
            Glide.with(this)
                    .load(user.getUser().getUserPhotoUrl())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            coverPhoto.setImageResource(R.drawable.ic_default_cover);
                            return true;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(coverPhoto);
        }else{
            coverPhoto.setImageResource(R.drawable.ic_default_cover);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        showBadge();
        if(concept==null){
            fetchConcepts();
        }else{
            refreshAdapter();
        }
    }

    private void fetchConcepts() {
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getConcepts(user.getAccessToken().getToken(), 139).enqueue(
                new Callback<ConceptModel>() {
                    @Override
                    public void onResponse(Call<ConceptModel> call, Response<ConceptModel> response) {
                        dismissDialog();
                        if (response.isSuccessful()) {
                            concept = response.body();
                            refreshAdapter();
                        }
                    }

                    @Override
                    public void onFailure(Call<ConceptModel> call, Throwable t) {
                        dismissDialog();
                        refreshAdapter();
                    }
                }
        );
    }

    private void refreshAdapter() {
        ArrayList<DetailModel> list = new ArrayList<>();
        list.add(new DetailModel(R.drawable.ic_adsoyadkadin,
                getString(R.string.detail_profile_settings),
                "İsimler, düğün tarihi, şifre, telefon…"));
        list.add(
                new DetailModel(R.drawable.password_icon, getString(R.string.detail_change_pass),
                        getString(R.string.detail_change_pass_hint)));
        list.add(new DetailModel(R.drawable.ic_mapicon, getString(R.string.detail_wedding_city),
                user.getUser().getCity().getName()));
        if(concept==null) {
            list.add(new DetailModel(R.drawable.ic_adsoyadkadin,
                    getString(R.string.detail_dream_wedding), ""));
        }else{
            list.add(new DetailModel(R.drawable.ic_adsoyadkadin,
                    getString(R.string.detail_dream_wedding), getSelectedConcepts()));
        }
        list.add(new DetailModel(R.drawable.ic_adsoyadkadin, getString(R.string.detail_favorites),
                likeCount + " Adet"));
        list.add(new DetailModel(R.drawable.ic_tekliflerim, getString(R.string.detail_bids),
                infoReqCount + " Teklif, " + infoReqCost + " TL"));
//      list.add (new DetailModel (R.drawable.ic_budget, getString (R.string.detail_budget),
//                                 "Kalan bütçemiz 23.500 TL"));
        list.add(new DetailModel(R.drawable.ic_planlarm, getString(R.string.detail_plans),
                todoCompleted + " / " + todoAll));
        list.add(new DetailModel(R.drawable.ic_badge, getString(R.string.detail_badge),
                badgesCompleted + " / " + badgesAll));
//      list.add (new DetailModel (R.drawable.ic_addfriend, getString (R.string.detail_share),
//                                 "2 davetli"));


        adapter = new DetailListAdapter(this, list);
        list_detail.setAdapter(adapter);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        switch (i) {
            case 0:
                startActivity(new Intent(this, ProfileActivity.class));
                break;
            case 1:
                startActivity(new Intent(this, PasswordActivity.class));
                break;
            case 2:
                startActivity(new Intent(this, ChangeCityActivity.class));
                break;
            case 3:
                startActivity(new Intent(this, ConceptActivity.class));
//            Toast.makeText (getApplicationContext (), "Servis bağlı değil",
//                            Toast.LENGTH_SHORT).show ();
                break;
            case 4:
                startActivity(new Intent(this, LikeCategoryActivity.class));
                break;
            case 5:
                startActivity(new Intent(this, LeadsCategoriesActivity.class));
                break;
//         case 6:
////            startActivity (new Intent (this, MyBudgetActivity.class));
//            Toast.makeText (getApplicationContext (), "Servis bağlı değil",
//                            Toast.LENGTH_SHORT).show ();
//            break;
            case 6:
                startActivity(new Intent(this, TodoCategoriesActivity.class));
                break;
            case 7:
                startActivity(new Intent(this, MyBadgesActivity.class));
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_settings:
                startActivity(new Intent(DetailActivity.this, SettingsActivity.class));
                break;
            case R.id.change_photo:
                new CameraGalleryDialog().show(getSupportFragmentManager(), "");
                break;
        }
    }


    private void sendFile(final File file, final Bitmap bitmap) {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        RequestBody rf = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("profile", file.getName(), rf);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "file");
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.uploadPhoto(user.getAccessToken().getToken(), name, body)
                .enqueue(new Callback<UserPhotosWrapper>() {
                             @Override
                             public void onResponse(Call<UserPhotosWrapper> call, Response<UserPhotosWrapper> response) {
                                 dismissDialog();
                                 if(response.isSuccessful()) {
                                     Toast.makeText(getApplicationContext(),
                                             "Kapak fotoğrafınız başarıyla değiştirildi",
                                             Toast.LENGTH_SHORT).show();
                                     Runnable r =
                                         ()-> {
                                             Glide.get(DetailActivity.this).clearDiskCache();
                                     };
                                     new Thread(r).start();
                                     Glide.get(DetailActivity.this).clearMemory();
                                     USER.updatePhoto(DetailActivity.this,response.body());
                                     coverPhoto.postDelayed(new Runnable() {
                                         @Override
                                         public void run() {
                                             loadCoverPhoto();
                                         }
                                     },200);

                                 }else{
                                     AppAlertDialog.showMessage(DetailActivity.this,
                                             getString(R.string.failure_message));
                                 }
                             }

                             @Override
                             public void onFailure(Call<UserPhotosWrapper> call, Throwable t) {
                                 dismissDialog();
                                 Log.d("PhotoUpdate", t.getMessage());
                                 AppAlertDialog.showMessage(DetailActivity.this,
                                         getString(R.string.failure_message));
                             }
                         }
                );
    }

    private void showBadge() {
        new BadgeTask(getApplicationContext(), BadgeType.ProfileComplete,getSupportFragmentManager()).execute();
    }


    @Override
    public void callBackCameraGallery(final String selectedImagePath, final Bitmap selectedImage) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_photo_result);

        Button ok = (Button) dialog.findViewById(R.id.DialogPhotoResultTamamButton);
        Button cancel = (Button) dialog.findViewById(R.id.DialogPhotoResultVazgecButton);
        ImageView image = (ImageView) dialog.findViewById(R.id.DialogPhotoResultResimImageView);
        image.setImageBitmap(selectedImage);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFile(new File(Uri.parse(selectedImagePath).getPath()), selectedImage);
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        new MyOnActivityResultClass().result(this, this, requestCode, resultCode, data);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
//      startActivity (new Intent (getApplicationContext (), MainActivity.class));
        finish();
    }


    private String getSelectedConcepts() {
        String selectedConcepts = "";


        for (int i = 0; i < concept.getFormOptions().size(); i++) {
            for (int j = 0; j < user.getUser().getWeddingPlaceType().length; j++) {
                if (user.getUser().getWeddingPlaceType()[j] == concept.getFormOptions().get(i).getOptionValue())
                    if (selectedConcepts.equals("")) {
                        selectedConcepts = concept.getFormOptions().get(i).getOptionText();
                    } else {
                        selectedConcepts = selectedConcepts + ", " + concept.getFormOptions().get(i).getOptionText();
                    }
            }
        }
        return selectedConcepts;
    }
}
