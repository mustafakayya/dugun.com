package com.dugun.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.dugun.R;
import com.gtomato.android.ui.transformer.LinearViewTransformer;
import com.gtomato.android.ui.widget.CarouselView;

/**
 * Created by ugurbasarir on 18/08/17.
 */

public class BrideDressCompanyPopupActivity extends Activity {


   @Override protected void onCreate (Bundle savedInstanceState) {
      super.onCreate (savedInstanceState);
      setContentView (R.layout.activity_bride_dress_company_popup);

      CarouselView carousel = (CarouselView) findViewById (R.id.carousel);


      carousel.setTransformer (new LinearViewTransformer ());
      carousel.setAdapter (new MyDataAdapter ());

      RelativeLayout detail = (RelativeLayout) findViewById (R.id.detail);
      detail.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            startActivity (
                    new Intent (BrideDressCompanyPopupActivity.this, ProviderDetailActivity.class));
         }
      });


   }

   public class ViewHolder extends RecyclerView.ViewHolder {

      private ImageView img;

      private ViewHolder (View itemLayoutView) {
         super (itemLayoutView);
         img = (ImageView) itemLayoutView.findViewById (R.id.image);
         img.setImageResource (R.drawable.ic_gelinlik);
      }

   }

   public class MyDataAdapter extends CarouselView.Adapter<ViewHolder> {

      @Override
      public ViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
         View view;
         view = LayoutInflater.from (parent.getContext ()).inflate (
                 R.layout.list_item_bride_dress_popup, parent, false);
         return new ViewHolder (view);
      }

      @Override
      public void onBindViewHolder (ViewHolder holder, int position) {
//         holder.getView().bind(position + 1);
      }

      @Override
      public int getItemCount () {
         return 10;
      }
   }

}
