package com.dugun.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dugun.Adapter.ProviderDetailFeatureAdapter;
import com.dugun.Model.FormDataModel;
import com.dugun.R;

public class ProviderDetailFeatureActivity extends BaseActivity {

   private TextView     title;
   private LinearLayout back;
   private GridView     gridView;
   private int          type;

   @Override
   protected void onCreate (Bundle savedInstanceState) {
      super.onCreate (savedInstanceState);
      setContentView (R.layout.activity_provider_detail_feature);

      type = getIntent ().getIntExtra ("type", -1);

      FormDataModel data = getIntent ().getParcelableExtra ("data");

      title = (TextView) findViewById (R.id.title);
      title.setText (data.getGroupName ());
      back = (LinearLayout) findViewById (R.id.back);
      back.setOnClickListener (new View.OnClickListener () {
         @Override public void onClick (View v) {
            onBackPressed ();
         }
      });
      gridView = (GridView) findViewById (R.id.gridView);
      if (type != -1)
         gridView.setAdapter (
                 new ProviderDetailFeatureAdapter (getApplicationContext (), data.getData (), type));

   }

}
