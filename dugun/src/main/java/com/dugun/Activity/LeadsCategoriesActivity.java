package com.dugun.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.dugun.Adapter.MakeCategoryAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.CategoryModel;
import com.dugun.Model.LeadsModel;
import com.dugun.R;
import com.dugun.Wrapper.LeadsWrapper;
import com.dugun.badges.BadgeTask;
import com.dugun.badges.BadgeType;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dugun.Others.Utils.isNetworkAvailable;

public class LeadsCategoriesActivity extends BaseActivity {

    private GridView gridView;
    private MakeCategoryAdapter adapter;
    private ArrayList<LeadsModel> leadsList;
    private ArrayList<CategoryModel> categories;
    private ArrayList<LeadsModel> leads;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leads_categories);
        setToolbarGeneral(getResources().getString(R.string.title_my_bids));


        gridView = (GridView) findViewById(R.id.gridView);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                leads = new ArrayList<>();
                for (int i = 0; i < leadsList.size(); i++) {
                    if (leadsList.get(i).getProvider().getCategory().getId() == categories.get(
                            position).getId()) {
                        leads.add(leadsList.get(i));
                    }
                }
                Intent i = new Intent(LeadsCategoriesActivity.this, LeadsListActivity.class);
                i.putParcelableArrayListExtra("leads", leads);
                startActivity(i);

            }
        });

        getLeads();

    }

    private void getLeads() {
        if (!isNetworkAvailable(context)) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getLeads(user.getAccessToken().getToken()).enqueue(
                new Callback<LeadsWrapper>() {
                    @Override
                    public void onResponse(Call<LeadsWrapper> call, Response<LeadsWrapper> response) {
                        dismissDialog();
                        leadsList = response.body().getInfoRequests();
                        setToolbarGeneral(getResources().getString(R.string.title_my_bids)
                                + " (" + leadsList.size() + ")");
                        groupLeads(leadsList);
                    }

                    @Override
                    public void onFailure(Call<LeadsWrapper> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(LeadsCategoriesActivity.this,
                                getString(R.string.failure_message));
                    }
                }
        );
    }

    private void groupLeads(ArrayList<LeadsModel> leadsList) {

        ArrayList ids = new ArrayList();
        categories = new ArrayList<>();

        for (int i = 0; i < leadsList.size(); i++) {
            int currentID = leadsList.get(i).getProvider().getCategory().getId();

            if (!ids.contains(currentID)) {

                ids.add(currentID);

                CategoryModel cat = new CategoryModel();
                cat.setId(currentID);
                cat.setName(leadsList.get(i).getProvider().getCategory().getName());
                cat.setIcon(leadsList.get(i).getProvider().getCategory().getIcon());

                for (int j = 0; j < leadsList.size(); j++) {
                    if (leadsList.get(j).getProvider().getCategory().getId() == currentID)
                        cat.setCount(cat.getCount() + 1);
                }

                categories.add(cat);
            }

        }

        adapter = new MakeCategoryAdapter(context, categories);
        gridView.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkLeadBadge();
    }



    private void checkLeadBadge() {
        new BadgeTask(getApplicationContext(), BadgeType.Lead,getSupportFragmentManager()).execute();
    }
}
