package com.dugun.Activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.CategoryModel;
import com.dugun.Model.IdModel;
import com.dugun.Model.ProviderModel2;
import com.dugun.R;
import com.dugun.Wrapper.ProviderWrapper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dugun.Others.Utils.isNetworkAvailable;

public class TodoFinishActivity extends BaseActivity {

    private ArrayList<ProviderModel2> providers;
    private AutoCompleteTextView filter;
    private ImageView clean, catImage;
    private Button save;
    private TextView providerTitle, priceTitle, title;
    private String filterArr[];
    private EditText edtPrice;

    private int selectedProviderId = 0;
    private ProviderModel2 provider;
    private CategoryModel categoryModel;
    private String TAG = "TodoFinishActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_finish);


        setToolbarGeneral("");
        save = (Button) findViewById(R.id.save);
        clean = (ImageView) findViewById(R.id.clean);
        title = (TextView) findViewById(R.id.content);
        catImage = (ImageView) findViewById(R.id.bagde);
        clean.setVisibility(View.GONE);
        filter = (AutoCompleteTextView) findViewById(R.id.filter);
        edtPrice = (EditText) findViewById(R.id.edtPrice);
        filter.setThreshold(1);
        providerTitle = (TextView) findViewById(R.id.provider_title);
        priceTitle = (TextView) findViewById(R.id.price_title);

        provider = getIntent().getParcelableExtra("provider");
        categoryModel = getIntent().getParcelableExtra("category");
        if (provider == null) {
            getProviders();
            filter.setEnabled(true);
        } else {
            filter.setEnabled(false);
            filter.setText(provider.getName());
            selectedProviderId = provider.getId();
        }

        if (categoryModel != null) {
            title.setText(categoryModel.getName());
            String iName = "cat_" + catImage.getId();
            int resId = getResources().getIdentifier(iName, "drawable", getPackageName());
            if (resId != 0)
                catImage.setImageResource(resId);
        }


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProviderId = 0;
                if (providers != null)
                    for (int i = 0; i < providers.size(); i++) {
                        if (providers.get(i).getName().equals(filter.getText().toString())) {
                            selectedProviderId = providers.get(i).getProvider().getId();
                        }
                    }
                addCoupleProvider();
            }
        });
        clean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter.setText("");
            }
        });
        filter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (filter.getText().toString().length() > 0) {
                    clean.setVisibility(View.VISIBLE);
                } else {
                    clean.setVisibility(View.GONE);
                }
            }
        });


    }


    private void getProviders() {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        Map<String, Integer> data = new HashMap<>();
        data.put("categoryId", categoryModel.getProviderCategory().getId());
        data.put("cityId", user.getUser().getCity().getId());
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getAllProvidersById(user.getAccessToken().getToken(),
                data).enqueue(
                new Callback<ProviderWrapper>() {
                    @Override
                    public void onResponse(Call<ProviderWrapper> call, Response<ProviderWrapper> response) {
                        dismissDialog();
                        if (response != null & response.body() != null) {

                            providers = response.body().getProviders();

                            filterArr = new String[providers.size()];
                            for (int i = 0; i < providers.size(); i++) {
                                filterArr[i] = providers.get(i).getName();
                            }
                            filter.setAdapter(new ArrayAdapter<>(TodoFinishActivity.this,
                                    android.R.layout.select_dialog_item,
                                    filterArr));

                        }

                    }

                    @Override
                    public void onFailure(Call<ProviderWrapper> call, Throwable t) {
                        dismissDialog();
                        Log.d(TAG, "onFailure: getAllProvidersById()" + t.getMessage());
                        AppAlertDialog.showMessage(TodoFinishActivity.this,
                                getString(R.string.failure_message));
                    }

                }
        );
    }

    private void addCoupleProvider() {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        HashMap<Object, Object> hashMap = new HashMap<>();
        hashMap.put("coupleId", user.getUser().getId());
        if(selectedProviderId!=0)
            hashMap.put("providerId", selectedProviderId);
        else
            hashMap.put("name", filter.getText().toString());
        hashMap.put("organizationTypeId", 1);
        hashMap.put("source", "taskApp");
        if (categoryModel != null)
            hashMap.put("categoryId", categoryModel.getProviderCategory().getId());
        if(!TextUtils.isEmpty(edtPrice.getText()))
            hashMap.put("budget", edtPrice.getText().toString());
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.addCoupleProvider(user.getAccessToken().getToken(),
                hashMap).enqueue(
                new Callback<IdModel>() {
                    @Override
                    public void onResponse(Call<IdModel> call, Response<IdModel> response) {
                        dismissDialog();
                        if (response.isSuccessful()) {
                            finish();
                        } else {
                            try {
                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                AppAlertDialog.showMessage(TodoFinishActivity.this,
                                        jObjError.getString("message"));
                            } catch (Exception e) {
                                AppAlertDialog.showMessage(TodoFinishActivity.this,
                                        e.getMessage());
                            }

                        }

                    }

                    @Override
                    public void onFailure(Call<IdModel> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(TodoFinishActivity.this,
                                getString(R.string.failure_message));
                    }
                }
        );
    }
}
