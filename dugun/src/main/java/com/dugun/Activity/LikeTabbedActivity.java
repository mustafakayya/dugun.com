package com.dugun.Activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Api.GeneralVariables;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Fragment.FragmentChild;
import com.dugun.Model.CategoryModel;
import com.dugun.Model.LikeMediaModel;
import com.dugun.Model.LikeProviderModel;
import com.dugun.R;
import com.dugun.Wrapper.LikeDataWrapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Api.GeneralVariables.LIKES;
import static com.dugun.Others.Utils.isNetworkAvailable;

public class LikeTabbedActivity extends BaseActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ArrayList<LikeMediaModel> mediaList;
    private ArrayList<LikeProviderModel> providerList;
    private CategoryModel category;
    private String TAG = "LikeTabbedActivity";
    private int from;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_like_tabbed);

        category = getIntent().getParcelableExtra("category");
        from = getIntent().getIntExtra("from", -1);
        setToolbarGeneral(category.getName() + " Favorilerim");

        viewPager = (ViewPager) findViewById(R.id.my_viewpager);
        tabLayout = (TabLayout) findViewById(R.id.my_tab_layout);

        getLikeData(getCatId());

    }


    int getCatId(){
        if (from == 1) {
            return category.getProviderCategory().getId();
        } else  {
            return category.getId();
        }
    }


    private void getLikeData(int id) {
        if (!isNetworkAvailable(context)) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        Map<String, Object> data = new HashMap<>();
        data.put("categoryId", id);
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getLikeData(user.getAccessToken().getToken(), data).enqueue(
                new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        dismissDialog();
                        if (response != null & response.body() != null) {

                            try {
                                LikeDataWrapper obj = new Gson().fromJson(response.body().toString(),
                                        LikeDataWrapper.class);
                                mediaList = obj.getData().getMediaList();
                                if (mediaList != null)
                                    for (int i = 0; i < mediaList.size(); i++) {
                                        mediaList.get(i).setLiked(GeneralVariables.isLiked(mediaList.get(id).getId(),true));
                                    }
                                providerList = obj.getData().getProviderList();
                                if (providerList != null)
                                    for (int i = 0; i < providerList.size(); i++) {
                                        providerList.get(i).setLiked(GeneralVariables.isLiked(providerList.get(i).getProvider().getId(),false));
                                    }
                            } catch (Exception e) {
                                e.printStackTrace();
                                mediaList = new ArrayList<>();
                                providerList = new ArrayList<>();
                            }

                            Optional<CategoryModel> categoryModel = Stream.of(GeneralVariables.categoryModels).filter(value -> value.getId() == id).findFirst();
                            if(categoryModel.isPresent()){
                                viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(),categoryModel.get().isHasGallery()));
                            }else{
                                viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(),false));
                            }


                            tabLayout.setupWithViewPager(viewPager);

                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        dismissDialog();
                        Log.d(TAG, "getLikeData () -> " + t.getMessage());
                        AppAlertDialog.showMessage(LikeTabbedActivity.this,
                                getString(R.string.failure_message));
                    }
                });
    }


    private class PagerAdapter extends FragmentPagerAdapter {

        private String tabTitles[] ;

        PagerAdapter(FragmentManager fm,boolean hasGallery) {
            super(fm);
            if(hasGallery){
               tabTitles = new String[]{ "Firmalar","Galeri"};
            }else{
                tabTitles = new String[]{"Firmalar"};
            }
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }

        @Override
        public Fragment getItem(int position) {

            FragmentChild child = new FragmentChild();
            Bundle bundle = new Bundle();
            bundle.putString("catName",category.getName());
            bundle.putInt("catId",getCatId());
            if (position == 1 && tabTitles.length == 2) {
                bundle.putParcelableArrayList("mediaList", mediaList!=null ? mediaList : new ArrayList<>());
            } else {
                bundle.putParcelableArrayList("providerList", providerList != null ? providerList : new ArrayList<>());
            }
            child.setArguments(bundle);
            return child;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }
    }
}
