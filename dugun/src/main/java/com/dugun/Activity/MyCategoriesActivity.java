package com.dugun.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.dugun.Adapter.CategoryListAdapter;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Dialog.DeleteCategoryDialog;
import com.dugun.Interface.DeleteCategoryCallback;
import com.dugun.Model.CategoryModel;
import com.dugun.Model.SuccessModel;
import com.dugun.R;
import com.dugun.Wrapper.CategoryWrapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.dugun.Others.Utils.isNetworkAvailable;

public class MyCategoriesActivity extends BaseActivity implements DeleteCategoryCallback,
        CategoryListAdapter.OnAddClickListener, CategoryListAdapter.OnItemClickListener {

    private RecyclerView recyclerView;
    private ArrayList<CategoryModel> categories;
    private CategoryListAdapter adapter;
    private CategoryModel currentCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_categories);
        setToolbarGeneral(getResources().getString(R.string.title_my_categories));

        recyclerView = (RecyclerView) findViewById(R.id.category_list);

        getCategoryList();

        ItemTouchHelper.Callback callback = new ItemTouchHelper.Callback() {
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                if (!categories.get(viewHolder.getLayoutPosition()).isSelected())
                    return 0;
                return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG,
                        ItemTouchHelper.DOWN | ItemTouchHelper.UP);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                if (viewHolder instanceof CategoryListAdapter.CategoryHolder) {

                    int from = viewHolder.getAdapterPosition();
                    int to = target.getAdapterPosition();
                    if (categories.get(to).isSelected()) {
                        Collections.swap(categories, from, to);
                        adapter.notifyItemMoved(from, to);
                    }
                }
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            }
        };

        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);

    }

    private void getCategoryList() {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        categories = new ArrayList<>();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.categoryList(user.getAccessToken().getToken()).enqueue(
                new Callback<CategoryWrapper>() {
                    @Override
                    public void onResponse(Call<CategoryWrapper> call, Response<CategoryWrapper> response) {
                        dismissDialog();
                        if (response != null & response.body() != null && response.body().getCategory() != null) {
                            categories = response.body().getCategory();
                            sortCategories();
                            recyclerView.setLayoutManager(
                                    new LinearLayoutManager(getApplicationContext()));
                        }
                    }

                    @Override
                    public void onFailure(Call<CategoryWrapper> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(MyCategoriesActivity.this,
                                getString(R.string.failure_message));
                    }
                });
    }


    private void addCategory(CategoryModel category) {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        HashMap<Object, Object> hashMap = new HashMap<>();
        hashMap.put("coupleTodoCategoryIds", new Integer[]{category.getId()});

        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.addCategory(user.getAccessToken().getToken(),
                user.getUser().getId(), hashMap).enqueue(
                new Callback<SuccessModel>() {

                    @Override
                    public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                        dismissDialog();
                        currentCategory.setSelected(response.body().success);
                        sortCategories();
                    }

                    @Override
                    public void onFailure(Call<SuccessModel> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(MyCategoriesActivity.this,
                                getString(R.string.failure_message));
                    }
                }
        );
    }

    private void deleteCategory(CategoryModel category) {
        if (!isNetworkAvailable(getApplicationContext())) {
            AppAlertDialog.showMessage(this, getString(R.string.connection_failed));
            return;
        }
        showDialog();
        HashMap<Object, Object> hashMap = new HashMap<>();
        hashMap.put("coupleTodoCategoryIds", new Integer[]{category.getId()});

        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.deleteCategory(user.getAccessToken().getToken(),
                user.getUser().getId(), hashMap).enqueue(
                new Callback<SuccessModel>() {

                    @Override
                    public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                        dismissDialog();
                        currentCategory.setSelected(false);
                        sortCategories();
                    }

                    @Override
                    public void onFailure(Call<SuccessModel> call, Throwable t) {
                        dismissDialog();
                        AppAlertDialog.showMessage(MyCategoriesActivity.this,
                                getString(R.string.failure_message));
                    }
                }
        );
    }


    private void clickButton(CategoryModel category) {

        this.currentCategory = category;
        if (this.currentCategory.isSelected()) {
            showDeleteCategoryDialog(category);
        } else {
            addCategory(category);
        }

    }

    private void sortCategories() {

        ArrayList<CategoryModel> tempSelectedCategories = new ArrayList<>();
        ArrayList<CategoryModel> tempUnSelectedCategories = new ArrayList<>();

        for (int i = 0; i < categories.size(); i++) {
            if (categories.get(i).isSelected()) {
                tempSelectedCategories.add(categories.get(i));
            } else {
                tempUnSelectedCategories.add(categories.get(i));
            }
        }

        categories.clear();
        for (int i = 0; i < tempSelectedCategories.size(); i++) {
            categories.add(tempSelectedCategories.get(i));
        }
        for (int i = 0; i < tempUnSelectedCategories.size(); i++) {
            categories.add(tempUnSelectedCategories.get(i));
        }

        if (adapter == null) {
            adapter = new CategoryListAdapter(getApplicationContext(), categories,
                    MyCategoriesActivity.this,
                    MyCategoriesActivity.this);
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }


    }

    private void showDeleteCategoryDialog(CategoryModel categoryModel) {
        DeleteCategoryDialog dialog = DeleteCategoryDialog.newInstance(categoryModel);
        DeleteCategoryDialog.callback = this;
        dialog.show(getFragmentManager(), "sort_selector");
    }

    @Override
    public void deleteCategoryCallback(boolean status) {
        deleteCategory(this.currentCategory);
        if (status) {
            Intent i = new Intent(MyCategoriesActivity.this, TodoFinishActivity.class);
            i.putExtra("category", currentCategory);
            startActivity(i);
        }

    }

    @Override
    public void addClickListener(CategoryModel category) {
        clickButton(category);
    }

    @Override
    public void OnItemClick(View v, CategoryModel category) {

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
