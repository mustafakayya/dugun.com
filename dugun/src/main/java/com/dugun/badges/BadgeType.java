package com.dugun.badges;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mustafakaya on 12/11/17.
 */

public enum BadgeType {

    SignUp(new ArrayList<>(Arrays.asList(1,14,13,26))),
    ProfileComplete(new ArrayList<>(Arrays.asList(2,15))),
    Lead(new ArrayList<>(Arrays.asList(3,4,5,6,7,16,17,18,19,20))),
    Provider(new ArrayList<>(Arrays.asList(8,9,10,11,12,21,22,23,24,25)));


    List<Integer> badgeIds;

    BadgeType(List<Integer> badgeIds) {
        this.badgeIds = badgeIds;
    }

    public List<Integer> getBadgeIds() {
        return badgeIds;
    }

    public void setBadgeIds(List<Integer> badgeIds) {
        this.badgeIds = badgeIds;
    }
}
