package com.dugun.badges;

import android.app.Dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Dialog.AppAlertDialog;
import com.dugun.Model.BadgeModel;
import com.dugun.Model.SuccessModel;
import com.dugun.Others.USER;
import com.dugun.R;
import com.dugun.Wrapper.UserWrapper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dugun.Others.Utils.isNetworkAvailable;


public class BadgeDialog extends DialogFragment {

    BadgeModel badgeModel;
    TextView lblTitle,lblDescription;
    ImageView ivBadge;

    public static BadgeDialog newInstance(BadgeModel badgeModel) {
        BadgeDialog fragment = new BadgeDialog ();
        fragment.badgeModel = badgeModel;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_badge_layout, container, false);
        lblTitle = view.findViewById(R.id.lblTitle_fBadge);
        lblDescription = view.findViewById(R.id.lblDescription_fBadge);
        ivBadge=view.findViewById(R.id.ivIcon_fBadge);
        FrameLayout layout_close = (FrameLayout) view.findViewById(R.id.layout_close);
        layout_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        lblTitle.setText(badgeModel.getName());
        lblDescription.setText(badgeModel.getDescription());
        ivBadge.setImageResource(BadgeMap.info.get(new Integer(badgeModel.getId())));

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        notifyBadge();
    }

    private void notifyBadge() {
        if (!isNetworkAvailable(getContext())) {
            return;
        }
        UserWrapper userWrapper = USER.getProfile(getContext());
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.notifyBadge(userWrapper.getAccessToken().getToken(),badgeModel.getId()).enqueue(new Callback<SuccessModel>() {
            @Override
            public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {

            }

            @Override
            public void onFailure(Call<SuccessModel> call, Throwable t) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;

    }

}
