package com.dugun.badges;

import com.dugun.R;

import java.util.HashMap;

/**
 * Created by mustafakaya on 12/10/17.
 */

public class BadgeMap {

    public static HashMap<Integer,Integer> info = new HashMap<>();
    static  {
        info.put(1, R.drawable.badge_1);
        info.put(2,R.drawable.badge_2);
        info.put(3,R.drawable.badge_3);
        info.put(4,R.drawable.badge_4);
        info.put(5,R.drawable.badge_5);
        info.put(6,R.drawable.badge_6);
        info.put(7,R.drawable.badge_7);
        info.put(8,R.drawable.badge_8);
        info.put(9,R.drawable.badge_9);
        info.put(10,R.drawable.badge_10);
        info.put(11,R.drawable.badge_11);
        info.put(12,R.drawable.badge_12);
        info.put(13,R.drawable.badge_13);
        info.put(14,R.drawable.badge_14);
        info.put(15,R.drawable.badge_15);
        info.put(16,R.drawable.badge_16);
        info.put(17,R.drawable.badge_17);
        info.put(18,R.drawable.badge_19);
        info.put(19,R.drawable.badge_19);
        info.put(20,R.drawable.badge_20);
        info.put(21,R.drawable.badge_20);
        info.put(22,R.drawable.badge_22);
        info.put(23,R.drawable.badge_22);
        info.put(24,R.drawable.badge_25);
        info.put(25,R.drawable.badge_25);
        info.put(26,R.drawable.badge_26);


/*
        info.put(3,"planligelin");
        info.put(4,"harikagelin");
        info.put(5,"supergelin");
        info.put(6,"mukemmelgelin");
        info.put(7,"fevkaladegelin");
        info.put(8,"duguneilkadim");
        info.put(9,"ucdugunsor");
        info.put(10,"dortdortlukdugun");
        info.put(11,"besyildizlidugun");
        info.put(12,"onparmaktaonmarifet");
        info.put(13,"olaganustugelin");
        info.put(14,"isinibilendamat");
        info.put(15,"eksiksizdamat");
        info.put(16,"cesurveyakisikli");
        info.put(17,"hizliveplanlidamat");
        info.put(18,"gorevimizdugun");
        info.put(19,"damatlarinefendisi");
        info.put(20,"damatkral");
        info.put(21,"millidamat");
        info.put(22,"ucdugunsor");
        info.put(23,"fantastikdortlu");
        info.put(24,"besincielement");
        info.put(25,"onparmaktaonmarifet");
        info.put(26,"olaganustudamat");
        */
    }
}
