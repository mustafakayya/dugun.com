package com.dugun.badges;


import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;

import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.dugun.Api.ApiClient;
import com.dugun.Api.ApiInterface;
import com.dugun.Model.BadgeModel;
import com.dugun.Model.BadgesModel;
import com.dugun.Others.USER;
import com.dugun.Wrapper.UserWrapper;

import java.util.List;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mustafakaya on 12/11/17.
 */

public class BadgeTask extends AsyncTask<Void,Void,Void> {

    Context context;
    BadgeType type;
    FragmentManager fragmentManager;

    public BadgeTask(Context context, BadgeType type,FragmentManager fragmentManager) {
        this.context = context;
        this.type = type;
        this.fragmentManager = fragmentManager;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        UserWrapper userWrapper = USER.getProfile(context);
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        apiInterface.getBadges(userWrapper.getAccessToken().getToken()).enqueue(new Callback<BadgesModel>() {
            @Override
            public void onResponse(Call<BadgesModel> call, Response<BadgesModel> response) {
                if(response.isSuccessful()){
                    List<BadgesModel.Badge> badges= response.body().getData();
                    for (int i = 0 ; i < badges.size(); i++ ){
                        BadgesModel.Badge badge = badges.get(0);
                        Optional<Integer> optional = Stream.of(type.badgeIds).filter(value -> badge.getBadge().getId().equals(value) && !badge.getNotified()).findFirst();
                        if(optional.isPresent()){
                            BadgeDialog fragment = BadgeDialog.newInstance(badge.getBadge());
                            fragment.show(fragmentManager, "badgeFragment");
                            break;
                        }

                    }

                }
            }

            @Override
            public void onFailure(Call<BadgesModel> call, Throwable t) {

            }
        });
        return null;
    }
}
