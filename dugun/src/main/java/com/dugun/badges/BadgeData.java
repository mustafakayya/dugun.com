package com.dugun.badges;

import android.support.annotation.IntegerRes;

/**
 * Created by mustafakaya on 12/10/17.
 */

public class BadgeData {

    String title;
    String description;
    @IntegerRes
    int iconId;

    public BadgeData(String title, String description, @IntegerRes int iconId) {
        this.title = title;
        this.description = description;
        this.iconId = iconId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }
}
