package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mustafakaya on 11/30/17.
 */

public class LeadMessageModel {
    @SerializedName("id")
    @Expose
    int id;
    @SerializedName("senderType")
    @Expose
    String senderType;
    @SerializedName("status")
    @Expose
    String status;
    @SerializedName("toAddress")
    @Expose
    String toAddress;
    @SerializedName("fromAddress")
    @Expose
    String fromAddress;
    @SerializedName("subject")
    @Expose
    String subject;
    @SerializedName("body")
    @Expose
    String body;
    @SerializedName("readAtEmail")
    @Expose
    boolean readAtEmail;
    @SerializedName("readAtPanel")
    @Expose
    boolean readAtPanel;

    public LeadMessageModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSenderType() {
        return senderType;
    }

    public void setSenderType(String senderType) {
        this.senderType = senderType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public boolean isReadAtEmail() {
        return readAtEmail;
    }

    public void setReadAtEmail(boolean readAtEmail) {
        this.readAtEmail = readAtEmail;
    }

    public boolean isReadAtPanel() {
        return readAtPanel;
    }

    public void setReadAtPanel(boolean readAtPanel) {
        this.readAtPanel = readAtPanel;
    }
}
