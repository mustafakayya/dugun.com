package com.dugun.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mustafakaya on 12/7/17.
 */

public class LeadInteractionModel {

    @SerializedName("id")
    int id;
    @SerializedName("type")
    String type;
    @SerializedName("comment")
    String comment;
    @SerializedName("createdAt")
    String createdAt;

    public LeadInteractionModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
