package com.dugun.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ResultIdModel {

   @SerializedName ("id")
   public Integer id;

}
