package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class LikeMediaModel implements Parcelable{

   @SerializedName ("id")
   private int id;

   @SerializedName ("itemId")
   private int itemId;

   @SerializedName ("media")
   private Media media;

   @SerializedName ("isLiked")
   private boolean isLiked;

   protected LikeMediaModel (Parcel in) {
      id = in.readInt ();
      itemId = in.readInt ();
      isLiked = in.readByte () != 0;
   }

   public static final Creator<LikeMediaModel> CREATOR = new Creator<LikeMediaModel> () {
      @Override
      public LikeMediaModel createFromParcel (Parcel in) {
         return new LikeMediaModel (in);
      }

      @Override
      public LikeMediaModel[] newArray (int size) {
         return new LikeMediaModel[size];
      }
   };

   @Override public int describeContents () {
      return 0;
   }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeInt (id);
      dest.writeInt (itemId);
      dest.writeByte ((byte) (isLiked ? 1 : 0));
   }


   public class Media {

      @SerializedName ("id")
      private int id;

      @SerializedName ("gallery")
      private Gallery gallery;

      public class Gallery {

         @SerializedName ("id")
         private int id;

         @SerializedName ("galleryCategory")
         private CategoryModel galleryCategory;

         @SerializedName ("provider")
         private String provider;

         @SerializedName ("name")
         private String name;

         @SerializedName ("categoryId")
         private int categoryId;

         @SerializedName ("slug")
         private String slug;

         public int getId () {
            return id;
         }

         public void setId (int id) {
            this.id = id;
         }

         public CategoryModel getGalleryCategory () {
            return galleryCategory;
         }

         public void setGalleryCategory (CategoryModel galleryCategory) {
            this.galleryCategory = galleryCategory;
         }

         public String getProvider () {
            return provider;
         }

         public void setProvider (String provider) {
            this.provider = provider;
         }

         public String getName () {
            return name;
         }

         public void setName (String name) {
            this.name = name;
         }

         public int getCategoryId () {
            return categoryId;
         }

         public void setCategoryId (int categoryId) {
            this.categoryId = categoryId;
         }

         public String getSlug () {
            return slug;
         }

         public void setSlug (String slug) {
            this.slug = slug;
         }
      }

      @SerializedName ("title")
      private String title;

      @SerializedName ("imageUrl")
      private UrlModel imageUrl;

      @SerializedName ("isMainImage")
      private boolean isMainImage;

      public int getId () {
         return id;
      }

      public void setId (int id) {
         this.id = id;
      }

      public Gallery getGallery () {
         return gallery;
      }

      public void setGallery (Gallery gallery) {
         this.gallery = gallery;
      }

      public String getTitle () {
         return title;
      }

      public void setTitle (String title) {
         this.title = title;
      }

      public UrlModel getImageUrl () {
         return imageUrl;
      }

      public void setImageUrl (UrlModel imageUrl) {
         this.imageUrl = imageUrl;
      }

      public boolean isMainImage () {
         return isMainImage;
      }

      public void setMainImage (boolean mainImage) {
         isMainImage = mainImage;
      }
   }

   public int getId () {
      return id;
   }

   public void setId (int id) {
      this.id = id;
   }

   public int getItemId () {
      return itemId;
   }

   public void setItemId (int itemId) {
      this.itemId = itemId;
   }

   public Media getMedia () {
      return media;
   }

   public void setMedia (Media media) {
      this.media = media;
   }

   public boolean isLiked () {
      return isLiked;
   }

   public void setLiked (boolean liked) {
      isLiked = liked;
   }
}
