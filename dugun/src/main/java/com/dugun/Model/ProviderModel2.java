package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ProviderModel2 implements Parcelable {

    @SerializedName("id")
    private int id;

    @SerializedName("provider")
    private ProviderInnerModel provider;

    @SerializedName("categoryId")
    private int categoryId;

    @SerializedName("category")
    private CategoryModel category;

    @SerializedName("cityId")
    private int cityId;

    @SerializedName("city")
    private CityModel city;

    @SerializedName("districtId")
    private int districtId;

    @SerializedName("district")
    private DistrictModel district;

    @SerializedName("areaId")
    private int areaId;

    @SerializedName("districtExtra")
    private int[] districtExtra;

    @SerializedName("districtStatic")
    private int[] districtStatic;

    @SerializedName("statusId")
    private int statusId;

    @SerializedName("isPromotion")
    private boolean isPromotion;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("profileUrl")
    private String profileUrl;

    @SerializedName("imageUrl")
    private String imageUrl;

    @SerializedName("imageFullPath")
    private String imageFullPath;

    @SerializedName("imageCount")
    private int imageCount;

    @SerializedName("videoCount")
    private int videoCount;

    @SerializedName("discountCount")
    private int discountCount;

    @SerializedName("reviewCount")
    private int reviewCount;

    @SerializedName("leadCount")
    private int leadCount;

    @SerializedName("viewCount")
    private int viewCount;

    @SerializedName("infoRequestCount")
    private int infoRequestCount;

    @SerializedName("inboundCallCount")
    private int inboundCallCount;

    @SerializedName("averageLeadCount")
    private int averageLeadCount;

    @SerializedName("averageViewCount")
    private int averageViewCount;

    @SerializedName("leadScore")
    private double leadScore;

    @SerializedName("pageWeight")
    private int pageWeight;

    @SerializedName("homePageShowCase")
    private boolean homePageShowCase;

    @SerializedName("categoryShowCase")
    private boolean categoryShowCase;

    @SerializedName("discountHomeDoping")
    private boolean discountHomeDoping;

    @SerializedName("discountMicrositeDoping")
    private boolean discountMicrositeDoping;

    @SerializedName("discountStaticDoping")
    private boolean discountStaticDoping;

    @SerializedName("showOnDiscountListingPage")
    private boolean showOnDiscountListingPage;

    @SerializedName("venueFeaturedDoping")
    private boolean venueFeaturedDoping;

    @SerializedName("filter1")
    private String[] filter1;

    @SerializedName("filter2")
    private String[] filter2;

    @SerializedName("filter3")
    private String[] filter3;

    @SerializedName("filter4")
    private String[] filter4;

    @SerializedName("filter5")
    private String[] filter5;

    @SerializedName("filter6")
    private String[] filter6;

    @SerializedName("mapLat")
    private String mapLat;

    @SerializedName("mapLng")
    private String mapLng;

    @SerializedName("productListingPriority")
    private String productListingPriority;

    @SerializedName("discountCategoryStatic")
    private boolean discountCategoryStatic;

    @SerializedName("isLiked")
    private boolean isLiked;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProviderInnerModel getProvider() {
        return provider;
    }

    public void setProvider(ProviderInnerModel provider) {
        this.provider = provider;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public CategoryModel getCategory() {
        return category;
    }

    public void setCategory(CategoryModel category) {
        this.category = category;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public CityModel getCity() {
        return city;
    }

    public void setCity(CityModel city) {
        this.city = city;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public DistrictModel getDistrict() {
        return district;
    }

    public void setDistrict(DistrictModel district) {
        this.district = district;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public int[] getDistrictExtra() {
        return districtExtra;
    }

    public void setDistrictExtra(int[] districtExtra) {
        this.districtExtra = districtExtra;
    }

    public int[] getDistrictStatic() {
        return districtStatic;
    }

    public void setDistrictStatic(int[] districtStatic) {
        this.districtStatic = districtStatic;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public boolean isPromotion() {
        return isPromotion;
    }

    public void setPromotion(boolean promotion) {
        isPromotion = promotion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageFullPath() {
        return imageFullPath;
    }

    public void setImageFullPath(String imageFullPath) {
        this.imageFullPath = imageFullPath;
    }

    public int getImageCount() {
        return imageCount;
    }

    public void setImageCount(int imageCount) {
        this.imageCount = imageCount;
    }

    public int getVideoCount() {
        return videoCount;
    }

    public void setVideoCount(int videoCount) {
        this.videoCount = videoCount;
    }

    public int getDiscountCount() {
        return discountCount;
    }

    public void setDiscountCount(int discountCount) {
        this.discountCount = discountCount;
    }

    public int getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(int reviewCount) {
        this.reviewCount = reviewCount;
    }

    public int getLeadCount() {
        return leadCount;
    }

    public void setLeadCount(int leadCount) {
        this.leadCount = leadCount;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public int getInfoRequestCount() {
        return infoRequestCount;
    }

    public void setInfoRequestCount(int infoRequestCount) {
        this.infoRequestCount = infoRequestCount;
    }

    public int getInboundCallCount() {
        return inboundCallCount;
    }

    public void setInboundCallCount(int inboundCallCount) {
        this.inboundCallCount = inboundCallCount;
    }

    public int getAverageLeadCount() {
        return averageLeadCount;
    }

    public void setAverageLeadCount(int averageLeadCount) {
        this.averageLeadCount = averageLeadCount;
    }

    public int getAverageViewCount() {
        return averageViewCount;
    }

    public void setAverageViewCount(int averageViewCount) {
        this.averageViewCount = averageViewCount;
    }

    public double getLeadScore() {
        return leadScore;
    }

    public void setLeadScore(double leadScore) {
        this.leadScore = leadScore;
    }

    public int getPageWeight() {
        return pageWeight;
    }

    public void setPageWeight(int pageWeight) {
        this.pageWeight = pageWeight;
    }

    public boolean isHomePageShowCase() {
        return homePageShowCase;
    }

    public void setHomePageShowCase(boolean homePageShowCase) {
        this.homePageShowCase = homePageShowCase;
    }

    public boolean isCategoryShowCase() {
        return categoryShowCase;
    }

    public void setCategoryShowCase(boolean categoryShowCase) {
        this.categoryShowCase = categoryShowCase;
    }

    public boolean isDiscountHomeDoping() {
        return discountHomeDoping;
    }

    public void setDiscountHomeDoping(boolean discountHomeDoping) {
        this.discountHomeDoping = discountHomeDoping;
    }

    public boolean isDiscountMicrositeDoping() {
        return discountMicrositeDoping;
    }

    public void setDiscountMicrositeDoping(boolean discountMicrositeDoping) {
        this.discountMicrositeDoping = discountMicrositeDoping;
    }

    public boolean isDiscountStaticDoping() {
        return discountStaticDoping;
    }

    public void setDiscountStaticDoping(boolean discountStaticDoping) {
        this.discountStaticDoping = discountStaticDoping;
    }

    public boolean isShowOnDiscountListingPage() {
        return showOnDiscountListingPage;
    }

    public void setShowOnDiscountListingPage(boolean showOnDiscountListingPage) {
        this.showOnDiscountListingPage = showOnDiscountListingPage;
    }

    public boolean isVenueFeaturedDoping() {
        return venueFeaturedDoping;
    }

    public void setVenueFeaturedDoping(boolean venueFeaturedDoping) {
        this.venueFeaturedDoping = venueFeaturedDoping;
    }

    public String[] getFilter1() {
        return filter1;
    }

    public void setFilter1(String[] filter1) {
        this.filter1 = filter1;
    }

    public String[] getFilter2() {
        return filter2;
    }

    public void setFilter2(String[] filter2) {
        this.filter2 = filter2;
    }

    public String[] getFilter3() {
        return filter3;
    }

    public void setFilter3(String[] filter3) {
        this.filter3 = filter3;
    }

    public String[] getFilter4() {
        return filter4;
    }

    public void setFilter4(String[] filter4) {
        this.filter4 = filter4;
    }

    public String[] getFilter5() {
        return filter5;
    }

    public void setFilter5(String[] filter5) {
        this.filter5 = filter5;
    }

    public String[] getFilter6() {
        return filter6;
    }

    public void setFilter6(String[] filter6) {
        this.filter6 = filter6;
    }

    public String getMapLat() {
        return mapLat;
    }

    public void setMapLat(String mapLat) {
        this.mapLat = mapLat;
    }

    public String getMapLng() {
        return mapLng;
    }

    public void setMapLng(String mapLng) {
        this.mapLng = mapLng;
    }

    public String getProductListingPriority() {
        return productListingPriority;
    }

    public void setProductListingPriority(String productListingPriority) {
        this.productListingPriority = productListingPriority;
    }

    public boolean isDiscountCategoryStatic() {
        return discountCategoryStatic;
    }

    public void setDiscountCategoryStatic(boolean discountCategoryStatic) {
        this.discountCategoryStatic = discountCategoryStatic;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeParcelable(this.provider, flags);
        dest.writeInt(this.categoryId);
        dest.writeParcelable(this.category, flags);
        dest.writeInt(this.cityId);
        dest.writeParcelable(this.city, flags);
        dest.writeInt(this.districtId);
        dest.writeParcelable(this.district, flags);
        dest.writeInt(this.areaId);
        dest.writeIntArray(this.districtExtra);
        dest.writeIntArray(this.districtStatic);
        dest.writeInt(this.statusId);
        dest.writeByte(this.isPromotion ? (byte) 1 : (byte) 0);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.profileUrl);
        dest.writeString(this.imageUrl);
        dest.writeString(this.imageFullPath);
        dest.writeInt(this.imageCount);
        dest.writeInt(this.videoCount);
        dest.writeInt(this.discountCount);
        dest.writeInt(this.reviewCount);
        dest.writeInt(this.leadCount);
        dest.writeInt(this.viewCount);
        dest.writeInt(this.infoRequestCount);
        dest.writeInt(this.inboundCallCount);
        dest.writeInt(this.averageLeadCount);
        dest.writeInt(this.averageViewCount);
        dest.writeDouble(this.leadScore);
        dest.writeInt(this.pageWeight);
        dest.writeByte(this.homePageShowCase ? (byte) 1 : (byte) 0);
        dest.writeByte(this.categoryShowCase ? (byte) 1 : (byte) 0);
        dest.writeByte(this.discountHomeDoping ? (byte) 1 : (byte) 0);
        dest.writeByte(this.discountMicrositeDoping ? (byte) 1 : (byte) 0);
        dest.writeByte(this.discountStaticDoping ? (byte) 1 : (byte) 0);
        dest.writeByte(this.showOnDiscountListingPage ? (byte) 1 : (byte) 0);
        dest.writeByte(this.venueFeaturedDoping ? (byte) 1 : (byte) 0);
        dest.writeStringArray(this.filter1);
        dest.writeStringArray(this.filter2);
        dest.writeStringArray(this.filter3);
        dest.writeStringArray(this.filter4);
        dest.writeStringArray(this.filter5);
        dest.writeStringArray(this.filter6);
        dest.writeString(this.mapLat);
        dest.writeString(this.mapLng);
        dest.writeString(this.productListingPriority);
        dest.writeByte(this.discountCategoryStatic ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isLiked ? (byte) 1 : (byte) 0);
    }

    public ProviderModel2() {
    }

    protected ProviderModel2(Parcel in) {
        this.id = in.readInt();
        this.provider = in.readParcelable(ProviderInnerModel.class.getClassLoader());
        this.categoryId = in.readInt();
        this.category = in.readParcelable(CategoryModel.class.getClassLoader());
        this.cityId = in.readInt();
        this.city = in.readParcelable(CityModel.class.getClassLoader());
        this.districtId = in.readInt();
        this.district = in.readParcelable(DistrictModel.class.getClassLoader());
        this.areaId = in.readInt();
        this.districtExtra = in.createIntArray();
        this.districtStatic = in.createIntArray();
        this.statusId = in.readInt();
        this.isPromotion = in.readByte() != 0;
        this.name = in.readString();
        this.description = in.readString();
        this.profileUrl = in.readString();
        this.imageUrl = in.readString();
        this.imageFullPath = in.readString();
        this.imageCount = in.readInt();
        this.videoCount = in.readInt();
        this.discountCount = in.readInt();
        this.reviewCount = in.readInt();
        this.leadCount = in.readInt();
        this.viewCount = in.readInt();
        this.infoRequestCount = in.readInt();
        this.inboundCallCount = in.readInt();
        this.averageLeadCount = in.readInt();
        this.averageViewCount = in.readInt();
        this.leadScore = in.readDouble();
        this.pageWeight = in.readInt();
        this.homePageShowCase = in.readByte() != 0;
        this.categoryShowCase = in.readByte() != 0;
        this.discountHomeDoping = in.readByte() != 0;
        this.discountMicrositeDoping = in.readByte() != 0;
        this.discountStaticDoping = in.readByte() != 0;
        this.showOnDiscountListingPage = in.readByte() != 0;
        this.venueFeaturedDoping = in.readByte() != 0;
        this.filter1 = in.createStringArray();
        this.filter2 = in.createStringArray();
        this.filter3 = in.createStringArray();
        this.filter4 = in.createStringArray();
        this.filter5 = in.createStringArray();
        this.filter6 = in.createStringArray();
        this.mapLat = in.readString();
        this.mapLng = in.readString();
        this.productListingPriority = in.readString();
        this.discountCategoryStatic = in.readByte() != 0;
        this.isLiked = in.readByte() != 0;
    }

    public static final Creator<ProviderModel2> CREATOR = new Creator<ProviderModel2>() {
        @Override
        public ProviderModel2 createFromParcel(Parcel source) {
            return new ProviderModel2(source);
        }

        @Override
        public ProviderModel2[] newArray(int size) {
            return new ProviderModel2[size];
        }
    };
}
