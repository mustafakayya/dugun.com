package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class TodoModel implements Parcelable {

   @SerializedName ("id")
   private int id;

   @SerializedName ("category")
   private CategoryModel category;

   @SerializedName ("title")
   private String title;

   @SerializedName ("description")
   private String description;

   @SerializedName ("completedAt")
   private String completedAt;

   @SerializedName ("notifyAt")
   private String notifyAt;

   @SerializedName ("actionType")
   private String actionType;

   @SerializedName ("actionItemId")
   private int actionItemId;

   @SerializedName ("webViewUrl")
   private String webViewUrl;

   public TodoModel () {
   }

   protected TodoModel (Parcel in) {
      id = in.readInt ();
      category = in.readParcelable (CategoryModel.class.getClassLoader ());
      title = in.readString ();
      description = in.readString ();
      completedAt = in.readString ();
      notifyAt = in.readString ();
      actionType = in.readString ();
      actionItemId = in.readInt ();
      webViewUrl = in.readString ();
   }

   public static final Creator<TodoModel> CREATOR = new Creator<TodoModel> () {
      @Override
      public TodoModel createFromParcel (Parcel in) {
         return new TodoModel (in);
      }

      @Override
      public TodoModel[] newArray (int size) {
         return new TodoModel[size];
      }
   };

   public int getId () {
      return id;
   }

   public void setId (int id) {
      this.id = id;
   }

   public CategoryModel getCategory () {
      return category;
   }

   public void setCategory (CategoryModel category) {
      this.category = category;
   }

   public String getTitle () {
      return title;
   }

   public void setTitle (String title) {
      this.title = title;
   }

   public String getDescription () {
      return description;
   }

   public void setDescription (String description) {
      this.description = description;
   }

   public String getCompletedAt () {
      return completedAt;
   }

   public void setCompletedAt (String completedAt) {
      this.completedAt = completedAt;
   }

   public String getNotifyAt () {
      return notifyAt;
   }

   public void setNotifyAt (String notifyAt) {
      this.notifyAt = notifyAt;
   }

   public String getActionType () {
      return actionType;
   }

   public void setActionType (String actionType) {
      this.actionType = actionType;
   }

   public int getActionItemId () {
      return actionItemId;
   }

   public void setActionItemId (int actionItemId) {
      this.actionItemId = actionItemId;
   }

   public String getWebViewUrl () {
      return webViewUrl;
   }

   public void setWebViewUrl (String webViewUrl) {
      this.webViewUrl = webViewUrl;
   }

   @Override public int describeContents () {
      return 0;
   }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeInt (id);
      dest.writeParcelable (category, flags);
      dest.writeString (title);
      dest.writeString (description);
      dest.writeString (completedAt);
      dest.writeString (notifyAt);
      dest.writeString (actionType);
      dest.writeInt (actionItemId);
      dest.writeString (webViewUrl);
   }
}
