package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class MyBudgetModel {

   @SerializedName ("id")
   @Expose
   private Integer id;

   @SerializedName ("name")
   @Expose
   private String name;

   @SerializedName ("allocated")
   @Expose
   private Integer allocated;

   @SerializedName ("spent")
   @Expose
   private Integer spent;

   public MyBudgetModel () {
   }

   public MyBudgetModel (Integer id, String name, Integer allocated, Integer spent) {
      this.id = id;
      this.name = name;
      this.allocated = allocated;
      this.spent = spent;
   }

   public Integer getId () {
      return id;
   }

   public void setId (Integer id) {
      this.id = id;
   }

   public String getName () {
      return name;
   }

   public void setName (String name) {
      this.name = name;
   }

   public Integer getAllocated () {
      return allocated;
   }

   public void setAllocated (Integer allocated) {
      this.allocated = allocated;
   }

   public Integer getSpent () {
      return spent;
   }

   public void setSpent (Integer spent) {
      this.spent = spent;
   }
}
