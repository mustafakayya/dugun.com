package com.dugun.Model;


import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

public class ProviderFormDataModel {

    @SerializedName ("data")
    private JSONObject data;

    public JSONObject getData () {
        return data;
    }

    public void setData (JSONObject data) {
        this.data = data;
    }

    //    @SerializedName ("providerFilter")
//    @Expose
//    private ProviderModel.ProviderFilter providerFilter;
//
//    public class ProviderFilter {

}
