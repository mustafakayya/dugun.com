package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class LikeProviderModel implements Parcelable{

   @SerializedName ("id")
   private int id;

   @SerializedName ("itemId")
   private int itemId;

   @SerializedName ("provider")
   private ProviderModel provider;

   @SerializedName ("isLiked")
   private boolean isLiked;


   protected LikeProviderModel (Parcel in) {
      id = in.readInt ();
      itemId = in.readInt ();
      provider = in.readParcelable (ProviderModel.class.getClassLoader ());
      isLiked = in.readByte () != 0;
   }

   public static final Creator<LikeProviderModel> CREATOR = new Creator<LikeProviderModel> () {
      @Override
      public LikeProviderModel createFromParcel (Parcel in) {
         return new LikeProviderModel (in);
      }

      @Override
      public LikeProviderModel[] newArray (int size) {
         return new LikeProviderModel[size];
      }
   };

   public boolean isLiked () {
      return isLiked;
   }

   public void setLiked (boolean liked) {
      isLiked = liked;
   }

   public int getId () {
      return id;
   }

   public void setId (int id) {
      this.id = id;
   }

   public int getItemId () {
      return itemId;
   }

   public void setItemId (int itemId) {
      this.itemId = itemId;
   }

   public ProviderModel getProvider () {
      return provider;
   }

   public void setProvider (ProviderModel provider) {
      this.provider = provider;
   }


   @Override public int describeContents () {
      return 0;
   }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeInt (id);
      dest.writeInt (itemId);
      dest.writeParcelable (provider, flags);
      dest.writeByte ((byte) (isLiked ? 1 : 0));
   }
}
