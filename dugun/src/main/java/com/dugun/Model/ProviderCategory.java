package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ProviderCategory implements Parcelable {

   @SerializedName ("id")
   public int id;

   @SerializedName ("mobileGalleryText")
   private String mobileGalleryText;

   @SerializedName ("mobileGalleryIcon")
   private String mobileGalleryIcon;

   public ProviderCategory() {
   }

   protected ProviderCategory (Parcel in) {
      id = in.readInt ();
      mobileGalleryText = in.readString ();
      mobileGalleryIcon = in.readString ();
   }

   public static final Creator<ProviderCategory> CREATOR = new Creator<ProviderCategory> () {
      @Override
      public ProviderCategory createFromParcel (Parcel in) {
         return new ProviderCategory (in);
      }

      @Override
      public ProviderCategory[] newArray (int size) {
         return new ProviderCategory[size];
      }
   };

   public int getId () {
      return id;
   }

   public void setId (int id) {
      this.id = id;
   }

   public String getMobileGalleryText () {
      return mobileGalleryText;
   }

   public void setMobileGalleryText (String mobileGalleryText) {
      this.mobileGalleryText = mobileGalleryText;
   }

   public String getMobileGalleryIcon () {
      return mobileGalleryIcon;
   }

   public void setMobileGalleryIcon (String mobileGalleryIcon) {
      this.mobileGalleryIcon = mobileGalleryIcon;
   }

   @Override public int describeContents () {
      return 0;
   }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeInt (id);
      dest.writeString (mobileGalleryText);
      dest.writeString (mobileGalleryIcon);
   }
}
