package com.dugun.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ProviderGalleryModel {

   @SerializedName ("id")
   private int id;

   @SerializedName("galleryId")
   private int galleryId;

   @SerializedName("image")
   private String image;

   @SerializedName ("orderNum")
   private int orderNum;

   @SerializedName ("imageCode")
   private String imageCode;

   @SerializedName ("width")
   private int width;

   @SerializedName ("height")
   private int height;

   @SerializedName ("status")
   private boolean status;

   @SerializedName ("type")
   private String type;

   @SerializedName ("url")
   private String url;

   @SerializedName ("imageUrl")
   private UrlModel imageUrl;

   @SerializedName ("isMainImage")
   private boolean isMainImage;


   public int getId () {
      return id;
   }

   public void setId (int id) {
      this.id = id;
   }

   public int getGalleryId () {
      return galleryId;
   }

   public void setGalleryId (int galleryId) {
      this.galleryId = galleryId;
   }

   public String getImage () {
      return image;
   }

   public void setImage (String image) {
      this.image = image;
   }

   public int getOrderNum () {
      return orderNum;
   }

   public void setOrderNum (int orderNum) {
      this.orderNum = orderNum;
   }

   public String getImageCode () {
      return imageCode;
   }

   public void setImageCode (String imageCode) {
      this.imageCode = imageCode;
   }

   public int getWidth () {
      return width;
   }

   public void setWidth (int width) {
      this.width = width;
   }

   public int getHeight () {
      return height;
   }

   public void setHeight (int height) {
      this.height = height;
   }

   public boolean isStatus () {
      return status;
   }

   public void setStatus (boolean status) {
      this.status = status;
   }

   public String getType () {
      return type;
   }

   public void setType (String type) {
      this.type = type;
   }

   public String getUrl () {
      return url;
   }

   public void setUrl (String url) {
      this.url = url;
   }

   public UrlModel getImageUrl () {
      return imageUrl;
   }

   public void setImageUrl (UrlModel imageUrl) {
      this.imageUrl = imageUrl;
   }

   public boolean isMainImage () {
      return isMainImage;
   }

   public void setMainImage (boolean mainImage) {
      isMainImage = mainImage;
   }
}
