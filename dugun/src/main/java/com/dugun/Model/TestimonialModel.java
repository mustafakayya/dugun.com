package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mustafakaya on 12/2/17.
 */

public class TestimonialModel implements Parcelable {

    @SerializedName("id")
    int id;
    @SerializedName("providerId")
    int providerId;
    @SerializedName("couple")
    String couple;
    @SerializedName("coupleType")
    String coupleType;
    @SerializedName("isRecommended")
    boolean isRecommended;
    @SerializedName("brideName")
    String brideName;
    @SerializedName("groomName")
    String groomName;
    @SerializedName("organizationDate")
    String organizationDate;
    @SerializedName("testimonial")
    String testimonial;
    @SerializedName("addedBy")
    String addedBy;
    @SerializedName("createdAt")
    String createdAt;

    public TestimonialModel() {
    }

    protected TestimonialModel(Parcel in) {
        id = in.readInt();
        providerId = in.readInt();
        couple = in.readString();
        coupleType = in.readString();
        isRecommended = in.readByte() != 0;
        brideName = in.readString();
        groomName = in.readString();
        organizationDate = in.readString();
        testimonial = in.readString();
        addedBy = in.readString();
        createdAt = in.readString();
    }

    public static final Creator<TestimonialModel> CREATOR = new Creator<TestimonialModel>() {
        @Override
        public TestimonialModel createFromParcel(Parcel in) {
            return new TestimonialModel(in);
        }

        @Override
        public TestimonialModel[] newArray(int size) {
            return new TestimonialModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProviderId() {
        return providerId;
    }

    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }

    public String getCouple() {
        return couple;
    }

    public void setCouple(String couple) {
        this.couple = couple;
    }

    public String getCoupleType() {
        return coupleType;
    }

    public void setCoupleType(String coupleType) {
        this.coupleType = coupleType;
    }

    public boolean isRecommended() {
        return isRecommended;
    }

    public void setRecommended(boolean recommended) {
        isRecommended = recommended;
    }

    public String getBrideName() {
        return brideName;
    }

    public void setBrideName(String brideName) {
        this.brideName = brideName;
    }

    public String getGroomName() {
        return groomName;
    }

    public void setGroomName(String groomName) {
        this.groomName = groomName;
    }

    public String getOrganizationDate() {
        return organizationDate;
    }

    public void setOrganizationDate(String organizationDate) {
        this.organizationDate = organizationDate;
    }

    public String getTestimonial() {
        return testimonial;
    }

    public void setTestimonial(String testimonial) {
        this.testimonial = testimonial;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(providerId);
        parcel.writeString(couple);
        parcel.writeString(coupleType);
        parcel.writeByte((byte) (isRecommended ? 1 : 0));
        parcel.writeString(brideName);
        parcel.writeString(groomName);
        parcel.writeString(organizationDate);
        parcel.writeString(testimonial);
        parcel.writeString(addedBy);
        parcel.writeString(createdAt);
    }


    public String getAvatarName() {
        if (!TextUtils.isEmpty(brideName) && !TextUtils.isEmpty(groomName)) {
            return (brideName.charAt(0) + "&" + groomName.charAt(0)).toUpperCase();
        } else {
            if (!TextUtils.isEmpty(brideName))
                return (brideName.charAt(0) + "").toUpperCase();
            else if (!TextUtils.isEmpty(groomName))
                return (groomName.charAt(0) + "").toUpperCase();
            else
                return "";
        }
    }


    public String getCoupleName() {
        if (!TextUtils.isEmpty(brideName) && !TextUtils.isEmpty(groomName)) {
            return brideName + " " + groomName;
        } else {
            if (!TextUtils.isEmpty(brideName))
                return brideName;
            else if (!TextUtils.isEmpty(groomName))
                return groomName;
            else
                return "";
        }
    }
}
