package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class CategoryModel implements Parcelable {

   @SerializedName ("id")
   private int id;

   @SerializedName ("name")
   private String name;

   @SerializedName ("mainCategoryId")
   private int mainCategoryId;

   @SerializedName ("categoryType")
   private String categoryType;

   @SerializedName ("count")
   private int count;

   @SerializedName ("icon")
   private String icon;

   @SerializedName ("slug")
   private String slug;

   @SerializedName ("imageUrl")
   private String imageUrl;

   @SerializedName ("isDefault")
   private boolean isDefault;

   @SerializedName ("orderNum")
   private int orderNum;

   @SerializedName ("orderNumber")
   private int orderNumber;

   @SerializedName ("description")
   private String description;

   @SerializedName ("typeText")
   private String typeText;

   @SerializedName ("hasGallery")
   private boolean hasGallery;

   @SerializedName ("isSelected")
   private boolean isSelected;

   @SerializedName ("isOpen")
   private boolean isOpen;

   @SerializedName ("todoStats")
   private CategoryStatModel todoStats;

   @SerializedName ("providerCategory")
   private ProviderCategory providerCategory;

   public CategoryModel () {
   }

   protected CategoryModel (Parcel in) {
      id = in.readInt ();
      name = in.readString ();
      mainCategoryId = in.readInt ();
      categoryType = in.readString ();
      count = in.readInt ();
      icon = in.readString ();
      slug = in.readString ();
      imageUrl = in.readString ();
      isDefault = in.readByte () != 0;
      orderNum = in.readInt ();
      orderNumber = in.readInt ();
      description = in.readString ();
      typeText = in.readString ();
      hasGallery = in.readByte () != 0;
      isSelected = in.readByte () != 0;
      isOpen = in.readByte () != 0;
      providerCategory = in.readParcelable (ProviderCategory.class.getClassLoader ());
   }

   public static final Creator<CategoryModel> CREATOR = new Creator<CategoryModel> () {
      @Override
      public CategoryModel createFromParcel (Parcel in) {
         return new CategoryModel (in);
      }

      @Override
      public CategoryModel[] newArray (int size) {
         return new CategoryModel[size];
      }
   };

   public int getId () {
      return id;
   }

   public void setId (int id) {
      this.id = id;
   }

   public String getName () {
      return name;
   }

   public void setName (String name) {
      this.name = name;
   }

   public int getMainCategoryId () {
      return mainCategoryId;
   }

   public void setMainCategoryId (int mainCategoryId) {
      this.mainCategoryId = mainCategoryId;
   }

   public String getCategoryType () {
      return categoryType;
   }

   public void setCategoryType (String categoryType) {
      this.categoryType = categoryType;
   }

   public int getCount () {
      return count;
   }

   public void setCount (int count) {
      this.count = count;
   }

   public String getIcon () {
      return icon;
   }

   public void setIcon (String icon) {
      this.icon = icon;
   }

   public String getSlug () {
      return slug;
   }

   public void setSlug (String slug) {
      this.slug = slug;
   }

   public String getImageUrl () {
      return imageUrl;
   }

   public void setImageUrl (String imageUrl) {
      this.imageUrl = imageUrl;
   }

   public boolean isDefault () {
      return isDefault;
   }

   public void setDefault (boolean aDefault) {
      isDefault = aDefault;
   }

   public int getOrderNum () {
      return orderNum;
   }

   public void setOrderNum (int orderNum) {
      this.orderNum = orderNum;
   }

   public int getOrderNumber () {
      return orderNumber;
   }

   public void setOrderNumber (int orderNumber) {
      this.orderNumber = orderNumber;
   }

   public String getDescription () {
      return description;
   }

   public void setDescription (String description) {
      this.description = description;
   }

   public String getTypeText () {
      return typeText;
   }

   public void setTypeText (String typeText) {
      this.typeText = typeText;
   }

   public boolean isHasGallery () {
      return hasGallery;
   }

   public void setHasGallery (boolean hasGallery) {
      this.hasGallery = hasGallery;
   }

   public boolean isSelected () {
      return isSelected;
   }

   public void setSelected (boolean selected) {
      isSelected = selected;
   }

   public boolean isOpen () {
      return isOpen;
   }

   public void setOpen (boolean open) {
      isOpen = open;
   }

   public CategoryStatModel getTodoStats () {
      return todoStats;
   }

   public void setTodoStats (CategoryStatModel todoStats) {
      this.todoStats = todoStats;
   }

   public ProviderCategory getProviderCategory () {
      return providerCategory;
   }

   public void setProviderCategory (ProviderCategory providerCategory) {
      this.providerCategory = providerCategory;
   }

   @Override public int describeContents () {
      return 0;
   }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeInt (id);
      dest.writeString (name);
      dest.writeInt (mainCategoryId);
      dest.writeString (categoryType);
      dest.writeInt (count);
      dest.writeString (icon);
      dest.writeString (slug);
      dest.writeString (imageUrl);
      dest.writeByte ((byte) (isDefault ? 1 : 0));
      dest.writeInt (orderNum);
      dest.writeInt (orderNumber);
      dest.writeString (description);
      dest.writeString (typeText);
      dest.writeByte ((byte) (hasGallery ? 1 : 0));
      dest.writeByte ((byte) (isSelected ? 1 : 0));
      dest.writeByte ((byte) (isOpen ? 1 : 0));
      dest.writeParcelable (providerCategory, flags);
   }
}
