package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryStatModel {

   @SerializedName ("total")
   @Expose
   private Integer total;

   @SerializedName ("completed")
   @Expose
   private String  completed;

   public Integer getTotal () {
      return total;
   }

   public void setTotal (Integer total) {
      this.total = total;
   }

   public String getCompleted () {
      return completed;
   }

   public void setCompleted (String completed) {
      this.completed = completed;
   }
}