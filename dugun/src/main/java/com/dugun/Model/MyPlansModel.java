package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class MyPlansModel {

   @SerializedName ("id")
   @Expose
   private Integer id;

   @SerializedName ("name")
   @Expose
   private String name;

   @SerializedName ("img")
   @Expose
   private Integer img;

   @SerializedName ("count")
   @Expose
   private Integer count;

   public MyPlansModel () {
   }

   public MyPlansModel (Integer id, String name, Integer img, Integer count) {
      this.id = id;
      this.name = name;
      this.img = img;
      this.count = count;
   }

   public Integer getId () {
      return id;
   }

   public void setId (Integer id) {
      this.id = id;
   }

   public String getName () {
      return name;
   }

   public void setName (String name) {
      this.name = name;
   }

   public Integer getImg () {
      return img;
   }

   public void setImg (Integer img) {
      this.img = img;
   }

   public Integer getCount () {
      return count;
   }

   public void setCount (Integer count) {
      this.count = count;
   }
}
