package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 27/10/17.
 */

public class ValueData implements Parcelable{

   @SerializedName ("valueId")
   private int valueId;

   @SerializedName ("text")
   private String text;

   protected ValueData (Parcel in) {
      valueId = in.readInt ();
      text = in.readString ();
   }

   public static final Creator<ValueData> CREATOR = new Creator<ValueData> () {
      @Override
      public ValueData createFromParcel (Parcel in) {
         return new ValueData (in);
      }

      @Override
      public ValueData[] newArray (int size) {
         return new ValueData[size];
      }
   };

   public int getValueId () {
      return valueId;
   }

   public void setValueId (int valueId) {
      this.valueId = valueId;
   }

   public String getText () {
      return text;
   }

   public void setText (String text) {
      this.text = text;
   }

   @Override public int describeContents () {
      return 0;
   }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeInt (valueId);
      dest.writeString (text);
   }
}
