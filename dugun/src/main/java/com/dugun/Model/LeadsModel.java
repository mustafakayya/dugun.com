package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class LeadsModel implements Parcelable {

   @SerializedName ("id")
   private int id;

   @SerializedName ("category")
   private CategoryModel category;

   @SerializedName ("provider")
   private ProviderModel2 provider;

   @SerializedName ("name")
   private String name;

   @SerializedName ("email")
   private String email;

   @SerializedName ("phone")
   private String phone;

   @SerializedName ("weddingDate")
   private String weddingDate;

   @SerializedName ("status")
   private String status;

   @SerializedName ("comment")
   private String comment;

   @SerializedName ("coupleNote")
   private String coupleNote;

   @SerializedName ("refUrl")
   private String refUrl;

   protected LeadsModel (Parcel in) {
      id = in.readInt ();
      category = in.readParcelable (CategoryModel.class.getClassLoader ());
      provider = in.readParcelable (ProviderModel2.class.getClassLoader ());
      name = in.readString ();
      email = in.readString ();
      phone = in.readString ();
      weddingDate = in.readString ();
      status = in.readString ();
      comment = in.readString ();
      coupleNote = in.readString ();
      refUrl = in.readString ();
   }

   public static final Creator<LeadsModel> CREATOR = new Creator<LeadsModel> () {
      @Override
      public LeadsModel createFromParcel (Parcel in) {
         return new LeadsModel (in);
      }

      @Override
      public LeadsModel[] newArray (int size) {
         return new LeadsModel[size];
      }
   };

   public int getId () {
      return id;
   }

   public void setId (int id) {
      this.id = id;
   }

   public CategoryModel getCategory () {
      return category;
   }

   public void setCategory (CategoryModel category) {
      this.category = category;
   }

   public ProviderModel2 getProvider () {
      return provider;
   }

   public void setProvider (ProviderModel2 provider) {
      this.provider = provider;
   }

   public String getName () {
      return name;
   }

   public void setName (String name) {
      this.name = name;
   }

   public String getEmail () {
      return email;
   }

   public void setEmail (String email) {
      this.email = email;
   }

   public String getPhone () {
      return phone;
   }

   public void setPhone (String phone) {
      this.phone = phone;
   }

   public String getWeddingDate () {
      return weddingDate;
   }

   public void setWeddingDate (String weddingDate) {
      this.weddingDate = weddingDate;
   }

   public String getStatus () {
      return status;
   }

   public void setStatus (String status) {
      this.status = status;
   }

   public String getComment () {
      return comment;
   }

   public void setComment (String comment) {
      this.comment = comment;
   }

   public String getCoupleNote () {
      return coupleNote;
   }

   public void setCoupleNote (String coupleNote) {
      this.coupleNote = coupleNote;
   }

   public String getRefUrl () {
      return refUrl;
   }

   public void setRefUrl (String refUrl) {
      this.refUrl = refUrl;
   }

   @Override public int describeContents () {
      return 0;
   }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeInt (id);
      dest.writeParcelable (category, flags);
      dest.writeParcelable (provider, flags);
      dest.writeString (name);
      dest.writeString (email);
      dest.writeString (phone);
      dest.writeString (weddingDate);
      dest.writeString (status);
      dest.writeString (comment);
      dest.writeString (coupleNote);
      dest.writeString (refUrl);
   }
}
