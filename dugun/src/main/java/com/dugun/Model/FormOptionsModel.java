package com.dugun.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class FormOptionsModel {

   @SerializedName ("id")
   private int id;

   @SerializedName ("providerFormId")
   private int providerFormId;

   @SerializedName ("orderNum")
   private int orderNum;

   @SerializedName ("optionValue")
   private int optionValue;

   @SerializedName ("optionText")
   private String optionText;

   @SerializedName ("minValue")
   private int minValue;

   @SerializedName ("maxValue")
   private int maxValue;

   @SerializedName ("selected")
   private boolean selected;

   @SerializedName ("imageUrl")
   private String imageUrl;

   @SerializedName ("iconUrl")
   private String iconUrl;

   public String getImageUrl () {
      return imageUrl;
   }

   public void setImageUrl (String imageUrl) {
      this.imageUrl = imageUrl;
   }

   public String getIconUrl () {
      return iconUrl;
   }

   public void setIconUrl (String iconUrl) {
      this.iconUrl = iconUrl;
   }

   public boolean isSelected () {
      return selected;
   }

   public void setSelected (boolean selected) {
      this.selected = selected;
   }

   public int getId () {
      return id;
   }

   public void setId (int id) {
      this.id = id;
   }

   public int getProviderFormId () {
      return providerFormId;
   }

   public void setProviderFormId (int providerFormId) {
      this.providerFormId = providerFormId;
   }

   public int getOrderNum () {
      return orderNum;
   }

   public void setOrderNum (int orderNum) {
      this.orderNum = orderNum;
   }

   public int getOptionValue () {
      return optionValue;
   }

   public void setOptionValue (int optionValue) {
      this.optionValue = optionValue;
   }

   public String getOptionText () {
      return optionText;
   }

   public void setOptionText (String optionText) {
      this.optionText = optionText;
   }

   public int getMinValue () {
      return minValue;
   }

   public void setMinValue (int minValue) {
      this.minValue = minValue;
   }

   public int getMaxValue () {
      return maxValue;
   }

   public void setMaxValue (int maxValue) {
      this.maxValue = maxValue;
   }
}
