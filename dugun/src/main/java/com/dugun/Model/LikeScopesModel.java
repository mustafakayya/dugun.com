package com.dugun.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class LikeScopesModel {

    @SerializedName("data")
    private Data data;

    public LikeScopesModel() {
        this.data = new Data();
    }

    public class Data {

        @SerializedName("media")
        private List<Integer> media;

        @SerializedName("provider")
        private List<Integer> provider;

        public Data() {
            media = new ArrayList<>();
            provider = new ArrayList<>();
        }

        public List<Integer> getMedia() {
            if(media==null)
                media = new ArrayList<>();
            return media;
        }

        public void setMedia(List<Integer> media) {
            this.media = media;
        }

        public List<Integer> getProvider() {
            if(provider==null)
                provider = new ArrayList<>();
            return provider;
        }

        public void setProvider(List<Integer> provider) {
            this.provider = provider;
        }



    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
