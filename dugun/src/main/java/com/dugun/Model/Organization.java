package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Organization {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("name")
@Expose
private String name;
@SerializedName("icon")
@Expose
private String icon;
@SerializedName("imageUrl")
@Expose
private String imageUrl;
@SerializedName("isDefault")
@Expose
private Boolean isDefault;
@SerializedName("orderNumber")
@Expose
private Integer orderNumber;
@SerializedName("description")
@Expose
private Object description;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getIcon() {
return icon;
}

public void setIcon(String icon) {
this.icon = icon;
}

public String getImageUrl() {
return imageUrl;
}

public void setImageUrl(String imageUrl) {
this.imageUrl = imageUrl;
}

public Boolean getIsDefault() {
return isDefault;
}

public void setIsDefault(Boolean isDefault) {
this.isDefault = isDefault;
}

public Integer getOrderNumber() {
return orderNumber;
}

public void setOrderNumber(Integer orderNumber) {
this.orderNumber = orderNumber;
}

public Object getDescription() {
return description;
}

public void setDescription(Object description) {
this.description = description;
}

}