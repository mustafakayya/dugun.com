package com.dugun.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ConceptModel {

   @SerializedName ("id")
   private int id;

   @SerializedName ("categoryId")
   private int categoryId;

   @SerializedName ("fieldName")
   private String fieldName;

   @SerializedName ("fieldType")
   private String fieldType;

   @SerializedName ("fieldExtra")
   private String fieldExtra;

   @SerializedName ("isRequired")
   private boolean isRequired;

   @SerializedName ("formTooltip")
   private String formTooltip;

   @SerializedName ("active")
   private boolean active;

   @SerializedName ("orderNum")
   private int orderNum;

   @SerializedName ("groupId")
   private int groupId;

   @SerializedName ("introLabel")
   private String introLabel;

   @SerializedName ("privileges")
   private String[] privileges;

   @SerializedName ("dependsOn")
   private String dependsOn;

   @SerializedName ("filterName")
   private String filterName;

   @SerializedName ("formOptions")
   private ArrayList<FormOptionsModel> formOptions;

   public int getId () {
      return id;
   }

   public void setId (int id) {
      this.id = id;
   }

   public int getCategoryId () {
      return categoryId;
   }

   public void setCategoryId (int categoryId) {
      this.categoryId = categoryId;
   }

   public String getFieldName () {
      return fieldName;
   }

   public void setFieldName (String fieldName) {
      this.fieldName = fieldName;
   }

   public String getFieldType () {
      return fieldType;
   }

   public void setFieldType (String fieldType) {
      this.fieldType = fieldType;
   }

   public String getFieldExtra () {
      return fieldExtra;
   }

   public void setFieldExtra (String fieldExtra) {
      this.fieldExtra = fieldExtra;
   }

   public boolean isRequired () {
      return isRequired;
   }

   public void setRequired (boolean required) {
      isRequired = required;
   }

   public String getFormTooltip () {
      return formTooltip;
   }

   public void setFormTooltip (String formTooltip) {
      this.formTooltip = formTooltip;
   }

   public boolean isActive () {
      return active;
   }

   public void setActive (boolean active) {
      this.active = active;
   }

   public int getOrderNum () {
      return orderNum;
   }

   public void setOrderNum (int orderNum) {
      this.orderNum = orderNum;
   }

   public int getGroupId () {
      return groupId;
   }

   public void setGroupId (int groupId) {
      this.groupId = groupId;
   }

   public String getIntroLabel () {
      return introLabel;
   }

   public void setIntroLabel (String introLabel) {
      this.introLabel = introLabel;
   }

   public String[] getPrivileges () {
      return privileges;
   }

   public void setPrivileges (String[] privileges) {
      this.privileges = privileges;
   }

   public String getDependsOn () {
      return dependsOn;
   }

   public void setDependsOn (String dependsOn) {
      this.dependsOn = dependsOn;
   }

   public String getFilterName () {
      return filterName;
   }

   public void setFilterName (String filterName) {
      this.filterName = filterName;
   }

   public ArrayList<FormOptionsModel> getFormOptions () {
      return formOptions;
   }

   public void setFormOptions (ArrayList<FormOptionsModel> formOptions) {
      this.formOptions = formOptions;
   }

}
