package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class BadgeModel implements Parcelable{

   @SerializedName ("id")
   private Integer id;

   @SerializedName ("name")
   private String name;

   @SerializedName ("type")
   private String type;

   @SerializedName ("coupleType")
   private String[] coupleType;

   @SerializedName ("description")
   private String description;

   @SerializedName ("image")
   private String image;

   @SerializedName ("orderNumber")
   private Integer orderNumber;

   @SerializedName ("active")
   private Boolean active;

   @SerializedName ("isEarned")
   private Boolean isEarned;

   public Integer getId () {
      return id;
   }

   public void setId (Integer id) {
      this.id = id;
   }

   public String getName () {
      return name;
   }

   public void setName (String name) {
      this.name = name;
   }

   public String getType () {
      return type;
   }

   public void setType (String type) {
      this.type = type;
   }

   public String[] getCoupleType () {
      return coupleType;
   }

   public void setCoupleType (String[] coupleType) {
      this.coupleType = coupleType;
   }

   public String getDescription () {
      return description;
   }

   public void setDescription (String description) {
      this.description = description;
   }

   public String getImage () {
      return image;
   }

   public void setImage (String image) {
      this.image = image;
   }

   public Integer getOrderNumber () {
      return orderNumber;
   }

   public void setOrderNumber (Integer orderNumber) {
      this.orderNumber = orderNumber;
   }

   public Boolean getActive () {
      return active;
   }

   public void setActive (Boolean active) {
      this.active = active;
   }

   public Boolean getEarned () {
      return isEarned;
   }

   public void setEarned (Boolean earned) {
      isEarned = earned;
   }


   @Override public int describeContents () { return 0; }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeValue (this.id);
      dest.writeString (this.name);
      dest.writeString (this.type);
      dest.writeStringArray (this.coupleType);
      dest.writeString (this.description);
      dest.writeString (this.image);
      dest.writeValue (this.orderNumber);
      dest.writeValue (this.active);
      dest.writeValue (this.isEarned);
   }

   public BadgeModel () {}

   protected BadgeModel (Parcel in) {
      this.id = (Integer) in.readValue (Integer.class.getClassLoader ());
      this.name = in.readString ();
      this.type = in.readString ();
      this.coupleType = in.createStringArray ();
      this.description = in.readString ();
      this.image = in.readString ();
      this.orderNumber = (Integer) in.readValue (Integer.class.getClassLoader ());
      this.active = (Boolean) in.readValue (Boolean.class.getClassLoader ());
      this.isEarned = (Boolean) in.readValue (Boolean.class.getClassLoader ());
   }

   public static final Creator<BadgeModel> CREATOR = new Creator<BadgeModel> () {
      @Override public BadgeModel createFromParcel (Parcel source) {return new BadgeModel (source);}

      @Override public BadgeModel[] newArray (int size) {return new BadgeModel[size];}
   };
}
