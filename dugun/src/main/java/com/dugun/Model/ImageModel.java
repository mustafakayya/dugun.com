package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ImageModel {

   @SerializedName ("id")
   @Expose
   private Integer id;

   @SerializedName("image")
   @Expose
   private String image;

   @SerializedName("title")
   @Expose
   private String title;

   @SerializedName("description")
   @Expose
   private String description;

   @SerializedName("orderNum")
   @Expose
   private Integer orderNum;

   @SerializedName("imageCode")
   @Expose
   private String imageCode;

   @SerializedName("width")
   @Expose
   private String width;

   @SerializedName("height")
   @Expose
   private String height;

   @SerializedName("metaTitle")
   @Expose
   private String metaTitle;

   @SerializedName("metaDescription")
   @Expose
   private String metaDescription;

   @SerializedName("status")
   @Expose
   private Boolean status;

   @SerializedName("createdAt")
   @Expose
   private String createdAt;

   @SerializedName("type")
   @Expose
   private String type;

   @SerializedName("url")
   @Expose
   private String url;

   @SerializedName("imageUrl")
   @Expose
   private UrlModel imageUrl;

   @SerializedName("isMainImage")
   @Expose
   private Boolean isMainImage;

   public Integer getId () {
      return id;
   }

   public void setId (Integer id) {
      this.id = id;
   }

   public String getImage () {
      return image;
   }

   public void setImage (String image) {
      this.image = image;
   }

   public String getTitle () {
      return title;
   }

   public void setTitle (String title) {
      this.title = title;
   }

   public String getDescription () {
      return description;
   }

   public void setDescription (String description) {
      this.description = description;
   }

   public Integer getOrderNum () {
      return orderNum;
   }

   public void setOrderNum (Integer orderNum) {
      this.orderNum = orderNum;
   }

   public String getImageCode () {
      return imageCode;
   }

   public void setImageCode (String imageCode) {
      this.imageCode = imageCode;
   }

   public String getWidth () {
      return width;
   }

   public void setWidth (String width) {
      this.width = width;
   }

   public String getHeight () {
      return height;
   }

   public void setHeight (String height) {
      this.height = height;
   }

   public String getMetaTitle () {
      return metaTitle;
   }

   public void setMetaTitle (String metaTitle) {
      this.metaTitle = metaTitle;
   }

   public String getMetaDescription () {
      return metaDescription;
   }

   public void setMetaDescription (String metaDescription) {
      this.metaDescription = metaDescription;
   }

   public Boolean getStatus () {
      return status;
   }

   public void setStatus (Boolean status) {
      this.status = status;
   }

   public String getCreatedAt () {
      return createdAt;
   }

   public void setCreatedAt (String createdAt) {
      this.createdAt = createdAt;
   }

   public String getType () {
      return type;
   }

   public void setType (String type) {
      this.type = type;
   }

   public String getUrl () {
      return url;
   }

   public void setUrl (String url) {
      this.url = url;
   }

   public UrlModel getImageUrl () {
      return imageUrl;
   }

   public void setImageUrl (UrlModel imageUrl) {
      this.imageUrl = imageUrl;
   }

   public Boolean getMainImage () {
      return isMainImage;
   }

   public void setMainImage (Boolean mainImage) {
      isMainImage = mainImage;
   }
}
