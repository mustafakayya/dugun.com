package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mustafakaya on 11/30/17.
 */

public class NewLeadInteraction {

    @SerializedName("comment")
    @Expose
    String comment;

    public NewLeadInteraction(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
