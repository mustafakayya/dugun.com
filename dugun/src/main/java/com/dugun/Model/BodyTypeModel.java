package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class BodyTypeModel {

   @SerializedName ("id")
   @Expose
   private Integer id;

   @SerializedName ("name")
   @Expose
   private String name;

   @SerializedName ("img")
   @Expose
   private Integer img;

   @SerializedName ("status")
   @Expose
   private Boolean status;

   public BodyTypeModel () {
   }

   public BodyTypeModel (Integer id, String name, Integer img, Boolean status) {
      this.id = id;
      this.name = name;
      this.img = img;
      this.status = status;
   }

   public Integer getId () {
      return id;
   }

   public void setId (Integer id) {
      this.id = id;
   }

   public String getName () {
      return name;
   }

   public void setName (String name) {
      this.name = name;
   }

   public Integer getImg () {
      return img;
   }

   public void setImg (Integer img) {
      this.img = img;
   }

   public Boolean getStatus () {
      return status;
   }

   public void setStatus (Boolean status) {
      this.status = status;
   }
}
