package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class FormDataModel implements Parcelable{

   @SerializedName ("groupId")
   private int groupId;

   @SerializedName ("groupName")
   private String groupName;

   @SerializedName ("data")
   public ArrayList<FormDataInnerData> data ;

   protected FormDataModel (Parcel in) {
      groupId = in.readInt ();
      groupName = in.readString ();
      data = in.createTypedArrayList (FormDataInnerData.CREATOR);
   }

   public FormDataModel () {
   }

   public static final Creator<FormDataModel> CREATOR = new Creator<FormDataModel> () {
      @Override
      public FormDataModel createFromParcel (Parcel in) {
         return new FormDataModel (in);
      }

      @Override
      public FormDataModel[] newArray (int size) {
         return new FormDataModel[size];
      }
   };

   public int getGroupId () {
      return groupId;
   }

   public void setGroupId (int groupId) {
      this.groupId = groupId;
   }

   public String getGroupName () {
      return groupName;
   }

   public void setGroupName (String groupName) {
      this.groupName = groupName;
   }

   public ArrayList<FormDataInnerData> getData () {
      return data;
   }

   public void setData (ArrayList<FormDataInnerData> data) {
      this.data = data;
   }

   @Override public int describeContents () {
      return 0;
   }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeInt (groupId);
      dest.writeString (groupName);
      dest.writeTypedList (data);
   }
}
