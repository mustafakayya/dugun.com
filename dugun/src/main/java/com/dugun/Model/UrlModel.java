package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class UrlModel {

   @SerializedName ("original")
   @Expose
   private String original;

   @SerializedName("prev")
   @Expose
   private String prev;

   @SerializedName("thumbnail")
   @Expose
   private String thumbnail;

   public String getOriginal () {
      return original;
   }

   public void setOriginal (String original) {
      this.original = original;
   }

   public String getPrev () {
      return prev;
   }

   public void setPrev (String prev) {
      this.prev = prev;
   }

   public String getThumbnail () {
      return thumbnail;
   }

   public void setThumbnail (String thumbnail) {
      this.thumbnail = thumbnail;
   }
}
