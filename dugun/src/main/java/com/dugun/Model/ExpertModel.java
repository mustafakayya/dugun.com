package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ExpertModel {

   @SerializedName ("id")
   @Expose
   private Integer id;

   @SerializedName ("name")
   @Expose
   private String name;

   @SerializedName ("googlePlusId")
   @Expose
   private String googlePlusId;

   public Integer getId () {
      return id;
   }

   public void setId (Integer id) {
      this.id = id;
   }

   public String getName () {
      return name;
   }

   public void setName (String name) {
      this.name = name;
   }

   public String getGooglePlusId () {
      return googlePlusId;
   }

   public void setGooglePlusId (String googlePlusId) {
      this.googlePlusId = googlePlusId;
   }
}
