package com.dugun.Model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserModel implements Serializable{

    @SerializedName("id")
    private Integer id;

    @SerializedName("uId")
    private String uId;

    @SerializedName("name")
    private String name;

    @SerializedName("coupleTypeId")
    private String coupleTypeId;

    @SerializedName("partnerName")
    private String partnerName;

    @SerializedName("partnerEmail")
    private String partnerEmail;

    @SerializedName("city")
    private CityModel city;

    @SerializedName("phone")
    private String phone;

    @SerializedName("weddingDate")
    private String weddingDate;

    @SerializedName("photos")
    UserPhotos photos;

    @SerializedName("isBounced")
    private Boolean isBounced;

    @SerializedName("isUnsubscribed")
    private Boolean isUnsubscribed;

    @SerializedName("coupleName")
    private String coupleName;

    @SerializedName("isWeddingDateKnown")
    private Boolean isWeddingDateKnown;

    @SerializedName("weddingPlaceType")
    private int[] weddingPlaceType;

    public int[] getWeddingPlaceType() {
        return weddingPlaceType;
    }

    public void setWeddingPlaceType(int[] weddingPlaceType) {
        this.weddingPlaceType = weddingPlaceType;
    }

    public CityModel getCity() {
        return city;
    }

    public void setCity(CityModel city) {
        this.city = city;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoupleTypeId() {
        return coupleTypeId;
    }

    public void setCoupleTypeId(String coupleTypeId) {
        this.coupleTypeId = coupleTypeId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getPartnerEmail() {
        return partnerEmail;
    }

    public void setPartnerEmail(String partnerEmail) {
        this.partnerEmail = partnerEmail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWeddingDate() {
        return weddingDate;
    }

    public void setWeddingDate(String weddingDate) {
        this.weddingDate = weddingDate;
    }

    public Boolean getBounced() {
        return isBounced;
    }

    public void setBounced(Boolean bounced) {
        isBounced = bounced;
    }

    public Boolean getUnsubscribed() {
        return isUnsubscribed;
    }

    public void setUnsubscribed(Boolean unsubscribed) {
        isUnsubscribed = unsubscribed;
    }

    public String getCoupleName() {
        return coupleName;
    }

    public void setCoupleName(String coupleName) {
        this.coupleName = coupleName;
    }

    public Boolean getWeddingDateKnown() {
        return isWeddingDateKnown;
    }

    public void setWeddingDateKnown(Boolean weddingDateKnown) {
        isWeddingDateKnown = weddingDateKnown;
    }


    public UserPhotos getPhotos() {
        return photos;
    }

    public void setPhotos(UserPhotos photos) {
        this.photos = photos;
    }


    public String getUserPhotoUrl(){
        if(photos==null || TextUtils.isEmpty(photos.getWidePhoto()) )
            return null;
        else
            return photos.getWidePhoto();
    }
}