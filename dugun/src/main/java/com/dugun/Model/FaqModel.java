package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class FaqModel {

   @SerializedName ("id")
   @Expose
   private Integer id;

   @SerializedName("question")
   @Expose
   private String question;

   @SerializedName("answer")
   @Expose
   private String answer;

   public FaqModel (Integer id, String question, String answer) {
      this.id = id;
      this.question = question;
      this.answer = answer;
   }

   public Integer getId () {
      return id;
   }

   public void setId (Integer id) {
      this.id = id;
   }

   public String getQuestion () {
      return question;
   }

   public void setQuestion (String question) {
      this.question = question;
   }

   public String getAnswer () {
      return answer;
   }

   public void setAnswer (String answer) {
      this.answer = answer;
   }
}
