package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class PhoneModel implements Parcelable{

   @SerializedName ("data")
   @Expose
   private String data;

   protected PhoneModel (Parcel in) {
      data = in.readString ();
   }

   public static final Creator<PhoneModel> CREATOR = new Creator<PhoneModel> () {
      @Override
      public PhoneModel createFromParcel (Parcel in) {
         return new PhoneModel (in);
      }

      @Override
      public PhoneModel[] newArray (int size) {
         return new PhoneModel[size];
      }
   };

   public String getData () {
      return data;
   }

   public void setData (String data) {
      this.data = data;
   }

   @Override public int describeContents () {
      return 0;
   }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeString (data);
   }
}
