package com.dugun.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class SettingsModel {

   @SerializedName ("data")
   public SettingsData data;

   public class SettingsData {

      @SerializedName ("about_us_page_url")
      public String about_us_page_url;

      @SerializedName ("privacy_page_url")
      public String privacy_page_url;

      @SerializedName ("terms_page_url")
      public String terms_page_url;

      @SerializedName ("faq_page_url")
      public String faq_page_url;

      @SerializedName ("version_page_url")
      public String version_page_url;

      public String getAbout_us_page_url () {
         return about_us_page_url;
      }

      public void setAbout_us_page_url (String about_us_page_url) {
         this.about_us_page_url = about_us_page_url;
      }

      public String getPrivacy_page_url () {
         return privacy_page_url;
      }

      public void setPrivacy_page_url (String privacy_page_url) {
         this.privacy_page_url = privacy_page_url;
      }

      public String getTerms_page_url () {
         return terms_page_url;
      }

      public void setTerms_page_url (String terms_page_url) {
         this.terms_page_url = terms_page_url;
      }

      public String getFaq_page_url () {
         return faq_page_url;
      }

      public void setFaq_page_url (String faq_page_url) {
         this.faq_page_url = faq_page_url;
      }

      public String getVersion_page_url () {
         return version_page_url;
      }

      public void setVersion_page_url (String version_page_url) {
         this.version_page_url = version_page_url;
      }
   }


}
