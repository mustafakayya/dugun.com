package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class MediaInfoTagModel {

   @SerializedName ("galleryCategoryInfoTagOption")
   @Expose
   private GalleryCategoryInfoTagOption galleryCategoryInfoTagOption;

   public class GalleryCategoryInfoTagOption {

      @SerializedName ("value")
      @Expose
      private String value;

      @SerializedName ("galleryCategoryInfoTag")
      @Expose
      private GalleryCategoryInfoTag galleryCategoryInfoTag;

      public class GalleryCategoryInfoTag {

         @SerializedName ("name")
         @Expose
         private String name;

         public String getName () {
            return name;
         }

         public void setName (String name) {
            this.name = name;
         }
      }

      public String getValue () {
         return value;
      }

      public void setValue (String value) {
         this.value = value;
      }

      public GalleryCategoryInfoTag getGalleryCategoryInfoTag () {
         return galleryCategoryInfoTag;
      }

      public void setGalleryCategoryInfoTag (GalleryCategoryInfoTag galleryCategoryInfoTag) {
         this.galleryCategoryInfoTag = galleryCategoryInfoTag;
      }
   }

   public GalleryCategoryInfoTagOption getGalleryCategoryInfoTagOption () {
      return galleryCategoryInfoTagOption;
   }

   public void setGalleryCategoryInfoTagOption (GalleryCategoryInfoTagOption galleryCategoryInfoTagOption) {
      this.galleryCategoryInfoTagOption = galleryCategoryInfoTagOption;
   }
}
