package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class BadgesModel implements Parcelable{

   @SerializedName ("meta")
   private MetaModel meta;

   @SerializedName ("data")
   private List<Badge> data;

   public class Badge {

      @SerializedName ("id")
      private Integer id;

      @SerializedName ("coupleId")
      private Integer coupleId;

      @SerializedName ("badge")
      private BadgeModel badge;

      @SerializedName ("isNotified")
      private Boolean isNotified;

      @SerializedName ("createdAt")
      private String createdAt;

      @SerializedName ("updatedAt")
      private String updatedAt;

      public Integer getId () {
         return id;
      }

      public void setId (Integer id) {
         this.id = id;
      }

      public Integer getCoupleId () {
         return coupleId;
      }

      public void setCoupleId (Integer coupleId) {
         this.coupleId = coupleId;
      }

      public BadgeModel getBadge () {
         return badge;
      }

      public void setBadge (BadgeModel badge) {
         this.badge = badge;
      }

      public Boolean getNotified () {
         return isNotified;
      }

      public void setNotified (Boolean notified) {
         isNotified = notified;
      }

      public String getCreatedAt () {
         return createdAt;
      }

      public void setCreatedAt (String createdAt) {
         this.createdAt = createdAt;
      }

      public String getUpdatedAt () {
         return updatedAt;
      }

      public void setUpdatedAt (String updatedAt) {
         this.updatedAt = updatedAt;
      }
   }

   @SerializedName ("notEarnedBadges")
   private List<BadgeModel> notEarnedBadges;

   public MetaModel getMeta () {
      return meta;
   }

   public void setMeta (MetaModel meta) {
      this.meta = meta;
   }

   public List<Badge> getData () {
      return data;
   }

   public void setData (List<Badge> data) {
      this.data = data;
   }

   public List<BadgeModel> getNotEarnedBadges () {
      return notEarnedBadges;
   }

   public void setNotEarnedBadges (List<BadgeModel> notEarnedBadges) {
      this.notEarnedBadges = notEarnedBadges;
   }

   @Override public int describeContents () { return 0; }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeParcelable (this.meta, flags);
      dest.writeList (this.data);
      dest.writeTypedList (this.notEarnedBadges);
   }

   public BadgesModel () {}

   private BadgesModel (Parcel in) {
      this.meta = in.readParcelable (MetaModel.class.getClassLoader ());
      this.data = new ArrayList<Badge> ();
      in.readList (this.data, Badge.class.getClassLoader ());
      this.notEarnedBadges = in.createTypedArrayList (BadgeModel.CREATOR);
   }

   public static final Creator<BadgesModel> CREATOR = new Creator<BadgesModel> () {
      @Override public BadgesModel createFromParcel (Parcel source) {
         return new BadgesModel (source);
      }

      @Override public BadgesModel[] newArray (int size) {return new BadgesModel[size];}
   };
}
