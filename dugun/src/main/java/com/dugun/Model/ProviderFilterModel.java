package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 21/09/17.
 */

public class ProviderFilterModel implements Parcelable {

   @SerializedName ("id")
   private int id;

   @SerializedName ("categoryId")
   private int categoryId;

   @SerializedName ("category")
   private CategoryModel category;

   @SerializedName ("cityId")
   private int cityId;

   @SerializedName ("city")
   private CityModel city;

   @SerializedName ("districtId")
   private int districtId;

   @SerializedName ("district")
   private DistrictModel district;

   @SerializedName ("areaId")
   private int areaId;

   @SerializedName ("districtExtra")
   private int[] districtExtra;

   @SerializedName ("districtStatic")
   private int[] districtStatic;

   @SerializedName ("statusId")
   private int statusId;

   @SerializedName ("isPromotion")
   private boolean isPromotion;

   @SerializedName ("name")
   private String name;

   @SerializedName ("description")
   private String description;

   @SerializedName ("profileUrl")
   private String profileUrl;

   @SerializedName ("imageUrl")
   private String imageUrl;

   @SerializedName ("imageFullPath")
   private String imageFullPath;

   @SerializedName ("imageCount")
   private int imageCount;

   @SerializedName ("videoCount")
   @Expose
   private int videoCount;

   @SerializedName ("discountCount")
   private int discountCount;

   @SerializedName ("reviewCount")
   private int reviewCount;

   @SerializedName ("leadCount")
   private int leadCount;

   @SerializedName ("viewCount")
   private int viewCount;

   @SerializedName ("infoRequestCount")
   private int infoRequestCount;

   @SerializedName ("inboundCallCount")
   private int inboundCallCount;

   @SerializedName ("averageLeadCount")
   private int averageLeadCount;

   @SerializedName ("averageViewCount")
   private int averageViewCount;

   @SerializedName ("leadScore")
   private float leadScore;

   @SerializedName ("productScore")
   private float productScore;

   @SerializedName ("pageWeight")
   private int pageWeight;

   @SerializedName ("pageWeightFilter")
   private int pageWeightFilter;

   @SerializedName ("homePageShowCase")
   private boolean homePageShowCase;

   @SerializedName ("categoryShowCase")
   private boolean categoryShowCase;

   @SerializedName ("discountHomeDoping")
   private boolean discountHomeDoping;

   @SerializedName ("discountMicrositeDoping")
   private boolean discountMicrositeDoping;

   @SerializedName ("discountStaticDoping")
   private boolean discountStaticDoping;

   @SerializedName ("showOnDiscountListingPage")
   private boolean showOnDiscountListingPage;

   @SerializedName ("venueFeaturedDoping")
   private boolean venueFeaturedDoping;

   @SerializedName ("filter1")
   private String[] filter1;

   @SerializedName ("filter2")
   private String[] filter2;

   @SerializedName ("filter3")
   private String[] filter3;

   @SerializedName ("filter4")
   private String[] filter4;

   @SerializedName ("filter5")
   private String[] filter5;

   @SerializedName ("filter6")
   private String[] filter6;

   @SerializedName ("mapLat")
   private String mapLat;

   @SerializedName ("mapLng")
   private String mapLng;

   @SerializedName ("productListingPriority")
   private String productListingPriority;

   @SerializedName ("discountCategoryStatic")
   private boolean discountCategoryStatic;

   protected ProviderFilterModel (Parcel in) {
      id = in.readInt ();
      categoryId = in.readInt ();
      category = in.readParcelable (CategoryModel.class.getClassLoader ());
      cityId = in.readInt ();
      city = in.readParcelable (CityModel.class.getClassLoader ());
      districtId = in.readInt ();
      district = in.readParcelable (DistrictModel.class.getClassLoader ());
      areaId = in.readInt ();
      districtExtra = in.createIntArray ();
      districtStatic = in.createIntArray ();
      statusId = in.readInt ();
      isPromotion = in.readByte () != 0;
      name = in.readString ();
      description = in.readString ();
      profileUrl = in.readString ();
      imageUrl = in.readString ();
      imageFullPath = in.readString ();
      imageCount = in.readInt ();
      videoCount = in.readInt ();
      discountCount = in.readInt ();
      reviewCount = in.readInt ();
      leadCount = in.readInt ();
      viewCount = in.readInt ();
      infoRequestCount = in.readInt ();
      inboundCallCount = in.readInt ();
      averageLeadCount = in.readInt ();
      averageViewCount = in.readInt ();
      leadScore = in.readFloat ();
      productScore = in.readFloat ();
      pageWeight = in.readInt ();
      pageWeightFilter = in.readInt ();
      homePageShowCase = in.readByte () != 0;
      categoryShowCase = in.readByte () != 0;
      discountHomeDoping = in.readByte () != 0;
      discountMicrositeDoping = in.readByte () != 0;
      discountStaticDoping = in.readByte () != 0;
      showOnDiscountListingPage = in.readByte () != 0;
      venueFeaturedDoping = in.readByte () != 0;
      filter1 = in.createStringArray ();
      filter2 = in.createStringArray ();
      filter3 = in.createStringArray ();
      filter4 = in.createStringArray ();
      filter5 = in.createStringArray ();
      filter6 = in.createStringArray ();
      mapLat = in.readString ();
      mapLng = in.readString ();
      productListingPriority = in.readString ();
      discountCategoryStatic = in.readByte () != 0;
   }

   public static final Creator<ProviderFilterModel> CREATOR = new Creator<ProviderFilterModel> () {
      @Override
      public ProviderFilterModel createFromParcel (Parcel in) {
         return new ProviderFilterModel (in);
      }

      @Override
      public ProviderFilterModel[] newArray (int size) {
         return new ProviderFilterModel[size];
      }
   };

   public int getId () {
      return id;
   }

   public void setId (int id) {
      this.id = id;
   }

   public int getCategoryId () {
      return categoryId;
   }

   public void setCategoryId (int categoryId) {
      this.categoryId = categoryId;
   }

   public CategoryModel getCategory () {
      return category;
   }

   public void setCategory (CategoryModel category) {
      this.category = category;
   }

   public int getCityId () {
      return cityId;
   }

   public void setCityId (int cityId) {
      this.cityId = cityId;
   }

   public CityModel getCity () {
      return city;
   }

   public void setCity (CityModel city) {
      this.city = city;
   }

   public int getDistrictId () {
      return districtId;
   }

   public void setDistrictId (int districtId) {
      this.districtId = districtId;
   }

   public DistrictModel getDistrict () {
      return district;
   }

   public void setDistrict (DistrictModel district) {
      this.district = district;
   }

   public int getAreaId () {
      return areaId;
   }

   public void setAreaId (int areaId) {
      this.areaId = areaId;
   }

   public int[] getDistrictExtra () {
      return districtExtra;
   }

   public void setDistrictExtra (int[] districtExtra) {
      this.districtExtra = districtExtra;
   }

   public int[] getDistrictStatic () {
      return districtStatic;
   }

   public void setDistrictStatic (int[] districtStatic) {
      this.districtStatic = districtStatic;
   }

   public int getStatusId () {
      return statusId;
   }

   public void setStatusId (int statusId) {
      this.statusId = statusId;
   }

   public boolean isPromotion () {
      return isPromotion;
   }

   public void setPromotion (boolean promotion) {
      isPromotion = promotion;
   }

   public String getName () {
      return name;
   }

   public void setName (String name) {
      this.name = name;
   }

   public String getDescription () {
      return description;
   }

   public void setDescription (String description) {
      this.description = description;
   }

   public String getProfileUrl () {
      return profileUrl;
   }

   public void setProfileUrl (String profileUrl) {
      this.profileUrl = profileUrl;
   }

   public String getImageUrl () {
      return imageUrl;
   }

   public void setImageUrl (String imageUrl) {
      this.imageUrl = imageUrl;
   }

   public String getImageFullPath () {
      return imageFullPath;
   }

   public void setImageFullPath (String imageFullPath) {
      this.imageFullPath = imageFullPath;
   }

   public int getImageCount () {
      return imageCount;
   }

   public void setImageCount (int imageCount) {
      this.imageCount = imageCount;
   }

   public int getVideoCount () {
      return videoCount;
   }

   public void setVideoCount (int videoCount) {
      this.videoCount = videoCount;
   }

   public int getDiscountCount () {
      return discountCount;
   }

   public void setDiscountCount (int discountCount) {
      this.discountCount = discountCount;
   }

   public int getReviewCount () {
      return reviewCount;
   }

   public void setReviewCount (int reviewCount) {
      this.reviewCount = reviewCount;
   }

   public int getLeadCount () {
      return leadCount;
   }

   public void setLeadCount (int leadCount) {
      this.leadCount = leadCount;
   }

   public int getViewCount () {
      return viewCount;
   }

   public void setViewCount (int viewCount) {
      this.viewCount = viewCount;
   }

   public int getInfoRequestCount () {
      return infoRequestCount;
   }

   public void setInfoRequestCount (int infoRequestCount) {
      this.infoRequestCount = infoRequestCount;
   }

   public int getInboundCallCount () {
      return inboundCallCount;
   }

   public void setInboundCallCount (int inboundCallCount) {
      this.inboundCallCount = inboundCallCount;
   }

   public int getAverageLeadCount () {
      return averageLeadCount;
   }

   public void setAverageLeadCount (int averageLeadCount) {
      this.averageLeadCount = averageLeadCount;
   }

   public int getAverageViewCount () {
      return averageViewCount;
   }

   public void setAverageViewCount (int averageViewCount) {
      this.averageViewCount = averageViewCount;
   }

   public float getLeadScore () {
      return leadScore;
   }

   public void setLeadScore (float leadScore) {
      this.leadScore = leadScore;
   }

   public float getProductScore () {
      return productScore;
   }

   public void setProductScore (float productScore) {
      this.productScore = productScore;
   }

   public int getPageWeight () {
      return pageWeight;
   }

   public void setPageWeight (int pageWeight) {
      this.pageWeight = pageWeight;
   }

   public int getPageWeightFilter () {
      return pageWeightFilter;
   }

   public void setPageWeightFilter (int pageWeightFilter) {
      this.pageWeightFilter = pageWeightFilter;
   }

   public boolean isHomePageShowCase () {
      return homePageShowCase;
   }

   public void setHomePageShowCase (boolean homePageShowCase) {
      this.homePageShowCase = homePageShowCase;
   }

   public boolean isCategoryShowCase () {
      return categoryShowCase;
   }

   public void setCategoryShowCase (boolean categoryShowCase) {
      this.categoryShowCase = categoryShowCase;
   }

   public boolean isDiscountHomeDoping () {
      return discountHomeDoping;
   }

   public void setDiscountHomeDoping (boolean discountHomeDoping) {
      this.discountHomeDoping = discountHomeDoping;
   }

   public boolean isDiscountMicrositeDoping () {
      return discountMicrositeDoping;
   }

   public void setDiscountMicrositeDoping (boolean discountMicrositeDoping) {
      this.discountMicrositeDoping = discountMicrositeDoping;
   }

   public boolean isDiscountStaticDoping () {
      return discountStaticDoping;
   }

   public void setDiscountStaticDoping (boolean discountStaticDoping) {
      this.discountStaticDoping = discountStaticDoping;
   }

   public boolean isShowOnDiscountListingPage () {
      return showOnDiscountListingPage;
   }

   public void setShowOnDiscountListingPage (boolean showOnDiscountListingPage) {
      this.showOnDiscountListingPage = showOnDiscountListingPage;
   }

   public boolean isVenueFeaturedDoping () {
      return venueFeaturedDoping;
   }

   public void setVenueFeaturedDoping (boolean venueFeaturedDoping) {
      this.venueFeaturedDoping = venueFeaturedDoping;
   }

   public String[] getFilter1 () {
      return filter1;
   }

   public void setFilter1 (String[] filter1) {
      this.filter1 = filter1;
   }

   public String[] getFilter2 () {
      return filter2;
   }

   public void setFilter2 (String[] filter2) {
      this.filter2 = filter2;
   }

   public String[] getFilter3 () {
      return filter3;
   }

   public void setFilter3 (String[] filter3) {
      this.filter3 = filter3;
   }

   public String[] getFilter4 () {
      return filter4;
   }

   public void setFilter4 (String[] filter4) {
      this.filter4 = filter4;
   }

   public String[] getFilter5 () {
      return filter5;
   }

   public void setFilter5 (String[] filter5) {
      this.filter5 = filter5;
   }

   public String[] getFilter6 () {
      return filter6;
   }

   public void setFilter6 (String[] filter6) {
      this.filter6 = filter6;
   }

   public String getMapLat () {
      return mapLat;
   }

   public void setMapLat (String mapLat) {
      this.mapLat = mapLat;
   }

   public String getMapLng () {
      return mapLng;
   }

   public void setMapLng (String mapLng) {
      this.mapLng = mapLng;
   }

   public String getProductListingPriority () {
      return productListingPriority;
   }

   public void setProductListingPriority (String productListingPriority) {
      this.productListingPriority = productListingPriority;
   }

   public boolean isDiscountCategoryStatic () {
      return discountCategoryStatic;
   }

   public void setDiscountCategoryStatic (boolean discountCategoryStatic) {
      this.discountCategoryStatic = discountCategoryStatic;
   }


   @Override public int describeContents () {
      return 0;
   }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeInt (id);
      dest.writeInt (categoryId);
      dest.writeParcelable (category, flags);
      dest.writeInt (cityId);
      dest.writeParcelable (city, flags);
      dest.writeInt (districtId);
      dest.writeParcelable (district, flags);
      dest.writeInt (areaId);
      dest.writeIntArray (districtExtra);
      dest.writeIntArray (districtStatic);
      dest.writeInt (statusId);
      dest.writeByte ((byte) (isPromotion ? 1 : 0));
      dest.writeString (name);
      dest.writeString (description);
      dest.writeString (profileUrl);
      dest.writeString (imageUrl);
      dest.writeString (imageFullPath);
      dest.writeInt (imageCount);
      dest.writeInt (videoCount);
      dest.writeInt (discountCount);
      dest.writeInt (reviewCount);
      dest.writeInt (leadCount);
      dest.writeInt (viewCount);
      dest.writeInt (infoRequestCount);
      dest.writeInt (inboundCallCount);
      dest.writeInt (averageLeadCount);
      dest.writeInt (averageViewCount);
      dest.writeFloat (leadScore);
      dest.writeFloat (productScore);
      dest.writeInt (pageWeight);
      dest.writeInt (pageWeightFilter);
      dest.writeByte ((byte) (homePageShowCase ? 1 : 0));
      dest.writeByte ((byte) (categoryShowCase ? 1 : 0));
      dest.writeByte ((byte) (discountHomeDoping ? 1 : 0));
      dest.writeByte ((byte) (discountMicrositeDoping ? 1 : 0));
      dest.writeByte ((byte) (discountStaticDoping ? 1 : 0));
      dest.writeByte ((byte) (showOnDiscountListingPage ? 1 : 0));
      dest.writeByte ((byte) (venueFeaturedDoping ? 1 : 0));
      dest.writeStringArray (filter1);
      dest.writeStringArray (filter2);
      dest.writeStringArray (filter3);
      dest.writeStringArray (filter4);
      dest.writeStringArray (filter5);
      dest.writeStringArray (filter6);
      dest.writeString (mapLat);
      dest.writeString (mapLng);
      dest.writeString (productListingPriority);
      dest.writeByte ((byte) (discountCategoryStatic ? 1 : 0));
   }
}
