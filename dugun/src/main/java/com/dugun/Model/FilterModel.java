package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class FilterModel implements Parcelable {

   @SerializedName ("id")
   private int id;

   @SerializedName ("categoryId")
   private int categoryId;

   @SerializedName ("fieldName")
   private String fieldName;

   @SerializedName ("introLabel")
   private String introLabel;

   @SerializedName ("fieldType")
   private String fieldType;

   @SerializedName ("fieldExtra")
   private String fieldExtra;

   @SerializedName ("filterName")
   private String filterName;

   @SerializedName ("formTooltip")
   private String formTooltip;

   @SerializedName ("isRequired")
   private boolean isRequired;

   @SerializedName ("groupId")
   private int groupId;

   @SerializedName ("status")
   private int status;

   @SerializedName ("searchType")
   private String searchType;

   @SerializedName ("formOptions")
   private ArrayList<FormOptionsModel> formOptions;

   @SerializedName ("selectedFormOption")
   private FormOptionsModel selectedFormOption;

   public FilterModel () {
   }

   public FilterModel (Parcel in) {
      id = in.readInt ();
      categoryId = in.readInt ();
      fieldName = in.readString ();
      introLabel = in.readString ();
      fieldType = in.readString ();
      fieldExtra = in.readString ();
      filterName = in.readString ();
      formTooltip = in.readString ();
      isRequired = in.readByte () != 0;
      groupId = in.readInt ();
      status = in.readInt ();
      searchType = in.readString ();
   }

   public static final Creator<FilterModel> CREATOR = new Creator<FilterModel> () {
      @Override
      public FilterModel createFromParcel (Parcel in) {
         return new FilterModel (in);
      }

      @Override
      public FilterModel[] newArray (int size) {
         return new FilterModel[size];
      }
   };

   public int getId () {
      return id;
   }

   public void setId (int id) {
      this.id = id;
   }

   public int getCategoryId () {
      return categoryId;
   }

   public void setCategoryId (int categoryId) {
      this.categoryId = categoryId;
   }

   public String getFieldName () {
      return fieldName;
   }

   public void setFieldName (String fieldName) {
      this.fieldName = fieldName;
   }

   public String getIntroLabel () {
      return introLabel;
   }

   public void setIntroLabel (String introLabel) {
      this.introLabel = introLabel;
   }

   public String getFieldType () {
      return fieldType;
   }

   public void setFieldType (String fieldType) {
      this.fieldType = fieldType;
   }

   public String getFieldExtra () {
      return fieldExtra;
   }

   public void setFieldExtra (String fieldExtra) {
      this.fieldExtra = fieldExtra;
   }

   public String getFilterName () {
      return filterName;
   }

   public void setFilterName (String filterName) {
      this.filterName = filterName;
   }

   public String getFormTooltip () {
      return formTooltip;
   }

   public void setFormTooltip (String formTooltip) {
      this.formTooltip = formTooltip;
   }

   public boolean isRequired () {
      return isRequired;
   }

   public void setRequired (boolean required) {
      isRequired = required;
   }

   public int getGroupId () {
      return groupId;
   }

   public void setGroupId (int groupId) {
      this.groupId = groupId;
   }

   public int getStatus () {
      return status;
   }

   public void setStatus (int status) {
      this.status = status;
   }

   public String getSearchType () {
      return searchType;
   }

   public void setSearchType (String searchType) {
      this.searchType = searchType;
   }

   public ArrayList<FormOptionsModel> getFormOptions () {
      return formOptions;
   }

   public void setFormOptions (ArrayList<FormOptionsModel> formOptions) {
      this.formOptions = formOptions;
   }

   public FormOptionsModel getSelectedFormOption () {
      return selectedFormOption;
   }

   public void setSelectedFormOption (FormOptionsModel selectedFormOption) {
      this.selectedFormOption = selectedFormOption;
   }

   @Override public int describeContents () {
      return 0;
   }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeInt (id);
      dest.writeInt (categoryId);
      dest.writeString (fieldName);
      dest.writeString (introLabel);
      dest.writeString (fieldType);
      dest.writeString (fieldExtra);
      dest.writeString (filterName);
      dest.writeString (formTooltip);
      dest.writeByte ((byte) (isRequired ? 1 : 0));
      dest.writeInt (groupId);
      dest.writeInt (status);
      dest.writeString (searchType);
   }
}
