package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class BrideDressModel implements Parcelable{

   @SerializedName ("id")
   @Expose
   private Integer id;

   @SerializedName("name")
   @Expose
   private String name;

   @SerializedName("img")
   @Expose
   private Integer img;

   @SerializedName("count")
   @Expose
   private Integer count;


   public BrideDressModel () {
   }

   public BrideDressModel (Integer id, String name, Integer img, Integer count) {
      this.id = id;
      this.name = name;
      this.img = img;
      this.count = count;
   }

   protected BrideDressModel (Parcel in) {
      name = in.readString ();
   }

   public static final Creator<BrideDressModel> CREATOR = new Creator<BrideDressModel> () {
      @Override
      public BrideDressModel createFromParcel (Parcel in) {
         return new BrideDressModel (in);
      }

      @Override
      public BrideDressModel[] newArray (int size) {
         return new BrideDressModel[size];
      }
   };

   public Integer getId () {
      return id;
   }

   public void setId (Integer id) {
      this.id = id;
   }

   public String getName () {
      return name;
   }

   public void setName (String name) {
      this.name = name;
   }

   public Integer getImg () {
      return img;
   }

   public void setImg (Integer img) {
      this.img = img;
   }

   public Integer getCount () {
      return count;
   }

   public void setCount (Integer count) {
      this.count = count;
   }

   @Override public int describeContents () {
      return 0;
   }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeString (name);
   }
}
