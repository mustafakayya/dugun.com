package com.dugun.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ModelGalleryModel {

   @SerializedName ("id")
   private int id;

   @SerializedName ("galleryId")
   private int galleryId;

   @SerializedName ("image")
   private String image;

   @SerializedName ("title")
   private String title;

   @SerializedName ("description")
   private String description;

   @SerializedName ("orderNum")
   private int orderNum;

   @SerializedName ("imageCode")
   private String imageCode;

   @SerializedName ("mediaInfoTags")
   private ArrayList<MediaInfoTagModel> mediaInfoTags;

   @SerializedName ("width")
   private int width;

   @SerializedName ("height")
   private int height;

   @SerializedName ("expert")
   private ExpertModel expert;

   @SerializedName ("expertComment")
   private String expertComment;

   @SerializedName ("priceMax")
   private int priceMax;

   @SerializedName ("priceMin")
   private int priceMin;

   @SerializedName ("metaTitle")
   private String metaTitle;

   @SerializedName ("metaDescription")
   private String metaDescription;

   @SerializedName ("status")
   private String status;

   @SerializedName ("type")
   private String type;

   @SerializedName ("url")
   private String url;

   @SerializedName ("imageUrl")
   private UrlModel imageUrl;

   @SerializedName ("isMainImage")
   private Boolean isMainImage;

   @SerializedName ("videoHost")
   private String videoHost;

   @SerializedName ("gallery")
   private Gallery gallery;

   public class Gallery {

      @SerializedName ("provider")
      private ProviderModel provider;

      @SerializedName ("providerId")
      private int providerId;

      public ProviderModel getProvider () {
         return provider;
      }

      public void setProvider (ProviderModel provider) {
         this.provider = provider;
      }

      public int getProviderId () {
         return providerId;
      }

      public void setProviderId (int providerId) {
         this.providerId = providerId;
      }
   }

   public int getId () {
      return id;
   }

   public void setId (int id) {
      this.id = id;
   }

   public int getGalleryId () {
      return galleryId;
   }

   public void setGalleryId (int galleryId) {
      this.galleryId = galleryId;
   }

   public String getImage () {
      return image;
   }

   public void setImage (String image) {
      this.image = image;
   }

   public String getTitle () {
      return title;
   }

   public void setTitle (String title) {
      this.title = title;
   }

   public String getDescription () {
      return description;
   }

   public void setDescription (String description) {
      this.description = description;
   }

   public int getOrderNum () {
      return orderNum;
   }

   public void setOrderNum (int orderNum) {
      this.orderNum = orderNum;
   }

   public String getImageCode () {
      return imageCode;
   }

   public void setImageCode (String imageCode) {
      this.imageCode = imageCode;
   }

   public ArrayList<MediaInfoTagModel> getMediaInfoTags () {
      return mediaInfoTags;
   }

   public void setMediaInfoTags (ArrayList<MediaInfoTagModel> mediaInfoTags) {
      this.mediaInfoTags = mediaInfoTags;
   }

   public int getWidth () {
      return width;
   }

   public void setWidth (int width) {
      this.width = width;
   }

   public int getHeight () {
      return height;
   }

   public void setHeight (int height) {
      this.height = height;
   }

   public ExpertModel getExpert () {
      return expert;
   }

   public void setExpert (ExpertModel expert) {
      this.expert = expert;
   }

   public String getExpertComment () {
      return expertComment;
   }

   public void setExpertComment (String expertComment) {
      this.expertComment = expertComment;
   }

   public int getPriceMax () {
      return priceMax;
   }

   public void setPriceMax (int priceMax) {
      this.priceMax = priceMax;
   }

   public int getPriceMin () {
      return priceMin;
   }

   public void setPriceMin (int priceMin) {
      this.priceMin = priceMin;
   }

   public String getMetaTitle () {
      return metaTitle;
   }

   public void setMetaTitle (String metaTitle) {
      this.metaTitle = metaTitle;
   }

   public String getMetaDescription () {
      return metaDescription;
   }

   public void setMetaDescription (String metaDescription) {
      this.metaDescription = metaDescription;
   }

   public String getStatus () {
      return status;
   }

   public void setStatus (String status) {
      this.status = status;
   }

   public String getType () {
      return type;
   }

   public void setType (String type) {
      this.type = type;
   }

   public String getUrl () {
      return url;
   }

   public void setUrl (String url) {
      this.url = url;
   }

   public UrlModel getImageUrl () {
      return imageUrl;
   }

   public void setImageUrl (UrlModel imageUrl) {
      this.imageUrl = imageUrl;
   }

   public Boolean getMainImage () {
      return isMainImage;
   }

   public void setMainImage (Boolean mainImage) {
      isMainImage = mainImage;
   }

   public String getVideoHost () {
      return videoHost;
   }

   public void setVideoHost (String videoHost) {
      this.videoHost = videoHost;
   }

   public Gallery getGallery () {
      return gallery;
   }

   public void setGallery (Gallery gallery) {
      this.gallery = gallery;
   }
}
