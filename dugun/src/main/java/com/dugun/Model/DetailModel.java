package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class DetailModel {

   @SerializedName ("resID")
   @Expose
   private int resID;

   @SerializedName("text")
   @Expose
   private String text;

   @SerializedName("detail")
   @Expose
   private String detail;

   public DetailModel (int resID, String text, String detail) {
      this.resID = resID;
      this.text = text;
      this.detail = detail;
   }

   public int getResID () {
      return resID;
   }

   public void setResID (int resID) {
      this.resID = resID;
   }

   public String getText () {
      return text;
   }

   public void setText (String text) {
      this.text = text;
   }

   public String getDetail () {
      return detail;
   }

   public void setDetail (String detail) {
      this.detail = detail;
   }
}
