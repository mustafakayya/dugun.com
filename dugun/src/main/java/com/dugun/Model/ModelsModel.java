package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ModelsModel {

   @SerializedName ("id")
   @Expose
   private Integer id;

   @SerializedName ("galleryCategoryId")
   @Expose
   private Integer galleryCategoryId;

   @SerializedName ("providerId")
   @Expose
   private Integer providerId;

   @SerializedName ("relatedProviderId")
   @Expose
   private Integer relatedProviderId;

   @SerializedName ("name")
   @Expose
   private String name;

   @SerializedName ("categoryId")
   @Expose
   private Integer categoryId;

   @SerializedName ("directory")
   @Expose
   private String directory;

   @SerializedName ("intro")
   @Expose
   private String intro;

   @SerializedName ("description")
   @Expose
   private String description;

   @SerializedName ("isPromoted")
   @Expose
   private Boolean isPromoted;

   @SerializedName ("isMicroSite")
   @Expose
   private Boolean isMicroSite;

   @SerializedName ("orderNum")
   @Expose
   private Integer orderNum;

   @SerializedName ("status")
   @Expose
   private Boolean status;

   @SerializedName ("showInList")
   @Expose
   private String showInList;

   @SerializedName ("showProductCode")
   @Expose
   private Boolean showProductCode;

   @SerializedName ("galleryGroupId")
   @Expose
   private Integer galleryGroupId;

   @SerializedName ("isGalleryGroupHead")
   @Expose
   private Boolean isGalleryGroupHead;

   @SerializedName ("showWatermark")
   @Expose
   private Boolean showWatermark;

   @SerializedName ("year")
   @Expose
   private Integer year;

   @SerializedName ("createdAt")
   @Expose
   private String createdAt;

   @SerializedName ("redirectTo")
   @Expose
   private String redirectTo;

   @SerializedName ("metaTitle")
   @Expose
   private String metaTitle;

   @SerializedName ("metaDescription")
   @Expose
   private String metaDescription;

   @SerializedName ("mainImage")
   @Expose
   private ImageModel mainImage;

   @SerializedName ("isMainGallery")
   @Expose
   private Boolean isMainGallery;

   @SerializedName ("mediaCount")
   @Expose
   private Integer mediaCount;

   @SerializedName ("subGalleryCount")
   @Expose
   private Integer subGalleryCount;

   @SerializedName ("slug")
   @Expose
   private String slug;

   public Integer getId () {
      return id;
   }

   public void setId (Integer id) {
      this.id = id;
   }

   public Integer getGalleryCategoryId () {
      return galleryCategoryId;
   }

   public void setGalleryCategoryId (Integer galleryCategoryId) {
      this.galleryCategoryId = galleryCategoryId;
   }

   public Integer getProviderId () {
      return providerId;
   }

   public void setProviderId (Integer providerId) {
      this.providerId = providerId;
   }

   public Integer getRelatedProviderId () {
      return relatedProviderId;
   }

   public void setRelatedProviderId (Integer relatedProviderId) {
      this.relatedProviderId = relatedProviderId;
   }

   public String getName () {
      return name;
   }

   public void setName (String name) {
      this.name = name;
   }

   public Integer getCategoryId () {
      return categoryId;
   }

   public void setCategoryId (Integer categoryId) {
      this.categoryId = categoryId;
   }

   public String getDirectory () {
      return directory;
   }

   public void setDirectory (String directory) {
      this.directory = directory;
   }

   public String getIntro () {
      return intro;
   }

   public void setIntro (String intro) {
      this.intro = intro;
   }

   public String getDescription () {
      return description;
   }

   public void setDescription (String description) {
      this.description = description;
   }

   public Boolean getPromoted () {
      return isPromoted;
   }

   public void setPromoted (Boolean promoted) {
      isPromoted = promoted;
   }

   public Boolean getMicroSite () {
      return isMicroSite;
   }

   public void setMicroSite (Boolean microSite) {
      isMicroSite = microSite;
   }

   public Integer getOrderNum () {
      return orderNum;
   }

   public void setOrderNum (Integer orderNum) {
      this.orderNum = orderNum;
   }

   public Boolean getStatus () {
      return status;
   }

   public void setStatus (Boolean status) {
      this.status = status;
   }

   public String getShowInList () {
      return showInList;
   }

   public void setShowInList (String showInList) {
      this.showInList = showInList;
   }

   public Boolean getShowProductCode () {
      return showProductCode;
   }

   public void setShowProductCode (Boolean showProductCode) {
      this.showProductCode = showProductCode;
   }

   public Integer getGalleryGroupId () {
      return galleryGroupId;
   }

   public void setGalleryGroupId (Integer galleryGroupId) {
      this.galleryGroupId = galleryGroupId;
   }

   public Boolean getGalleryGroupHead () {
      return isGalleryGroupHead;
   }

   public void setGalleryGroupHead (Boolean galleryGroupHead) {
      isGalleryGroupHead = galleryGroupHead;
   }

   public Boolean getShowWatermark () {
      return showWatermark;
   }

   public void setShowWatermark (Boolean showWatermark) {
      this.showWatermark = showWatermark;
   }

   public Integer getYear () {
      return year;
   }

   public void setYear (Integer year) {
      this.year = year;
   }

   public String getCreatedAt () {
      return createdAt;
   }

   public void setCreatedAt (String createdAt) {
      this.createdAt = createdAt;
   }

   public String getRedirectTo () {
      return redirectTo;
   }

   public void setRedirectTo (String redirectTo) {
      this.redirectTo = redirectTo;
   }

   public String getMetaTitle () {
      return metaTitle;
   }

   public void setMetaTitle (String metaTitle) {
      this.metaTitle = metaTitle;
   }

   public String getMetaDescription () {
      return metaDescription;
   }

   public void setMetaDescription (String metaDescription) {
      this.metaDescription = metaDescription;
   }

   public ImageModel getMainImage () {
      return mainImage;
   }

   public void setMainImage (ImageModel mainImage) {
      this.mainImage = mainImage;
   }

   public Boolean getMainGallery () {
      return isMainGallery;
   }

   public void setMainGallery (Boolean mainGallery) {
      isMainGallery = mainGallery;
   }

   public Integer getMediaCount () {
      return mediaCount;
   }

   public void setMediaCount (Integer mediaCount) {
      this.mediaCount = mediaCount;
   }

   public Integer getSubGalleryCount () {
      return subGalleryCount;
   }

   public void setSubGalleryCount (Integer subGalleryCount) {
      this.subGalleryCount = subGalleryCount;
   }

   public String getSlug () {
      return slug;
   }

   public void setSlug (String slug) {
      this.slug = slug;
   }
}
