package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ProviderInfoRequestFormsModel {

    @SerializedName("id")
    private int id;

    @SerializedName("categoryId")
    private int categoryId;

    @SerializedName("fieldLabel")
    private String fieldLabel;

    @SerializedName("providerLabel")
    private String providerLabel;

    @SerializedName("fieldName")
    private String fieldName;

    @SerializedName("fieldType")
    private String fieldType;

    @SerializedName("orderNum")
    private int orderNum;

    @SerializedName("fieldValue")
    private String fieldValue;

    @SerializedName("isRequired")
    private boolean isRequired;

    @SerializedName("isActive")
    private boolean isActive;

    @SerializedName("related")
    private int related;

    @SerializedName("showOnGallery")
    private boolean showOnGallery;

    @SerializedName("providerId")
    private int providerId;

    @SerializedName("notInProviderId")
    private String notInProviderId;

    @SerializedName("fieldDataType")
    private String fieldDataType;

    @SerializedName("labelProvider")
    private String labelProvider;

    @SerializedName("selectedFormOption")
    private FormOptionsModel selectedFormOption;

    @SerializedName("infoRequestFormOptions")
    private ArrayList<FormOptionsModel> infoRequestFormOptions;

    @Expose
    String selectedDate;

    @Expose
    String textValue;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getFieldLabel() {
        return fieldLabel;
    }

    public void setFieldLabel(String fieldLabel) {
        this.fieldLabel = fieldLabel;
    }

    public String getProviderLabel() {
        return providerLabel;
    }

    public void setProviderLabel(String providerLabel) {
        this.providerLabel = providerLabel;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public void setRequired(boolean required) {
        isRequired = required;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getRelated() {
        return related;
    }

    public void setRelated(int related) {
        this.related = related;
    }

    public boolean isShowOnGallery() {
        return showOnGallery;
    }

    public void setShowOnGallery(boolean showOnGallery) {
        this.showOnGallery = showOnGallery;
    }

    public int getProviderId() {
        return providerId;
    }

    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }

    public String getNotInProviderId() {
        return notInProviderId;
    }

    public void setNotInProviderId(String notInProviderId) {
        this.notInProviderId = notInProviderId;
    }

    public String getFieldDataType() {
        return fieldDataType;
    }

    public void setFieldDataType(String fieldDataType) {
        this.fieldDataType = fieldDataType;
    }

    public String getLabelProvider() {
        return labelProvider;
    }

    public void setLabelProvider(String labelProvider) {
        this.labelProvider = labelProvider;
    }

    public FormOptionsModel getSelectedFormOption() {
        return selectedFormOption;
    }

    public void setSelectedFormOption(FormOptionsModel selectedFormOption) {
        this.selectedFormOption = selectedFormOption;
    }

    public ArrayList<FormOptionsModel> getInfoRequestFormOptions() {
        return infoRequestFormOptions;
    }

    public void setInfoRequestFormOptions(ArrayList<FormOptionsModel> infoRequestFormOptions) {
        this.infoRequestFormOptions = infoRequestFormOptions;
    }


    public String getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(String selectedDate) {
        this.selectedDate = selectedDate;
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }
}
