package com.dugun.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class LikeStatsModel {

   @SerializedName ("id")
   private int id;

   @SerializedName ("name")
   private String name;

   @SerializedName ("likeCount")
   private String likeCount;

   public int getId () {
      return id;
   }

   public void setId (int id) {
      this.id = id;
   }

   public String getName () {
      return name;
   }

   public void setName (String name) {
      this.name = name;
   }

   public String getLikeCount () {
      return likeCount;
   }

   public void setLikeCount (String likeCount) {
      this.likeCount = likeCount;
   }
}
