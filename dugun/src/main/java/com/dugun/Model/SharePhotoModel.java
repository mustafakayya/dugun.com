package com.dugun.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mustafakaya on 1/27/18.
 */

public class SharePhotoModel {

    @SerializedName("success")
    boolean success;
    @SerializedName("url")
    String url;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
