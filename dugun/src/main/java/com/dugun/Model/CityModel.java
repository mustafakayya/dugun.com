package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class CityModel implements Parcelable{

   @SerializedName ("id")
   private int id;

   @SerializedName("name")
   private String name;

   @SerializedName("slug")
   private String slug;

   @SerializedName("status")
   private boolean status;


   protected CityModel (Parcel in) {
      id = in.readInt ();
      name = in.readString ();
      slug = in.readString ();
      status = in.readByte () != 0;
   }

   public static final Creator<CityModel> CREATOR = new Creator<CityModel> () {
      @Override
      public CityModel createFromParcel (Parcel in) {
         return new CityModel (in);
      }

      @Override
      public CityModel[] newArray (int size) {
         return new CityModel[size];
      }
   };

   public int getId () {
      return id;
   }

   public void setId (int id) {
      this.id = id;
   }

   public String getName () {
      return name;
   }

   public void setName (String name) {
      this.name = name;
   }

   public String getSlug () {
      return slug;
   }

   public void setSlug (String slug) {
      this.slug = slug;
   }

   public boolean isStatus () {
      return status;
   }

   public void setStatus (boolean status) {
      this.status = status;
   }

   @Override public int describeContents () {
      return 0;
   }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeInt (id);
      dest.writeString (name);
      dest.writeString (slug);
      dest.writeByte ((byte) (status ? 1 : 0));
   }
}
