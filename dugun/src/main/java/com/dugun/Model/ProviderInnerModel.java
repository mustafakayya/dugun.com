package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ProviderInnerModel implements Parcelable{

   @SerializedName ("id")
   public int id;

   @SerializedName ("slug")
   public String slug;

   @SerializedName ("name")
   public String name;

   @SerializedName ("inboundPhones")
   public InboundPhonesModel[] inboundPhones;

   @SerializedName ("providerImage")
   public String providerImage;

   @SerializedName ("providerPreviewImage")
   public String  providerPreviewImage;

   @SerializedName ("redirectedTo")
   public String redirectedTo;

   protected ProviderInnerModel (Parcel in) {
      id = in.readInt ();
      slug = in.readString ();
      name = in.readString ();
      providerImage = in.readString ();
      providerPreviewImage = in.readString ();
      redirectedTo = in.readString ();
   }

   public static final Creator<ProviderInnerModel> CREATOR = new Creator<ProviderInnerModel> () {
      @Override
      public ProviderInnerModel createFromParcel (Parcel in) {
         return new ProviderInnerModel (in);
      }

      @Override
      public ProviderInnerModel[] newArray (int size) {
         return new ProviderInnerModel[size];
      }
   };

   public int getId () {
      return id;
   }

   public void setId (int id) {
      this.id = id;
   }

   public String getSlug () {
      return slug;
   }

   public void setSlug (String slug) {
      this.slug = slug;
   }

   public String getName () {
      return name;
   }

   public void setName (String name) {
      this.name = name;
   }

   public InboundPhonesModel[] getInboundPhones () {
      return inboundPhones;
   }

   public void setInboundPhones (InboundPhonesModel[] inboundPhones) {
      this.inboundPhones = inboundPhones;
   }

   public String getProviderImage () {
      return providerImage;
   }

   public void setProviderImage (String providerImage) {
      this.providerImage = providerImage;
   }

   public String getProviderPreviewImage () {
      return providerPreviewImage;
   }

   public void setProviderPreviewImage (String providerPreviewImage) {
      this.providerPreviewImage = providerPreviewImage;
   }

   public String getRedirectedTo () {
      return redirectedTo;
   }

   public void setRedirectedTo (String redirectedTo) {
      this.redirectedTo = redirectedTo;
   }

   @Override public int describeContents () {
      return 0;
   }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeInt (id);
      dest.writeString (slug);
      dest.writeString (name);
      dest.writeString (providerImage);
      dest.writeString (providerPreviewImage);
      dest.writeString (redirectedTo);
   }
}
