package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class CompanyDetailModel {

   @SerializedName ("id")
   @Expose
   private Integer id;

   @SerializedName("name")
   @Expose
   private String name;

   @SerializedName("img")
   @Expose
   private Integer img;

   @SerializedName("imgCount")
   @Expose
   private Integer imgCount;

   @SerializedName("videoCount")
   @Expose
   private Integer videoCount;

   @SerializedName("category")
   @Expose
   private String category;

   @SerializedName("price")
   @Expose
   private String price;

   @SerializedName("capacity")
   @Expose
   private String capacity;

   @SerializedName("city")
   @Expose
   private String city;

   @SerializedName("district")
   @Expose
   private String district;

   @SerializedName("area")
   @Expose
   private String area;

   @SerializedName("feature")
   @Expose
   private String feature;

   @SerializedName("view")
   @Expose
   private String view;







   public Integer getId () {
      return id;
   }

   public CompanyDetailModel (Integer id, String name, Integer img) {
      this.id = id;
      this.name = name;
      this.img = img;
   }

   public void setId (Integer id) {
      this.id = id;
   }

   public String getName () {
      return name;
   }

   public void setName (String name) {
      this.name = name;
   }

   public Integer getImg () {
      return img;
   }

   public void setImg (Integer img) {
      this.img = img;
   }
}
