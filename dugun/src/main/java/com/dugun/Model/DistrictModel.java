package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class DistrictModel implements Parcelable{

   @SerializedName ("id")
   private int id;

   @SerializedName("name")
   private String name;


   protected DistrictModel (Parcel in) {
      id = in.readInt ();
      name = in.readString ();
   }

   public static final Creator<DistrictModel> CREATOR = new Creator<DistrictModel> () {
      @Override
      public DistrictModel createFromParcel (Parcel in) {
         return new DistrictModel (in);
      }

      @Override
      public DistrictModel[] newArray (int size) {
         return new DistrictModel[size];
      }
   };

   public int getId () {
      return id;
   }

   public void setId (int id) {
      this.id = id;
   }

   public String getName () {
      return name;
   }

   public void setName (String name) {
      this.name = name;
   }

   @Override public int describeContents () {
      return 0;
   }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeInt (id);
      dest.writeString (name);
   }
}
