package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ugurbasarir on 27/10/17.
 */

public class FormDataInnerData implements Parcelable{

   @SerializedName ("label")
   private String label;

   @SerializedName ("value")
   public ArrayList<ValueData> value;

   protected FormDataInnerData (Parcel in) {
      label = in.readString ();
      value = in.createTypedArrayList (ValueData.CREATOR);
   }

   public static final Creator<FormDataInnerData> CREATOR = new Creator<FormDataInnerData> () {
      @Override
      public FormDataInnerData createFromParcel (Parcel in) {
         return new FormDataInnerData (in);
      }

      @Override
      public FormDataInnerData[] newArray (int size) {
         return new FormDataInnerData[size];
      }
   };

   public String getLabel () {
      return label;
   }

   public void setLabel (String label) {
      this.label = label;
   }

   public ArrayList<ValueData> getValue () {
      return value;
   }

   public void setValue (ArrayList<ValueData> value) {
      this.value = value;
   }

   @Override public int describeContents () {
      return 0;
   }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeString (label);
      dest.writeTypedList (value);
   }
}
