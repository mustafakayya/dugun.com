package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class ProviderModel implements Parcelable {

    @SerializedName("id")
    private int id;

    @SerializedName("slug")
    private String slug;

    @SerializedName("phone")
    private String phone;

    @SerializedName("category")
    private CategoryModel category;

    @SerializedName("name")
    private String name;

    @SerializedName("segment")
    private String segment;

    @SerializedName("listingName")
    private String listingName;

    @SerializedName("statusId")
    private int statusId;

    @SerializedName("city")
    private CityModel city;

    @SerializedName("district")
    private DistrictModel district;

    @SerializedName("isPromotion")
    private boolean isPromotion;

    @SerializedName("showForm")
    private boolean showForm;

    @SerializedName("createdAt")
    private String createdAt;

    @SerializedName("facebook")
    private String facebook;

    @SerializedName("twitter")
    private String twitter;

    @SerializedName("instagram")
    private String instagram;

    @SerializedName("customer")
    private CustomerModel customer;

    @SerializedName("providerFilter")
    private ProviderFilterModel providerFilter;

    @SerializedName("providerImage")
    private String providerImage;

    @SerializedName("providerPreviewImage")
    private String providerPreviewImage;

    @SerializedName("redirectTo")
    private String redirectTo;

    @SerializedName("isRedirected")
    private boolean isRedirected;

    @SerializedName("mainCategoryId")
    private int mainCategoryId;

    @SerializedName("categoryType")
    private String categoryType;

    @SerializedName("status")
    private boolean status;

    @SerializedName("orderNum")
    private int orderNum;

    @SerializedName("homeListOrder")
    private int homeListOrder;

    @SerializedName("discount")
    private Discount discount;

    @SerializedName("isLiked")
    private boolean isLiked;

    @SerializedName("hasActiveDiscount")
    private boolean hasActiveDiscount;

    public ProviderModel() {
    }

    protected ProviderModel(Parcel in) {
        id = in.readInt();
        slug = in.readString();
        phone = in.readString();
        category = in.readParcelable(CategoryModel.class.getClassLoader());
        name = in.readString();
        segment = in.readString();
        listingName = in.readString();
        statusId = in.readInt();
        city = in.readParcelable(CityModel.class.getClassLoader());
        district = in.readParcelable(DistrictModel.class.getClassLoader());
        isPromotion = in.readByte() != 0;
        showForm = in.readByte() != 0;
        createdAt = in.readString();
        facebook = in.readString();
        twitter = in.readString();
        instagram = in.readString();
        customer = in.readParcelable(CustomerModel.class.getClassLoader());
        providerFilter = in.readParcelable(ProviderFilterModel.class.getClassLoader());
        providerImage = in.readString();
        providerPreviewImage = in.readString();
        redirectTo = in.readString();
        isRedirected = in.readByte() != 0;
        mainCategoryId = in.readInt();
        categoryType = in.readString();
        status = in.readByte() != 0;
        orderNum = in.readInt();
        homeListOrder = in.readInt();
        isLiked = in.readByte() != 0;
        hasActiveDiscount = in.readByte() != 0;
    }

    public static final Creator<ProviderModel> CREATOR = new Creator<ProviderModel>() {
        @Override
        public ProviderModel createFromParcel(Parcel in) {
            return new ProviderModel(in);
        }

        @Override
        public ProviderModel[] newArray(int size) {
            return new ProviderModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(slug);
        dest.writeString(phone);
        dest.writeParcelable(category, flags);
        dest.writeString(name);
        dest.writeString(segment);
        dest.writeString(listingName);
        dest.writeInt(statusId);
        dest.writeParcelable(city, flags);
        dest.writeParcelable(district, flags);
        dest.writeByte((byte) (isPromotion ? 1 : 0));
        dest.writeByte((byte) (showForm ? 1 : 0));
        dest.writeString(createdAt);
        dest.writeString(facebook);
        dest.writeString(twitter);
        dest.writeString(instagram);
        dest.writeParcelable(customer, flags);
        dest.writeParcelable(providerFilter, flags);
        dest.writeString(providerImage);
        dest.writeString(providerPreviewImage);
        dest.writeString(redirectTo);
        dest.writeByte((byte) (isRedirected ? 1 : 0));
        dest.writeInt(mainCategoryId);
        dest.writeString(categoryType);
        dest.writeByte((byte) (status ? 1 : 0));
        dest.writeInt(orderNum);
        dest.writeInt(homeListOrder);
        dest.writeByte((byte) (isLiked ? 1 : 0));
        dest.writeByte((byte) (hasActiveDiscount ? 1 : 0));
    }


    public class Discount {

        @SerializedName("id")
        private int id;

        @SerializedName("campaignName")
        private String campaignName;

        @SerializedName("validUntil")
        private String validUntil;

        @SerializedName("validFrom")
        private String validFrom;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCampaignName() {
            return campaignName;
        }

        public void setCampaignName(String campaignName) {
            this.campaignName = campaignName;
        }

        public String getValidUntil() {
            return validUntil;
        }

        public void setValidUntil(String validUntil) {
            this.validUntil = validUntil;
        }

        public String getValidFrom() {
            return validFrom;
        }

        public void setValidFrom(String validFrom) {
            this.validFrom = validFrom;
        }
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public CategoryModel getCategory() {
        return category;
    }

    public void setCategory(CategoryModel category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getListingName() {
        return listingName;
    }

    public void setListingName(String listingName) {
        this.listingName = listingName;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public CityModel getCity() {
        return city;
    }

    public void setCity(CityModel city) {
        this.city = city;
    }

    public DistrictModel getDistrict() {
        return district;
    }

    public void setDistrict(DistrictModel district) {
        this.district = district;
    }

    public boolean isPromotion() {
        return isPromotion;
    }

    public void setPromotion(boolean promotion) {
        isPromotion = promotion;
    }

    public boolean isShowForm() {
        return showForm;
    }

    public void setShowForm(boolean showForm) {
        this.showForm = showForm;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public CustomerModel getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerModel customer) {
        this.customer = customer;
    }

    public ProviderFilterModel getProviderFilter() {
        return providerFilter;
    }

    public void setProviderFilter(ProviderFilterModel providerFilter) {
        this.providerFilter = providerFilter;
    }

    public String getProviderImage() {
        return providerImage;
    }

    public void setProviderImage(String providerImage) {
        this.providerImage = providerImage;
    }

    public String getProviderPreviewImage() {
        return providerPreviewImage;
    }

    public void setProviderPreviewImage(String providerPreviewImage) {
        this.providerPreviewImage = providerPreviewImage;
    }

    public String getRedirectTo() {
        return redirectTo;
    }

    public void setRedirectTo(String redirectTo) {
        this.redirectTo = redirectTo;
    }

    public boolean isRedirected() {
        return isRedirected;
    }

    public void setRedirected(boolean redirected) {
        isRedirected = redirected;
    }

    public int getMainCategoryId() {
        return mainCategoryId;
    }

    public void setMainCategoryId(int mainCategoryId) {
        this.mainCategoryId = mainCategoryId;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public int getHomeListOrder() {
        return homeListOrder;
    }

    public void setHomeListOrder(int homeListOrder) {
        this.homeListOrder = homeListOrder;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    public boolean isHasActiveDiscount() {
        return hasActiveDiscount;
    }

    public void setHasActiveDiscount(boolean hasActiveDiscount) {
        this.hasActiveDiscount = hasActiveDiscount;
    }
}
