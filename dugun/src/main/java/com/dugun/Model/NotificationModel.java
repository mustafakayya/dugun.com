package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class NotificationModel {

   @SerializedName ("id")
   @Expose
   private Integer id;

   @SerializedName ("name")
   @Expose
   private String name;

   @SerializedName ("status")
   @Expose
   private Boolean status;

   public NotificationModel (Integer id, String name, Boolean status) {
      this.id = id;
      this.name = name;
      this.status = status;
   }

   public Integer getId () {
      return id;
   }

   public void setId (Integer id) {
      this.id = id;
   }

   public String getName () {
      return name;
   }

   public void setName (String name) {
      this.name = name;
   }

   public Boolean getStatus () {
      return status;
   }

   public void setStatus (Boolean status) {
      this.status = status;
   }
}
