package com.dugun.Model;



public class OrganizationModel {

    public String text;
    public int type;
    boolean isChecked;

    public OrganizationModel(String text, int type, boolean isChecked) {
        this.text = text;
        this.type = type;
        this.isChecked = isChecked;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }


}
