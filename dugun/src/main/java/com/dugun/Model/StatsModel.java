package com.dugun.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class StatsModel {

   @SerializedName ("data")
   public StatsModel.InnerData data;

   public class InnerData {

      @SerializedName ("budgetCounts")
      public int                   budgetCounts;

      @SerializedName ("badgeCounts")
      public InnerBadgeCounts      badgeCounts;

      @SerializedName ("sharedCounts")
      public int                   sharedCounts;

      @SerializedName ("infoRequests")
      public InnerInfoRequests     infoRequests;

      @SerializedName ("likeCounts")
      public int                   likeCounts;

      @SerializedName ("todoCounts")
      public InnerTodoCounts       todoCounts;

      @SerializedName ("weddingCountDown")
      public InnerWeddingCountdown weddingCountDown;


      public class InnerBadgeCounts {

         @SerializedName ("completed")
         public int completed;
         @SerializedName ("all")
         public int all;
      }

      public class InnerInfoRequests {

         @SerializedName ("counts")
         public int counts;
         @SerializedName ("cost")
         public double cost;
      }

      public class InnerTodoCounts {

         @SerializedName ("completed")
         public int completed;
         @SerializedName ("all")
         public int all;
      }

      public class InnerWeddingCountdown {

         @SerializedName ("year")
         public int year;
         @SerializedName ("month")
         public int month;
         @SerializedName ("week")
         public int week;
         @SerializedName ("day")
         public int day;
      }


   }

}
