package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 21/09/17.
 */

public class CustomerModel implements Parcelable{

   @SerializedName ("id")
   private Integer id;

   @SerializedName ("name")
   private String name;

   protected CustomerModel (Parcel in) {
      name = in.readString ();
   }

   public static final Creator<CustomerModel> CREATOR = new Creator<CustomerModel> () {
      @Override
      public CustomerModel createFromParcel (Parcel in) {
         return new CustomerModel (in);
      }

      @Override
      public CustomerModel[] newArray (int size) {
         return new CustomerModel[size];
      }
   };

   public Integer getId () {
      return id;
   }

   public void setId (Integer id) {
      this.id = id;
   }

   public String getName () {
      return name;
   }

   public void setName (String name) {
      this.name = name;
   }

   @Override public int describeContents () {
      return 0;
   }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeString (name);
   }
}
