package com.dugun.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class MetaModel implements Parcelable {

   @SerializedName ("total")
   @Expose
   private Integer total;

   @SerializedName ("perPage")
   @Expose
   private Integer perPage;

   @SerializedName ("currentPage")
   @Expose
   private Integer currentPage;

   @SerializedName ("lastPage")
   @Expose
   private Integer lastPage;

   public Integer getTotal () {
      return total;
   }

   public void setTotal (Integer total) {
      this.total = total;
   }

   public Integer getPerPage () {
      return perPage;
   }

   public void setPerPage (Integer perPage) {
      this.perPage = perPage;
   }

   public Integer getCurrentPage () {
      return currentPage;
   }

   public void setCurrentPage (Integer currentPage) {
      this.currentPage = currentPage;
   }

   public Integer getLastPage () {
      return lastPage;
   }

   public void setLastPage (Integer lastPage) {
      this.lastPage = lastPage;
   }


   @Override public int describeContents () { return 0; }

   @Override public void writeToParcel (Parcel dest, int flags) {
      dest.writeValue (this.total);
      dest.writeValue (this.perPage);
      dest.writeValue (this.currentPage);
      dest.writeValue (this.lastPage);
   }

   public MetaModel () {}

   protected MetaModel (Parcel in) {
      this.total = (Integer) in.readValue (Integer.class.getClassLoader ());
      this.perPage = (Integer) in.readValue (Integer.class.getClassLoader ());
      this.currentPage = (Integer) in.readValue (Integer.class.getClassLoader ());
      this.lastPage = (Integer) in.readValue (Integer.class.getClassLoader ());
   }

   public static final Creator<MetaModel> CREATOR = new Creator<MetaModel> () {
      @Override public MetaModel createFromParcel (Parcel source) {return new MetaModel (source);}

      @Override public MetaModel[] newArray (int size) {return new MetaModel[size];}
   };
}
