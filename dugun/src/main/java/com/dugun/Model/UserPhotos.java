package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by mustafakaya on 11/30/17.
 */

public class UserPhotos implements Serializable{
    @SerializedName("profilePictureUrl")
    @Expose
    String profilePictureUrl;
    // @SerializedName("coverPhotoUrl")
    // @Expose
    //String coverPhotoUrl;
    //@SerializedName("profilePhotoThumbUrl")
    //@Expose
    //String profilePhotoThumbUrl;
    @SerializedName("1x1")
    @Expose
    String userPhoto;
    @SerializedName("1x1_thumb")
    @Expose
    String thumbPhoto;
    @SerializedName("56x15")
    @Expose
    String bigWidePhoto;
    @SerializedName("16x9")
    @Expose
    String widePhoto;


    public UserPhotos() {
    }

    public String getProfilePictureUrl() {
        return profilePictureUrl;
    }

    public void setProfilePictureUrl(String profilePictureUrl) {
        this.profilePictureUrl = profilePictureUrl;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getThumbPhoto() {
        return thumbPhoto;
    }

    public void setThumbPhoto(String thumbPhoto) {
        this.thumbPhoto = thumbPhoto;
    }

    public String getBigWidePhoto() {
        return bigWidePhoto;
    }

    public void setBigWidePhoto(String bigWidePhoto) {
        this.bigWidePhoto = bigWidePhoto;
    }

    public String getWidePhoto() {
        return widePhoto;
    }

    public void setWidePhoto(String widePhoto) {
        this.widePhoto = widePhoto;
    }
}
