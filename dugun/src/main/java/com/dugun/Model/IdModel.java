package com.dugun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ugurbasarir on 11/08/17.
 */

public class IdModel {

   @SerializedName ("id")
   @Expose
   private Integer id;

   public Integer getId () {
      return id;
   }

   public void setId (Integer id) {
      this.id = id;
   }
}
