package com.dugun.Model;

import com.google.gson.annotations.SerializedName;

public class AccessTokenModel {

   @SerializedName ("username")
   private String username;

   @SerializedName ("userType")
   private String userType;

   @SerializedName ("token")
   private String token;

   @SerializedName ("createdAt")
   private CreatedAt createdAt;

   public class CreatedAt {

      @SerializedName ("date")
      private String date;

      @SerializedName ("timezone_type")
      private int timezone_type;

      @SerializedName ("timezone")
      private String timezone;

      public String getDate () {
         return date;
      }

      public void setDate (String date) {
         this.date = date;
      }

      public int getTimezone_type () {
         return timezone_type;
      }

      public void setTimezone_type (int timezone_type) {
         this.timezone_type = timezone_type;
      }

      public String getTimezone () {
         return timezone;
      }

      public void setTimezone (String timezone) {
         this.timezone = timezone;
      }
   }


   public String getUsername () {
      return username;
   }

   public void setUsername (String username) {
      this.username = username;
   }

   public String getUserType () {
      return userType;
   }

   public void setUserType (String userType) {
      this.userType = userType;
   }

   public String getToken () {
      return token;
   }

   public void setToken (String token) {
      this.token = token;
   }
}